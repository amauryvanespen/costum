
costum.init = function(where){
    if(typeof costum.filters != "undefined"){
        if(typeof costum.filters.scopes != "undefined"){
            if(typeof costum.filters.scopes.cities != "undefined"){
                setOpenBreadCrum({'cities': costum.filters.scopes.cities }, true, null, "costum");
            }
            if(typeof costum.filters.scopes.zones != "undefined"){
                setOpenBreadCrum({'zones': costum.filters.scopes.zones}, true, null, "costum");
            }
        
        }
        if(typeof costum.filters.sourceKey != "undefined"){
            searchObject.sourceKey=costum.request.sourceKey;
        }
    }
    // if(typeof costum.js != "undefined" && typeof costum.js.urls != "undefined"){
    //     $.each(costum.js.urls, function(e,v){
    //         lazyLoad(assetPath+"/js/"+costum.slug+"/"+v, null, function(){
                if( typeof costum[costum.slug] != "undefined" && 
                    typeof costum[costum.slug].init != "undefined" && 
                    typeof costum[costum.slug].init == "function")
                    costum[costum.slug].init(); 
    //         });
    //     });
    // }
    
    if(typeof costum.css != "undefined"){
        costum.initCssCustom();
    }
    if(typeof costum.typeObj != "undefined")
        costum.initTypeObj(costum.typeObj);
    if(typeof costum != "undefined" && typeof costum.searchObject != "undefined" && typeof costum.searchObject.indexStep != "undefined") 
        searchObject.indexStep=costum.searchObject.indexStep;
    if(typeof costum != "undefined" && typeof costum.searchObject != "undefined" && typeof costum.searchObject.sortBy != "undefined")
        searchObject.sortBy=costum.searchObject.sortBy;

    // mylog.log("directoryViewMode 1", directoryViewMode);
    // if(typeof costum.htmlConstruct != "undefined" 
    //     && typeof costum.htmlConstruct.directory != "undefined"
    //     && typeof costum.htmlConstruct.directory.viewMode != "undefined"){
    //     mylog.log("directoryViewMode 2", directoryViewMode);
    //     directoryViewMode=costum.htmlConstruct.directory.viewMode;
    // }
    if(typeof costum.htmlConstruct != "undefined"
        && typeof costum.htmlConstruct.directoryViewMode != "undefined"){
        mylog.log("directoryViewMode 2", directoryViewMode);
        directoryViewMode=costum.htmlConstruct.directoryViewMode;
        directory.viewMode = directoryViewMode;
    }
    mylog.log("directoryViewMode 3", directoryViewMode); 
};
costum.initTypeObj=function(typeObjCostum){
    mylog.log("costum.initTypeObj", typeObjCostum);
    $.each(typeObj, function(e, obj){
        var entryObj=e;
        if(typeof obj.sameAs != "undefined")
                entryObj=obj.sameAs;
        if(typeof typeObjCostum[e] != "undefined"){
            if(typeof typeObjCostum[e] == "object"){
                $.each(typeObjCostum[e], function(entry, value){
                    typeObj[entryObj][entry]=value;
                });
            }
        }else if(typeof obj.add != "undefined" && obj.add){
            typeObj[entryObj].add=false;
        }
    });
    $.each(typeObjCostum, function(e, v){
        if(typeof typeObj[e]=="undefined")
            typeObj[e]=v;
    });
    mylog.log("initTypeObj typeObj", typeObj);
    typeObj.buildCreateButton(".toolbar-bottom-adds", true, null, costum);
    coInterface.bindButtonOpenForm();
};
costum.getStyleCustom=function(cssObject){
    cssAdd="";
    cssObject = 'costum.css'+cssObject;
    if(jsonHelper.notNull(cssObject+".background"))
        cssAdd+="background-color:"+costum.value(costum,cssObject+".background")+" !important;";
    if(jsonHelper.notNull(cssObject+".border "))
        cssAdd+="border:"+costum.value(costum,cssObject+".border")+" !important;";
    if(jsonHelper.notNull(cssObject+".borderBottom"))
        cssAdd+="border-bottom:"+costum.value(costum,cssObject+".borderBottom")+" !important;";
    if(jsonHelper.notNull(cssObject+".box"))
        cssAdd+="box-shadow:"+costum.value(costum,cssObject+".box")+" !important;";
    if(jsonHelper.notNull(cssObject+".boxShadow"))
        cssAdd+="box-shadow:"+costum.value(costum,cssObject+".boxShadow")+" !important;";
    if(jsonHelper.notNull(cssObject+".fontSize"))
        cssAdd+="font-size:"+costum.value(costum,cssObject+".fontSize")+"px !important;";
    if(jsonHelper.notNull(cssObject+".fontWeight"))
        cssAdd+="font-weight:"+costum.value(costum,cssObject+".fontWeight")+" !important;";
    if(jsonHelper.notNull(cssObject+".color"))
        cssAdd+="color:"+costum.value(costum,cssObject+".color")+" !important;";
    if(jsonHelper.notNull(cssObject+".paddingTop"))
        cssAdd+="padding-top:"+costum.value(costum,cssObject+".paddingTop")+" !important;";
    if(jsonHelper.notNull(cssObject+".paddingLeft"))
        cssAdd+="padding-left:"+costum.value(costum,cssObject+".paddingLeft")+" !important;";
    if(jsonHelper.notNull(cssObject+".paddingBottom"))
        cssAdd+="padding-bottom:"+costum.value(costum,cssObject+".paddingBottom")+" !important;";
    if(jsonHelper.notNull(cssObject+".height"))
        cssAdd+="height:"+costum.value(costum,cssObject+".height")+"px !important;";
    if(jsonHelper.notNull(cssObject+".width"))
        cssAdd+="width:"+costum.value(costum,cssObject+".width")+"px !important;";
    if(jsonHelper.notNull(cssObject+".top"))
        cssAdd+="top:"+costum.value(costum,cssObject+".top")+"px !important;";
    if(jsonHelper.notNull(cssObject+".bottom"))
        cssAdd+="bottom:"+costum.value(costum,cssObject+".bottom")+"px !important;";
    if(jsonHelper.notNull(cssObject+".right"))
        cssAdd+="right:"+costum.value(costum,cssObject+".right")+"px !important;";
    if(jsonHelper.notNull(cssObject+".left"))
        cssAdd+="left:"+costum.value(costum,cssObject+".left")+"px !important;";
    if(jsonHelper.notNull(cssObject+".borderWidth"))
        cssAdd+="border-width:"+costum.value(costum,cssObject+".borderWidth")+"px !important;";
    if(jsonHelper.notNull(cssObject+".borderColor"))
        cssAdd+="border-color:"+costum.value(costum,cssObject+".borderColor")+" !important;";
    if(jsonHelper.notNull(cssObject+".borderRadius"))
        cssAdd+="border-radius:"+costum.value(costum,cssObject+".borderRadius")+" !important;";
    if(jsonHelper.notNull(cssObject+".lineHeight"))
        cssAdd+="line-height:"+costum.value(costum,cssObject+".lineHeight")+"px !important;";
    if(jsonHelper.notNull(cssObject+".padding"))
        cssAdd+="padding:"+costum.value(costum,cssObject+".padding")+" !important;";
    if(jsonHelper.notNull(cssObject+".margin"))
        cssAdd+="margin:"+costum.value(costum,cssObject+".margin")+" !important;";
    if(jsonHelper.notNull(cssObject+".display"))
        cssAdd+="display:"+costum.value(costum,cssObject+".display")+" !important;";
    if(jsonHelper.notNull(cssObject+".textTransform"))
        cssAdd+="text-transform:"+costum.value(costum,cssObject+".textTransform")+" !important;";
    if(jsonHelper.notNull(cssObject+".verticalAlign"))
        cssAdd+="vertical-align:"+costum.value(costum,cssObject+".verticalAlign")+" !important;";
    if(jsonHelper.notNull(cssObject+".float"))
        cssAdd+="float:"+costum.value(costum,cssObject+".float")+" !important;";

    return cssAdd;
};
costum.initCssCustom=function(){
    var style = "costum.css";
    // if(jsonHelper.notNull(style+".urls")  && costum.css.urls.length >= 1){
    //     $.each(costum.css.urls, function(e,v){
    //         lazyLoad(null, assetPath+"/css/"+costum.slug+"/"+v, null);
    //     });
    // }

    str="<style type='text/css'>";
    //alert(jsonHelper.notNull(style+".font"));
    if(jsonHelper.notNull(style+".font") ){
        //alert("costum.initCssCustom : "+style+".font");
        str+="@font-face {font-family: 'customFont';src: url('"+assetPath+"/"+costum.css.font.url+"')}";
        str+="body, h1, h2, h3, h4, h5, h6, button, input, select, textarea, a, p, span{font-family: 'customFont' !important;}";
    }

    if(jsonHelper.notNull(style+".fonts") ){

        $.each(costum.css.fonts, function(kF,vF){
            console.log("HERE ", kF,vF);
            str+="@font-face { font-family: '"+vF.name+"';src: url('"+assetPath+"/"+vF.url+"')}";
        });
    }

    if(jsonHelper.notNull(style+".styles") ){
        $.each(costum.css.styles, function(e,r){
           if(e.indexOf("#"))
            str += e+"{";
           $.each(r, function(k,c){
            str += k+":"+c;
           });
           str += "}"; 
        });

    }
    if(jsonHelper.notNull(style+".menuTop") ){
        str+="#mainNav{"+costum.getStyleCustom(".menuTop")+"}";
        if(jsonHelper.notNull(style+".menuTop.header") )
            str+="#headerBand{"+costum.getStyleCustom(".menuTop.header")+"}";
        if(jsonHelper.notNull(style+".menuTop.app") ){
            str+="#mainNav .menu-app-top{"+costum.getStyleCustom(".menuTop.app")+"}";
            if(jsonHelper.notNull(style+".menuTop.app.button") )
                str+="#mainNav .menu-app-top .menu-button{"+costum.getStyleCustom(".menuTop.app.button")+"}";
        }
        if(jsonHelper.notNull(style+".menuTop.logo") )
            str+="#mainNav .logo-menutop{"+costum.getStyleCustom(".menuTop.logo")+"}";
        if(jsonHelper.notNull(style+".menuTop.button") )
            str+="#mainNav .menu-btn-top{"+costum.getStyleCustom(".menuTop.button")+"}";
        if(jsonHelper.notNull(style+".menuTop.badge") )
            str+=".btn-menu-notif .notifications-count, .btn-menu-chat .chatNotifs, .btn-dashboard-dda .coopNotifs{"+costum.getStyleCustom(".menuTop.badge")+"}";
        if(jsonHelper.notNull(style+".menuTop.scopeBtn") ){
            str+=".menu-btn-scope-filter{"+costum.getStyleCustom(".menuTop.scopeBtn")+"}";
            if(jsonHelper.notNull(style+".menuTop.scopeBtn.hover") )
                str+=".menu-btn-scope-filter:hover, .menu-btn-scope-filter.active{"+costum.getStyleCustom(".menuTop.scopeBtn.hover")+"}";
        }
        if(jsonHelper.notNull(style+".menuTop.filterBtn")){
            str+=".btn-show-filters{"+costum.getStyleCustom(".menuTop.filterBtn")+"}";
            if(jsonHelper.notNull(style+".menuTop.filterBtn.hover"))
                str+=".btn-show-filters:hover, .btn-show-filters.active{"+costum.getStyleCustom(".menuTop.filterBtn.hover")+"}";
        }
        if(jsonHelper.notNull(style+".menuTop.connectBtn"))
            str+="#mainNav .btn-menu-connect{"+costum.getStyleCustom(".menuTop.connectBtn")+"}";
    }
    if(jsonHelper.notNull(style+".menuApp") ){
        str+="#menuApp{"+costum.getStyleCustom(".menuApp")+"}";
        if(jsonHelper.notNull(style+".menuApp.button") )
            str+="#menuApp .btn-menu-to-app{"+costum.getStyleCustom(".menuApp.button")+"}";
        if(jsonHelper.notNull(style+".menuApp.button.hover") )
            str+="#menuApp .btn-menu-to-app:hover, #menuApp .btn-menu-to-app:active, #menuApp .btn-menu-to-app:focus, #menuApp .btn-menu-to-app.active{"+costum.getStyleCustom(".menuApp.button.hover")+"}";
    }
    if(jsonHelper.notNull(style+".progress") ){
        if(jsonHelper.notNull(style+".progress.bar") )
            str+=".progressTop::-webkit-progress-bar { "+costum.getStyleCustom(".progress.bar")+" }";
        if(jsonHelper.notNull(style+".progress.value") )
            str+=".progressTop::-webkit-progress-value{"+costum.getStyleCustom(".progress.value")+"}.progressTop::-moz-progress-bar{"+costum.getStyleCustom(".progress.value")+"}";
    }
    //if(jsonHelper.notNull(style+".loader") ){
      //  if(jsonHelper.notNull(style+".loader.background") )
        //    str+="#loadingModal, .blockUI.blockMsg.blockPage{background-color:"+costum.getStyleCustom(".loader.background")+" !important}";
       // if(jsonHelper.notNull(style+".loader.ring1") )
       //     str+=".lds-dual-ring div{"+costum.getStyleCustom(".loader.ring1")+"}";
       // if(jsonHelper.notNull(style+".loader.ring2") )
       //     str+=".lds-dual-ring div:nth-child(2){"+costum.getStyleCustom(".loader.ring2")+"}";
    //}
    if(jsonHelper.notNull(style+".button") ){
        if(jsonHelper.notNull(style+".button.footer") ){
            if(jsonHelper.notNull(style+".button.footer.add") )
                str+="#show-bottom-add{"+costum.getStyleCustom(".button.footer.add")+"}";
            if(jsonHelper.notNull(style+".button.footer.toolbarAdds") )
                str+=".toolbar-bottom-adds{"+costum.getStyleCustom(".button.footer.toolbarAdds")+"}";
            if(jsonHelper.notNull(style+".button.footer.donate") )
                str+="#donation-btn{"+costum.getStyleCustom(".button.footer.donate")+"}";
        }
    }
    if(typeof costum.css.color != "undefined" ){
        colorCSS=["purple", "orange", "blue", "red", "black", "green", "green-k", "yellow", "yellow-k", "vine", "brown"];
        $.each(colorCSS, function(e,v){
            if(typeof costum.css.color[v] != "undefined" ){
                str+=".text-"+v+" { color:"+costum.css.color[v]+" !important;}"+
                     " .bg-"+v+" { background-color:"+costum.css.color[v]+" !important;}";
            }
        });
    }
    str+="@media (max-width: 767px){"+
            "#mainNav{margin-top:0px !important;}"+
            "#mainNav .menu-btn-top{font-size:22px !important;}"+
            "#mainNav .logo-menutop{height:40px;}"+
        "}";
    str+="</style>"; 

    $("head").append(str);
};

costum.value = function(obj,path){
    //mylog.log("costum.value ",path);
    if( jsonHelper.notNull(path) ){
        //mylog.log("costum.value ",path);
        if( path.indexOf("this.") > -1 ){
            pieces = path.split('.');
            return jsonHelper.eval(obj,pieces[1]);
        } else {
            path=path.replace("costum.","");
            return jsonHelper.getValueByPath(obj,path);
        }
    }
    else 
        return null;
};
//costum.init();
