adminPanel.views.group = function(){
	var data={
		title : "Validation des Organisations",
		types : [ "organizations" ],
		table : {
            name: {
                name : "Nom"
            },
            validated : { 
                name : "Valider",
                class : "col-xs-2 text-center"
            },
            actions : {
                class : "col-xs-3 text-center"
            }
        },
        actions : {
            validated : true
        }
	};
	ajaxPost('#content-view-admin', baseUrl+'/costum/default/groupadmin/', data, function(){},"html");
};

///----- addd JSON  ------
            // "events" : {
            //                     "label" : "Validation des événements",
            //                     "costumAdmin" : true,
            //                     "class" : "text-dark",
            //                     "href" : "javascript:;",
            //                     "icon" : "calendar",
            //                     "view" : "events"
            //                 },
adminPanel.views.events = function(){
    var data={
        title : "Evénements",
        types : [ "events" ],
        table : {
            name: {
                name : "Nom"
            },
            actions : {
                class : "col-xs-3 text-center"
            }
        },
        actions : {
            delete : true
        }
    };
    ajaxPost('#content-view-admin', baseUrl+'/costum/default/groupadmin/', data, function(){},"html");
};

adminPanel.views.annonces = function(){
    var data={
        title : "Annonces",
        types : [ "classifieds" ],
        table : {
            name: {
                name : "Nom"
            },
            actions : {
                class : "col-xs-3 text-center"
            }
        },
        actions : {
            delete : true
        }
    };
    ajaxPost('#content-view-admin', baseUrl+'/costum/default/groupadmin/', data, function(){},"html");
};

adminPanel.views.community = function(){
    //alert("HZE");
    var data={
        title : "Membres",
        id : costum.contextId,
        collection : costum.contextType,
        types : [ "citoyens"],
        url : baseUrl+'/costum/default/communityadmin',
        invite : true,
        table : {
            name: {
                name : "Membres"
            },
            tobeactivated : {
                name : "Validation de compte",
                class : "col-xs-2 text-center"
            },
            isInviting : {
                name : "Validation pour être membres",
                class : "col-xs-2 text-center"
            },
            roles : {
                name : "Roles",
                class : "col-xs-1 text-center"
            },
            admin : {
                name : "Admin",
                class : "col-xs-1 text-center"
            }
        },
        actions : {
            admin : true,
            roles : true,
            disconnect : true
        }
    };
    ajaxPost('#content-view-admin', data.url, data, function(){},"html");
};