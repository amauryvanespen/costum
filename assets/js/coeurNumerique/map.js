var allMaps = {
	maps: {},
	clear: function () {
		$.each(allMaps.maps, function (e, v) {
			v.clearMap();
		});
	}
};
var mapObj = {
	parentContainer: 'mapContainer',
	container: 'mapContainerMap',
	hideContainer: 'search-content',
	map: null,
	data: {},
	arrayBounds: [],
	bounds: null,
	markersCluster: null,
	markerList: {},
	activeCluster: true,
	activePopUp: false,
	activePreview: false,
	menuRight: false,
	listItem: true,
	distanceToMax: 0,
	mapOpt: {
		dragging: true,
		draggable: false,
		center: [0, 0],
		zoom: 10,
		offset: {
			x: 0,
			y: 0
		},
		menuRight: false,
		btnHide: false,
		// maxBounds: bounds,
		minZoom: 0,
		maxZoom: 18

	},
	init: function (pInit = null) {
		mylog.log("mapObj.init", pInit);
		//Init variable
		var copyMap = null;
		if (pInit != null &&
			typeof pInit.container != "undefined" &&
			pInit.container != null &&
			$("#" + pInit.container).length > 0) {
			copyMap = jQuery.extend(true, {}, mapObj);
			copyMap.initVar(pInit);
		}
		return copyMap;

	},
	initVar: function (pInit = null) {
		mylog.log("mapObj.initVar", pInit);
		//Init variable
		this.parentContainer = ((pInit != null && typeof pInit.container != "undefined") ? pInit.container : "mapContainer");
		//this.container =  ( (pInit != null && typeof pInit.container != "undefined") ? pInit.container : this.parentContainer+"Map" );

		this.container = this.parentContainer + "Map";
		this.hideContainer = ((pInit != null && typeof pInit.hideContainer != "undefined") ? pInit.hideContainer : "search-content");
		this.initView(pInit);
		this.showLoader();
		this.arrayBounds = [];
		this.bounds = null;
		this.markersCluster = null;
		this.markerList = {};
		this.activeCluster = ((pInit != null && typeof pInit.activeCluster != "undefined") ? pInit.activeCluster : true);
		this.mapOpt.dragging = ((pInit != null && typeof pInit.dragging != "undefined") ? pInit.dragging : true);
		this.mapOpt.center = ((pInit != null && typeof pInit.center != "undefined") ? pInit.center : [0, 0]);
		this.mapOpt.zoom = ((pInit != null && typeof pInit.zoom != "undefined") ? pInit.zoom : 10);
		this.activePopUp = ((pInit != null && typeof pInit.activePopUp != "undefined") ? pInit.activePopUp : false);
		this.activePreview = ((pInit != null && typeof pInit.activePreview != "undefined") ? pInit.activePreview : false);

		this.forced = ((pInit != null && typeof pInit.forced != "undefined") ? pInit.forced : false);
		//creation de la carte 
		this.map = L.map(this.container, this.mapOpt);
		// création tpl
		var tile = ((pInit != null && typeof pInit.tile != "undefined") ? pInit.tile : "toner");

		this.setTile(tile);

		if (this.activeCluster === true) {
			this.markersCluster = new L.markerClusterGroup({
				iconCreateFunction: function (cluster) {
					return mapCustom.clusters.default(cluster);
				}
			});
		}

		if (pInit != null && typeof pInit.elts != "undefined") {

			this.addElts(pInit.elts);
		} else {
			setTimeout(function () {
				if (this.map != null)
					this.map.invalidateSize();
			}, 400);
		}
		//this.hideLoader();


	},
	initView: function (pInit) {
		mylog.log("mapObj.initView", pInit);
		var str = "";
		str += "<div class='mapLoading'></div>"
		this.mapOpt.btnHide = ((pInit != null && typeof pInit.btnHide != "undefined") ? pInit.btnHide : false);
		if (notNull(pInit) && notNull(pInit.menuRight) && pInit.menuRight === true) {
			this.mapOpt.menuRight = true;

			var sizeList = $("#" + this.parentContainer).height() - 80;
			str += '<div id="menuRight' + this.parentContainer + '" class="hidden-xs col-sm-3 no-padding menuRight" style="">' +
				'<div id="menuRight_search' + this.parentContainer + '" class="col-sm-12 no-padding menuRight_search">' +
				'<div class="menuRight_header col-sm-12 padding-10">' +
				'<div class="col-xs-6">' +
				'<span id="result' + this.parentContainer + '" class=""></span>' +
				'<span class="menuRight_header_title"> Résultats </span>' +
				'</div>';


			if (this.mapOpt.btnHide === true) {
				str += '<div class="col-xs-2 pull-right">' +
					'<button type="button" class="btn btnHeader dropdown-toggle btn-hide-map" data-toggle="dropdown">' +
					'<i class="fa fa-times"></i>' +
					'</button>' +
					'</div>';
			}


			str += '<div class="col-xs-2 pull-right">' +
				'<button type="button" class="btn btnHeader dropdown-toggle" id="btn-filters' + this.parentContainer + '" data-toggle="dropdown">' +
				'<i class="fa fa-filter"></i>' +
				'</button>' +
				'<ul class="dropdown-menu panel_map" id="panel_filter' + this.parentContainer + '" role="menu" aria-labelledby="panel_filter" style="overflow-y: auto; max-height: 400px;">' +
				'<h3 class="title_panel_map"><i class="fa fa-angle-down"></i> Filtrer les résultats par types</h3>' +
				'<button class="item_panel_map active" id="item_panel_filter_all' + this.parentContainer + '">' +
				'<i class="fa fa-star"></i> Tout' +
				'</button>' +
				'</ul>' +
				'</div>' +
				'<div class="col-xs-2 pull-right">' +
				'<button type="button" class="btn btnHeader dropdown-toggle" id="btn-panel' + this.parentContainer + '" data-toggle="dropdown">' +
				'<i class="fa fa-tags"></i>' +
				'</button>' +
				'<ul class="dropdown-menu panel_map pull-right" id="panel_map' + this.parentContainer + '" role="menu" aria-labelledby="panel_map" style="overflow-y: auto; max-height: 400px;">' +
				'<h3 class="title_panel_map"><i class="fa fa-angle-down"></i> Filtrer les résultats par mot-clés</h3>' +
				'<button class="item_panel_map active" id="item_panel_map_all' + this.parentContainer + '">' +
				'<i class="fa fa-star"></i> Tout' +
				'</button>' +
				'</ul>' +
				'</div>' +
				'</div>' +


				'<div id="menuRight_search_filters' + this.parentContainer + '" class="col-sm-12 no-padding menuRight_search_filters">' +
				'<input class="form-control date-range active" type="text" id="input_name_filter' + this.parentContainer + '" placeholder="Filtrer par noms...">' +
				'</div>' +
				'<div class="menuRight_body col-sm-12 no-padding" id="liste_map_element' + this.parentContainer + '" ' +
				' style="height: ' + sizeList + 'px;"></div>' +
				'</div>' +
				'</div>';

			str += '<div id="modalItemNotLocated' + this.parentContainer + '" class="modal fade" role="dialog">' +
				'<div class="modal-dialog">' +
				'<div class="modal-content">' +
				'<div class="modal-header">' +
				'<button type="button" class="close" data-dismiss="modal">&times;</button>' +
				'<h4 class="modal-title text-dark">' +
				'<i class="fa fa-map-marker"></i>' + trad["This data is not geolocated"] +
				'</h4>' +
				'</div>' +
				'<div class="modal-body"></div>' +
				'<div class="modal-footer">' +
				// '<button type="button" id="btn-open-details" class="btn btn-default btn-sm btn-success" data-dismiss="modal"><i class="fa fa-plus"></i>'+trad["Show the details"]+'</button>'+
				'<button type="button" class="btn btn-default btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>' + trad.closed + '</button>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>';
		} else {
			if (this.mapOpt.btnHide === true) {
				str += '<div class="col-xs-2 div-btn-hide">' +
					'<button type="button" class="btn btnHeader dropdown-toggle btn-hide-map" id="" data-toggle="dropdown" style="background-color: #e6434d; color: white;">' +
					'<i class="fa fa-times"></i>' +
					'</button>' +
					'</div>';
			}
		}

		str += '<div id="' + this.container + '" style="z-index: 1; width: 100%; height: 100%; position: relative;"></div>';
		$("#" + this.parentContainer).html(str);
	},
	setTile: function (tile) {
		mylog.log("mapObj.setTile", this.map);

		if (typeof tile != "undefined" && tile == "mapbox" && $('script[src="https://api.mapbox.com/mapbox.js/v2.4.0/mapbox.js"]').length) {
			//MAPBOX
			var accessToken = 'pk.eyJ1IjoiY29tbXVuZWN0ZXIiLCJhIjoiY2l6eTIyNTYzMDAxbTJ3bng1YTBsa3d0aCJ9.elyGqovHs-mrji3ttn_Yjw';
			// Replace 'mapbox.streets' with your map id.
			var mapboxTiles = L.tileLayer('https://api.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=' + accessToken, {
				attribution: '© <a href="https://www.mapbox.com/feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
			});
			this.map.addLayer(mapboxTiles);
		} else if (typeof tile != "undefined" && tile == "toner-lite") {
			//Noir et Blanc Lite
			L.tileLayer('http://stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}.png', {
				attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>', //'Map tiles by <a href="http://stamen.com">Stamen Design</a>',
				// zIndex:1,
				// minZoom: 3,
				// maxZoom: 17
			}).addTo(this.map);
		} else if (typeof tile != "undefined" && tile == "satelite") {
			//SATELITE
			L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {

			}).addTo(this.map);

		} else {
			//Noir et Blanc 
			L.tileLayer('http://stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png', {
				attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>', //'Map tiles by <a href="http://stamen.com">Stamen Design</a>',
				// zIndex:1,
				// minZoom: 3,
				// maxZoom: 17
			}).addTo(this.map);
		}

		//OSM
		//L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', {foo: 'bar'}).addTo(mapObj.map);

	},
	filtres: {
		types: [],
		typesActived: [],
		tags: [],
		tagsActived: [],
		search: "",
		result: function (mObj, elt) {
			//mylog.log("mapObj.filtres.result", elt);
			var res = false;
			if (mObj.filtres.isTags(mObj, elt) &&
				mObj.filtres.isSearch(mObj, elt) &&
				mObj.filtres.isType(mObj, elt)) {
				res = true;
			}
			//mylog.log("mapObj.filtres.result end", res);
			return res;
		},
		isTags: function (mObj, elt) {
			var res = false;
			//mylog.log("mapObj.filtres.isTags elt", elt, mObj.filtres.tags);
			if (Object.keys(mObj.filtres.tagsActived).length == 0 ||
				typeof mObj.filtres.tagsActived == "undefined" ||
				mObj.filtres.tagsActived.length == 0) {
				res = true;
			} else {
				$.each(mObj.filtres.tagsActived, function (k, v) {

					if (typeof elt.tags != "undefined" && $.inArray(v, elt.tags) > -1) {
						//mylog.log("mapObj.filtres.isTags tag", v);
						res = true;
					}
				});
			}
			//mylog.log("mapObj.filtres.isTags end", res);
			return res;
		},
		isType: function (mObj, elt) {
			var res = false;
			//mylog.log("mapObj.filtres.isType elt", elt, mObj.filtres.types);
			if (Object.keys(mObj.filtres.typesActived).length == 0 ||
				typeof mObj.filtres.typesActived == "undefined" ||
				mObj.filtres.typesActived.length == 0) {
				res = true;
			} else {
				$.each(mObj.filtres.typesActived, function (k, v) {

					if (typeof elt.type != "undefined" && v === elt.type) {
			//			mylog.log("mapObj.filtres.isType type", v);
						res = true;
					}
				});
			}
			//mylog.log("mapObj.filtres.isType end", res);
			return res;
		},
		isSearch: function (mObj, elt) {
			//mylog.log("mapObj.filtres.isSearch", elt);
			var res = false;
			//alert(mObj.filtres.search);
			if (elt.name.search(new RegExp(mObj.filtres.search, "i")) >= 0) {
				res = true;
			}
			return res;
		}
	},
	addElts: function (data) {
		//mylog.log("mapObj.addElts", data);
		var mObj = this;
		//mapCustom.clearMap(this.parentContainer);
		$.each(data, function (k, v) {
			//mylog.log("mapObj.data", this);
			if (typeof mObj.data[k] == "undefined")
				mObj.data[k] = v;

			if (mObj.filtres.result(mObj, v)) {
				v.id = k;
				mObj.addMarker({ elt: v });
				mapCustom.lists.createItemList(mObj, k, v, mObj.container);
			}
		});
		// if(notNull(mObj.filtres.timeoutAddCity)) 
		// 	clearTimeout(mObj.filtres.timeoutAddCity);
		// mObj.filtres.timeoutAddCity = setTimeout(function(){
		if (mObj.arrayBounds.length > 0) {
			if (mObj.arrayBounds.length == 1) {
				var point = {
					x: mObj.arrayBounds[0][0],
					y: mObj.arrayBounds[0][1]
				}
				//mylog.log("mapObj.addElts if one POINT", point);
			} else if (mObj.arrayBounds.length > 1) {
				mObj.bounds = L.bounds(mObj.arrayBounds);
				var point = mObj.bounds.getCenter();
				//mylog.log("mapObj.addElts if bounds POINT", point);
			}
			
			if (!isNaN(point.x) && !isNaN(point.y)) {
				//if (typeof mObj.forced == "undefined" || !mObj.forced || mObj.forced.zoom == "undefined")
				mObj.getZoomByDistance();
				//mylog.log("mapObj.addElts POINT after NaN", point, mObj.mapOpt.offset.x, mObj.mapOpt.offset.y);
				var lat = parseFloat(point.x) + ((parseFloat(point.x) < 0) ? -Math.abs(mObj.mapOpt.offset.x) : mObj.mapOpt.offset.x);
				var lon = parseFloat(point.y) + ((parseFloat(point.y) < 0) ? -Math.abs(mObj.mapOpt.offset.y) : mObj.mapOpt.offset.y);
				//mylog.log("mapObj.addElts POINT after parseFloat ", [lat, lon], mObj.mapOpt.zoom);
				mObj.mapOpt.center = [lat, lon];
				//if (typeof mObj.forced == "undefined" || !mObj.forced || mObj.forced.latLon == "undefined")
				//mObj.map.panTo([lat, lon]);
				if (mObj.forced && mObj.forced.latLon && mObj.forced.zoom) {
					mObj.map.setView(mObj.forced.latLon, mObj.forced.zoom);
				} else
					mObj.map.setView([lat, lon], mObj.mapOpt.zoom);
			}
		}

		if (mObj.activeCluster === true)
			mObj.map.addLayer(mObj.markersCluster);
		mObj.actions.bindMap(mObj);

		if (mObj.forced) {
			//mylog.log("mapObj.addElts mObj.forced ", mObj.forced);
			if ( mObj.forced.latLon && mObj.forced.zoom ){
				//mylog.log("mapObj.addElts mObj.forced if ", mObj.forced);
				mObj.map.setView(mObj.forced.latLon, mObj.forced.zoom);
			} else {
				//mylog.log("mapObj.addElts mObj.forced else ", mObj.forced);
				if (mObj.forced.latLon)
					mObj.map.panTo(mObj.forced.latLon);

				if (mObj.forced.zoom)
					mObj.map.setZoom(mObj.forced.zoom);
			}
			
		}

		mObj.hideLoader();
		mObj.map.invalidateSize();
		//}, 500);
	},
	showLoader: function () {
		//mylog.log("mapObj.showLoader");
		$(".mapLoading").show();
		var heightMap = $("#" + this.parentContainer).outerHeight();
		var widthMap = $("#" + this.parentContainer).outerWidth();
		if ($("#" + this.parentContainer + " #menuRight" + this.parentContainer).is(":visible")) {
			widthMap = widthMap - $("#" + this.parentContainer + " #menuRight" + this.parentContainer).outerWidth();
		}
		coInterface.showLoader(".mapLoading", trad.currentlyresearching);
		var marginLeftLoader = (widthMap - $(".mapLoading").outerWidth()) / 2;
		var marginTopLoader = (heightMap - $(".mapLoading").outerHeight()) / 2;
		$(".mapLoading").css({ "margin-left": marginLeftLoader + "px", "margin-top": marginTopLoader + "px" });
	},
	hideLoader: function () {
		//mylog.log("mapObj.hideLoader");
		$(".mapLoading").fadeOut();
	},
	clearMap: function () {
		//mylog.log("mapObj.clearMap");
		// Supprime les markers
		var mObj = this;
		//mylog.log("markers to clean", mObj.markerList);

		//mObj.map.eachLayer(function (layer) {
		//	mObj.map.removeLayer(layer);
		//});
		//mObj.clearPolygon();
		if (mObj.markersCluster != null) {
			mObj.markersCluster.clearLayers();
		}
		if (Object.keys(mObj.markerList).length > 0) {
			$.each(mObj.markerList, function () {
				mObj.map.removeLayer(this);
			});
		}
		if (mObj.markerSingleList != null)
			$.each(mObj.markerSingleList, function () {
				mObj.map.removeLayer(this);
			});
		mObj.markerList = {};
		mObj.arrayBounds = [];
		mObj.bounds = null;

		mObj.showLoader();
		mapCustom.clearMap(mObj.parentContainer);
	},
	distanceTo: function (latLon) {
		//mylog.log("mapObj.distanceTo", latLon);
		if (Object.keys(this.markerList).length) {
			latLonObj = L.latLng(latLon[0], latLon[1]);
			var mObj = this;
			$.each(mObj.markerList, function (k, v) {
				var dist = latLonObj.distanceTo(mObj.getLatLng(k));
				if (dist > mObj.distanceToMax)
					mObj.distanceToMax = dist;
			});
		}
	},
	getZoomByDistance: function () {
		// mylog.log("mapObj.getZoomByDistance");
		if (Object.keys(this.markerList).length == 0) {
			this.mapOpt.zoom = 0;
			this.mapOpt.offset.x = 0;
			this.mapOpt.offset.y = 0;
		} else if (Object.keys(this.markerList).length == 1) {
			this.mapOpt.zoom = 14;
			this.mapOpt.offset.x = 0.002;
			this.mapOpt.offset.y = 0.003;
		} else {
			if (this.distanceToMax > 9000000) {
				this.mapOpt.zoom = 2;
				this.mapOpt.offset.x = 0;
				this.mapOpt.offset.y = 15;
			} else if (this.distanceToMax > 7500000) {
				this.mapOpt.zoom = 3;
				this.mapOpt.offset.x = 0;
				this.mapOpt.offset.y = 15;
			} else if (this.distanceToMax > 6000000) {
				this.mapOpt.zoom = 4;
				this.mapOpt.offset.x = 0;
				this.mapOpt.offset.y = 0.2;
			} else if (this.distanceToMax > 50000) {
				this.mapOpt.zoom = 6;
				this.mapOpt.offset.x = 0;
				this.mapOpt.offset.y = 0.2;
				// }else if(this.distanceToMax > 7000){
			} else if (this.distanceToMax > 20000) {
				this.mapOpt.zoom = 8;
				this.mapOpt.offset.x = 0;
				this.mapOpt.offset.y = 0.2;
				// }else if(this.distanceToMax > 7000){
			} else if (this.distanceToMax > 10000) {
				this.mapOpt.zoom = 10;
				this.mapOpt.offset.x = 0;
				this.mapOpt.offset.y = 0.2;
				// }else if(this.distanceToMax > 7000){
			} else {
				this.mapOpt.zoom = 12;
				this.mapOpt.offset.x = 0;
				this.mapOpt.offset.y = 0.2;
			}
		}

		if (this.mapOpt.menuRight === false) {
			this.mapOpt.offset.x = 0;
			this.mapOpt.offset.y = 0;
		}


	},
	setZoomByDistance: function () {
		//mylog.log("mapObj.setZoomByDistance");
		this.getZoomByDistance();
		this.map.setZoom(this.mapOpt.zoom);
	},
	addMarker: function (params) {
		//params elt, addPopUp, center=true opt = {}{
		//mylog.log("mapObj.addMarker", params);
		if (typeof params.elt != "undefined" && params.elt != null &&
			typeof params.elt.geo != "undefined" && params.elt.geo != null &&
			typeof params.elt.geo.latitude != "undefined" && params.elt.geo.latitude != null &&
			typeof params.elt.geo.longitude != "undefined" && params.elt.geo.longitude != null) {

			var myIcon = L.icon({
				iconUrl: mapCustom.markers.getMarker(params.elt),
				iconSize: [45, 55],
				iconAnchor: [25, 45],
				popupAnchor: [-3, -30],
				shadowUrl: '',
				shadowSize: [68, 95],
				shadowAnchor: [22, 94]
			});

			if (typeof params.opt == "undefined" || params.opt == null)
				params.opt = {};

			params.opt.icon = myIcon;
			//params.opt.draggable = true;

			var latLon = [params.elt.geo.latitude, params.elt.geo.longitude];
			this.distanceTo(latLon);

			var marker = L.marker(latLon, params.opt);

			this.markerList[params.elt.id] = marker;

			if (typeof this.activePopUp != "undefined" && this.activePopUp === true)
				this.addPopUp(marker, params.elt);

			this.arrayBounds.push(latLon);
			if (this.activeCluster === true) {
				//mylog.log("giiiiiiiiiiiiiiiiii", marker);
				this.markersCluster.addLayer(marker);
			}
			else {
				marker.addTo(this.map);
				if (typeof params.center == "undefined" || params.center === true) {
					//mylog.log("mapObj.panTo");
					this.map.panTo(latLon);
				}
			}

			var mObj = this;
			marker.on('click', function (e) {
				mObj.centerOne(mObj, params.elt.geo.latitude, params.elt.geo.longitude);

				coInterface.bindLBHLinks();
				if( mObj.activePreview === true){
				// var link = params.elt.hash;
				// var previewHash=link.split(".");
				// hashT=location.hash.split("?");
				// getStatus=searchInterface.getUrlSearchParams();
				// urlHistoric=hashT[0].substring(0)+"?preview="+previewHash[2]+"."+previewHash[4];
				// if(getStatus != "") urlHistoric+="&"+getStatus; 
				// history.replaceState({}, null, urlHistoric);
				// urlCtrl.openPreview(link);
				}

			});
		}
	},
	addPolygon: function () {
		//mylog.log("mapObj.addPolygon");
		var polygon = L.polygon([
			[51.509, -0.08],
			[51.503, -0.06],
			[51.51, -0.047]
		]).addTo(this.map);
		this.addPopUp(polygon);
	},
	addCircle: function () {
		//mylog.log("mapObj.addCircle");
		var circle = L.circle([51.508, -0.11], {
			color: 'red',
			fillColor: '#f03',
			fillOpacity: 0.5,
			radius: 500
		}).addTo(this.map);
		this.addPopUp(circle, true);
	},
	addPopUp: function (marker, elt = null, open = false) {
		//mylog.log("mapObj.addPopUp", marker, elt, open);
		marker.bindPopup(this.getPopupSimple(elt),
			{
				width: "300px"
			}).openPopup();
		if (open === true)
			marker.openPopup();
	},
	getPopupSimple: function (data) {
		//mylog.log("mapObj.getPopupSimple", data);
		return mapCustom.popup.default(data, this);
	},
	setLatLng: function (latLon, marker) {
		//mylog.log("mapObj.setLatLng", latLon, marker);
		this.markerList[marker].setLatLng(latLon/*, { draggable: true }*/);
		this.map.panTo(latLon);
	},
	getLatLng: function (marker) {
		//mylog.log("mapObj.getLatLng", marker);
		return this.markerList[marker].getLatLng();
	},
	addFct: function (marker, event, fct) {
		mylog.log("mapObj.addFct", marker, event, fct);
		// expmle : event == 'dragend'
		this.markerList[marker].on('dragend', fct);
	},
	centerOne: function (mObj, lat, lon) {
		mylog.log("mapObj.centerOne", lat, lon);
		//alert("oui");
		//mObj.map.setZoom(14);
		if (notNull(mObj.filtres.timeoutAddCity))
			clearTimeout(mObj.filtres.timeoutAddCity);
		mObj.filtres.timeoutAddCity = setTimeout(function () {
			var moveLat = "0.000";
			var moveLon = "0.000";
			// if(mObj.mapOpt.menuRight === true){
			// 	moveLat = "0.003" ;
			// 	moveLon = "0.003" ;
			// }
			mylog.log("mapObj.centerOne after setTimeout ", moveLat, parseFloat(moveLat), moveLon, parseFloat(moveLon));

			lat = parseFloat(lat) + parseFloat(moveLat);
			lon = parseFloat(lon) + parseFloat(moveLon);
			// lat = lat + moveLat ;
			// lon = lon + moveLon ;
			var center = [lat, lon];
			mylog.log("mapObj.centerOne after parseFloat", center);
			mObj.map.panTo(center);
		}, 500);
	},
	actions: {
		bindMap: function (mObj) {
			mylog.log("mapObj.actions.bindMap");
			//alert(mObj.parentContainer);
			$("#input_name_filter" + mObj.parentContainer).keyup(function () {
				mylog.log("mapObj.actions.bindMap #input_name_filter" + mObj.parentContainer);
				//alert("here");
				if (notNull(mObj.filtres.timeoutAddCity))
					clearTimeout(mObj.filtres.timeoutAddCity);
				mObj.filtres.timeoutAddCity = setTimeout(function () {
					mylog.log("mapObj.mObj input_name_filter" + mObj.parentContainer);
					mObj.filtres.search = $("#input_name_filter" + mObj.parentContainer).val();
					mObj.clearMap();
					mObj.addElts(mObj.data);
				}, 500);

			});

			$("#item_panel_filter_all" + mObj.parentContainer).click(function () {
				$(".item_panel_map").removeClass("active");
				$("#item_panel_filter_all" + mObj.parentContainer).addClass("active");
				mObj.filtres.typesActived = [];
				mObj.clearMap();
				mObj.addElts(mObj.data);
			});

			$("#item_panel_map_all" + mObj.parentContainer).click(function () {
				mylog.log("mapObj.actions.bindMap #item_panel_map_all" + mObj.parentContainer);
				mObj.filtres.tagsActived = [];
				$(".item_panel_map").removeClass("active");
				$("#item_panel_map_all" + mObj.parentContainer).addClass("active");
				mObj.clearMap();
				mObj.addElts(mObj.data);
			});

			$(".btn-hide-map").click(function () {
				mylog.log("mapObj.actions.bindMap .btn-hide-map");
				showMap(false);
			});
			
		}
	}
}

var mapCustom = {
	css: [
	],
	markers: {
		default: modules.map.assets + '/images/markers/citizen-marker-default.png',
		organization: modules.map.assets + '/images/markers/ngo-marker-default.png',
		classified: modules.map.assets + '/images/markers/classified-marker-default.png',
		proposal: modules.map.assets + '/images/markers/proposal-marker-default.png',
		poi: modules.map.assets + '/images/markers/poi-marker-default.png',
		project: modules.map.assets + '/images/markers/project-marker-default.png',
		event: modules.map.assets + '/images/markers/event-marker-default.png',
		getMarker: function (data) {
			//mylog.log("mapObj.getMarker", data.type);
			var imgM = mapCustom.markers.default;
			if (typeof data != "undefined" && data == null)
				imgM = mapCustom.markers.default;
			else if (typeof data.profilMarkerImageUrl !== "undefined" && data.profilMarkerImageUrl != "")
				imgM = baseUrl + data.profilMarkerImageUrl;
			else if (typeof data.type != "undefined" && data.type == null)
				imgM = mapCustom.markers.default;
			else if (jQuery.inArray(data.type, ["organization", "organizations", "NGO"]) != -1)
				imgM = mapCustom.markers.organization;
			else if (jQuery.inArray(data.type, ["project", "projects"]) != -1)
				imgM = mapCustom.markers.project;
			else if (jQuery.inArray(data.type, ["event", "events"]) != -1)
				imgM = mapCustom.markers.event;
			else if (jQuery.inArray(data.type, ["classified", "classifieds"]) != -1)
				imgM = mapCustom.markers.classified;
			else if (jQuery.inArray(data.type, ["proposal", "proposals"]) != -1)
				imgM = mapCustom.markers.proposal;
			else if (jQuery.inArray(data.type, ["poi"]) != -1)
				imgM = mapCustom.markers.poi;
			else
				imgM = mapCustom.markers.default;
			//mylog.log("mObj imgM", imgM);
			return imgM;
		}
	},
	custom: {
		getThumbProfil: function (data) {
			var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
			if (typeof data.profilThumbImageUrl !== "undefined" && data.profilThumbImageUrl != "")
				imgProfilPath = baseUrl + data.profilThumbImageUrl;
			else
				imgProfilPath = modules.map.assets + "/images/thumb/default_" + data.type + ".png";

			return imgProfilPath;
		}

	},
	clusters: {
		default: function (cluster) {
			var childCount = cluster.getChildCount();
			var c = ' marker-cluster-';
			if (childCount < 10) {
				c += 'small';
			} else if (childCount < 100) {
				c += 'medium';
			} else {
				c += 'large';
			}
			return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
		}
	},
	clearMap: function (dom) {
		$("#" + dom + " #liste_map_element" + dom).html("");
	},
	popup: {
		default: function (data, mObj) {
			//mylog.log("mapObj.mapCustom.popup.default", data);
			var id = (typeof data.id != "undefined") ? data.id : data._id.$id;

			var imgProfil = mapCustom.custom.getThumbProfil(data);
			mylog.log("mObj imgProfil", imgProfil);
			var eltName = (typeof data.title != "undefined") ? data.title : data.name;
			var popup = "";
			popup += "<div class='padding-5' id='popup" + id + "'>";
			popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
			popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";

			if (typeof data.tags != "undefined" && data.tags != null && data.tags.length > 0) {
				popup += "<div style='margin-top : 5px;'>";
				var totalTags = 0;
				$.each(data.tags, function (index, value) {
					totalTags++;
					if (totalTags < 3) {
						popup += "<div class='popup-tags'>#" + value + " </div>";
					}
				});
				popup += "</div>";
			}
			if(typeof data.address != "undefined" && data.address != null){
				addressStr="";
				if(typeof data.address.streetAddress != "undefined")
					addressStr+=data.address.streetAddress;
				if(typeof data.address.postalCode != "undefined")
					addressStr+=((addressStr != "") ? ", " : "")+data.address.postalCode;
				if(typeof data.address.addressLocality != "undefined")
					addressStr+=((addressStr != "") ? " " : "")+data.address.addressLocality;
				popup += "<div class='popup-address text-dark'>";
				popup += 	"<i class='fa fa-map-marker'></i> "+addressStr;
				popup += "</div>";
			}
			if (typeof data.shortDescription != "undefined" &&
				data.shortDescription != "" &&
				data.shortDescription != null) {
				popup += "<div class='popup-section'>";
				popup += "<div class='popup-subtitle'>Description</div>";
				popup += "<div class=''>" + data.shortDescription + "</div>";
				popup += "</div>";
			}

			if ((typeof data.url != "undefined" && data.url != null && typeof data.url == "string") ||
				(typeof data.email != "undefined" && data.email != null)) {
				popup += "<div id='pop-contacts' class='popup-section'>";
				popup += "<div class='popup-subtitle'>Contacts</div>";

				if (typeof data.url != "undefined" && data.url != null && typeof data.url == "string") {
					popup += "<div class='popup-info-profil'>";
					popup += "<i class='fa fa fa-desktop fa_url'></i> ";
					popup += "<a href='" + data.url + "' target='_blank'>" + data.url + "</a>";
					popup += "</div>";
				}

				if (typeof data.email != "undefined" && data.email != null) {
					popup += "<div class='popup-info-profil'>";
					popup += "<i class='fa fa-envelope fa_email'></i> " + data.email;
					popup += "</div>";
				}

				popup += "</div>";
				popup += "</div>";
			}
			var url = '#page.type.' + data.type + '.id.' + id;

			// if (data.type.substr(0,11) == "poi.interop") {
			// 	url = data.url;
			// 	popup += "<a href='"+url+"' target='_blank' class='item_map_list popup-marker' id='popup"+id+"'>";
			// }else if (typeof TPL_IFRAME != "undefined" && TPL_IFRAME==true){
			// 	url = "https://www.communecter.org/"+url;
			// 	popup += "<a href='"+url+"' target='_blank' class='item_map_list popup-marker' id='popup"+id+"'>";
			// }else if (typeof networkJson != "undefined" && notNull(networkJson) && notNull(networkJson.dataSrc) && notNull(data.source)){
			// 	popup += "<a href='"+data.source+"' target='_blank' class='item_map_list popup-marker' id='popup"+id+"'>";
			// }else{
			// 	onclick = 'urlCtrl.loadByHash("'+url+'");';					
			// 	popup += "<a href='"+url+"' onclick='"+onclick+"' class='item_map_list popup-marker lbh' id='popup"+id+"'>";
			// }
			popup += "<div class='popup-section'>";
			if(mObj.activePreview)
				popup += "<a href='" + url + "' class='lbh-preview-element item_map_list popup-marker' id='popup" + id + "'>";
			else
				popup += "<a href='" + url + "' target='_blank' class='lbh item_map_list popup-marker' id='popup" + id + "'>";
			popup += '<div class="btn btn-sm btn-more col-md-12">';
			popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
			popup += '</div></a>';
			popup += '</div>';
			popup += '</div>';
			return popup;
		}
	},
	lists: {
		text: "",
		createItemList: function (mObj, kElt, element, container) {
			//mylog.log("mapObj.mObj createItemList ", mObj, kElt, element, container);
			//mylog.log(element.type);
			//mylog.log(mObj.filtres.types);

			if (typeof element.type != "undefined" && $.inArray(element.type, mObj.filtres.types) == -1) {
				mObj.filtres.types.push(element.type);
				var valueIdType = element.type.replace(/[^A-Za-z0-9]/g, "");
				if (!$('#item_panel_map_' + valueIdType).length) {
                    
                    var elementType =
                        element.type == "organizations" ? "organisations"
                        : element.type == "events" ? "évènements"
                        : element.type == "GovernmentOrganization" ? "Service Public"
                        : element.type == "NGO" ? "Association"
                        : element.type == "LocalBusiness" ? "Entreprise"
                        : element.type == "Group" ? "Autre"
                        : "projets";

					var newItem = "<button class='item_panel_map' id='item_panel_map_" + valueIdType + "'>" +
						"<i class='fa fa-tag' ></i> " + elementType +
						"</button>";
					$('#panel_filter' + mObj.parentContainer).append(newItem);

					//var valueId = value.replace(/[^A-Za-z0-9]/g,"");


					$("#item_panel_map_" + valueIdType).click(function () {
						$(".item_panel_map").removeClass("active");
						$("#item_panel_map_" + valueIdType).addClass("active");
						mObj.filtres.typesActived = [];
						if ($.inArray(valueIdType, mObj.filtres.typesActived) == -1)
							mObj.filtres.typesActived.push(valueIdType);
						mObj.clearMap();
						mObj.addElts(mObj.data);
					});
				}
			}
			//prépare le nom (ou "anonyme")
			var name = (element.name != null) ? element.name : (element.title != null) ? element.title : "Anonyme";
			//recuperation de l'image de profil (ou image par defaut)
			var imgProfilPath = mapCustom.custom.getThumbProfil(element);
			//return l'élément html
			var button = '<div class="element-right-list" id="element-right-list-' + kElt + '">' +
				'<button class="item_map_list item_map_list_' + kElt + '">' +
				"<div class='left-col'>" +
				"<div class='thumbnail-profil'><img height=50 width=50 src='" + imgProfilPath + "'></div>" +
				"</div>" +
				"<div class='right-col'>" +
				"<div class='info_item pseudo_item_map_list'>" + name + "</div>";


			if ("undefined" != typeof element.tags && element.tags != null) {
				button += "<div class='info_item items_map_list'>";
				var totalTags = 0;
				if (typeof element.tags != "undefined" && element.tags != null) {
					$.each(element.tags, function (index, value) {
						totalTags++;
						if (totalTags < 4) {
							var t = typeof tradCategory[value] != "undefined" ? tradCategory[value] : value;
							button += "<a href='javascript:' class='tag_item_map_list'>#" + t + " </a>";
						}
						//var nameTag = value ;
						var valueId = value.replace(/[^A-Za-z0-9]/g, "");
						//si l'item n'existe pas deja
						if (!$('#item_panel_map_' + valueId).length) {

							var newItem = "<button class='item_panel_map' id='item_panel_map_" + valueId + "'>" +
								"<i class='fa fa-tag' ></i> " + value +
								"</button>";
							$('#panel_map' + mObj.parentContainer).append(newItem);

							$("#item_panel_map_" + valueId).click(function () {
								$(".item_panel_map").removeClass("active");
								$("#item_panel_map_" + valueId).addClass("active");
								mObj.filtres.tagsActived = [];
								if ($.inArray(value, mObj.filtres.tagsActived) == -1)
									mObj.filtres.tagsActived.push(value);
								mObj.clearMap();
								//mapCustom.clearMap(mObj.parentContainer);
								mObj.addElts(mObj.data);
							});

						}
						if ($.inArray(value, mObj.filtres.tags) == -1)
							mObj.filtres.tags.push(value);
					});
				}
				button += "</div>";
			}

			if (typeof element.address != "undefined" && element.address != null) {
				button += "<div class='info_item city_item_map_list inline'>";
				if (typeof element.address.addressLocality != "undefined")
					button += element.address.addressLocality + " ";
				if (typeof element.address.postalCode != "undefined")
					button += element.address.postalCode + " ";
				if (typeof element.address.addressCountry != "undefined")
					button += element.address.addressCountry + " ";
				button += "</div>";

			}
			button += '</div>';

			if (element.type == "event" || element.type == "events") {
				button += displayStartAndEndDate(element);
			}

			button += '<div class="col-xs-12 separation"></div>' +
				'</button>' +
				'</div>';
			$("#" + mObj.parentContainer + " #liste_map_element" + mObj.parentContainer).append(button);


			$(".item_map_list_" + kElt).click(function () {

				if (typeof element.geo != "undefined" && element.geo != null &&
					typeof element.geo.latitude != "undefined" && typeof element.geo.longitude != "undefined" &&
					element.geo.latitude != null && element.geo.longitude != null) {
					//mObj.centerOne(mObj, element.geo.latitude, element.geo.longitude);
					//il faut zoomer au max puis ouvrir le pop
					var m = mObj.markerList[kElt];
					mObj.markersCluster.zoomToShowLayer(m, function () {
						m.openPopup();
					});

				} else {
					//mylog.log("showModalItemNotLocated", element)
					$("#modalItemNotLocated" + mObj.parentContainer).modal('show');
					$("#modalItemNotLocated" + mObj.parentContainer + " .modal-body").html("<i class='fa fa-spin fa-reload'></i>");

					// var objectId = this.getObjectId(data);
					// var popup = this.getPopup(data);
					$("#modalItemNotLocated" + mObj.parentContainer + "  .modal-body").html(mapCustom.popup.default(element));
					$("#modalItemNotLocated" + mObj.parentContainer + "  #btn-open-details").click(function () {
						$("#popup" + kElt).click();
						if (typeof networkJson != "undefined" && notNull(networkJson) && notNull(networkJson.dataSrc)) {
							urlCtrl.loadByHash("'+url+'");
						}
					});

					$("#modalItemNotLocated" + mObj.parentContainer + "  #popup" + kElt).click(function () {
						$("#modalItemNotLocated" + mObj.parentContainer).modal('hide');
					});
				}
			});
		}
	}


}

