adminPanel.views.collectifLocaux = function(){

	/*var data={
		types : [ "projects"],
        table : {
            pdf : true,
            status : true
        },
        actions : {
            status : {
                list : [ 
                    "Territoire Candidat",
                    "Territoire Candidat refusé", 
                    "Territoire lauréat", 
                    "Dossier Territoire Complet",
                    "CTE Signé"
                ]
            }
        }
	};*/
	 // "label" : "Status",
  //               "costumAdmin" : true,
  //               "class" : "text-dark",
  //               "id" : "btn-open-costum",
  //               "href" : "javascript:;",
  //               "icon" : "group",
  //               "dataHref" : "default/groupadmin",
  //               
	ajaxPost('#content-view-admin', baseUrl+'/costum/pacte/groupadmin', {}, function(){},"html");

	//alert("dans la home, c'est ici");
};
adminPanel.views.contract = function(){

  var data={
    title : "Contrat",
    types : ["poi"],
    table : {
      type:{
        name : "Type",
        notLink : true
      },
      name: {
                name : "Nom",
                notLink : true
      },
      insee:{
        name : "Insee"
      }
      //pdf : true,
      //status : true
    },
    actions:{
       delete : true
    }  
      
  };
  ajaxPost('#content-view-admin', baseUrl+'/costum/default/groupadmin/', data, function(){},"html");
};
adminPanel.views.exportcontract = function(){
  window.open(baseUrl+'/api/poi/get/format/csv/key/siteDuPactePourLaTransition/type/contract/limit/0');
};
adminPanel.views.exportcandidat = function(){
  window.open(baseUrl+'/api/person/get/format/csv/key/siteDuPactePourLaTransition/tags/candidat2020/limit/0');
};
adminPanel.views.importcontrat = function(){
  ajaxPost('#content-view-admin', baseUrl+'/costum/pacte/importcontrat/', {}, function(){},"html");
};