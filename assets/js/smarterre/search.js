$("#second-search-bar").off().on("keyup",function(e){ 
            $("#input-search-map").val($("#second-search-bar").val());
            $("#second-search-xs-bar").val($("#second-search-bar").val());
            if(e.keyCode == 13){
                searchObject.text=$(this).val();
                searchObject.category = window.location.href.split("#")[1];
                myScopes.type="open";
                myScopes.open={};
               startGlobalSearch(0, indexStepGS);
           		$("#dropdown").css('display','block');
             }	
});

$("#second-search-xs-bar").off().on("keyup",function(e){ 
            $("#input-search-map").val($("#second-search-xs-bar").val());
            $("#second-search-bar").val($("#second-search-xs-bar").val());
            if(e.keyCode == 13){
                searchObject.text=$(this).val();
                searchObject.category = window.location.href.split("#")[1];
                myScopes.type="open";
                myScopes.open={};
                startGlobalSearch(0, indexStepGS);
           		$("#dropdown").css('display','block');            
            }
});

$("#second-search-bar-addon-smarterre, #second-search-xs-bar-addon").off().on("click", function(){
            $("#input-search-map").val($("#second-search-bar").val());
            searchObject.text=$("#second-search-bar").val();
            searchObject.category = window.location.href.split("#")[1];
            myScopes.type="open";
            myScopes.open={};
           	startGlobalSearch(0, indexStepGS);
           	$("#dropdown").css('display','block');
});