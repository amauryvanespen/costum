var keys = { 37 : 1, 38 : 1, 39 : 1, 40 : 1};

function preventDefault(e){
    e = e || window.event;
    if(e.preventDefault)
        e.preventDefault();
    
    e.returnValue = false;
}

function preventDefaultKeys(e){
    if(keys[e.keyCode]){
        preventDefault(e);
        return false;
    }
}

function disableScroll(){
    
    // if(window.addEventListener)
    //     window.addEventListener("DOMMouseScroll", preventDefault, false);
    
   // document.addEventListener("wheel", preventDefault, {passive: false});
   // window.onwheel = preventDefault;
    document.onkeydown = preventDefaultKeys;
}

$(document).ready(function(){
    disableScroll();
});