var p; // for polygon
var c; // for class
var file; // for file json
var n = 0; // for count polygon
var count = 1;
var co; // pour la couleur du fill;
var file_json; // pour les images & css



$(document).ready(function(){
  
//   reset_polygon();
//   reset_file();
//   color();
})

//Quand on va changer de page on va reset le polygon
function reset_polygon(){
    p = document.getElementsByClassName("cls-1");
    c = "." + $(p).attr("class");
    n = p.length;
    count = 1;
    co = $("#up").css("fill");
}


//Permet de reset le fichier
function reset_file(){

    var params = {
        thematique : window.location.href.split("#")[1]
    };

    
    // file = costum.themes.params.thematique;
    if(typeof params.thematique != "undefined"){
        
        $.ajax({
            type : "POST",
            url : baseUrl + "/costum/smarterre/getthermatique",
            data : params,
            dataType : "json",
            async : false,
            success : function(data){
                //On récupère le file
                // if(data.result == true){
                  
                  file = data;
                // }
            },
            error : function(e){
                
            }
        }); 
    }
}

//Action pour monter/descendre
function floor(action){
    hideAnimation();
    var pcount = n /2 ;

    mylog.log("n HERE",pcount);

    if(action == "down"){
        if(count >= 1 && count<=pcount-1){
            count++;
            color();
            
            return;
        }
        else{
            count = 1;
            color();
            return;
        }
    }
    else if(action == "up"){
        if(count >= 2 && count <= pcount){
            count--;
            color();
            
            return;
        }
        else{
            count = pcount;
            color();
            return;
        }
    }
}

//Fonction pour colorié le polygon
function color(){
    reset();

    $(c).each(function(){
         if(count == this.id){
            $(this).css("fill",co);
            description();
         }
    });
    $("g").each(function(){
        if("g"+count == this.id){
            $(this).children().each(function(){
                $(this).css("fill","white");
            });
        }
    });
}


//Reset the color (oui un commentaire en anglais)
function reset(){
    $(c).each(function(){
        if(count != this.id){
            $(this).css("fill","white");
        }
    });
    $("g").each(function(){
        if(this.id){
            if("g"+count != this.id){
                $(this).children().each(function(){
                    $(this).css("fill","black");
                });
            }
        }
    });
}

function description(){
    
    var str = "";
    var countid = 1;

    var speedA = 500;


    $(file.element).each(function(k,v){
      
        if(v.count == count){
          
            $(".titre").html(v.title);
            $(".titre").css("color",co);

  

            // $(".engage").attr("img",src);
            $(v.description).each(function(key,value){
                
                str += "<li><a data-toggle='collapse' href='#menuopen"+countid+"' aria-onclick='false' aria-controls='collapseMenu' onclick=stepOneLink('"+value.id+"',"+countid+") >"+value.name+"</a></li>";
                str += "<div class='collapse' id='menuopen"+countid+"'><div class='card card-body affiche"+countid+"'></div><a class='addBookmark btn btn-secondary' type='button' href='javascript:;' data-id='"+value.id+"'><i class='fa fa-plus-circle'></i> Ajouter un lien</a></div>";
                countid++;

                if(value.sous_menu.result != false){
                    
                    $(value.sous_menu).each(function(keys,contenus){
                      
                        str += "<ul><li><a data-toggle='collapse' href='#sousmenuopen"+countid+"' aria-onclick='false' aria-controls='collapseMenu' onclick=stepOneLink('"+contenus.id+"',"+countid+") >"+contenus.name+"</a></ul></li>";
                        str += "<div class='collapse' id='sousmenuopen"+countid+"'><ul><div class='card card-body affiche"+countid+"'></div><a class='addBookmark btn-btn secondary' type='button' href='javascript:;' data-id='"+contenus.id+"'><li><i class='fa fa-plus-circle'></i> Ajouter un lien</a></li></ul></div>";
                        countid++;
                    });
                }
       
         
            });


            $(".description_li").html(str);
            

            $(".titre").fadeIn(speedA);
            $(".description_li").fadeIn(speedA);

            return;
        }
    });

    //Pour rentrer dans json... JE SUIS SI DROLE HAHAHAHAHAHAHA
    $(file_json.element).each(function(k,v){
      if(v.id == count){
        
        if (typeof v.css != "undefined" && v.css!= "") {
            $(".contenu").css({
                "margin-left" : v.css.left,
                "margin-top" : v.css.top
            });
        }

        //Toujours notre petite secret ;)
        var img = $("#urlImage").val()+v.img;

        $(".engage").attr("src",img);
        $(".engage").fadeIn(speedA);

      }
    });

    //For add bookmark (ENCORE MAIS HEIN HEIN)
    $(".addBookmark").click(function(){
        
        dyFObj.openForm("bookmark",null,{category : window.location.href.split("#")[1], parentId : $(this).data("id"), parentType: "sentence"});
    });
}

//Pour préparer l'animation, on doit le caché
function hideAnimation(){
    $(".titre").hide();
    $(".engage").hide();
    $(".description_li").hide();
}

//Utilisation des flèches pour changer la thematique
$(document).keydown(function (event){
    if(event.which == 37 || event.which == 40) floor("down");
    if(event.which == 38 || event.which == 39) floor("up");
});

// Pour passer un coup de souris sur les themartique
function mouse(id){
    if(count != id){
        hideAnimation();
        count = id;
        color();
    }
}