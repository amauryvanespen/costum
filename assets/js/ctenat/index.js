//$("#filter-scopes-menu").remove();
/*
"links" : {
                            "order" : 6,
                            "inputType" : "finder",
                            "label" : "Nom des élus porteurs de la candidature ?",
                            "multiple" : true,
                            "invite" : true,
                            "type" : [ 
                                "persons"
                            ],
                            "roles" : [ 
                                "Elu"
                            ],
                            "openSearch" : true
                        },
                        ,
        "#slide" :{
            "inMenu" : true,
            "hash" : "#ctenat.slide",
            "icon" : "desktop",
            "subdomainName" : "Presentation",
            "module":"costum"
        }
                        
*/
$("#mainNav.vertical .btn-show-map").html("Carte <i class='fa fa-angle-double-right'></i>");
costum.ctenat={
	init : function(){
		mylog.log("CTENAT INDEXJS");
		if($.inArray(location.hash,["#live", "#documentation", "#forum", "#press"]) >= 0 
			&& (typeof costum.isMember == "undefined"
				|| !costum.isMember)){	
			urlCtrl.loadByHash("");
			bootbox.dialog({message:'<div class="alert-danger text-center"><strong>Vous n\'êtes pas autorisé.e à accéder à cet espace de l\'application</strong></div>'});
			//return false;
		}
		if(typeof costum.rolesCTE != "undefined"){
			costum.ctenat.floopInit();
		}
		emojiconReactions=[
	         {"emocion":"love","TextoEmocion":trad.ilove, "class" : "amo", "color": "text-red" },
	         {"emocion":"bothered","TextoEmocion":trad.bothering,"class" : "molesto", "color": "text-orange"},
	         {"emocion":"scared","TextoEmocion":trad.what, "class" : "asusta", "color": "text-purple"},
	         {"emocion":"enjoy","TextoEmocion":trad.toofunny, "class" : "divierte","color": "text-orange"},
	         {"emocion":"like","TextoEmocion":"+1", "class" : "gusta", "color": "text-red"},
	         {"emocion":"sad","TextoEmocion":trad.sad, "class" : "triste", "color": "text-azure"},
	         {"emocion":"glad","TextoEmocion":trad.cool, "class" : "alegre", "color":"text-orange"}
	    ];
	},
	floopInit : function(){
		htmlFloop="";
		appendBadge=false;
		addBadge="<span class='badge-roles-ctenat faa-pulse animated'><i class='fa fa-id-badge'></i></span>";
		$.each(costum.rolesCTE, function(e,v){
			if(v==true || ( typeof v == "object" && Object.keys(v).length >0)){
				appendBadge=true;return true;
			}
		});
		if(appendBadge){
			$(".menu-name-profil").removeClass("lbh").addClass("open-modal-ctenat margin-right-10").append(addBadge).find(".tooltips-top-menu-btn").text("Tableau de bord");
			$("body").append(
				"<div class='dashboard-cte modal' style='display:none; overflow-y:scroll;background-color: rgba(0,0,0,0.9);z-index: 100000;position:fixed;top:"+$("#mainNav").outerHeight()+"px;left:"+$("#menuApp.menuLeft").width()+"px;'>"+
				"</div>");
			ajaxPost(".dashboard-cte", baseUrl+"/costum/ctenat/dashboardaccount",{roles:costum.rolesCTE}, function(){ }, "html");
			$(".open-modal-ctenat").off().on('click',function(){
				$(".dashboard-cte").show(200);
				var $win = $(window); // or $box parent container
				var $box = $(".dashboard-cte");
				var $lbhBtn=$(".lbh, .lbhp, .close-modal");
				//var $log = $(".log");
				
				$win.on("click.Bst", function(event){		
					if ($box.has(event.target).length == 0 //checks if descendants of $box was clicked
            				&&
            				!$box.is(event.target) //checks if the $box itself was clicked
				          ){
						$(".dashboard-cte").hide(200);
					} else {
						if($lbhBtn.has(event.target).length != 0 //checks if descendants of $box was clicked
            				||
            				$lbhBtn.is(event.target)){
								$(".dashboard-cte").hide(200);
						}
						//alert("you clicked inside the box");
					}
				});
 
			});
			setTimeout(function(){ $(".badge-roles-ctenat").removeClass("faa-pulse animated")}, 8000);
		}
	},
	badges : {
		formData : function(data){
			if(typeof data.linksProjects){
				if(typeof data.links == "undefined") data.links={};
				data.links.projects=data.linksProjects;
				delete data.linksProjects;
				
				
			}
			return data;
		},
		afterSave : function(data){
			if(dyFObj.editMode){
				
				if($.inArray(data.map.category, ["cibleDD", "domainAction"]) >= 0){
					oldName=costum.badges[data.map.category][data.id].name;
					if(data.map.name!=oldName){
						if(typeof costum.badges[data.map.category][data.id].parent != "undefined"){
							parentNameB=costum.badges[data.map.category][Object.keys(costum.badges[data.map.category][data.id].parent)[0]].name;
							costum.lists[data.map.category][parentNameB].splice(costum.lists[data.map.category][parentNameB].indexOf(oldName), 1,data.map.name);
						}else{
							costum.lists[data.map.category][data.map.name]=costum.lists[data.map.category][oldName];
							delete costum.lists[data.map.category][oldName];
						}
						costum.badges[data.map.category][data.id].name=data.map.name;
					}
				}
			}
			dyFObj.commonAfterSave();
		}
	},
	indicator : {
		afterSave : function(data){
			//if(dyFObj.editMode){
				
				dyFObj.closeForm();
				urlCtrl.loadByHash(location.hash);
				// if($.inArray(data.map.category, ["cibleDD", "domainAction"]) >= 0){
				// 	oldName=costum.badges[data.map.category][data.id].name;
				// 	if(data.map.name!=oldName){
				// 		if(typeof costum.badges[data.map.category][data.id].parent != "undefined"){
				// 			parentNameB=costum.badges[data.map.category][Object.keys(costum.badges[data.map.category][data.id].parent)[0]].name;
				// 			costum.lists[data.map.category][parentNameB].splice(costum.lists[data.map.category][parentNameB].indexOf(oldName), 1,data.map.name);
				// 		}else{
				// 			costum.lists[data.map.category][data.map.name]=costum.lists[data.map.category][oldName];
				// 			delete costum.lists[data.map.category][oldName];
				// 		}
				// 		costum.badges[data.map.category][data.id].name=data.map.name;
				// 	}
				// }
			//}
			//dyFObj.commonAfterSave();
		}
	}
};


var filterObj = {
	container : "#filterContainer" ,
	urlData : baseUrl+"/" + moduleId + "/search/globalautocomplete",
	init : function(pInit = null){
		mylog.log("fObj init",pInit);
		//Init variable
		var copyFilters = jQuery.extend(true, {}, filterObj);
		copyFilters.initVar(pInit);
		return copyFilters;

	},
	initVar : function(pInit){
		mylog.log("fObj initVar",pInit);
		this.container =  ( (pInit != null && typeof pInit.container != "undefined") ? pInit.container : "#filterContainer" );
		this.urlData =  ( (pInit != null && typeof pInit.urlData != "undefined") ? pInit.urlData : "#filterContainer" );
		this.initDefaults(pInit);
		this.initViews(pInit);
		this.initActions(pInit);
	},
	initDefaults : function(pInit){
		mylog.log("fObj initDefaults",pInit);
		var str = "";
		var fObj = this;
		if(typeof pInit.defaults != "undefined"){
			mylog.log("fObj initDefaults defaults");
			if(typeof pInit.defaults.fields != "undefined"){
				$.each(pInit.defaults.fields,function(k,v){
					mylog.log("fObj initDefaults fields",k, v);
					if(typeof searchObject.filters == "undefined" )
						searchObject.filters = {};
					if(typeof searchObject.filters[k] == "undefined" )
						searchObject.filters[k] = [];
					searchObject.filters[k].push(v);
					
				});
			}
		}
	},
	initActions : function(pInit){
		mylog.log("fObj initActions",pInit);
		var str = "";
		var fObj = this;
		if(typeof pInit.filters != "undefined"){
			$.each(pInit.filters,function(k,v){
				mylog.log("fObj initActions each", k,v);
				if(typeof v.action != "undefined" && typeof fObj.actions[v.action] != "undefined"){
					fObj.actions[v.action](fObj);
				}
			});
		}
	},
	initViews : function(pInit){
		mylog.log("fObj initViews",pInit);
		var str = '<div class="pull-left text-dark labelFilterTit"><i class="fa fa-filter"></i> <span class="hidden-sm">Filtres</span></div>';
		var fObj = this;
		if(typeof pInit.filters != "undefined"){
			$.each(pInit.filters,function(k,v){
				mylog.log("fObj initViews each", k,v);
				if(typeof v.view != "undefined" && typeof fObj.views[v.view] != "undefined"){
					str += fObj.views[v.view](k,v);
				}
			});
		}
		str+='<div id="activeFilters" class="col-xs-12 padding-5"></div>';
		$(fObj.container).html(str);
		fObj.initFilters();

	},
	initFilters : function(){
		fObj=this;
		getParamsUrls=location.hash.split("?");
		if(typeof getParamsUrls[1] != "undefined" ){
			var parts = getParamsUrls[1].split("&");
	        var $_GET = {};
	         var initScopesResearch={"key":"open","ids":[]};
	        for (var i = 0; i < parts.length; i++) {
	            var temp = parts[i].split("=");
	            $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
	        }
	        if(Object.keys($_GET).length > 0){
	            $.each($_GET, function(e,v){
	                v=decodeURI(v);
	                if(e=="tags"){
	                	tags=v.split(",");
	                	$.each(tags, function(i, tag){
	                		fObj.manage.addActive(fObj,"tags",tag);
	                	});
	                }
	                if(e=="cities" || e=="zones"){
	                	if($.inArray(e,["cities","zones","cp"]) > -1) $.each(v.split(","), function(i, j){ initScopesResearch.ids.push(j) });
	                    if(initScopesResearch.key!="" && initScopesResearch.ids.length > 0)
	                    	checkMyScopeObject(initScopesResearch, $_GET, function(){
	                    		if(typeof myScopes.open != "undefined" && Object.keys(myScopes.open).length > 0){
			                    	$.each(myScopes.open, function(i, scope){
			                    		if(typeof scope.active != "undefined" && scope.active)
			                				fObj.manage.addActive(fObj, "scope",scope.name,i);
			                		});
			                	}
	                    	});
	                    
	                    if(typeof myScopes.open != "undefined" && Object.keys(myScopes.open).length > 0){
	                    	$.each(myScopes.open, function(i, scope){
	                    		if(typeof scope.active != "undefined" && scope.active)
	                				fObj.manage.addActive(fObj, "scope",scope.name,i);
	                		});
	                	}
	                }    
	         	});
	        }
	    }
	},
	search : function(){
		searchObject.nbPage=0;
		searchObject.count=true;
		startSearch(0/*, null, function(){ if($(".searchEntityContainer").length == 0) $(".projectsProgress").show(); }*/);
	},
	views : {
		tags : function(k,v){
			mylog.log("fObj views tags", k,v);
			var str = "";
			return str;
		},
		select : function(k,v){
			mylog.log("fObj views select", k,v);
			var str = "<select>";
			str += '<option value="-1" >ALL</option>';
			if(typeof v.list != "undefined"){
				$.each( v.list ,function(kL,vL){
					str +='<option value="'+vL+'" >'+vL+'</option>';
				});
			}
			str += "</select>";
			return str;
		},
		selectList : function(k,v){
			mylog.log("fObj views selectList", k,v);
			label=(typeof v.name != "undefined") ? v.name: "Ajouter un filtre";
			str='<li class="dropdown">'+
					'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-toggle="tooltip" data-placement="bottom">'+
						label+' <i class="fa fa-angle-down margin-left-5"></i>'+
					'</a>'+
					'<div class="dropdown-menu arrow_box" style="overflow-y: auto;" aria-labelledby="dropdownTypes">'+
						'<div class="list-filters">';
						if(typeof v.list != "undefined"){
							includeSubList=(typeof v.list=="object" && !Array.isArray(v.list)) ? true : false;
							$.each( v.list ,function(kL,vL){
							if(includeSubList){
									str +='<button class="mainTitle col-xs-12"  data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+kL+'" >'+kL+'</button>';
									if(Object.keys(vL).length > 0){
										$.each(vL, function(i, sub){
											str +='<button class="col-xs-12"  data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+sub+'" >'+sub+'</button>';
										});
									}
								}else{
									str +='<button class="col-xs-12" data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+vL+'" >'+vL+'</button>';
								}
							});
						}
					str+='</div></div>'+
				'</li>';
			return str;
		},
		scope : function(){
			mylog.log("fObj views scope");
			return 	'<div id="costum-scope-search"><div id="input-sec-search">'+
							'<div class="input-group shadow-input-header">'+
								'<span class="input-group-addon scope-search-trigger">'+
									'<i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>'+
								'</span>'+
								'<input type="text" class="form-control input-global-search" autocomplete="off"'+
									' id="searchOnCity" placeholder="Où ?">'+
							'</div>'+
							'<div class="dropdown-result-global-search col-xs-12 col-sm-5 col-md-5 col-lg-5 no-padding" '+
								' style="display: none;">'+
								'<div class="content-result">'+
								'</div>'+
							'</div>'+
					'</div></div>';
		}
	},
	actions : {
		tags : function(fObj){
			mylog.log("fObj actions tags");
			$("button[data-type='tags']").off().on("click",function(){
				searchObject.tags.push($(this).data("value"));
				fObj.search();
				fObj.manage.addActive(fObj,"tags",$(this).data("value"), $(this).data("key"), $(this).data("field"));
			});
		},
		filters : function(fObj){
			mylog.log("fObj actions filters");
			$("button[data-type='filters']").off().on("click",function(){
				mylog.log("fObj actions filters button[data-type='filters']");
				if(typeof $(this).data("field") != "undefined"){
					
					if(typeof searchObject.filters == "undefined" )
						searchObject.filters = {};
					
					if(typeof searchObject.filters[$(this).data("field")] == "undefined" )
						searchObject.filters[$(this).data("field")] = [];
                    searchObject.filters[$(this).data("field")].push($(this).data("value"));
                    //searchObject.searchType[] = "organizations";
					fObj.search();
					fObj.manage.addActive(fObj, "filters",$(this).data("value"), $(this).data("key"), $(this).data("field"));
				}
				
			});
		},
		scope : function(fObj){
			mylog.log("fObj actions scope");
			myScopes.open={};
			bindSearchCity("#costum-scope-search", function(){
				$(".item-globalscope-checker").off().on('click', function(){ 
					mylog.log("fObj actions scope .item-globalscope-checker");
					$("#costum-scope-search .input-global-search").val("");
					$(".dropdown-result-global-search").hide(700).html("");
					myScopes.type="open";
					mylog.log("fObj actions scope globalscope-checker",  $(this).data("scope-name"), $(this).data("scope-type"));
					newScope=scopeObject(myScopes.search[$(this).data("scope-value")]);
					$.each(newScope, function(e, v){
						if(typeof v.active == "undefined" || !v.active)
							delete newScope[e];
						else
							newKeyScope=e;
					});
					myScopes.open[newKeyScope]=newScope[newKeyScope];
					localStorage.setItem("myScopes",JSON.stringify(myScopes));
					fObj.manage.addActive(fObj, "scope",$(this).data("scope-name"),newKeyScope);
					fObj.search();
				});
			});
		}
	},
	manage:{
		addActive: function(fObj, type, value, key, field){
			mylog.log("fObj manage addActive", type, value, key);
			var dataKey = (typeof key != "undefined") ? 'data-key="'+key+'" ' : "" ;
			var dataField = (typeof field != "undefined") ? 'data-field="'+field+'" ' : "" ;
			str='<div class="filters-activate tooltips" data-position="bottom" data-title="Effacer" '+
						dataKey +
						dataField +
						'data-value="'+value+'" '+
						'data-name="'+name+'" '+
						'data-type="'+type+'">' + 
					"<i class='fa fa-times-circle'></i>"+
					"<span "+
						"class='activeFilters' "+
						dataKey +
						dataField +
						'data-value="'+value+'" '+
						'data-scope-name="'+name+'" '+
						'data-type="'+type+'">' + 
						value + 
					"</span>"+
				"</div>";
			$(fObj.container+" #activeFilters").append(str);
			coInterface.initHtmlPosition();
			$(fObj.container+" .filters-activate").off().on("click",function(){
				fObj.manage.removeActive(fObj, $(this).data("type"), $(this).data("value"), $(this).data("key"), $(this).data("field"));
				$(this).fadeOut(200);
				coInterface.initHtmlPosition();
			});
		},
		removeActive : function(fObj, type, value, key, field){
			mylog.log("fObj manage removeActive", type, value, key, field);
			if(type=="filters"){
				mylog.log("fObj manage removeActive filters", searchObject);
				if( typeof searchObject.filters != "undefined" && typeof searchObject.filters[field] != "undefined")
					searchObject.filters[field].splice(searchObject.filters[field].indexOf(value),1);
			}
			else if(type=="tags")
				searchObject.tags.splice(searchObject.tags.indexOf(value),1);
			else if(type=="scope"){
				delete myScopes.open[key];
			}
			fObj.search();
		}
	}
};

/* OLD JSON AJOUT DE TERRITOIRES
"projects" : {
            "color" : "dark",
            "icon" : "map-marker",
            "name" : "Territoire CTE",
            "add" : true,
            "createLabel" : "Candidater au CTE",
            "dynFormCostum" : {
                "beforeBuild" : {
                    "properties" : {
                        "scope" : {
                            "inputType" : "scope",
                            "label" : "Quel est votre territoire",
                            "order" : 5,
                            "subParams" : {
                                "cities" : {
                                    "type" : [ 
                                        "zones"
                                    ]
                                }
                            }
                        },
                        "links" : {
                            "order" : 6,
                            "inputType" : "finder",
                            "label" : "Nom des élus porteurs de la candidature ?",
                            "multiple" : true,
                            "invite" : true,
                            "type" : [ 
                                "persons"
                            ],
                            "roles" : [ 
                                "Elu"
                            ],
                            "openSearch" : true
                        },
                        "why" : {
                            "order" : 7,
                            "inputType" : "textarea",
                            "markdown" : true,
                            "label" : "Décrivez le contexte de mise en oeuvre de votre CTE et ses principales motivations",
                            "rules" : {
                                "maxlength" : 2000
                            }
                        },
                        "filRouge":{
                            "order" : 8,
                            "inputType" : "textarea",
                            "markdown" : true,
                            "label" : "Déclarer le fil rouge du CTE",
                            "rules" : {
                                "maxlength" : 2000
                            }
                        },
                        "siren" : {
                            "order" : 9,
                            "inputType" : "text",
                            "label" : "Numéro SIREN?"
                        },
                        "documentation" : {
                            "inputType" : "uploader",
                            "label" : "Fichier à joindre à la candidature : lettre de candidature signée par l’ensemble des présidents des EPCI du territoire et de la structure porteuse éventuelle, adressée à Emmanuelle WARGON, secrétaire d’Etat auprès du ministre d’Etat, ministre de la transition écologique et solidaire",
                            "showUploadBtn" : false,
                            "docType" : "file",
                            "itemLimit" : 1,
                            "contentKey" : "file",
                            "order" : 10,
                            "domElement" : "documentationFile",
                            "placeholder" : "Le pdf de la mesure",
                            "afterUploadComplete" : null,
                            "template" : "qq-template-manual-trigger",
                            "filetypes" : [ 
                                "pdf"
                            ]
                        },
                        "nbHabitant" : {
                            "order" : 11,
                            "inputType" : "text",
                            "label" : "Nombre d'habitants concernés?"
                        },
                        "dispositif" : {
                            "order" : 12,
                            "inputType" : "textarea",
                            "markdown" : true,
                            "label" : "Quels sont les dispositifs actuellement mis en place sur tout ou partie du territoire (contractuels et/ou en lien avec la transition écologique) ? TEPCV, TEPOS, TIGA, Territoire d’industrie, Action coeur de ville, French Impact…",
                            "rules" : {
                                "maxlength" : 2000
                            }
                        },
                        "planPCAET" : {
                            "order" : 13,
                            "inputType" : "textarea",
                            "markdown" : true,
                            "label" : "Avez élaboré un Plan climat-air-énergie territorial (PCAET) sur votre territoire ?",
                            "rules" : {
                                "maxlength" : 2000
                            }
                        },
                        "singulartiy" : {
                            "order" : 14,
                            "inputType" : "textarea",
                            "markdown" : true,
                            "label" : "Qu'est ce qui constitue la singularité de votre territoire ? Quel est le contexte économique et quels sont ses enjeux en termes de transition écologique ?",
                            "rules" : {
                                "maxlength" : 2000
                            }
                        },
                        "caracteristique" : {
                            "order" : 15,
                            "inputType" : "textarea",
                            "markdown" : true,
                            "label" : "Quelles sont les caractéristiques de votre territoire sur lesquelles vous agissez/vous souhaiteriez agir dans le cadre du CTE ? (exemples : problèmes majeurs/risques auxquels vous êtes confrontés, thèmes sur lesquels vous vous êtes engagés à agir)",
                            "rules" : {
                                "maxlength" : 2000
                            }
                        },
                        "actions" : {
                            "order" : 16,
                            "inputType" : "textarea",
                            "markdown" : true,
                            "label" : "Quelles sont les actions envisagées dans votre projet de CTE ? Décrivez au moins 3 pistes d’actions.",
                            "rules" : {
                                "maxlength" : 2000
                            }
                        },
                        "actors" : {
                            "order" : 17,
                            "inputType" : "textarea",
                            "markdown" : true,
                            "label" : "Quelles sont les principales entreprises et associations présentes sur votre territoire ?",
                            "rules" : {
                                "maxlength" : 2000
                            }
                        },
                        "economy" : {
                            "order" : 18,
                            "inputType" : "textarea",
                            "markdown" : true,
                            "label" : "Quels acteurs socio-économiques prévoyez-vous de mobiliser pour le portage d’actions du CTE ?",
                            "rules" : {
                                "maxlength" : 2000
                            }
                        },
                        "inCharge" : {
                            "order" : 19,
                            "inputType" : "textarea",
                            "markdown" : true,
                            "label" : "Qui serait en charge du projet au sein de la collectivité (chargé de mission, équipe dédiée, élu...) ?",
                            "rules" : {
                                "maxlength" : 2000
                            }
                        },
                        "autreElu" : {
                            "order" : 20,
                            "inputType" : "textarea",
                            "markdown" : true,
                            "label" : "Autres élus engagés politiquement dans la réussite du projet (député, sénateur, maires) ?",
                            "rules" : {
                                "maxlength" : 2000
                            }
                        },
                        "contact" : {
                            "order" : 21,
                            "inputType" : "textarea",
                            "markdown" : true,
                            "label" : "Nom du référent local, Adresse postale, numéro de téléphone et adresse mail ?",
                            "rules" : {
                                "maxlength" : 2000
                            }
                        },
                        "preferences[isOpenEdition]" : {
                            "inputType" : "hidden",
                            "value" : false
                        },
                        "category" : {
                            "inputType" : "hidden",
                            "value" : "cteR"
                        }
                        
                    }
                },
                "onload" : {
                    "actions" : {
                        "setTitle" : "Informations clé du Territoire porteur du CTE",
                        "html" : {
                            "infocustom" : "<br/>Dans un premier temps, veuillez inviter au moins une personne pour pouvoir former un groupe local valable sur un territoire"
                        },
                        "presetValue" : {
                            "public" : false,
                            "preferences[isOpenEdition]":false
                        },
                        "hide" : {
                            "infocustom":1,
                            "publiccheckboxSimple" : 1,
                            "urltext" : 1,
                            "tagstags" : 1,
                            "shortDescriptiontextarea" : 1,
                            "publiccheckboxSimple" : 1
                        },
                        "required" : {
                            "links" : 0
                        }
                    }
                }
            }
        }
    },
*/
function autoCompleteSearchGS(search, indexMin, indexMax, input, callB){
	mylog.log("autoCompleteSearchGS",search);

	var data = {"name" : search, "locality" : "", "searchType" : searchTypeGS, "searchBy" : "ALL",
	"indexMin" : indexMin, "indexMax" : indexMax, "category" : searchObject.category  };

	if(!notNull(input)){
		data.indexStep=10;
		data.count=true;
		data.sourceKey=costum["slug"];
		data.countType = [  "organizations", "projects", "events" ];
		data.searchType = [ "organizations", "projects", "events" ];
	}
	if(typeof costum != "undefined" && notNull(costum) && typeof costum.filters != "undefined" && typeof costum.filters.searchTypeGS != "undefined" 
		&& (!notNull(input) || $.inArray(input, ["#filter-scopes-menu", "#scopes-news-form"]) < 0)){
		data.countType = costum.filters.searchTypeGS;
		data.searchType = costum.filters.searchTypeGS;
	}
	var domTarget = (notNull(input)) ? input+" .dropdown-result-global-search" : ".dropdown-result-global-search";
	var dropDownVisibleDom=(notNull(input)) ? input+" .dropdown-result-global-search" : ".dropdown-result-global-search";
	 if($(domTarget+" .content-result").length > 0 && domTarget != ".dropdown-result-global-search")
        domTarget+=" .content-result";
             
	showDropDownGS(true, dropDownVisibleDom);
	if(indexMin > 0)
		$("#btnShowMoreResultGS").html("<i class='fa fa-spin fa-circle-o-notch'></i> "+trad.currentlyresearching+" ...");
	else{
		$(domTarget).html("<h5 class='text-dark center padding-15'><i class='fa fa-spin fa-circle-o-notch'></i> "+trad.currentlyresearching+" ...</h5>");  
	}

//	showIsLoading(true);

	if(search.indexOf("co.") === 0 ){
		searchT = search.split(".");
		if( searchT[1] && typeof co[ searchT[1] ] == "function" ){
			co[ searchT[1] ](search);
			return;
		} else {
			co.mands();
		}
	}

	$.ajax({
		type: "POST",
		url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
		data: data,
		dataType: "json",
		error: function (data){
			mylog.log("error"); mylog.dir(data);          
		},
		success: function(data){
			spinSearchAddon();
			if(!data){ toastr.error(data.content); }
			else{
				mylog.log("DATA GS");
				mylog.dir(data);

				var countData = 0;
				if(typeof data.count != "undefined")
					$.each(data.count, function(e, v){countData+=v;});
				else
					$.each(data.results, function(i, v) { if(v.length!=0){ countData++; } });

				totalDataGS += countData;

				str = "";
				var city, postalCode = "";

				if(totalDataGS == 0)      totalDataGSMSG = "<i class='fa fa-ban'></i> "+trad.noresult;
				else if(totalDataGS == 1) totalDataGSMSG = totalDataGS + " "+trad.result;   
				else if(totalDataGS > 1)  totalDataGSMSG = totalDataGS + " "+trad.results;   

				if(totalDataGS > 0){
					labelSearch=(Object.keys(data.results).length == totalDataGS) ? trad.extendedsearch : "Voir tous les résultats";
					str += '<div class="text-left col-xs-12" id="footerDropdownGS" style="">';
						str += "<label class='text-dark margin-top-5'><i class='fa fa-angle-down'></i> " + totalDataGSMSG + "</label>";
						str += '<a href="#search?text='+encodeURI(search)+'" class="btn btn-default btn-sm pull-right lbh" id="btnShowMoreResultGS">'+
									'<i class="fa fa-angle-right"></i> <i class="fa fa-search"></i> '+labelSearch+
								'</a>';
					str += '</div>';
					str += "<hr style='margin: 0px; float:left; width:100%;'/>";
				}
              //parcours la liste des résultats de la recherche
				$.each(data.results, function(i, o) {
					mylog.log("globalsearch res : ", o);
					var typeIco = i;
					var ico = "fa-"+typeObj["default"].icon;
					var color = mapColorIconTop["default"];

					mapElementsGS.push(o);
					if(typeof( typeObj[o.type] ) == "undefined")
						itemType="poi";
					typeIco = o.type;
					//if(directory.dirLog) mylog.warn("itemType",itemType,"typeIco",typeIco);
					if(typeof o.typeOrga != "undefined")
						typeIco = o.typeOrga;

					var obj = (dyFInputs.get(typeIco)) ? dyFInputs.get(typeIco) : typeObj["default"] ;
					ico =  "fa-"+obj.icon;
					color = obj.color;
					
					htmlIco ="<i class='fa "+ ico +" fa-2x bg-"+color+"'></i>";
					if("undefined" != typeof o.profilThumbImageUrl && o.profilThumbImageUrl != ""){
						var htmlIco= "<img width='80' height='80' alt='' class='img-circle bg-"+color+"' src='"+baseUrl+o.profilThumbImageUrl+"'/>"
					}

					city="";

					var postalCode = o.postalCode
					if (o.address != null) {
						city = o.address.addressLocality;
						postalCode = o.postalCode ? o.postalCode : o.address.postalCode ? o.address.postalCode : "";
					}

					var id = getObjectId(o);
					var insee = o.insee ? o.insee : "";
					type = o.type;
					if(type=="citoyens") type = "person";
					//var url = "javascript:"; //baseUrl+'/'+moduleId+ "/default/simple#" + o.type + ".detail.id." + id;
					var url = (notEmpty(o.type) && notEmpty(id)) ? 
					        '#page.type.'+o.type+'.id.' + id : "";

					//var onclick = 'urlCtrl.loadByHash("#' + type + '.detail.id.' + id + '");';
					var onclickCp = "";
					var target = " target='_blank'";
					var dataId = "";
					if(type == "city"){
						dataId = o.name; //.replace("'", "\'");
					}


					var tags = "";
					if(typeof o.tags != "undefined" && o.tags != null){
						$.each(o.tags, function(key, value){
							if(value != "")
								tags +=   "<a href='javascript:' class='badge bg-red btn-tag'>#" + value + "</a>";
						});
					}

					var name = typeof o.name != "undefined" ? o.name : "";
					if(typeof o.title != "undefined")
						name =  o.title;
					var postalCode = (	typeof o.address != "undefined" &&
										o.address != null &&
										typeof o.address.postalCode != "undefined") ? o.address.postalCode : "";

					if(postalCode == "") postalCode = typeof o.postalCode != "undefined" ? o.postalCode : "";
					var cityName = (typeof o.address != "undefined" &&
									o.address != null &&
									typeof o.address.addressLocality != "undefined") ? o.address.addressLocality : "";
                  	var countryCode=(typeof o.address != "undefined" && notNull(o.address) && typeof o.address.addressCountry != "undefined") ? "("+o.address.addressCountry+")" : ""; 
					var fullLocality = postalCode + " " + cityName+" "+countryCode;
					if(fullLocality == " Addresse non renseignée" || fullLocality == "" || fullLocality == " ") 
						fullLocality = "<i class='fa fa-ban'></i>";
					mylog.log("fullLocality", fullLocality);

					var description = (	typeof o.shortDescription != "undefined" &&
										o.shortDescription != null) ? o.shortDescription : "";
					if(description == "") description = (	typeof o.description != "undefined" &&
															o.description != null) ? o.description : "";
           
					var startDate = (typeof o.startDate != "undefined") ? "Du "+dateToStr(o.startDate, "fr", true, true) : null;
					var endDate   = (typeof o.endDate   != "undefined") ? "Au "+dateToStr(o.endDate, "fr", true, true)   : null;

					var followers = (typeof o.links != "undefined" && o.links != null && typeof o.links.followers != "undefined") ?
					                o.links.followers : 0;
					var nbFollower = 0;
					if(followers !== 0)                 
						$.each(followers, function(key, value){
						nbFollower++;
					});

					target = "";
					if(type=="proposals")
						url="javascript:;";
					classA=(type=="proposals") ? "openCoopPanelHtml" : "lbh";
					attrA="";
					if(type=="proposals"){ 
					 	attrA="data-coop-type='proposals' data-coop-id='"+id+"' data-coop-idparentroom='";
					 	if(typeof o.idParentRoom != "undefined") attrA+=o.idParentRoom;
					 	attrA+="' data-coop-parentid='"+data.parentId+"' data-coop-parenttype='"+data.parentType+"'";
					}
					mylog.log("type", type);
					if(type != "city" && type != "zone" ){ 
						str += "<a href='"+url+"' class='"+classA+" col-md-12 col-sm-12 col-xs-12 no-padding searchEntity' "+attrA+">";
						str += "<div class='col-md-2 col-sm-2 col-xs-2 no-padding entityCenter text-center'>";
						str +=   htmlIco;
						str += "</div>";
						str += "<div class='col-md-10 col-sm-10 col-xs-10 entityRight'>";

						str += "<div class='entityName text-dark'>" + name + "</div>";

						str += '<div data-id="' + dataId + '"' + "  class='entityLocality'>"+
						"<i class='fa fa-home'></i> " + fullLocality;

						if(nbFollower >= 1)
						str +=    " <span class='pull-right'><i class='fa fa-chain margin-left-10'></i> " + nbFollower + " follower</span>";

						str += '</div>';

						str += "</div>";

						str += "</a>";
					}else{
						o.input = input;
	         	 		mylog.log("Here",o, typeof o.postalCode);

	         	 		
						if(type == "city"){
							var valuesScopes = {
								city : o._id.$id,
								cityName : o.name,
								postalCode : (typeof o.postalCode == "undefined" ? "" : o.postalCode),
								postalCodes : (typeof o.postalCodes == "undefined" ? [] : o.postalCodes),
								country : o.country,
								allCP : o.allCP,
								uniqueCp : o.uniqueCp,
								level1 : o.level1,
								level1Name : o.level1Name
							}

							if( notEmpty( o.nameCity ) ){
								valuesScopes.name = o.nameCity ;
							}

							if( notEmpty( o.uniqueCp ) ){
								valuesScopes.uniqueCp = o.uniqueCp;
							}
							if( notEmpty( o.level5 ) && valuesScopes.id != o.level5){
								valuesScopes.level5 = o.level5 ;
								valuesScopes.level5Name = o.level5Name ;
							}
							if( notEmpty( o.level4 ) && valuesScopes.id != o.level4){
								valuesScopes.level4 = o.level4 ;
								valuesScopes.level4Name = o.level4Name ;
							}
							if( notEmpty( o.level3 ) && valuesScopes.id != o.level3 ){
								valuesScopes.level3 = o.level3 ;
								valuesScopes.level3Name = o.level3Name ;
							}
							if( notEmpty( o.level2 ) && valuesScopes.id != o.level2){
								valuesScopes.level2 = o.level2 ;
								valuesScopes.level2Name = o.level2Name ;
							}

							valuesScopes.type = o.type;
							valuesScopes.key = valuesScopes.city+valuesScopes.type+valuesScopes.postalCode ;
							o.key = valuesScopes.key;
							mylog.log("valuesScopes city", valuesScopes);
		         	 		myScopes.search[valuesScopes.key] = valuesScopes;
							str += directory.cityPanelHtml(o);
						}
						else if(type == "zone"){


							valuesScopes = {
								id : o._id.$id,
								name : o.name,
								country : o.countryCode,
								level : o.level
							}
							mylog.log("valuesScopes",valuesScopes);

							if(o.level.indexOf("1") >= 0){
								typeSearchCity="level1";
								levelSearchCity="1";
								valuesScopes.numLevel = 1;
							}else if(o.level.indexOf("2") >= 0){
								typeSearchCity="level2";
								levelSearchCity="2";
								valuesScopes.numLevel = 2;
							}else if(o.level.indexOf("3") >= 0){
								typeSearchCity="level3";
								levelSearchCity="3";
								valuesScopes.numLevel = 3;
							}else if(o.level.indexOf("4") >= 0){
								typeSearchCity="level4";
								levelSearchCity="4";
								valuesScopes.numLevel = 4;
							}else if(o.level.indexOf("5") >= 0){
								typeSearchCity="level5";
								levelSearchCity="5";
								valuesScopes.numLevel = 5;
							}
							if(notNull(typeSearchCity))
								valuesScopes.type = typeSearchCity;				

							mylog.log("valuesScopes test", (valuesScopes.id != o.level1), valuesScopes.id, o.level1);

							if( notEmpty( o.level1 ) && valuesScopes.id != o.level1){
								mylog.log("valuesScopes test", (valuesScopes.id != o.level1), valuesScopes.id, o.level1);
								valuesScopes.level1 = o.level1 ;
								valuesScopes.level1Name = o.level1Name ;
							}

							var subTitle = "";
							if( notEmpty( o.level5 ) && valuesScopes.id != o.level5){
								valuesScopes.level5 = o.level5 ;
								valuesScopes.level5Name = o.level5Name ;
								subTitle +=  (subTitle == "" ? "" : ", ") +  o.level4Name ;
							}
							if( notEmpty( o.level4 ) && valuesScopes.id != o.level4){
								valuesScopes.level4 = o.level4 ;
								valuesScopes.level4Name = o.level4Name ;
								subTitle +=  (subTitle == "" ? "" : ", ") +  o.level4Name ;
							}
							if( notEmpty( o.level3 ) && valuesScopes.id != o.level3 ){
								valuesScopes.level3 = o.level3 ;
								valuesScopes.level3Name = o.level3Name ;
								subTitle +=  (subTitle == "" ? "" : ", ") +  o.level3Name ;
							}
							if( notEmpty( o.level2 ) && valuesScopes.id != o.level2){
								valuesScopes.level2 = o.level2 ;
								valuesScopes.level2Name = o.level2Name ;
								subTitle +=  (subTitle == "" ? "" : ", ") +  o.level2Name ;
							}
							//objToPush.id+objToPush.type+objToPush.postalCode
							valuesScopes.key = valuesScopes.id+valuesScopes.type ;
							mylog.log("valuesScopes.key", valuesScopes.key, valuesScopes);
							myScopes.search[valuesScopes.key] = valuesScopes;

							mylog.log("myScopes.search", myScopes.search);
							o.key = valuesScopes.key;
							str += directory.zonePanelHtml(o);
						}
					}
				}); //end each

              //ajout du footer
              /*str+="<div class='text-center col-md-12 col-sm-12 col-xs-12 padding-10'>"+
                      '<label class="text-dark italic"><i class="fa fa-ban"></i> '+trad.youdontfindcityyouwantfor+' "'+search+'"</label><br/>'+
                      '<span class="info letter-blue"><i class="fa fa-info-circle"></i> '+trad.explainnofoundcity+'</span><br/>'+
                      '<button class="btn btn-blue bg-blue text-white main-btn-create" '+
                        'data-target="#dash-create-modal" data-toggle="modal">'+
                          '<i class="fa fa-plus-circle"></i> '+trad.createpage+
                      '</button>'+
                    "</div>";   */
              extendMsg=trad.extendedsearch;
              extendUrl="#search?text="+encodeURI(search);
              //if(typeof costum != "undefined" && notNull(costum) && typeof costum.searchOpenMenu != "undefined"){
              	//extendMsg=costum.searchOpenMenu.msg;
              	//extendUrl=costum.searchOpenMenu.url;
              //}

              str += '<div class="text-center padding-bottom-10" id="footerDropdownGS">';
              str += "<label class='text-dark'>" + totalDataGSMSG + "</label><br/>";
              str += '<a href="'+extendUrl+'" class="btn btn-default btn-sm lbh margin-bottom-10" id="btnShowMoreResultGS">'+
                        '<i class="fa fa-angle-right"></i> <i class="fa fa-search"></i> Voir tout'+
                      '</a>';
              str += '</div>';

           	if(countData==0 && searchTypeGS == "cities"){
           		str="<div class='text-center col-md-12 col-sm-12 col-xs-12 padding-10'>"+
                      '<label class="text-dark italic"><i class="fa fa-ban"></i> Aucun résultat pour <italic>"'+search+'"</italic>+</label><br/>'+
                     "</div>";
               /* str="<div class='text-center col-md-12 col-sm-12 col-xs-12 padding-10'>"+
                      '<label class="text-dark italic"><i class="fa fa-ban"></i> '+trad.nocityfoundfor+' "'+search+'"</label><br/>'+
                      '<span class="info letter-blue"><i class="fa fa-info-circle"></i> '+trad.explainnofoundcity+'</span><br/>'+
                      '<button class="btn btn-blue bg-blue text-white main-btn-create" '+
                        'data-target="#dash-create-modal" data-toggle="modal">'+
                          '<i class="fa fa-plus-circle"></i> '+trad.createpage+
                      '</button>'+
                    "</div>";*/
              }


              //on ajoute le texte dans le html
                  	$(domTarget).html(str);
              //on scroll pour coller le haut de l'arbre au menuTop
              $(domTarget).scrollTop(0);
              
              //on affiche la dropdown
              showDropDownGS(true, dropDownVisibleDom);
              bindScopesInputEvent();
              if(notEmpty(callB)){
              	callB();
              }
              /*$(".start-new-communexion").click(function(){
                  $("#main-search-bar, #second-search-bar, #input-search-map").val("");
                  // setGlobalScope( $(this).data("scope-value"), $(this).data("scope-name"), $(this).data("scope-type"), "city",
                  //                  $(this).data("insee-communexion"), $(this).data("name-communexion"), $(this).data("cp-communexion"),
                  //                   $(this).data("region-communexion"), $(this).data("dep-communexion"), $(this).data("country-communexion") ) ;
                  itenGlobalScopeChecker($(this));
                  urlCtrl.loadByHash("#search")
              });*/

              coInterface.bindLBHLinks();

            //signal que le chargement est terminé
            mylog.log("loadingDataGS false");
            loadingDataGS = false;

          }

          //si le nombre de résultat obtenu est inférieur au indexStep => tous les éléments ont été chargé et affiché
          if(indexMax - countData > indexMin){
            $("#btnShowMoreResultGS").remove(); 
            scrollEndGS = true;
          }else{
            scrollEndGS = false;
          }

          if(isMapEnd){
            //affiche les éléments sur la carte
            showDropDownGS(false);
            Sig.showMapElements(Sig.map, mapElementsGS, "globe", "Recherche globale");
          }

          //$("#footerDropdownGS").append("<br><a class='btn btn-default' href='javascript:' onclick='urlCtrl.loadByHash("+'"#default.directory"'+")'><i class='fa fa-plus'></i></a>");
        }
    });

                    
  }

  directory.searchTypeHtml = function(){
      spanType="";
      lengthType=searchObject.types.length;
      $.each( searchObject.types, function(key, val){
            mylog.log("searchTypeHtml typeHeader", val, typeObj[val]);
            var params = (searchInterface.isForced("types, category") && typeObj.isDefined(searchObject.forced.category)) ?
              typeObj.get(searchObject.forced.category) : typeObj.get(val);
            spanType +=// "<a href='#search?types="+typeHeader+"' class='text-"+params.color+" lbh'>"+
                        "<span class='text-"+params.color+"'>"+
                          "<i class='fa fa-"+params.icon+" hidden-sm hidden-md hidden-lg padding-5'></i> <span class='hidden-xs'>"+params.name+"</span>"+
                        "</span>";
            if(val=="projects"){
            	  spanType +=// "<a href='#search?types="+typeHeader+"' class='text-"+params.color+" lbh'>"+
                        "<span class='' style='color: #976BB6 !important;'>"+
                          "<i class='fa fa-lightbulb-o hidden-sm hidden-md hidden-lg padding-5'></i> <span class='hidden-xs'>Fiche action</span>"+
                        "</span>";
            }
                      //"</a> ";
      });
      return spanType;
    }