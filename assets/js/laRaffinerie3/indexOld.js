var laRObj = {
	context : {
		id : "",
		type : ""
	},
	parent : {
		id : "",
		type : ""
	},
	init : function(params){
		mylog.log("laRObj.init", params);
		setTitle("La Raffinerie");
		laRObj.parent.type = params.typeP;
		laRObj.parent.id = params.idP;
		if(typeof params.idC != "undefined"){
			laRObj.context.id = params.idC;
			laRObj.context.type = params.typeC;
		}else{
			laRObj.context.id = laRObj.parent.id;
			laRObj.context.type = laRObj.parent.type;
		}

		this.bindRaffinerie();
		this.hideProjectMask();
		laRObj.getElt();
	},
	bindRaffinerie : function () {
		mylog.log("laRObj.bindRaffinerie");
		calendar.init("#profil-content-calendar");
		$('.toggleProjects').click(function(){
			if( $(this).hasClass('fa-plus') ){
				$(this).removeClass('fa-plus').addClass('fa-minus');
			}
			else{
				$(this).removeClass('fa-minus').addClass('fa-plus');
			}
			$(this).parents('li').next('.projectNavThirdLvl').toggle();
		});

		$('.toggleAllProjects').click(function(){
			if( $(this).hasClass('open') ) {
				$('.projectNavThirdLvl').hide();
				$(this).removeClass('open');
				$('.toggleProjects').removeClass('fa-minus').addClass('fa-plus');
			}
			else{
				$('.projectNavThirdLvl').show();
				$(this).addClass('open');
				$('.toggleProjects').removeClass('fa-plus').addClass('fa-minus');
			}
		});
		$('#projectDescription button.customBtnTrigger').click(function(){
			if($(this).hasClass('open')){
				$('#projectDescription #descriptionAbout').css({'max-height': '148px', 'overflow': 'hidden'});
				$(this).removeClass('open').html('Lire la suite <i class="fa fa-angle-down"></i>');
			}
			else{
				$('#projectDescription #descriptionAbout').css({'max-height': 'none', 'overflow': 'auto'});
				$(this).addClass('open').html('Réduire <i class="fa fa-angle-up"></i>');
			}
		});

		$('.projectNavTriggerMobile').click(function(e){
			e.preventDefault();
			if(!$('.projectNav').hasClass('isAnimated')){

				$('.projectNav').addClass('isAnimated');
				if($(this).hasClass('active')){
					laRObj.projectCloseMobileNav();
				}
				else{
					laRObj.projectOpenMobileNav();
				}
			}
		});
		$("#outils2").click(function(){
			uiCoop.startUI();
			$("#modalCoop").modal("show");
		});
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var target = $(e.target).attr("href") // activated tab
			mylog.log("HERE journal", target, initNews);
			if(target == "#agenda"){
				if(initEvent === false){
					initEvent = true;
					laRObj.loadCalendar('projects', idElt);
					$(".fc-month-button").trigger("click");
				}
			}

			if(target == "#documents"){
				if(initDoc === false){
					initDoc = true;
					laRObj.loadGallery("", "", "");
				}
			}

			if(target == "#journal"){
				if(initNews === false){
					initNews = true;
					laRObj.loadMyStream('projects', idElt);
				}
			}
		});

		$('#follows').click(function(){
			var id = $(this).data("id");
			var isco = $(this).data("isco");
			if(isco == false){
				links.connectAjax('projects',id,userId,'citoyens','contributors', null, function(){
					$('#follows').html("Désinscrire");
					$('#follows').data("isco", true);
				});
			}else{
				links.disconnectAjax('projects',id,userId,'citoyens','contributors', null, function(){
					$('#follows').html("S'inscrire");
					$('#follows').data("isco", false);
				});
			}
		});
		$('.customTab').hide();
		$('.customTabTrigger').click(function(e){
			e.preventDefault();

			var $this = $(this);
			var href = $this.attr('href');
			var $tab = $(href);

			if( $tab.hasClass('active') ){
				$tab.hide().removeClass('active');
				$this.removeClass('active');
			}
			else{
				$('.customTab').hide().removeClass('active');
				$('.customTabTrigger').removeClass('active');
				$tab.show().addClass('active');
				$this.addClass('active');
			}
		});
		$('.closeCustomTab').click(function(e){
			e.preventDefault();
			var $tab = $(this).parents('.customTab');
			$tab.hide().removeClass('active');
		});

		$('.btn-show-mainmenu i').removeClass('fa-bars').addClass('fa-cog');

		$(".projectNav").on('click','.linkMenu',function(e){
			//e.preventDefault();
			$this = $(this);
			var key  = $this.data("key");
			var col  = $this.data("col");
			
			// var countMembers = $this.data('countmembers');
			// var countEvents = $this.data('countevents');
			// var countProjects = $this.data('countprojects');
			var toggleBtn = $this.prev('i');

			laRObj.context.id = key ;
			if(toggleBtn.hasClass('fa-plus'))
				toggleBtn.click();

			$(".linkMenu").removeClass('active');
			$this.addClass('active');


		 	laRObj.getElt();
		});
	},
	getElt: function(){
		mylog.log("laRObj.getElt");
		// var color = eltByColor[laRObj.context.id];
		// var colorHover = color.replace("1)",".8)" );
		// colorHover = colorHover.replace(".82)",".6)");

		// $('#tempStyle').remove();

		// $('<style id="tempStyle">.projectBorderColorHover:hover{border-color:'+colorHover+' !important;}.projectBgColorHover:not(.active):hover{background-color:'+colorHover+' !important;}.projectBgColorActive.active{background-color:'+color+';}.projectNav .linkMenu.active::after{background-color:'+color+'}</style>').appendTo('head');

		// $('.projectColorBorder').css('border-color', color);
		// $('.projectBgColor').css('background-color', color);

		$('.customTab').hide().removeClass('active');
		$('.customTabTrigger').removeClass('active');
		$.ajax({
			type: "POST",
			url: baseUrl+'/co2/element/get/id/'+laRObj.context.id+'/type/'+laRObj.context.type,
			data: {},
			dataType: "json",
			beforeSend: laRObj.showProjectMask()
		}).done(function(data){
			laRObj.feedProject(data);
		//mylog.log( "Data Ajax : " + data.toSource() );
			//loadMyStream(col, key);
		}).fail(function(error) {
		//mylog.log( "error : " + error.toSource() );
			$('.ret').html(error);
		}).always(function(){
			laRObj.hideProjectMask();
			if(laRObj.isMobile()) 
				laRObj.projectCloseMobileNav();
		});
	},
	showProjectMask : function (){
		mylog.log("laRObj.showProjectMask");
		$('.projectMask').show();
	},
	hideProjectMask : function (){
		mylog.log("laRObj.hideProjectMask");
		$('.projectMask').hide();
	},
	projectCloseMobileNav : function (){
		mylog.log("laRObj.projectCloseMobileNav");
		$('.projectNav').animate({left: '-100%'}, 200, function(){
			$('.projectNavTriggerMobile').removeClass('active');
			$('.projectNav').removeClass('isAnimated');
		});
	},
	projectOpenMobileNav : function (){
		mylog.log("laRObj.projectOpenMobileNav");
		$('.projectNav').animate({left: '0'}, 300, function(){
			$('.projectNavTriggerMobile').addClass('active');
			$('.projectNav').removeClass('isAnimated');
		});
	},
	isMobile : function () {
		mylog.log("laRObj.isMobile");
		return ( $(window).outerWidth() < 997 ) ? true : false ;
	},
	feedProject : function (el){
	 	mylog.log('laRObj.feedProject : ', el);
		
		// name
		elt = el.map;
		var id = el.map._id.$id ;
		idElt = id;
		//$(".projectName, .projectNameParent").html(el.map.name);

		$("#follows").data( "id", id);

		// shortdecription
	 	var shortDescription = el.map.shortDescription ? el.map.shortDescription : "";
	 	if(el.map.shortDescription){
	 		$(".projectShortDescription").show().html(el.map.shortDescription);
	 	}
	 	else{
	 		$(".projectShortDescription").hide();
	 	}

	 	// description
	 	if(el.map.description){
	 		$(".customTabTriggerDescr").show();
	 		if(el.map.description.length > 0){
				descHtml = dataHelper.markdownToHtml(el.map.description) ;
			}
	 		$("#descriptionAbout").html(descHtml);
	 	}
	 	else{
	 		$(".customTabTriggerDescr").hide();
	 	}

	 	// banner image
	 	var bannerImgUrl = el.map.profilBannerUrl ? baseUrl + el.map.profilBannerUrl : defaultBannerUrl;
	 	$(".projectBanner").css('background-image', 'url(' + bannerImgUrl + ')');
	 	//alert(getBaseUrl() + el.map.profilBannerUrl);

	 	// thumb image
	 	var thumbImgUrl = el.map.profilMediumImageUrl ? baseUrl + el.map.profilMediumImageUrl : defaultBannerUrl;
	 	$(".projectThumb").css('background-image', 'url(' + thumbImgUrl + ')');

	 	// Members count
	 	// Members count
		var countMembers = 0 ;
		var countProjects = 0 ;
		var countEvents = 0 ;
		//$('#projectChildren .row').html('');

		if(typeof el.map.links != "undefined"){
			if(typeof el.map.links.members != "undefined"){
				countMembers = Object.keys(el.map.links.members).length ;
			}

			if(typeof el.map.links.events != "undefined"){
				countEvents = Object.keys(el.map.links.events).length ;
			}
			
			if(typeof el.map.links.projects != "undefined"){
				countProjects = Object.keys(el.map.links.projects).length ;
				$('.customTabTriggerProj').show();
				// $(el.map.links.projects).each(function(){
				// 	for (key in this) {
				// 	mylog.log("ProjectKey : " + key );
				// 	var $link = $('.linkMenu[data-key="' + key + '"]');
				// 		var projectName = $link.text();
				// 		var projectCollection = $link.attr('data-col');
				// 		var projectImage = $link.attr('data-img');
				// 		var htmlProjects = "<div class='col col-xs-6 col-md-3'>" +
				// 		"<a href='#" + el.map.slug + "' class='linkMenu' data-col='" + projectCollection + "'" + 
				// 		"data-key='" + key + "' style='background-image: url(" + projectImage + ");'>" + 
				// 		"<span class='projectBorderColorHover'><h4>" + projectName+ "</h4></span></a></div>";

				// 		$(htmlProjects).appendTo('#projectChildren .row');
				// 	}
				// });

				// $.each(el.map.links.projects, function (key,value){
				// 	if(typeof eltByColor[key] != "undefined"){
				// 		mylog.log("ProjectKey : " + key );
				// 		var elTlink = $('.linkMenu[data-key="' + key + '"]');
				// 		//mylog.log("HERE elTlink", elTlink);
				// 		var projectName = elTlink.text();
				// 		var projectCollection = elTlink.attr('data-col');
				// 		var projectImage = elTlink.attr('data-img');
				// 		var htmlProjects = "<div class='col col-xs-6 col-md-3'>" +
				// 		"<a href='#welcome.idLaR."+key+"' class='linkMenu' data-col='" + projectCollection + "'" + 
				// 		"data-key='" + key + "' style='background-image: url(" + projectImage + ");'>" + 
				// 		"<span class='projectBorderColorHover'><h4>" + projectName+ "</h4></span></a></div>";

				// 		$(htmlProjects).appendTo('#projectChildren .row');
				// 	}
					
				// });
			}else{
		 		$('.customTabTriggerProj').hide();
		 	}
		}else{
	 		$('.customTabTriggerProj').hide();
	 	}
	 	$('.projectNbMembers').text(countMembers);
	 	$('.projectNbProjects').text(countProjects);
	 	$('.projectNbEvents').text(countEvents);

	 	// Project List
	 	
	 	// $('#projectChildren .row').html('');

	 	// if( el.map.links.projects != null ){

	 		

		 //  $(el.map.links.projects).each(function(){
		 // 		//mylog.log("Project : " + this.toSource() );
		 // 		for (key in this) {

			// 	  mylog.log("ProjectKey : " + key );

			// 	  var $link = $('.linkMenu[data-key="' + key + '"]');
		 // 			var projectName = $link.text();
		 // 			var projectCollection = $link.attr('data-col');
		 // 			var projectImage = $link.attr('data-img');
		 // 			var htmlProjects = "<div class='col col-xs-6 col-md-3'>" +
		 // 			"<a href='#" + el.map.slug + "' class='linkMenu' data-col='" + projectCollection + "'" + 
		 // 			"data-key='" + key + "' style='background-image: url(" + projectImage + ");'>" + 
		 // 			"<span class='projectBorderColorHover'><h4>" + projectName+ "</h4></span></a></div>";

		 // 			$(htmlProjects).appendTo('#projectChildren .row');

			// 	}
		 // 	});
	 	// }
	 	// else{
	 	// 	$('.customTabTriggerProj').hide();
	 	// }

	 	// CONTACTS

	 	$('#projectContacts .row').html('');

	 	if( el.map.contacts != null ){
	 		$('.customTabTriggerContacts').show();

	 		// $(el.map.contacts).each(function(index){
	 		$.each(el.map.contacts, function (key,value){
		 		//mylog.log("Project : " + this.toSource() );
		 			var htmlContacts = "<div class='col col-xs-6 col-md-3'>";
		 			htmlContacts += "<h4>" + (typeof value['name'] != "undefined" ? value['name'] : "") + "</h4>";
		 			if(typeof value['email'] != "undefined")
		 				htmlContacts += "<a href='mailto:"+value['email']+"'>"+value['email']+"</a>" ;
		 			if(typeof value['telephone'] != "undefined" && typeof value['telephone'][0] != "undefined")
		 				htmlContacts += "<br><a href='tel:"+value['telephone'][0]+"'>"+value['telephone'][0]+"</a>";
		 			htmlContacts += "</div>";
		 			$(htmlContacts).appendTo('#projectContacts .row');
		 
		 	});
	 	} else{
	 		$('.customTabTriggerContacts').hide();
	 	}

	 	initEvent = false;
		initDoc = false;
		initNews = true;

	 	laRObj.isContributor();
	 	// loadMyStream('projects', id);
	 	// loadCalendar('projects', id);
	 	// loadGallery("", "", "");

	 	contextData = el.map;
	 	contextData.type = 'projects';
	 	contextData.id = id;
	 	// if(location.hash != hashUrlPage+".idLaR."+id)
	 	// 	//location.hash=hashUrlPage+".idLaR."+id;
	 	// 	history.pushState(null, null, hashUrlPage+".idLaR."+id);

		$("#btnChatRaffinerie").attr("href","https://chat.communecter.org/channel/"+el.map.slug);

	 	if(notNull(el.map.links) && 
	 		notNull(el.map.links.contributors) && 
	 		notNull(el.map.links.contributors[userId]) &&
	 		notNull(el.map.links.contributors[userId].isAdmin) &&
	 		el.map.links.contributors[userId].isAdmin == true){
	 		$(".projectAdmin").attr("href",baseUrl+"/#@"+el.map.slug);
	 		$(".projectAdmin").show()
		} else {
			$(".projectAdmin").hide();
		}
		$('a[href="#journal"]').trigger("click");
		laRObj.loadMyStream('projects', idElt);
		mylog.log("laRObj.feedProject end");
	 	
	},
	loadGallery : function (dir, key, folderId){
		mylog.log("laRObj.");
		toogleNotif(false);
		var url = "gallery/index/type/"+laRObj.context.type+"/id/"+laRObj.context.id;
		showLoader('#documents');
		ajaxPost('#documents', baseUrl+'/'+moduleId+'/'+url, 
			null,
			function(){
				$("#header-gallery .nav-pills").hide();

			},"html");
	},
	loadMyStream : function (typeElt, id){
		mylog.log("laRObj.loadMyStream", typeElt, id);
		$('#journalTimeline').html("");
	    var url = "news/co/index/type/"+typeElt+"/id/"+id;///date/"+dateLimit;
	    setTimeout(function(){ //attend que le scroll retourn en haut (kscrollto)
	        showLoader('#journalTimeline');
	        ajaxPost('#journalTimeline', baseUrl+'/'+url, 
	            {
	                nbCol: 1,
	                inline : true
	            },
	            function(){ 
	               // loadLiveNow();
	            },"html");
	    }, 700);
	},
	loadCalendar : function (typeElt, id) {
		mylog.log("laRObj.loadCalendar",typeElt, id);
		$.ajax({
			type: "POST",
			url: baseUrl+'/co2/element/getdatadetail/type/'+typeElt+'/id/'+id+'/dataName/events/sub/2',
			data: {},
			dataType: "json"
		}).done(function(data){
			mylog.log("loadCalendar", data);
			$("#profil-content-calendar").fullCalendar('destroy');
			calendar.showCalendar("#profil-content-calendar", data, "month");
			$("#profil-content-calendar").fullCalendar("gotoDate", moment(Date.now()));
			$(window).on('resize', function(){
				$("#profil-content-calendar").fullCalendar('destroy');
				calendar.showCalendar("#profil-content-calendar", data, "month");
			});

			var str = "";
			var str = directory.showResultsDirectoryHtml( data, null, null, false, "list");
			$("#list-calendar").html(str);
			$(".fc-month-button").trigger("click");
			directory.bindBtnElement();

		}).fail(function(error) {
			$('.ret').html(error);
		});
	},
	isContributor : function () {
		mylog.log("laRObj.isContributor", userId,  elt);
		if( typeof userId != "undefined" &&
			userId != null &&
			userId != "" &&
			typeof elt != "undefined" && 
			typeof elt.links != "undefined" && 
			typeof elt.links.contributors != "undefined" && 
			typeof elt.links.contributors[userId] != "undefined" ) {
			$('#follows').html("Désinscrire");
			$('#follows').data("isco", true);
		} else {
			$('#follows').html("S'inscrire");
			$('#follows').data("isco", false);
		}
	}
}