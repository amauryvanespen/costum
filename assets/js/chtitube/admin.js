adminPanel.views.validation = function(){
	var data={
		title : "Gestion des ajouts",
		types : [ "organizations", "projects", "events", "poi", "proposals" ],
		headerFilters : {
			private : true
		},
		table : {
            name: {
                name : "Nom",
                preview:true
            },
            type:{
            	name : "Type d'ajout"
            },
            creator : {
            	name : "Créateur"
            },
            private : { 
            	name : "A valider",
        		class : "col-xs-1 text-center"
        	}
        },
		actions : {
			creatorMailing : true,
			private : true,
			delete : true
		}
	};
	ajaxPost('#content-view-admin', baseUrl+'/costum/default/groupadmin/', data, function(){},"html");
};
adminDirectory.events.private = function(aObj){
	$("#"+aObj.container+" .privateBtn").off().on("click", function(){
		mylog.log("adminDirectory..privateBtn ", $(this).data("id"), $(this).data("type"));
		var thisObj=$(this);
		var id = $(this).data("id");
		var type = $(this).data("type");
		
		mylog.log("privatePublicThisBtn click",$(this).data("private"));
		//if private doesn't exist then == not private == fasle > switch to true
		var value = ($(this).data("private") && $(this).data("private") != "undefined") ? null : true;
		var params = {
			id    		: id, 
			type  		: type
		};
		$.ajax({
			type: "POST",
     	   	url: baseUrl+"/costum/chtitube/validate",
        	data: params,
        	dataType: "json",
        	success:function(data) { 
		 		if(typeof data.elt.preferences != "undefined"){
					elt.preferences = data.elt.preferences ;
				}
				aObj.setElt(elt, id, type) ;
				$("#"+type+id+" .private").html( aObj.values.private(elt, id, type, aObj));
				thisObj.replaceWith( aObj.actions.private(elt, id, type, aObj) );
				aObj.bindAdminBtnEvents(aObj);
			},
			error: function (error) {
				toastr.error("Une erreur s'est produite. Contactez l'admin");
			}
		});
	});
};

adminDirectory.mailTo.defaultObject = function(elt){
	return "[chtitube] Modération avant validation de "+elt.name;
};
adminDirectory.mailTo.defaultRedirect = function(elt, type, id){
	return "<a href='"+((typeof costum.host != "undefined") ? costum.host : baseUrl+costum.url )+"#references?preview='"+type+"."+id+"' target='_blank'>Retrouvez la page "+elt.name+" en cliquant sur ce lien</a>";
};
adminDirectory.mailTo.defaultMessage = function(elt, type, id){
	nameContact=(typeof elt.creator != "undefined" && elt.creator.name) ? elt.creator.name : "";
	str="Bonjour"+((notEmpty(nameContact)) ? " "+nameContact : "")+",\n\n"+
		"Le contenu que vous avez publié demande d'être approfondi : \n"+
		" - Référencer l'adresse\n"+
		" - Ajouter la description, des mots clés, une image\n"+
		" - Les dates ne sont pas cohérentes\n\n"+
		"Encore un petit effort et vos points seront attribués";
	return str;
};