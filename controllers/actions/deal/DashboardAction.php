<?php
//http://127.0.0.1/ph/costum/co/dashboard/sk/laCompagnieDesTierslieux
class DashboardAction extends CAction
{
    public function run($sk=null,$tpl=null)
    {
    	$controller = $this->getController();    	

    	$tpl = "costum.views.custom.process.dashboard";
    	
    	$answers = PHDB::find( Form::ANSWER_COLLECTION,[ //"formId"     => $params["formId"],
                                              			 "parentSlug" => Yii::app()->session['costum']["contextSlug"] ] );
    	
    	/*echo count($answers)." Dossiers";
    	echo "<br/> <b>Dossier per status pie</b>";
    	echo "<br/>---- validated Dossiers";
    	echo "<br/>---- pending decision Dossiers";
    	echo "<br/>---- pending financing Dossiers";
    	echo "<br/>---- no participation Dossiers";
    	echo "<br/> <b>Decisions per status pie</b>";
    	echo "<br/>---- en cours de Decisions";
    	echo "<br/>---- Decisions validé";
    	echo "<br/>---- Decisions interompu";
    	echo "<br/> <b>Travaux per status pie</b>";
    	echo "<br/>----  for open projects Tasks distribution";
    	echo "<br/>----  for each worker Tasks distribution per project";
    	echo "<br/>----  xxx Tasks";
    	echo "<br/> <b>Fianncement per type or contexte pie</b>";
    	echo "<br/>----  for open projects Tasks distribution per task types";
    	echo "<br/>----  for each worker Finances distribution per project";
    	echo "<br/>----  xxx Tasks";*/
    	//count 
$blocks = [];
		
		$lists = [
			"chiffresDoss" =>[
				"title"=>"Dossier",
				"data" => [
					["data"=>24,"name"=>"validated Dossier","icon"=>"thumbs-up","type"=>"success"],
					["data"=>45,"name"=>"pending decision Dossier","type"=>"danger","icon"=>"map-marker"],
					["data"=>60,"name"=>"pending financing Dossier","type"=>"danger","icon"=>"money"],
					["data"=>24,"name"=>"Dossier with pending intents no commited","icon"=>"hourglass-half"],
					["data"=>45,"name"=>"no participation Dossier","icon"=>"group"],
				],
				"tpl"  => "costum.views.tpls.list"
			],
			"chiffresDecis" =>[
				"title"=>"Decision",
				"data" => [
					["data"=>24,"name"=>"active Projects","icon"=>"thumbs-up","type"=>"success"],
					["data"=>45,"name"=>"finished Projects","type"=>"danger","icon"=>"map-marker"],
					["data"=>60,"name"=>"for open projects Tasks distribution","type"=>"danger","icon"=>"money"],
					["data"=>24,"name"=>"for each worker Tasks distribution per project","icon"=>"hourglass-half"],
				],
				"tpl"  => "costum.views.tpls.list"
			],
			"chiffresFin" =>[
				"title"=>"Finance",
				"data" => [
					["data"=>24,"name"=>"for open projects Finance distribution per presta/ commons","icon"=>"thumbs-up","type"=>"success"],
					["data"=>45,"name"=>"for open projects Finance distribution per task types","type"=>"danger","icon"=>"map-marker"],
					["data"=>60,"name"=>"for each worker Finances distribution per project","type"=>"danger","icon"=>"money"]
				],
				"tpl"  => "costum.views.tpls.list"
			],
			"dossier" =>[
				"title"=>"Dossier",
				"data" => [32,65,36,21,10],
				"lbls" => ["Dossier 0","Dossier 1","Dossier 2","Dossier 3","Dossier 4"],
				"url"  => "/graph/co/dash/g/graph.views.co.line"
			],
			"decision" =>[
				"title"=>"Décision",
				"data" => [32,344,36,21,10],
				"lbls" => ["Décision 0","Décision 1","Décision 2","Décision 3","Décision 4"],
				"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany"
			],
			"tasks" =>[
				"title"=>"Travaux",
				"data" => [32,65,3,21,10],
				"lbls" => ["Travaux 0","Travaux 1","Travaux 2","Travaux 3","Travaux 4"],
				"url"  => "/graph/co/dash/g/graph.views.co.line"
			],
			"finance" =>[
				"title"=>"Finance",
				"data" => [32,65,6,1,10],
				"lbls" => ["Finance 0","Finance 1","Finance 2","Finance 3","Finance 4"],
				"url"  => "/graph/co/dash/g/graph.views.co.scatter"
			],
		];
		foreach ($lists as $ki => $list) 
		{
			$kiCount = 0;
			foreach ($list["data"] as $ix => $v) {
				if(is_numeric($v))
					$kiCount += $v;
				else 
					$kiCount ++;
			}
			$blocks[$ki] = [
				"title"   => $list["title"],
				"counter" => $kiCount,
			];
			if(isset($list["tpl"])){
				$blocks[$ki]["data"] = $list["data"];
				$blocks[$ki]["tpl"] = $list["tpl"];
			}
			else 
				$blocks[$ki]["graph"] = [
					"url"=>$list["url"],
					"key"=>"pieMany".$ki,
					"data"=> [
						"datasets"=> [
							[
								"data"=> $list["data"],
								"backgroundColor"=> Ctenat::$COLORS
							]
						],
						"labels"=> $list["lbls"]
					]
				];
		}
	
		$params = [
			"title" => "Observatoire <br/>de l'amélioration de l'habitat",
    		"blocks" 	=> $blocks
    	];	
    	if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		$this->getController()->render($tpl,$params);
        }
    	
    }
}