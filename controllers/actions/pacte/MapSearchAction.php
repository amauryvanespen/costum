<?php
class MapSearchAction extends CAction
{
	public function run(){ 	
		ini_set('memory_limit', '-1');
		$res = array(
			"result" =>  array(),
			"msg"  => ""
		);
		$where = array( "source.key" => "siteDuPactePourLaTransition",
						"source.toBeValidated" => array('$exists' => false )  );
		$res["result"] = PHDB::find(Organization::COLLECTION, $where, array("email", "name", "profilThumbImageUrl", "profilMarkerImageUrl", "scope", "geo", "geoPosition","profilImageUrl", "profilMediumImageUrl"));
		/*if(!empty($exist)){
			$res["exist"] = true;
			$res["elt"] = $exist;
		}else{
			$res["exist"] = false;
		}*/
		$gzdata=gzencode(json_encode($res), 9);
		header("Content-type: text/javascript");
		header('Content-Encoding: gzip'); 

		echo $gzdata;  
		//$fp = fopen("index.html.gz", "w");
		//fwrite($fp, $gzdata);
		//fclose($fp);
		//Rest::json(gzencode($res, 9)) ;
	}
}