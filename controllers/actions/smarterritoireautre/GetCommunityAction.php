<?php
class GetCommunityAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = Smarterritoireautre::getCommunity($_POST["contextId"],$_POST["contextType"]);
        
        Rest::json($params);
    }
}