<?php
//http://127.0.0.1/ph/costum/co/dashboard/sk/laCompagnieDesTierslieux
class DashboardAction extends CAction
{
    public function run($sk=null,$tpl=null)
    {
    	$controller = $this->getController();    	

    	$tpl = "costum.views.custom.process.dashboard";
    	
    	$answers = PHDB::find( Form::ANSWER_COLLECTION,[ //"formId"     => $params["formId"],
                                              			 "parentSlug" => Yii::app()->session['costum']["contextSlug"] ] );
    	
    	echo count($answers)." Proposals";
    	echo "<br/> <b>Proposals per status pie</b>";
    	echo "<br/>---- validated Proposals";
    	echo "<br/>---- pending decision Proposals";
    	echo "<br/>---- pending financing Proposals";
    	echo "<br/>---- Proposals with pending intents no commited ";
    	echo "<br/>---- Proposals pending evaluations and intent proposals ";
    	echo "<br/>---- no participation Proposals";
    	echo "<br/> <b>Projects per status pie</b>";
    	echo "<br/>---- active Projects";
    	echo "<br/>---- finished Projects";
    	echo "<br/> <b>Tasks per status pie</b>";
    	echo "<br/>----  for open projects Tasks distribution";
    	echo "<br/>----  for each worker Tasks distribution per project";
    	echo "<br/>----  xxx Tasks";
    	echo "<br/> <b>Fianncement per type or contexte pie</b>";
    	echo "<br/>----  for open projects Finance distribution per presta/ commons";
    	echo "<br/>----  for open projects Finance distribution per task types";
    	echo "<br/>----  for each worker Finances distribution per project";
    	echo "<br/>----  xxx Tasks";
    	//count 
		$blocks = [];
		$lists = [
			"proposal" =>[
				"title"=>"Propositions",
				"data" => [32,65,36,21,10],
				"lbls" => ["Propositions 0","Propositions 1","Propositions 2","Propositions 3","Propositions 4"]
			],
			"projects" =>[
				"title"=>"Projects",
				"data" => [32,65,36,21,10],
				"lbls" => ["Projects 0","Projects 1","Projects 2","Projects 3","Projects 4"]
			],
			"tasks" =>[
				"title"=>"Taches",
				"data" => [32,65,36,21,10],
				"lbls" => ["Taches 0","Taches 1","Taches 2","Taches 3","Taches 4"]
			],
			"finance" =>[
				"title"=>"Finance",
				"data" => [32,65,36,21,10],
				"lbls" => ["Finance 0","Finance 1","Finance 2","Finance 3","Finance 4"]
			],
		];
		foreach ($lists as $ki => $list) 
		{
			$kiCount = 0;
			foreach ($list["data"] as $ix => $v) {
				$kiCount += $v;
			}
			$blocks["pie".$ki] = [
				"title"   => $list["title"],
				"counter" => $kiCount,
				"graph" => [
					"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany/size/S",
					"key"=>"pieMany".$ki,
					"data"=> [
						"datasets"=> [
							[
								"data"=> $list["data"],
								"backgroundColor"=> Ctenat::$COLORS
							]
						],
						"labels"=> $list["lbls"]
					]
				]
			];
		}
	
		$params = [
			"title" => "Observatoire des process Opal",
    		"blocks" 	=> $blocks
    	];	
		
    	if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		$this->getController()->render($tpl,$params);
        }
    	
    }
}