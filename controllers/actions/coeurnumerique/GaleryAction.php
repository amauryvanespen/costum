<?php
class GaleryAction extends CAction{
    public function run(){
        $controller = $this->getController();
        $params = CoeurNumerique::galery($_POST);

        Rest::json($params);
    }
}