<?php
class GenerateCopilAction extends CTKAction{
	public function run($id, $title=null,$date=null){
		// $id = "cte" ;
		// $user = "5ac4c5536ff9928b248b458a";
		// $session = "1";
		$controller=$this->getController();
		ini_set('max_execution_time',1000);

		$answer = PHDB::findOne( Form::ANSWER_COLLECTION, array("_id"=>new MongoId($id)));
		$form = PHDB::findOne( Form::COLLECTION , array("id"=>$answer["formId"]));
		$forms = PHDB::find( Form::COLLECTION , array("parentSurvey"=>$answer["formId"]));
		$parent = Slug::getElementBySlug($form["id"]);
		
		foreach ($forms as $key => $value) {
			$forms[$value["id"]] = $value;
		}
		$formSrc = null;
		if( is_string($form["scenario"]) && stripos( $form["scenario"] , "db.") !== false){
			$pathT = explode(".",$form["scenario"]); 
			$formSrc = PHDB::findOne( $pathT[1] , array( $pathT[2] => $pathT[3] ) );
			$form["scenario"] = $formSrc[$pathT[4]];
		}

		$title = ( @$answer["answers"]["cte2"]["answers"]["project"]  ) ?  $answer["answers"]["cte2"]["answers"]["project"]["name"] : "Dossier";
		
			//var_dump($_POST["title"]);exit;
		$params = array(
			//"author" => @$answer["name"],
			"idAnswer"=>$id,
			"answer" => $answer,
			//"saveOption"=>"F",
			//"urlPath"=>$res["uploadDir"],
			"title" => @$_POST["title"],
			"participants" => @$_POST["participants"],
			"dateComment" => @$_POST["date"],
			"subject" => "CTE",
			//"custom" => $form["custom"],
			"footer" => true,
			"tplData" => "cteDossier",
			"form" => $form,
			"forms" => $forms,
			"canAdmin"=>null,
			"parentName"=> $parent["el"]["name"],
			"actionName"=> $answer["answers"][$form["id"]]["answers"]["project"]["name"]

		);
		if(isset($_POST["subKey"])){
			$params["subKey"]=$_POST["subKey"];
			$params["validation"] = @$answer['validation'];
		}
	      
		if(isset($_POST["copil"])){
			$res=Document::checkFileRequirements([], null, 
				array(
					"dir"=>"communecter",
					"typeEltFolder"=> Form::ANSWER_COLLECTION,
					"idEltFolder"=> $id,
					"docType"=> "file",
					"nameUrl"=>"/pdf.pdf",
					"sizeUrl"=>1000
				)
			);
			$params["comment"]=true;
			$params["saveOption"]="F";
			$params["urlPath"]=$res["uploadDir"];
			$params["docName"]="copil".date("d-m-Y",strtotime($_POST["date"])).".pdf";
			 if(isset($_POST["subKey"]))
				$params["docName"]=$_POST["subKey"].date("d-m-Y",strtotime($_POST["date"])).".pdf";
	       
		}
		if(isset($answer['answers']))
			$params["answers"] = $answer['answers'][$answer["formId"]]['answers'];
		$html= $controller->renderPartial('costum.views.custom.ctenat.pdf.copil', $params, true);

		$params["html"] = $html ;
		$res=Pdf::createPdf($params);
		if(isset($_POST["copil"])){	
			$kparams = array(
                "id" => $id,
                "type" => Form::ANSWER_COLLECTION,
                "folder" => Form::ANSWER_COLLECTION."/".$id."/".Document::GENERATED_FILE_FOLDER,
                "moduleId" => "communecter",
                "name" => $params["docName"],
                "size" => "",
                "contentKey" => "",
                "doctype"=> "file",
                "author" => Yii::app()->session["userId"]
            );
            if(isset($_POST["subKey"]))
				$kparams["subKey"]=$_POST["subKey"];
	        $res2 = Document::save($kparams);
	        return Rest::json(array("res"=>true, "docPath"=>"/upload/".$kparams["moduleId"]."/".$kparams["folder"]."/".$params["docName"], "fileName"=>$params["docName"]));
	    }
		
	}
}