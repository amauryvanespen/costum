<?php 
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class DashboardAccountAction extends CAction
{
	public function run(){
		$controller = $this->getController();
		$params = array() ;
		$page = "element/dashboard";
		if(Yii::app()->request->isAjaxRequest)
			echo $controller->renderPartial("/custom/ctenat/".$page,$_POST,true);
		
		
	}
}
