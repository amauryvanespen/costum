<?php
class DashboardAction extends CAction
{
    public function run($slug=null,$tag=null)
    {
    	$controller = $this->getController();
    	$params=array("page"=>"pie");
    	

    	$tpl = "costum.views.custom.ctenat.dashboard";
    	
    	$actionCount =0;
		$actionIds= [];

    	//$data = 
    	//pour les graphs d'un tag d'un CTER
    	//onglet en chiffres
    	if($tag && $slug){
    		$el = Slug::getElementBySlug($slug);
    		$tagsLbls = Yii::app()->session["costum"]["lists"]["domainAction"][$tag];
    		$answers = PHDB::find( Form::ANSWER_COLLECTION, [   "source.key"=>"ctenat",
    															"formId"=>$slug,
    															"priorisation" => ['$in'=> Ctenat::$validActionStates ] ] );
    		$actionsIds = [];
    		$actionsByDA = [];
    		foreach ($answers as $key => $ans) {
    			$formId = $ans["formId"];
    			if(    isset( $ans["answers"][$formId]["answers"]["project"]["id"] ) 
					&& isset($ans["answers"][$formId]["answers"]["caracter"]["actionPrincipal"] )
					&& in_array($ans["answers"][$formId]["answers"]["caracter"]["actionPrincipal"],$tagsLbls ) ) 
				{
					$actionPrincipal = $ans["answers"][$formId]["answers"]["caracter"]["actionPrincipal"];
					$projectId = $ans["answers"][$formId]["answers"]["project"]["id"];
					$actionsIds[] = new MongoId($projectId);


					$actionsDA[$projectId] = ["family"=>$tag, 
											  "1st" =>$actionPrincipal];
					if( !isset( $ans["answers"][$formId]["answers"]["caracter"]["actionSecondaire"]))
						$actionsDA[$projectId]['2nd'] = @$ans["answers"][$formId]["answers"]["caracter"]["actionSecondaire"];
					
					if( !isset( $actionsByDA[$actionPrincipal]))
						$actionsByDA[$actionPrincipal] = [];
					$actionsByDA[$actionPrincipal][] = $projectId;
				}
    		}

    		$actions = PHDB::find( Project::COLLECTION, array("_id" => array( '$in'=>$actionsIds ) ),
    												   ["name","slug","description","tags","profilMediumImageUrl","links","geo","geoPosition","address"]);
    		//rajouter les DA dans 
			foreach ($actions as $key => $value) {
				$actions[$key]["domaineAction"] = $actionsDA[$key];
			}
    		$countActions = [];
			foreach ($tagsLbls as $i => $t) {
					$tagsLbls[] =  $t;
					$countActions[] = (isset($actionsByDA[$t])) ? count($actionsByDA[$t]) : 0;
			}
			$params = array(
				"elements" => $actions,
				"slug" => $slug,
				"css" => array('border' => false ),
	    		"title" => "<span style='font-size:12px'>".$el["el"]["name"]."</span><br/>Tableau de bord",
	    		"blocks" 	=> array(
					"pieActionsByTagBySlug" => array(
						"title"   => $tag,
						"counter" => count( $actionsIds ),
						"graph" => array (
							"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany/size/S",
							"key"=>"pieManyByTagBySlug",
							"data"=> array(
								"datasets"=> array(
									array(
										"data"=> $countActions,
										"backgroundColor"=> Ctenat::$COLORS
										)
									),
								"labels"=> $tagsLbls
							)
						)
					)
				)
	    	);
    	}
    	//quand on click sur une thematique donnée, ex type d'ation Energie
    	else if( $tag ){
    		$tagsLbls = Yii::app()->session["costum"]["lists"]["domainAction"][$tag];
    		$answers = PHDB::find( Form::ANSWER_COLLECTION, [ "source.key"=>"ctenat",
    														  "priorisation" => ['$in'=> Ctenat::$validActionStates ] ] );
    		$actionsIds = [];
    		$actionsIdsStr = [];
    		$actionsByDA = [];
    		//gather all answers avec caraction.actionPrincipale
    		//fill $actionsIds with all projectIds and tags for carater in $actionsDA
    		//var_dump($tagsLbls);
    		foreach ($answers as $key => $ans) {
    			$formId = $ans["formId"];
    			if(    isset( $ans["answers"][$formId]["answers"]["project"]["id"] ) 
					&& isset($ans["answers"][$formId]["answers"]["caracter"]["actionPrincipal"] )
					&& in_array($ans["answers"][$formId]["answers"]["caracter"]["actionPrincipal"],$tagsLbls ) ) 
				{
					$actionPrincipal = $ans["answers"][$formId]["answers"]["caracter"]["actionPrincipal"];
					$projectId = $ans["answers"][$formId]["answers"]["project"]["id"];
					//regle un probleme de doublon dans la liste des actions
					if(!isset($actionsDA[$projectId]))
					{
						$actionsIds[] = new MongoId($projectId);
						$actionsDA[$projectId] = ["family"=>$tag, 
												   "1st" =>$actionPrincipal];
						if( !isset( $ans["answers"][$formId]["answers"]["caracter"]["actionSecondaire"]))
							$actionsDA[$projectId]['2nd'] = @$ans["answers"][$formId]["answers"]["caracter"]["actionSecondaire"];

						if( !isset( $actionsByDA[$actionPrincipal]))
							$actionsByDA[$actionPrincipal] = [];
						$actionsByDA[$actionPrincipal][] = $projectId;
					} else {
						// var_dump(count($formId));
						// var_dump(count($projectId));
					}
				}
    		}
    		
    		$actions = PHDB::find(Project::COLLECTION, ["_id" => array( '$in'=>$actionsIds ) ],
    												   ["name","slug","description","tags","profilMediumImageUrl","links","geo","geoPosition","address"]);
    		
    		// var_dump(count($actionsIds));
    		// var_dump($actionsIds);exit;
    		// var_dump(count($actions));exit;

    		foreach ( $actions as $key => $value ) {
    			$actions[$key]["domaineAction"] = $actionsDA[$key];
    		}

    		$countActions = [];
			foreach ($tagsLbls as $i => $t) {
					//$tagsLbls[] =  $t;
					$countActions[] = (isset($actionsByDA[$t])) ? count($actionsByDA[$t]) : 0;
			}
			//var_dump($tagsLbls);
			$params = [
				"elements" => $actions,
				"css" => ['border' => false ],
	    		"title" => "Tableau de bord",

	    		"blocks" 	=> [
					"barActionsByTag" => [
						"title"   => $tag,
						"counter" => count( $actions ),
						"noborder" => 1,
						"graph" => [
							"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.barMany",
							"key"=>"barManyByTag",
							"data"=> [
								"data"=>  $countActions,
								"labels"=> $tagsLbls
							]
						]
					]
					// ,
					// "mapView" => [
					// 	"map" => ["id"=>$tag]
					// ]
				]
	    	];
    	}
    	//onglet en chiffre d'un CTER
    	else if($slug){
    		$el = Slug::getElementBySlug($slug);

    		//financement 
			$financeData = [];
			$financeLbls = [];
			$answers = PHDB::find( Form::ANSWER_COLLECTION, ["source.key"=>"ctenat",
    														 "formId"=>$slug,
    														 "priorisation" => ['$in'=> Ctenat::$validActionStates ] ] );
			
			$finance = Ctenat::chiffreFinancementByType($answers,$slug);
			$financeData = $finance["data"];
			$financeLbls = $finance["lbls"];
			

			//actions 
			$tagsLbls = Yii::app()->session["costum"]["lists"]["domainAction"];
			$actionData = [];
			$actionLbls	 = [];
			
    		
    		$actionsIds = [];
    		$actionsByDA = [];
    		foreach ($answers as $key => $ans) {
    			if(    isset( $ans["answers"][$slug]["answers"]["project"]["id"] ) 
					&& isset($ans["answers"][$slug]["answers"]["caracter"]["actionPrincipal"] ) ) 
				{
					$actionPrincipal = $ans["answers"][$slug]["answers"]["caracter"]["actionPrincipal"]; 
					$projectId = $ans["answers"][$slug]["answers"]["project"]["id"];
					$parentBadge = "";
					foreach (Yii::app()->session["costum"]["lists"]["domainAction"] as $key => $childBadges) {
						foreach ($childBadges as $ic => $cb) {
							if($actionPrincipal == $cb)
								$parentBadge = $key;
						}			                
				    }
				    $actionsDA[$projectId] = ["family"=>$tag, 
											  "1st" =>$actionPrincipal];
				    if( !isset( $ans["answers"][$slug]["answers"]["caracter"]["actionSecondaire"]))
						$actionsDA[$projectId]['2nd'] = @$ans["answers"][$slug]["answers"]["caracter"]["actionSecondaire"];

				    //check $parentBadge is really a familyBadge
				    if(isset($tagsLbls[$parentBadge])){
						$projectId = $ans["answers"][$slug]["answers"]["project"]["id"];
						$actionsIds[] = new MongoId($projectId);

						if( !isset( $actionsByDA[$parentBadge]))
							$actionsByDA[$parentBadge] = [];
						$actionsByDA[$parentBadge][] = $projectId;

					}
				}
    		}
    		$actions = PHDB::find(Project::COLLECTION, array("_id" => array( '$in'=>$actionsIds ) ),
    												   ["name","slug","description","tags","profilMediumImageUrl","links","geo","geoPosition","address"]);
    		
    		foreach ($actions as $key => $value) {
    			$actions[$key]["domaineAction"] = $actionsDA[$key];
    		}
    		$totalCountActions = 0;
			foreach ($tagsLbls as $t => $childBadges) {
				$actionLbls[] =  $t;
				$actionData[] = (isset($actionsByDA[$t])) ? count($actionsByDA[$t]) : 0;
				$totalCountActions = $totalCountActions + ((isset($actionsByDA[$t])) ? count($actionsByDA[$t]) : 0);
			}

			$params = array(
				"elements" => null,
				"slug" => $slug,
	    		"title" => "<span style='font-size:12px'>".$el["el"]["name"]."</span><br/> Tableau de bord",
	    		"blocks" 	=> array(
					"barActions" => array(
						"title"   => "Actions",
						"counter" => $totalCountActions,
						"graph" => array (
							"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.barActionsCTErr",
							"key"=>"barActionsCTErr",
							"data"=> array (
								"datasets"=> array(
									array(
										"data"=> $actionData, 
										"backgroundColor"=> Ctenat::$COLORS,
										"borderWidth"=> 1
										)
									),
								"labels"=> $actionLbls
							)
						)
					)
				)
	    	);
			if(isset($finance["total"]))
				$params["blocks"]["pieFinance"] = [
					"title"   => "millions €",
					"counter" => $finance["total"],
					"graph" => [
						"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany",
						"key"=>"pieManyFinance",
						"data"=> [
							"datasets"=> [
								[ "data"=> $financeData,
								  "backgroundColor"=> Ctenat::$COLORS ]],
							"labels"=> $financeLbls
						]
					]
				];


			$states = [ Ctenat::STATUT_ACTION_VALID,
	 					Ctenat::STATUT_ACTION_COMPLETED,
	 					Ctenat::STATUT_ACTION_MATURATION,
	 					Ctenat::STATUT_ACTION_CONTRACT ];
			$answersList2 = PHDB::find(Form::ANSWER_COLLECTION,[
	 					"formId"=>$slug,
	 					"priorisation" => ['$in'=>$states]],["_id", "priorisation"]);
			$stateData = [0,0,0];
			foreach ($answersList2 as $k => $v) {
				if($v["priorisation"] == Ctenat::STATUT_ACTION_VALID)
					$stateData[0] = $stateData[0]+1; 
	 			if($v["priorisation"] == Ctenat::STATUT_ACTION_COMPLETED)
	 				$stateData[1] = $stateData[1]+1;
	 			if($v["priorisation"] == Ctenat::STATUT_ACTION_MATURATION)
	 				$stateData[2] = $stateData[2]+1;
			}
			//clear zero values break the pie
			// foreach ($stateData as $k => $v) {
			// 	if($v == 0) {
			// 		array_splice($stateData,$k,1);
			// 		array_splice($states,$k,1);
			// 	}
			// }
			$colors = array_splice(Ctenat::$COLORS ,0,2); 
			$params["blocks"]["pieStatuts"] = [
					"title"   => "Statuts des actions",
					"counter" => null,
					"graph" => [
						"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany",
						"key"=>"pieManyState",
						"data"=> [
							"datasets"=> [
								[ "data"=> $stateData,
								  "backgroundColor"=> Ctenat::$COLORS ]],
							"labels"=> $states
						]
					]
				];
			$indicators = [];
			//[
			//"title"=>"emploi",
			//"obj"=>"10",
			//"done"=>"12",
			//]
			$indicateurs = Ctenat::getIndicator();
			foreach ($answers as $i => $a) {
				if( isset( $a["answers"][$slug]["answers"]["murir"]["results"] ) )
				{
					foreach ( $a["answers"][$slug]["answers"]["murir"]["results"] as $ind => $vind ) 
					{
						if(isset($indicateurs[ $vind["indicateur"] ])){
							if(!isset( $indicators[$vind["indicateur"]] ))
								$indicators[$vind["indicateur"]] = ["title" => $indicateurs[ $vind["indicateur"] ] , "obj"=>0,"done"=>0];
							if(isset( $vind["reality"] ))
							{
								if(isset($vind["reality"]["res2019"]))
									$indicators[$vind["indicateur"]]["done"] += $vind["reality"]["res2019"];
								if(isset($vind["reality"]["res2020"]))
									$indicators[$vind["indicateur"]]["done"] += $vind["reality"]["res2020"];
								if(isset($vind["reality"]["res2021"]))
									$indicators[$vind["indicateur"]]["done"] += $vind["reality"]["res2021"];
								if(isset($vind["reality"]["res2022"]))
									$indicators[$vind["indicateur"]]["done"] += $vind["reality"]["res2022"];
							}
							if(isset($vind["objectif"]))
							{
								if(isset($vind["objectif"]["res2019"]))
									$indicators[$vind["indicateur"]]["obj"] += $vind["objectif"]["res2019"];
								if(isset($vind["objectif"]["res2020"]))
									$indicators[$vind["indicateur"]]["obj"] += $vind["objectif"]["res2020"];
								if(isset($vind["objectif"]["res2021"]))
									$indicators[$vind["indicateur"]]["obj"] += $vind["objectif"]["res2021"];
								if(isset($vind["objectif"]["res2022"]))
									$indicators[$vind["indicateur"]]["obj"] += $vind["objectif"]["res2022"];
							}
						}
					}
				}
			}
			$params["blocks"]["indicateurs"] = [
					"title"   => "Résultats attendus des actions",
					"counter" => null,
					"indicator" => $indicators
				];
    		//$tpl = "costum.views.custom.ctenat.dashboardCTErr";
    	}
    	//dashboard vision globale, aggregation 
    	else{ 
    		$answers = PHDB::find( Form::ANSWER_COLLECTION, array("source.key"=>"ctenat",
    									"priorisation" => ['$in'=>Ctenat::$validActionStates]) );
			$ssBadgeFamily = [];
			foreach (Yii::app()->session["costum"]["lists"]["domainAction"] as $pb => $bChild) {
				foreach ($bChild as $key => $bName) {
					$ssBadgeFamily[$bName] = $pb;
				}
			}
			$indicatorCount = [
				"co2"=>0,
				"job"=>0,
				"kwInstalled"=>0,
				"building"=>0,
				"agricol"=>0,
				"person"=>0,
				"friches"=>0,
				"cyclo"=>0,
				"water"=>0,
				"trees"=>0,
				"haie"=>0,
				"tonnes"=>0];
			$financeTotal = 0;
			foreach ( $answers as $id => $ans ) {

				$daPrinci = @$ans["answers"][ $ans["formId"] ]["answers"]["caracter"]["actionPrincipal"];
				if( isset( $ssBadgeFamily[$daPrinci] )
					&& isset($ans["answers"][$ans["formId"]]["answers"]["project"]["id"]) ) 
					$actionCount++;

				//indicator counts
				if(isset($ans["answers"][$ans["formId"]]["answers"]["murir"]["results"]) ) {
					$results = $ans["answers"][$ans["formId"]]["answers"]["murir"]["results"];
					
					//name" : "Emissions de gaz à effet de serre du projet"
					$indicatorCount["co2"] += round(Ctenat::indicatorCount($results,"5d7fa1c540bb4e8f7b496b03"));
					// "Emissions de gaz à effet de serre économisées grâce au projet de TCSP"
					$indicatorCount["co2"] += round(Ctenat::indicatorCount($results,"5d7fa1c540bb4e8f7b496b05"));
					
					//Création d'emploi
					$indicatorCount["job"] += round(Ctenat::indicatorCount($results,"5d7fa1c540bb4e8f7b496afe"));
					
					//Production d'énergie renouvelable
					$indicatorCount["kwInstalled"] += round(Ctenat::indicatorCount($results,"5dbc5d156908648d4c8b456e"));

					//nombre de logements rénovés
					$indicatorCount["building"] += round(Ctenat::indicatorCount($results,"5db2e307690864f6088b45f3"));
					//"Nombre de bâtiments et de logements rénovés (partiel ou total)"
					$indicatorCount["building"] += round(Ctenat::indicatorCount($results,"5d7fa1ca40bb4e8f7b496b42"));
					//"Nombre de rénovations globales / BBC"
					$indicatorCount["building"] += round(Ctenat::indicatorCount($results,"5d7fa1ca40bb4e8f7b496b43"));

					//Surfaces concernées par des évolutions de pratiques agricoles (ha)
					$indicatorCount["agricol"] += round(Ctenat::indicatorCount($results,"5d7fa1cc40bb4e8f7b496b67"));
	    			//"Surface dédiée à l'agriculture urbaine
	    			$indicatorCount["agricol"] += round(Ctenat::indicatorCount($results,"5d7fa1c140bb4e8f7b496a88"));	
	    			//"Surface luzerne implantée(Ha)
					$indicatorCount["agricol"] += round(Ctenat::indicatorCount($results,"5d7fa1cd40bb4e8f7b496b6f"));	

					//"nombre de personnes sensibilisées (/an)"
					$indicatorCount["person"] += round(Ctenat::indicatorCount($results,"5d7fa1cb40bb4e8f7b496b50"));
					//"nombre de personnes formées (/an)
					$indicatorCount["person"] += round(Ctenat::indicatorCount($results,"5d7fa1cb40bb4e8f7b496b4d"));
					// "Nombre d'acteurs impliqués dans la démarche (total)
					$indicatorCount["person"] += round(Ctenat::indicatorCount($results,"5d7fa1ca40bb4e8f7b496b48"));

					//Réaménagement des friches industrielles
					$indicatorCount["friches"] += round(Ctenat::indicatorCount($results,"5d7fa1c440bb4e8f7b496ae0"));
					
					//"Mètres linéaires de pistes ou bandes cyclables crées
					$indicatorCount["cyclo"] += round(Ctenat::indicatorCount($results,"5dcd5ed0690864e2048b45e7"));
					//  "Linéaire de voiries à vitesse limitée
					$indicatorCount["cyclo"] += round(Ctenat::indicatorCount($results,"5d7fa1c140bb4e8f7b496a75"));
					//  "Voiries dédiées aux modes doux
					$indicatorCount["cyclo"] += round(Ctenat::indicatorCount($results,"5d7fa1c540bb4e8f7b496af5"));
					
					// "_id" : ObjectId("5dbfe846690864bc7c8b4679"),"name" : "Economies d'eau"
					//"_id" : ObjectId("5dc05edc6908644b748b4594"),"name" : "Economies d'eau"
					$indicatorCount["water"] += round(Ctenat::indicatorCount($results,"5dbfe846690864bc7c8b4679"));
					$indicatorCount["water"] += round(Ctenat::indicatorCount($results,"5dc05edc6908644b748b4594"));
					
					//"_id" : ObjectId("5d7fa1c140bb4e8f7b496a78"),"name" : "Nombre d'arbres plantés"
					$indicatorCount["trees"] += round(Ctenat::indicatorCount($results,"5d7fa1c140bb4e8f7b496a78"));
					
					//"_id" : ObjectId("5dcd3af2690864945c8b4577"),"name" : "Linéaire de haies planté"
					$indicatorCount["haie"] += round(Ctenat::indicatorCount($results,"5dcd3af2690864945c8b4577"));
					// "Nb de kilomètres de haies gérées durablement – restaurées - plantées
					$indicatorCount["haie"] += round(Ctenat::indicatorCount($results,"5d7fa1cd40bb4e8f7b496b72"));

					//"_id" : ObjectId("5d7fa1cc40bb4e8f7b496b5d"),"name" : "Quantité annuelle de déchets valorisés (T/an)"
					$indicatorCount["tonnes"] += round(Ctenat::indicatorCount($results,"5d7fa1cc40bb4e8f7b496b5d"));
					// "Valorisation des déchets en phase chantier
					$indicatorCount["tonnes"] += round(Ctenat::indicatorCount($results,"5d7fa1c440bb4e8f7b496af1"));
				}
				$financeTotalSlug = Ctenat::chiffreFinancementByType([$id=>$ans],$ans["formId"]);
				$financeTotal += $financeTotalSlug['total'];
			}
	 		$cteCount = PHDB::count(Project::COLLECTION, 
											[ "category" => Ctenat::CATEGORY_CTER,
											  "source.status.ctenat"=> ['$in'=> Ctenat::$validCter ] ]);
	    	$params = [
	    		"elements" => null,
	    		"title" => "Tableau de bord national des CTE",
	    		"blocks" 	=> [
					"lineCTE" => [
						"title"   => "CTE lancés",
						"counter" => $cteCount,
						"graph" => [
							"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.lineCTE/size/S",
							"key"=>"lineCTE",
							"data"=> array(
									"data"=> [3,5,8,19,$cteCount],
									"labels"=> [
										"janvier 2018",
										"mai 2018",
										"septembre 2018",
										"février 2019",
										"juillet 2019"
									]
								)
							]
					],
					"barPorteurbyDomaine" => [
						"title"   => "Actions",
						"counter" => $actionCount,
						"graph" => ["url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.barPorteurbyDomaine/size/S",
							"key"=>"barPorteurbyDomaine"]
					],
					"pieFinance" => [
						"title"   => "Millions €",
						"counter" => $financeTotal,
						"graph" => ["url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieFinance/size/S",
							"key"=>"pieFinance"]
					],
					"co2" => [
						"title"   => "(TeqCO2/an)",
						"counter" => $indicatorCount["co2"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/co2.png'><div class='title2'>Nombre de Tonnes de CO2 équivalent évitées</div>"
					],
					"job" => [
						"title"   => "(Personnes)",
						"counter" => $indicatorCount["job"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/job.png'><div class='title2'>Nombre d'emplois directs créés et maintenus</div>"
					],
					"kwInstalled" => [
						"title"   => "(KWh/an)",
						"counter" => $indicatorCount["kwInstalled"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/kwInstalled.png'><div class='title2'>Production d'energie renouvelable</div>"
					],
					"building" => [
						"title"   => "(Bâtiments et logements)",
						"counter" => $indicatorCount["building"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/building.png'><div class='title2'>Nombre de bâtiments et logements rénovés</div>"
					],
					"agricol" => [
						"title"   => "(Hectares)",
						"counter" => $indicatorCount["agricol"] ,
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/agricol.png'><div class='title2'>Surfaces concernées par des évolutions de pratiques agricoles</div>"
					],
					"person" => [
						"title"   => "(Personnes)",
						"counter" => $indicatorCount["person"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/person.png'><div class='title2'>Nombre de personnes sensibilisées et formées</div>"
					],
					"friches" => [
						"title"   => "(Hectares)",
						"counter" => $indicatorCount["friches"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/friches.png'><div class='title2'>Surface de friche réhabilitée</div>"
					],
					"cyclo" => [
						"title"   => "(Mètres linéaire total)",
						"counter" => $indicatorCount["cyclo"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/cyclo.png'><div class='title2'>Linéaire de pistes cyclables créées</div>"
					],
					"water" => [
						"title"   => "(M3/an)",
						"counter" => $indicatorCount["water"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/water.png'><div class='title2'>Mètres cube d’eau économisés</div>"
					],
					"trees" => [
						"title"   => "(Arbres)",
						"counter" => $indicatorCount["trees"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/trees.png'><div class='title2'>Nombre d’arbres plantés</div>"
					],
					"haie" => [
						"title"   => "Mètres",
						"counter" => $indicatorCount["haie"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/haie.png'><div class='title2'>Kilomètres de haie plantée ou restaurée</div>"
					],
					"tonnes" => [
						"title"   => "(Tonnes/an)",
						"counter" => $indicatorCount["tonnes"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/tonnes.png'><div class='title2'>Quantité de déchets valorisés</div>"
					]
				]
	    	];		
    	 }
    	if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		$this->getController()->render($tpl,$params);
        }
    	
    }
}