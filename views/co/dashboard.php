<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/jquery-counterUp/waypoints.min.js",
		"/plugins/jquery-counterUp/jquery.counterup.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<style type="text/css">
    <?php if( !isset($css["border"]) || $css["border"]==true ){ ?>
	.counterBlock{min-height:340px;}
    .wborder{border : 2px solid #002861; border-radius : 20px;}
    .title2{font-size : 24px; font-weight : bold;}
    <?php }?>
    
</style>

<div class="col-md-12 col-sm-12 col-xs-12 no-padding margin-top-20 dashboard app-<?php echo @$page ?>">
    <div class="col-xs-12 text-center" style=" color:#24284D">
    	<h1 style="margin:0px"><?php echo $title ?></h1>
    </div>
   <div classs="col-xs-12" style="">
		<?php //echo $this->renderPartial("graph.views.co.menuSwitcher",array()); ?> 
		<div class="col-xs-12" id="graphSection">
   				<?php       					
   				$blocksize = (!empty($blocks)) ? floor(12/count($blocks)) : 12;
                $colCount = 0;
                $fontSize = "24px;font-weight:bold;";
                
   				 foreach ($blocks as $id => $d) 
   				 { 
                    $borderClass = (isset($d["noborder"])) ? "" : "wborder";
                    if( isset($d["graph"]) && count($blocks) > 3){
                        $blocksize = 4;
                        $borderClass =  "wborder";     
                        $fontSize = "24px;font-weight:bold;";
                    }
                    else if( isset($d["html"]) ){
                        $blocksize = 2;
                        $fontSize = "18px;";
                    }
                    else if( isset($d["offset"]) ){
                        $blocksize = 1;
                    }

                    ?>

                    
                    
                        <?php
                        
                        if( isset($d["map"]) )  { ?> 
                            <div class="col-md-<?php echo $blocksize?>  text-center padding-10"  >
                                <div id='map<?php echo $id?>' class='col-xs-12' style='height: 420px;'></div>
                                <script type="text/javascript">
                                    mylog.log("map","id:map<?php echo $id?>","var js :mapDash<?php echo $id?>");
                                    var mapDash<?php echo $id?> = {};
                                    jQuery(document).ready(function() {
                                        mapDash<?php echo $id?> =  mapObj.init({
                                            container : "map<?php echo $id?>",
                                            latLon : [ 39.74621, -104.98404],
                                            activeCluster : true,
                                            zoom : 16,
                                            activePopUp : true,
                                            menuRight : false
                                        });
                                        mapDash<?php echo $id?>.addElts(<?php echo json_encode( $elements) ?>, true);
                                    });
                                    //.clearMap
                                </script>
                            </div>

                        <?php } elseif( isset($d["indicator"])  )  { 
                            ?>
                            <div class="col-md-12 text-center padding-10"  >
                            <h1><?php echo $d["title"] ?></h1>
                            </div>
                            <?php
                            foreach ($d["indicator"] as $ix => $ind) {
                            ?>
                            <div class=" col-md-<?php echo $blocksize?>  text-center padding-10"  >
                                <div class="<?php echo $borderClass; ?>">
                                    <h4 style="color:#18a47d"><?php echo $ind["title"];?></h4>
                                    
                                    <h5>Engagement : <?php echo $ind["obj"];?></h5>
                                    <h5>Réalisé : <?php echo $ind["done"];?></h5>
                                </div>    
                            </div>
                            
                            <?php }

                         
                         } elseif( isset($d["graph"]) || isset($d["html"])  )  { ?>     
                            <div class="col-xs-<?php echo $blocksize?>  text-center padding-10"  >
                                <h1><span class="<?php echo (!empty($d["counter"]))? "counter" : ""?>"><?php echo (!empty($d["counter"]))?$d["counter"] : "&nbsp;"?></span></h1>
                                <div style='font-size:<?php echo $fontSize?>'><?php echo $d["title"]?></div>
                                <div class="counterBlock <?php echo $borderClass; ?>" id="<?php echo $id?>" >
                                    <?php if( isset($d["html"]) ) 
                                        echo $d["html"];?>
                                </div>
                            </div>
                         <?php 
                         
                         } ?>
                    <?php 
                         $colCount = $colCount + $blocksize;
                         if($colCount == 12){
                            echo '<div class="col-xs-12"></div>';
                            $colCount = 0;
                         } ?>
                 <?php }?>
   		</div>
   </div>

</div>



<script type="text/javascript">
//alert("/modules/costum/views/custom/ctenat/dashboard.php")
//prepare global graph variable needed to build generic graphs
<?php  
foreach ($blocks as $id => $d) {
	if( isset($d["graph"]) ) {
		?>
		var <?php echo $d["graph"]["key"]."Data" ?> = <?php echo ( isset( $d["graph"]['data'] ) ) ? json_encode( $d["graph"]["data"] ) : "null" ?>;
		mylog.log('url', 'graphs data', '<?php echo $d["graph"]["key"] ?>Data',<?php echo $d["graph"]["key"] ?>Data);
<?php }
} ?>

jQuery(document).ready(function() {

	setTitle("CTE : <?php echo strip_tags($title) ?>");
	mylog.log("render graph","/modules/costum/views/custom/ctenat/dashboard.php");

	<?php  foreach ($blocks as $id => $d) {
		if( isset($d["graph"]) ) { ?>
			mylog.log('url graphs',' <?php echo $d["graph"]["url"]?>');
			ajaxPost('#<?php echo $id?>', baseUrl+'<?php echo $d["graph"]["url"]?>'+"/id/<?php echo $d["graph"]["key"] ?>", null, function(){},"html");
	<?php }
	} ?>

	$('.counter').counterUp({
	  delay: 10,
	  time: 2000
	});

	$('.counter').addClass('animated fadeInDownBig');
	$('h3').addClass('animated fadeIn');

});
</script>

