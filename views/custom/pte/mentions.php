    <style type="text/css">
    	.text-explain{
    		font-size: 22px;
    	}
    </style>
<div id="sub-doc-page">
    <div class="col-xs-12 support-section section-home col-md-10 col-md-offset-1">
        <div class="col-xs-12 header-section">
        	<h3 class="title-section col-sm-8">Mentions légales</h3>
        	<hr>
        </div>
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">Éditeur du site :</h3>
        	<span class="col-xs-12 text-left text-explain">
        	    Collectif Eco Citoyen<br/>
        		Montreuil sur Mer<br/>
    		</span>
        </div> 
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">Hébergement :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		le site est hébergé chez OVH – <a href="https://www.ovh.com" target="_blank">https://www.ovh.com</a>
    		</span>
        </div>
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">Protection des données personnelles :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		Les informations collectées nous permettront de mieux vous connaître. Elles seront utilisées pour vous informer sur le PACTE de TRANSITION écologique et la construction de son programme.<br/><br/> Conformément aux dispositions de la loi Informatique et Libertés du 6 Janvier 1978, vous disposez d’un droit d’accès, de modification, de rectification et de suppression des données qui vous concernent. Pour exercer ce droit, adressez-vous à : Simon Sarazin, Montreuil Sur Mer
    		</span>
        </div>
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">Propriété intellectuelle :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		Les contenus de ce site sont soumis à la licence Creative Commons BY-SA 4.0. Pour améliorer l’accès à la culture et à la connaissance libres, #PTE autorise la redistribution et réutilisation libre de ses contenus pour n’importe quel usage. Une telle utilisation n’est autorisée que si l’auteur est précisé, et la liberté de réutilisation et de redistribution s’applique également à tout travail dérivé de ces contributions.
    		</span>
        </div>
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12"> Responsabilité quant au contenu :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		Alors même que #PTE s’efforce que le contenu de son site soit le plus à jour et le plus fiable possible, il ne peut donner aucune garantie quant à l’ensemble des informations présentes sur le site, qu’elles soient fournies par ses soins, par ses partenaires ou par tout tiers, par l’envoi d’e-mails ou de toute autre forme de communication.<br/><br/>
        		Compte tenu des caractéristiques du réseau Internet que sont la libre captation des informations diffusées et la difficulté, voire l’impossibilité, de contrôler l’utilisation qui pourrait en être faite par des tiers, #PTE n’apporte aucune garantie quant à l’utilisation des dites informations.<br/><br/>
        		#PTE ne saurait en conséquence être tenue pour responsable du préjudice ou dommage pouvant résulter de l’utilisation des informations présentes sur le site, ni d’erreurs ou omissions dans celles-ci.<br/><br/>
        		#PTE décline toute responsabilité en ce qui concerne les contenus des sites web édités par des tiers et accessibles depuis <a href="https://www.mayenne-demain.fr">www.mayenne-demain.fr</a> par des liens hypertextes ou répertoriés dans la cartographie du site.
    		</span>
        </div> 
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12"> Crédits :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		Le costum PTE a été développé par le collectif Pixel Humain sur la base du réseau <a href="https://www.communecter.org">communecter.org</a> . .
    		</span>
    		<span class="">Le site est hébergé par <a href="https://gitlab.adullact.net/pixelhumain" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/LOGO_PIXEL_HUMAIN.png" height=50></a></span>
        </div> 
    </div>
</div>
