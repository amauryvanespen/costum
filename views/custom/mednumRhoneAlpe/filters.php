<?php
//$cssAnsScriptFilesModule = array(
//	'/css/ctenat/filters.css',
//);
//HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl() );
?>

<!-- <div class="projectsProgress col-xs-12 col-md-10 col-md-offset-1 margin-bottom-20">
</div> -->
<!-- <div id="filterContainer" class="col-xs-12 col-md-10 col-md-offset-1">
</div -->

<script type="text/javascript">
	var pageApp= window.location.href.split("#")[1];
if (pageApp == "search") {
	var paramsFilter= {
	 	container : "#filters-nav",
	 	filters : {
            scope : {
                view : "scope",
                type : "scope",
                action : "scope"
            },
            actor : {
                view : "selectList",
                type : "filters",
                field : "typePlace",
                // field : "sourceType",
                name : "Type",
                action : "filters",
                list : [
                    "Amicale Laïque",
					"BIJ/PIJ",
					"Centre Social",
					"Cyberbase / Cybercentre",
					"Fablab / Hackerspace",
					"Maison de Quartier",
					"Maison des Services",
					"Médiathèque"
                ]
            },
	 		domainAction : {
	 			view : "selectList",
	 			type : "typePlace",
                // type : "sourceType",
	 			name : "Services",
	 			// action : "sourceType",
	 			action : "typePlace",
	 			list : costum.lists.domainAction
	 		}
         }  
     };

	function lazyFilters(time){
	  if(typeof filterObj != "undefined" ){
        pageApp = window.location.href.split("#")[1];
        
        filterGroup = filterObj.init(paramsFilter);
      }
	  else
	    setTimeout(function(){
	      lazyFilters(time+200)
	    }, time);
	}

	jQuery(document).ready(function() {
		lazyFilters(0);
	});
}
</script>