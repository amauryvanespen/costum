<?php HtmlHelper::registerCssAndScriptsFiles(array('/plugins/blockUI/jquery.blockUI.js'), Yii::app()->getRequest()->getBaseUrl(true)); ?>
<style type="text/css">
    @keyframes lds-dual-ring {
      0% {
        -webkit-transform: rotate(0);
        transform: rotate(0);
      }
      100% {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }
    @-webkit-keyframes lds-dual-ring {
      0% {
        -webkit-transform: rotate(0);
        transform: rotate(0);
      }
      100% {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }
    @keyframes lds-dual-ring_reverse {
      0% {
        -webkit-transform: rotate(0);
        transform: rotate(0);
      }
      100% {
        -webkit-transform: rotate(-360deg);
        transform: rotate(-360deg);
      }
    }
    @-webkit-keyframes lds-dual-ring_reverse {
      0% {
        -webkit-transform: rotate(0);
        transform: rotate(0);
      }
      100% {
        -webkit-transform: rotate(-360deg);
        transform: rotate(-360deg);
      }
    }
    @media (min-width: 992px){
        .left-logo{
            margin-top: 30px;
        }
    }
     @media (min-width: 768px) and (max-width: 991px){
        .loadingPageImg{
            margin-top: 40px;
        }
    }
    #overMind{
        z-index: 100000000;
        position: fixed;
        top:0px;
        left:0px;
        right:0px;
        bottom: 0px;
        background-color: white;
    }
    .lds-dual-ring {
        position: relative;
        line-height: 310px;
        -webkit-transform: translate(-100px, -100px) scale(1) translate(100px, 100px);
  transform: translate(-100px, -100px) scale(1) translate(100px, 100px);

    }
    #overMind .selectedCity {
    font-size: 30px;
    font-variant: small-caps;
    top: 65px !important;
    position: absolute;
    font-weight: 800;
    }
    .lds-dual-ring div {
    height: 360px !important;
    width: 360px !important;
    top: -35px !important;
    left: -15px !important;
    border: solid;
    border-width: 4px !important;
    border-radius:50%;
    position: absolute;
    -webkit-animation: lds-dual-ring 2s linear infinite;
    animation: lds-dual-ring 2s linear infinite;
}
.lds-dual-ring div:nth-child(2) {
       height: 350px !important;
    width: 350px !important;
    top: -30px !important;
    left: -10px !important;
}
</style>
<div id="overMind"></div>
<script type="text/javascript">
if(typeof costum.userPreferences != "undefined" && costum.userPreferences !== null && typeof costum.userPreferences.cities != "undefined" && costum.userPreferences.cities !== null){
    costum.scopeActivated=costum.scopeSelector[costum.userPreferences.cities[0]];
}
else if( localStorage !== null && localStorage.costum !== null){
    localStorageCostum=JSON.parse(localStorage.getItem("costum"));
    if(localStorageCostum !== null && typeof localStorageCostum[costum["slug"]] != "undefined" && typeof localStorageCostum[costum["slug"]].cities != "undefined"){
        costum.scopeActivated=costum.scopeSelector[localStorageCostum[costum["slug"]].cities[0]];
    }
}


jQuery(document).ready(function() { 
    themeObj.blockUi.setLoader=function(){
    if(costum!== null){
        logoLoader=costum.logo;
        if(typeof costum.css != "undefined" && typeof costum.css.loader !="undefined"){
            if(typeof costum.css.loader.ring1 != "undefined" && costum.css.loader.ring1.color != "undefined")
                color1=costum.css.loader.ring1.color;
            if(typeof costum.css.loader.ring2 != "undefined" && costum.css.loader.ring2.color != "undefined")
                color2=costum.css.loader.ring2.color;
        }
    }
    if(typeof costum.scopeActivated != "undefined"){
          themeObj.blockUi.processingMsg=
                '<div class="lds-css ng-scope">'+
                    '<div style="width:100%;height:100%" class="lds-dual-ring">'+
                        '<img src="'+logoLoader+'" class="loadingPageImg" height=80>'+
                        '<div style="border-color: transparent '+color2+' transparent '+color2+';"></div>'+
                        '<div style="border-color: transparent '+color1+' transparent '+color1+';"></div>'+
                        '<span class="selectedCity text-blue">'+costum.scopeActivated.name+'</span>'+
                    '</div>'+
            '</div>';
        $.blockUI({ message : themeObj.blockUi.processingMsg});
        setTimeout(function(){$("#overMind").remove();},300);
        setTimeout(function(){ urlCtrl.loadByHash(location.hash,true);},4000);
        myScopes.type="open";
        myScopes.open=costum.scopeSelector;
        $.each(myScopes.open, function(e, v){
            if(v.id == costum.scopeActivated.id)
                myScopes.open[e].active=true;
            else
                myScopes.open[e].active=false;
        });
        if(typeof costum.filters != "undefined"){
            costum.filters.scopes= {cities: costum.scopeActivated.id };
        }else{
            costum.filters={scopes : {cities: costum.scopeActivated.id}};            
        }
        costum.scopes=myScopes.open;
        localStorage.setItem("myScopes",JSON.stringify(myScopes));
    }else{
        $(".main-container").hide();
        $(".progressTop").val(40);

        setTimeout(function(){ $(".progressTop").val(60);},300);

        setTimeout(function(){ $(".progressTop").val(80);},300);

        setTimeout(function(){ $(".progressTop").val(100);},300);

        setTimeout(function(){ $(".progressTop").fadeOut(200);},100);
        
        $("#overMind").css({'background-image': 'url("'+assetPath+'/images/'+costum.slug+'/la_meuse_bis.jpg")'});
//        cssBlock=' initScope';
        //$(".blockUI.blockMsg.blockPage").addClass("initScope");
        initView=
            '<div class="col-xs-12">'+
                //'<h3 class="text-blue">Bienvenu-e sur</h3>'+
                '<div class="col-xs-12">'+
                    '<div class="col-xs-12 col-sm-6 col-md-4 left-logo">'+
                        '<span class="subTextExplain text-center col-xs-12">Et si on se communectait ?</span><br/><br/>'+
                        '<span class="textExplain text-center col-xs-12">Vous êtes au bon endroit</span>'+
                            '<span class="subTextExplain text-center col-xs-12 visible-sm" style="font-size:20px;"><br/>L’ambition de cette plateforme est de rassembler, en un seul lieu numérique, toutes les initiatives associatives & citoyennes d’une commune, de faciliter leurs interactions et de susciter le contact entre tous ses acteurs.</span>'+
                    
                    '</div>'+
                    '<div class="col-xs-12 col-sm-6 col-md-4 text-center"><img src="'+logoLoader+'" class="loadingPageImg" height=120></div>'+
                    '<div class="col-xs-12 col-sm-6 col-md-4 hidden-sm">'+
                        '<span class="subTextExplain text-center col-xs-12" style="font-size:20px;">L’ambition de cette plateforme est de rassembler, en un seul lieu numérique, toutes les initiatives associatives & citoyennes d’une commune, de faciliter leurs interactions et de susciter le contact entre tous ses acteurs.</span>'+
                    '</div>'+
                '</div>'+
                '<span class="col-xs-12 text-center padding-20 textExplain">Prêt.e.s ?</span>'+
                
                '<span class="col-xs-12 text-center padding-20 subTextExplain margin-bottom-20">Choisissez d\'abord votre commune</span>'+
                '<div class="selectMeuseScope col-xs-12 text-center">';
                $.each(costum.scopeSelector, function(e, v){
                    initView+="<button class='btn bg-white init-selector-city col-xs-8 col-xs-offset-2 col-sm-4' "+
                        ' onclick="initMeuseScopeSelection(\''+e+'\')">'+
                            "<span class='col-xs-12 text-center color-btn-"+v.name+"'>"+v.name+"</span>"+
                            "<img src='"+assetPath+"/images/"+costum.slug+"/"+v.name+"/select-pic.jpg' class='img-responsive'/>"
                        "</button>";
                });
            initView+='</div></div>';
            $("#overMind").html(initView);
        //$.blockUI({ message : themeObj.blockUi.processingMsg, blockMsgClass : "blockMsg"+cssBlock});

        setTimeout(function(){/*$("#overMind").remove(); $("#firstLoader").remove();*/}, 500);
        
    }
};

themeObj.firstLoad=function(){  
    themeObj.blockUi.setLoader();
}
});
</script>