<?php  
$cssAnsScriptFilesTheme = array(
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js'            
    );
// $cssAndScriptFilesModule = array(
//     '/js/default/profilSocial.js',
// );

$cssAndScriptFilesModuleForNews = array(
    '/js/news/index.js',
);

$cssAnsScriptFilesModule = array( 
    '/leaflet/leaflet.css',
    '/leaflet/leaflet.js',
    '/css/map.css',
    '/markercluster/MarkerCluster.css',
    '/markercluster/MarkerCluster.Default.css',
    '/markercluster/leaflet.markercluster.js',
    '/js/map.js',
);
// HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Map::MODULE )->getAssetsUrl() );
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

$colorSectionAgenda = @Yii::app()->session["costum"]["cms"]["colorSectionAgenda"];
$colorTitleAgenda = @Yii::app()->session["costum"]["cms"]["colorTitleAgenda"];
$colorSectionActu = @Yii::app()->session["costum"]["cms"]["colorSectionActu"];
$colorTitleActu = @Yii::app()->session["costum"]["cms"]["colorTitleActu"];
$colorCardEvent = @Yii::app()->session["costum"]["cms"]["colorCardEvent"];
$colorPathPlus = @Yii::app()->session["costum"]["cms"]["colorPathPlus"];
?>

<style type="text/css">
    .hexagon {
    overflow: hidden;
    visibility: hidden;
    -webkit-transform: rotate(120deg);
       -moz-transform: rotate(120deg);
        -ms-transform: rotate(120deg);
         -o-transform: rotate(120deg);
            transform: rotate(120deg);
    cursor: pointer;
}

.hexagon1 {
    width: 485px;
    height: 250px;
    margin: 0 0 0 -80px;
}

footer{
    margin-top: -1%;
}

.hexagon-in1 {
    overflow: hidden;
    width: 100%;
    height: 100%;
    -webkit-transform: rotate(-60deg);
       -moz-transform: rotate(-60deg);
        -ms-transform: rotate(-60deg);
         -o-transform: rotate(-60deg);
            transform: rotate(-60deg);
}

.hexagon-in2 {
    width: 100%;
    height: 100%;
    background-repeat: no-repeat;
    background-position: 50%;
    visibility: visible;
    -webkit-transform: rotate(-60deg);
       -moz-transform: rotate(-60deg);
        -ms-transform: rotate(-60deg);
         -o-transform: rotate(-60deg);
            transform: rotate(-60deg);
}
    .img-hexa{
        margin-top: -25%;
    }

    .info-card{
        border-radius: 15px;
        padding: 3%;
        font-size: 25px;
        color : white;
        margin-left: 13%;
        margin-right: 13%;
    }
</style>
<div style="background-color: white" class="col-xs-12 col-lg-12">
    <center>
    	<div style="margin-top: 2vw" class="col-xs-12 col-lg-12">
            <img style="margin-bottom: 9vw;" class="img-responsive ultra" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/hubMedNum/hub-01.png'>
        </div>
    </center>
</div>

	<div class="hidden-xs col-lg-12" id="searchBar" style="margin-left: 9vw;">
        <input type="text" class="form-control pull-left text-center main-search-bars" id="second-search-bar" placeholder="je recherche une structure d’accompagnement, un atelier d’apprentissage, …">
            <a data-type="filters" href="javascript:;"> 
                <span id="second-search-bar-addon-mednum"  class="text-white input-group-addon pull-left main-search-bar-addon-mednum">
                  <i class="fa fa-search searchIcone"></i>
                </span>
            </a> 
    </div>

	<div style="margin-left: 12vw;" id="dropdown" class="dropdown-result-global-search hidden-xs col-sm-5 col-md-5 col-lg-5 no-padding">
	</div>


<div style="background-color: #d7e4e0;">
    <div class="explication-acteurs row">
        <div class="explication-title col-xs-12 col-sm-12 col-lg-12">
            <div>
                <h1 style="color:<?php echo $colorSectionActu ?>;" class="titleBandeau"><i class="fa fa-users" aria-hidden="true"></i>  Les différents acteurs de la médiation numérique</h1>
            </div>
        </div>

        <div id="containcommunity" style="margin-top: 5vw;" class="col-xs-12 col-lg-12 col-sm-12">
                
        </div>

        <div style="margin-top: 3vw;margin-bottom: 2vw;" class="text-center container col-lg-12 col-sm-12 col-xs-12">
            <a href="javascript:;" data-hash="#annuaire" class="lbh-menu-app btn btn-redirect-home btn-small-orange">
                <svg style="border-radius: 38px;box-shadow: 0px 0px 8px gray;background-color: white" fill="white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="64px" height="64px">
                    <path fill="transparent" d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/>
                    <path fill="black" d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/>
                </svg>
            </a>
        </div>
    </div>
</div>


<div class="col-lg-12 col-md-12 col-xs-12" style="background-color: black;">
	<div class="explication row">
	    <div class="explication-title col-xs-12 col-sm-12 col-lg-12">
	        <div>
	            <h1 style="color:<?php echo $colorSectionActu ?>;" class="titleBandeau"><i class="fa fa-newspaper-o" aria-hidden="true"></i>  Actualité</h1>
	        </div>
	    </div>
	    <!-- NEWS -->
	    <div style="margin-left: -5vw;width: 89vw;margin-top: 5vw;background-color: white" id="newsstream" class="col-xs-10 col-sm-12">
	        <div style="background-color: white;">
	        </div>
	    </div>

	    <div style="margin-top: 3vw;margin-bottom: 2vw;" class="text-center container col-lg-12 col-sm-12 col-xs-12">
            <a href="javascript:;" data-hash="#annuaire" class="lbh-menu-app btn btn-redirect-home btn-small-orange">
                <svg style="border-radius: 38px;box-shadow: 0px 0px 8px gray;background-color: white" fill="white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="64px" height="64px">
                    <path fill="transparent" d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/>
                    <path fill="black" d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/>
                </svg>
            </a>
        </div>
	</div>
</div>

<div class="col-lg-12 col-xs-12 col-md-12" style="background-color: #d7e4e0;">
	<div class="explicationAgenda row">
	    <div class="explication-title col-xs-12 col-sm-12 col-lg-12">
	        <div>
	            <h1 style="color:<?php echo $colorSectionAgenda ?>" class="titleBandeau"><i class="fa fa-calendar" aria-hidden="true"></i>  Agenda</h1>
	        </div>
	    </div>

        <div style="margin-top: 7vw;" id="containEvent" class="col-lg-12 col-xs-12">
            
        </div>

	    <div style="margin-top: 3vw;margin-bottom: 2vw;" class="text-center container col-lg-12 col-sm-12 col-xs-12">
            <a href="javascript:;" data-hash="#agenda" class="lbh-menu-app btn btn-redirect-home btn-small-orange">
                <svg style="border-radius: 38px;box-shadow: 0px 0px 8px gray;background-color: white" fill="white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="64px" height="64px">
                    <path fill="transparent" d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/>
                    <path fill="black" d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/>
                </svg>
            </a>
        </div>
	</div>
</div>

<div style="background-color: black;" class="row">
    <div class="explication-title col-xs-12 col-sm-12 col-lg-12">
        <div>
            <h1 style="color:<?php echo $colorTitleAgenda; ?>;" class="titleBandeau"><i class="fa fa-map-marker" aria-hidden="true"></i>  La carte des acteurs</h1>
        </div>
    </div>
    <!--Ajout de la map -->
    <div style="margin-top: 1vw;" class="col-xs-12 mapBackground no-padding" id="mapHubMednum">

    </div>

    <!-- <div style="margin-top: 3vw;margin-bottom: 2vw;" class="text-center container col-lg-12 col-sm-12 col-xs-12">
            <a href="javascript:;" data-hash="#map" class="lbh-menu-app btn btn-redirect-home btn-small-orange">
                <svg style="border-radius: 38px;box-shadow: 0px 0px 8px gray;" fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="64px" height="64px">
                    <path fill="transparent" d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/>
                    <path fill="<?php //echo $colorPathPlus ?>" d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/>
                </svg>
            </a>
        </div> -->
</div>  
<script type="text/javascript">
	jQuery(document).ready(function() {

    var mapHubMednumHome = {};

    urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";

    ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false, members:true}, function(news){}, "html");

    

    afficheCommunity();
    afficheEvent();
});


	function afficheCommunity(){
    mylog.log("----------------- Affichage évènement");

    var params = {
        contextId : costum.contextId,
        contextType : costum.contextType
    };

    paramsMapHubMednum = {
        zoom : 3,
        container : "mapHubMednum",
        activePopUp : true,
        tile : "mapbox",
        menuRight : true,
        mapOpt:{
            latLon : ["-21.115141", "55.536384"],
        }
    };

    mapHubMednumHome = mapObj.init(paramsMapHubMednum);

    $.ajax({
        type : "POST",
        url : baseUrl + "/costum/hubmednum/getcommunityaction",
        data : params,
        dataType : "json",
        async : false,
        success : function(data){
            mylog.log("success : ",data);
            var str = "";
            var ctr = "";
            var itr = "";
            var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.events.defaultImg;
            var ph = "<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>";

            var i = 1;
            mapHubMednumHome.addElts(data.elt);
            
            if(data.result == true){

                test = Object.values(data.elt);

                $(test).each(function(key,value){
                    i = i + 1;
                    if ( i <=4) {
                        
                        var startDate = (typeof value.startDate != "undefined") ? "Du "+dateToStr(value.startDate, "fr", false, false) : null;

                        var costum = baseUrl+"/costum/co/index/slug/"+value.slug;  

                        var imgMedium = (value.imgMedium != "none") ? baseUrl + value.imgMedium : url;
                        var img = (value.img != "none") ? baseUrl + value.img : url;  
                        
                        str += '<div class="card">';
                        str += '<div class="card-color col-md-4">';
                        str += '<div class="info-card text-center">';
                        str += '<a style="color:#DF522C" target="_blank" href="'+costum+'">';
                        str += '<div style="border-radius: 100vw;box-shadow: 0vw 0vw 1vw -5px gray;">';
                        str += '<img style="border-radius:100vw;" class="img-responsive" src="'+img+'"><br>';
                        str += '</div>';
                        str += '<p style="margin-top:1vw;color:black">'+value.name+"</a><br><p style='text-align: center;color:black'>"+value.countActeurs+' Acteur(s)<br>'+value.countActus+' Actu(s)<br>'+value.countEvent+' Évènement(s)<br>'+value.countProjet+' Projets'+'</p></p>';
                        // str += '<p class="pull-left col-lg-6" style="font-size:15px;">''</p>';
                        str += '</div>';
                        str += '</div>';
                        str += '</div>';
                    }
                    else{
                        $("#containcommunity").html(str);
                        mylog.log(str);
                    }
                });
            }
            else{
                str += "<div class='col-xs-12 col-sm-12 col-md-12'><b class='p-mobile-description' >Aucun évènement n'est prévu</b></div>";
            }
            $("#containcommunity").html(str);
            mylog.log(str);
        },
        error : function(e){
            mylog.log("error : ",e);
        }
    });
}

function afficheEvent(){
    mylog.log("----------------- Affichage évènement");

    var params = {
        contextId : costum.contextId,
        contextType : costum.contextType
    };

    $.ajax({
        type : "POST",
        data : params,
        url : baseUrl + "/costum/hubmednum/geteventcommunityaction",
        dataType : "json",
        async : false,
        success : function(data){
            mylog.log("success : ",data);
            var str = "";
            var ctr = "";
            var itr = "";
            var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.events.defaultImg;
            var ph = "<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>";

            var fleches = ph + "/images/smarterritoireautre/fleche-plus-noire.svg";
            
            var i = 1;
            
            if(data.result == true){
                
                $(data.element).each(function(key,value){
                    mylog.log("data.element",data.element);
                    i = i + 1;
                    if ( i <=4) {

                    var startDate = (typeof value.startDate != "undefined") ? "Du "+dateToStr(value.startDate, "fr", false, false) : null;  

                    var imgMedium = (value.imgMedium != "none") ? baseUrl + value.imgMedium : url;
                    var img = (value.img != "none") ? baseUrl + value.img : url;
                    
                    
                    str += '<div class="card">';
                    str += '<div id="event-affiche" class="card-color col-md-4">';
                    str += '<a style="color:white;" href="#@'+value.slug+'" class="lbh"><?php echo Yii::t("common", "") ?>';
                    str += '<div id="affichedate" class="info-card text-center">';
                    str += '<center><p id="text-description'+i+'" class="text-center col-lg-8 col-md-12" style="display:none;color:white;position:absolute;z-index:1000;margin-left: -11vw;">';
                    str += value.resume;
                    str += '</p></center>';
                    str += '<img id="'+i+'" width="100%" src="'+imgMedium+'" class="img-fluid img-event">';
                    str += '<p style="background-color:black;color:white;" class="text-center col-lg-12 col-md-12">';
                    str += value.name;
                    str += '</p>';
                    str += '</a>';
                    str += '</div>';
                    str += '</div>';
                    str += '</div>';
                }
                });
            }
            else{
                str += "<div class='col-xs-12 col-sm-12 col-md-12'><b class='p-mobile-description' >Aucun évènement n'est prévu</b></div>";
            }
            $("#containEvent").html(str);
            mylog.log(str);
        },
        error : function(e){
            mylog.log("error : ",e);
        }
    });

    $(".img-event").mouseover(function(){
        $(this).css("-webkit-filter","brightness(20%)");
        $("#text-description"+$(this).attr("id")).css("display","initial");
    });

    $(".img-event").mouseleave(function(){
        $(this).css("-webkit-filter","brightness(100%)");
        $("#text-description"+$(this).attr("id")).css("display","none");
    });
}

</script>