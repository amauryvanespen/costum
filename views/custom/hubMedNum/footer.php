<footer style="background-color:#DF522C" class="text-center col-xs-12  col-lg-12 col-sm-12 no-padding">
    <div class="row" style="margin-top: 3%;">
        <div class="col-xs-12 col-sm-12 col-lg-12 text-center col-footer col-footer-step">
            <a class="col-lg-4 col-xs-4" style="color:white" href="#" class="lbh-menu-app">
            	<img style="width:9vw;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/hubMedNum/solidarnum.png">
            	</br>
            	Solidarnum
            </a>
            <a class="col-lg-4 col-xs-4" style="color: white" href="https://societenumerique.gouv.fr/" class="lbh-menu-app">
            	<img style="width:9vw;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/hubMedNum/logomarianne_typo-sombre.png">
            	</br>
            	Société du numérique
            </a>
            <a class="col-lg-4 col-xs-4" style="color: white" href="https://www.banquedesterritoires.fr/" class="lbh-menu-app">
                <img style="width:9vw;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/hubMedNum/banque-territoire.png">
                </br>
                Banque des territoires
            </a>
        </div>
    </div>
</footer>
