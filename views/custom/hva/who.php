
<style type="text/css">

.menuWho .nav-tabs a{
	background-color: #a0cd3f;
}

.menuWho .nav-tabs .active a, .menuWho .nav-tabs .active a:hover{
	background-color: #257c87 !important ;
	color : white !important ;
}

.contentWho{
	background-color: #faf8e5 ;
}

</style>
<div class=" textHVA col-xs-10 col-xs-offset-1 padding-10">

	<div class="menuWho">
		<ul class="row nav nav-tabs text-center" style="font-size: 18px">
			<li class="nav-tab col-sm-4 col-xs-12  active"><a data-toggle="tab" href="#presentation"><b>Présentation</b></a></li>
			<li class="nav-tab col-sm-4 col-xs-12 "><a data-toggle="tab" href="#contribuer"><b>Inscription</b></a></li>
			<!-- <li class="nav-tab col-sm-3 col-xs-12 "><a data-toggle="tab" href="#history"><b>L'histoire</b></a></li>
			<li class="nav-tab col-sm-3 col-xs-12 "><a data-toggle="tab" href="#status"><b>Les statuts</b></a></li> -->
			<li class="nav-tab col-sm-4 col-xs-12 "><a data-toggle="tab" href="#rihva"><b>Activation</b></a></li>
		</ul>
	</div>

	<div class="contentWho tab-content col-xs-12 padding-10">
		<div id="presentation" class="tab-pane active row col-xs-10 col-xs-offset-1">
			Découlant de la volonté d'acteurs impliqués dans l'économie sociale, solidaire et écologique du territoire, l'association <b>RIHVA</b> (Réseau des Initiatives de la Haute Vallée de l'Aude) a créé ce site dans la lignée de ses événements comme la Tambouille des Initiatives, le Festi-Forum des Associations et la Fête des Plantes.<br/><br/>

			<center><span>Il permet de :</span></center> <br/>
			<ul>
				<li>
					<b>Promouvoir les activités des acteurs locaux par une vitrine qui :</b>
					<ul>
						<li>Augmente leur visibilité et leur mutualisation.</li>
						<li>Allège leurs efforts de communication en réduisant leurs coûts de conception, de publication et de distribution de leurs informations.</li>
						<li>Consolide leur ancrage au sein du territoire en accroissant leur panel d'utilisateurs, de consommateurs.</li>
					</ul>
				</li>
				<li>
					<b>Offrir aux habitants et aux visiteurs un espace dédié qui :</b>
					<ul>
						<li>Concentre, classe et filtre les informations pour faciliter la recherche, le choix et l'identification.</li>
						<li>Encourage la rencontre, l'échange intergénérationnel, la participation, l'implication.</li>
						<li>Aide à la découverte des nombreuses et dynamiques ressources locales.</li>
						<li>Réduit l'impact écologique en rationalisant les moyens de communications.</li>
					</ul>
				</li>
			</ul>
			<center>L'historique :</center> <br/>

			En  2016 à Greffeil ont eu lieu les rencontres annuelles des amis de François De Ravignan ayant pour thème ''<b>Relier les Initiatives de la Haute Vallée de l'Aude</b>''.  Les échanges sur ce sujet ont permis de mettre en exergue un ensemble de constats assez particuliers:
			<ul class="niv1">
				<li><b>Un foisonnement d'acteurs et de structures collectives</b>, pour la plupart nouvellement arrivés sur ce territoire, œuvrant principalement dans des domaines sociaux, environnementaux et culturels et portés par une <b>vision commune d'un vivre ensemble bienveillant, épanouissant et réjouissant.</b></li>

				<li>Mais ce tissu, quoique dynamique, souffre d'un manque de réelles synergies entre les initiateurs et de carence de visibilité de leurs actions, les deux étant souvent liées à des problématiques rurales et organisationnelles.</li>
				<li><b>Un besoin apparaissait donc en faveur de la valorisation et du renforcement de ces ressources et de ces potentialités</b> locales inscrites dans une démarche de développement social, culturel et économique local.</b></li>
			</ul>
			<br/>
			Les rencontres suivantes des ami-es de François de Ravignan à Serres, Luc/Aude, Festes, Rouvenac, ...ainsi que plusieurs ateliers organisés par des acteurs-initiateurs locaux intéressés à creuser ce sujet, ont montré une convergence d'idées et des pistes de propositions qui exprimaient :

			<ul class="niv1">
				<li><b>Une perception commune</b> que la co-construction d’événements ainsi que leurs efforts de publication pour promouvoir leurs activités était sacrément chronophages et énergivores avec parfois au final des ressentis de faible efficacité et de gaspillage, tous deux créateurs de frustration. </li>

				<li><b>Le souhait de renforcer</b> les liens entre les nouveaux ''arrivants''  sur ce territoire cosmopolite et les habitants autochtones aux structures plus institutionnelles.</li>

				<li>S'est aussi <b>affichée une volonté d'orientation ''éthique''</b> respectant les valeurs d'une démarche solidaire et écologique dans un cadre d'un développement local de transition.</li>
			</ul>

			<br/>

			Pour donner une autre dimension à leurs actions, il s'avérait ainsi nécessaire de faciliter leur mise en réseau par la mutualisation de leurs différents canaux d'informations et donc de <b>créer un outil de communication collaboratif dans la contribution et la diffusion. </b>
		</div>
		<div id="contribuer" class="tab-pane row col-xs-10 col-xs-offset-1">
			<span>
				Vous souhaitez rendre <b>votre activité, vos initiatives plus visibles et plus lisibles</b> et ainsi de renforcer votre ancage sur les deux communautés de communes du Limouxin et des Pyrénées Audoises.<br/>
				Ce site vous offre un <b>mode de communication plus rapide et plus efficace</b> entre vous acteurs porteurs d'initiatives (contributeurs au site) et tout visiteur, en procurant à chacun un <b>regard plus précis, plus exhaustif et plus pertinent</b> sur toutes les activités du territoire et en permettant des <b>mises en relation plus efficientes.</b><br/>

				Il est construit autour d'une <b>charte</b> qui rassemble certaines valeurs afin de promouvoir une information indépendante, cohérente et de confiance.<br/> <br/>

				<!-- Ce site est construit autour d'une <b>charte</b> qui rassemble certaines valeurs afin de promouvoir une information indépendante, cohérente et de confiance. <br/>
				<b>Créer un compte sur <a href="https://www.portailhva.org/" >www.portailhva.org</a> implique votre acceptation implicite de la charte ci dessous : </b> <br/> <br/> -->

				<center><b>Contribuer au portail implique:</b></center><br/>

				<b>1. L'acceptation de la charte ci dessous: </b><br/>
				Ce portail regroupe et présente les acteurs qui œuvrent dans la Haute Vallée de l'Aude avec comme <b>engagements communs</b> de: <br/>

				<ul>
					<li>Valoriser et renforcer les ressources et les potentiels de la haute vallée de l'Aude dans une perspective de “transition” vers une société plus humaine, plus solidaire et plus respectueuse de l’environnement.</li>
					<li>Faciliter la découverte et la promotion ainsi que la mise en réseau de toutes les initiatives locales qui sont utiles socialement, écologiquement et culturellement.</li>
					<li>Impulser une véritable participation citoyenne en encourageant à échanger et à agir collectivement pour un mieux vivre ensemble.</li>
					<li>Favoriser le pluralisme d'une expression libre, démocratique, non prosélyte et qui ne promeut aucune forme de domination (économique, sexiste, raciste, ...).</li>
					<li>Porter un message de progrès social et écologique dont le but premier ne soit pas purement lucratif.</li>
					<li>Contribuer par son implication personnelle et sa responsabilisation individuelle à la vie harmonieuse et pérenne du portail.</li>

				</ul>
				<b>2. L'adhésion à l'Association RIHVA </b>(Réseau des Initiatives de la Haute Vallée de
l’Aude):<br/>
					Cette association a pour objet de <b>faciliter la mise en réseau</b> des acteurs porteurs d'initiatives en haute vallée de l'Aude est aussi organisatrice de la <b>Tambouille des Initiatives, du Festi-Forum et de la Fête de Plantes.</b> <br/>

					<ul>
						<li>Elle garantit un fonctionnement démocratique, transparent et assure le caractère désintéressé de sa gestion mais ce site ne sera pérenne que grâce à votre <b>contribution financière et bien-sur et surtout à vos contributions rédactionnelles.</b></li>
						<li>Le montant minimum annuel est seulement de <b>20€.</b> Vous trouverez le <b>bulletin d'adhésion</b> en cliquant sur le bouton ci-dessous.</li>
						<li>Il est à remplir et à renvoyer avec votre règlement. Un reçu pourra vous être délivré à la demande.</li>
						<li><b>Votre compte ne sera actif qu’après réception de votre réglement.</b></li>

					</ul>

					<div class="col-xs-12 text-center">
						<a style="background-color: #5b2649 !important; color : white;"
							href="https://www.communecter.org/upload/communecter/organizations/5b713de540bb4eb87a808363/file/1583244016_bulletin-dadhYosion-2020.pdf" target="_blanc" class="btn btn-default">Bulletin d'adhésion</a>
					</div>

				


				<!-- <center> <b> Important </b> </center><br/> <br/>

				Ce portail <b>existe et n'est pérenne</b> que grâce à vos <b>contributions rédactionnelles mais aussi financières</b> dont <b>le montant minimum annuel est de 20€.</b>  Ce montant est à s’acquitter en envoyant un chèque à l’ordre Réseau des Initiatives de la Haute Vallée de l’Aude à l’adresse suivante : Bennavail Georges, 1 chemin de camières, 11260 Rouvenac.  <br/> <br/>
				<b>Votre compte ne sera actif qu’après réception de votre paiement.</b><br/> <br/> -->
			</span>
	
		</div>
		<!-- <div id="history" class="tab-pane row col-xs-10 col-xs-offset-1">
			En  2016 à Greffeil ont eu lieu les rencontres annuelles des amis de François De Ravignan ayant pour thème <b>''Relier les Initiatives de la Haute Vallée de l'Aude''. </b>
			Les échanges sur ce sujet ont permis de mettre en exergue un ensemble de constats assez particuliers:<br/><br/>
			<ul class="niv1">
			<li><b>Un foisonnement d'acteurs et de structures collectives</b>, pour la plupart nouvellement arrivés sur ce territoire, œuvrant principalement dans des domaines sociaux, environnementaux et culturels et portés par une vision commune d'un vivre ensemble bienveillant, épanouissant et réjouissant.</li>

			<li>Mais ce tissu, quoique dynamique, souffre d'un manque de réelles synergies entre les initiateurs et de carence de visibilité de leurs actions, les deux étant souvent liées à des problématiques rurales et organisationnelles.</li>
			<li><b>Un besoin apparaissait donc en faveur de la valorisation et du renforcement de ces ressources et de ces potentialités locales inscrites dans une démarche de développement social et économique local.</b></li>
			</ul><br/><br/>
			Les rencontres suivantes des ami-es de François de Ravignan à Serres, Luc/Aude, Festes, Rouvenac, ...ainsi que plusieurs ateliers organisés par des acteurs-initiateurs locaux intéressés à creuser ce sujet, ont montré une convergence d'idées et des pistes de propositions qui exprimaient :

			<ul class="niv1">
				<li><b>Une perception commune</b> que la co-construction d’événements ainsi que leurs efforts de publication pour promouvoir leurs activités était sacrément chronophages et énergivores avec parfois au final des ressentis de faible efficacité et de gaspillage, tous deux créateurs de frustration.</li>

				<li><b>Le souhait de renforcer</b> les liens entre les nouveaux ''arrivants''  sur ce territoire cosmopolite et les habitants autochtones aux structures plus institutionnelles.</li>

				<li>S'est aussi <b>affichée une volonté</b> d'orientation ''éthique'' respectant les valeurs d'une démarche solidaire et écologique dans un cadre d'un développement local de transition.</li>
			</ul>

			<br/>

			Pour donner une autre dimension à leurs actions, il s'avérait ainsi nécessaire de faciliter leur mise en réseau par la mutualisation de leurs différents canaux d'informations et donc de <b>créer un outil de communication collaboratif dans la contribution et la diffusion.</b>
			
			<br/>  <br/>
			En se réunissant plus fréquemment depuis le début d'année 2018, un collectif composé d'une quinzaine d'acteurs locaux s'est alors attelé à élaborer <b>un portail numérique</b> convivial, facile, rapide et actualisé.
			<br/>  <br/>

			Il voit ainsi le jour en 2019 !
		</div>
		<div id="status" class="tab-pane row col-xs-10 col-xs-offset-1">

<b>Article 1er : déclaration</b> <br/><br/>

Il est fondé entre les adhérents aux présents statuts une association collégiale régie par la loi du 1er juillet 1901 et le décret du 16 août 1901 dite 
« Réseau des Initiatives de la Haute Vallée de l'Aude ».<br/><br/>

<b>Article 2 : objets</b><br/><br/>

Cette association a pour objet de faciliter la mise en réseau des acteurs porteurs d'initiatives en haute vallée de l'Aude.<br/>
Par ailleurs, l’association inscrit son projet dans une dimension d’intérêt général en s’ouvrant à tous les publics, notamment les plus fragiles. En toutes circonstances, l’association garantit un fonctionnement démocratique, transparent et assure le caractère désintéressé de sa gestion.<br/><br/>

<b>Article 3 : siège social</b><br/><br/>

Le siège social de l’association est fixé au 1 chemin de Camières, 11260 Rouvenac.<br/>
Il pourra être transféré par simple décision du conseil d’administration ; la ratification par l’assemblée générale sera nécessaire.<br/><br/>

<b>Article 4 : durée</b><br/><br/>

La durée de l’association est illimitée.<br/><br/>

<b>Article 5 : membres</b><br/><br/>

L’association se compose de membres actifs, personnes physiques ou morales.
- conditions d’admission 
Pour faire partie de l’association, il faut être agréé par le conseil d’administration qui statue, lors de ses réunions, sur les demandes d’admission.
- qualités requises
Sont membres actifs les personnes physiques ou morales qui sont à jour de leur cotisation  annuelle fixée lors de la dernière assemblée générale.
La qualité de membre se perd par :
<ul>
	<li>la démission</li>
	<li>le décès</li>
	<li>la radiation prononcée par le conseil d’administration pour non-paiement de la cotisation ou pour motif grave (ex : non respect des objets, du règlement intérieur, de la charte,  non réponse à la convocation,...).</li>
</ul>


<br/><br/>

							
<b>Article 6 : ressources</b><br/><br/>

Les ressources de l’association comprennent :
<ul>
	<li>le produit des cotisations, dont le montant est fixé chaque année par le conseil d’administration </li>
	<li>les subventions de l’état, des régions, des départements, des communes, des communautés de communes, des établissements publics, de l'Europe.</li>
	<li>du produit de manifestations, des intérêts des biens et valeurs qu’elle pourrait posséder ainsi que des rétributions pour services rendus,</li>
	<li>des toutes autres ressources ou subventions qui ne seraient pas contraires aux lois en vigueur.</li>
</ul>


<br/><br/>

<b>Article 7 : conseil d’administration </b><br/><br/>

La direction de l’association est assurée par un conseil d’administration collégial composé d'au moins cinq membres.
Les membres sont élus pour une année par l’assemblée générale.
Les membres du conseil d’administration collégial sont rééligibles. 
En cas de vacances, de démission, d’exclusion ou de décès, le conseil d’administration pourvoit provisoirement au remplacement de ses membres. Il est procédé à leur remplacement définitif lors de l’assemblée générale suivante. Les pouvoirs des membres ainsi élus prennent fin à l’époque où devrait normalement expirer le mandat des membres remplacés.
Nul ne peut faire partie du conseil s’il n’est pas majeur.<br/><br/>

Le conseil d'administration assure la conduite collective des objectifs et des projets de l’association, et participe à la mise en place des orientations et actions prévues par l’assemblée générale.
Il est investi des pouvoirs nécessaires au fonctionnement de l’association, et peut ainsi agir en toutes circonstances en son nom, notamment sur le plan légal.<br/><br/>

Le conseil d’administration collégial est l’organe qui représente légalement l’association en justice. En cas de poursuites judiciaires, les membres du conseil d’administration collégial en place au moment des faits prendront collectivement et solidairement leurs responsabilités devant les tribunaux compétents. Aucun membre de l’association n’est personnellement responsable des engagements contractés par elle. <br/><br/>

Un ou plusieurs des membres du conseil d’administration sont désignés pour représenter l’association dans tous les autres actes de la vie civile et dans ses rapports avec les médias et l’administration. Ils peuvent ainsi être habilités à remplir, au cours d’une période déterminée, toutes les formalités de déclaration et de publication prescrites par la législation et tout autre acte administratif nécessaire au fonctionnement de l’association.<br/><br/>

Le conseil se réunit au moins une fois tous les six mois ou à la demande d’un tiers au moins de ses membres. 
Tout membre du conseil qui, sans excuse valable, n’aura pas assisté à trois réunions consécutives pourra être considéré comme démissionnaire.<br/><br/>


<b>Article 8 : prise de décisions</b><br/><br/>

L’association et ses organes décisionnels s’efforceront de prendre leurs décisions par consentement des membres présents dans l’objectif d’inclure l’opinion de chacun(e), la participation de tous sans pour autant l’imposer. Le consentement est atteint lorsqu’une décision ne rencontre pas ou plus d'objection.<br/><br/>

<b>Article 9 : assemblée générale ordinaire</b><br/><br/>

L’assemblée générale ordinaire comprend tous les membres de l’association à jour de leur cotisation annuelle. <br/>L’assemblée générale ordinaire se réunit une fois par an.<br/>
Les membres sont convoqués quinze jours au moins avant la date fixée. L’ordre du jour est indiqué sur les convocations.<br/><br/>

L’assemblé générale ordinaire présente aux membres:
<ul>
	<li>le rapport moral et d’activités</li>
	<li>le rapport financier</li>
	<li>les perspectives et le budget prévisionnel</li>
</ul>

Elle demande l'approbation des comptes. 
Il est procédé à l'élection des membres du nouveau conseil d'administration.<br/><br/>

Les résolutions de l’assemblée générale ordinaire sont prises au consentement des membres  présents. En cas d'impossibilité de résolutions, le conseil d'administration convoque une assemblée générale extraordinaire.<br/><br/>

<b>Article 10 : assemblée générale extraordinaire</b><br/><br/>

Une assemblée générale extraordinaire peut être convoquée sur décision du conseil d’administration ou sur la demande d’au moins un tiers des membres de l'association.
Les membres sont convoqués quinze jours au moins avant la date fixée. L’ordre du jour est indiqué sur les convocations.
Les résolutions de l’assemblée générale extraordinaire sont prises au consentement ou à défaut à la majorité absolue des membres présents.<br/><br/>

<b>Article 11 : règlement intérieur</b><br/><br/>

Un règlement intérieur peut être établi par le conseil d’administration qui le fait alors approuver par l’assemblée générale.<br/><br/>

<b>Article 12 : dissolution</b><br/><br/>

La dissolution est prononcée à la demande du conseil d’administration par une assemblée générale extraordinaire convoquée spécialement à cet effet dans les conditions prévues à l’art. 10 des statuts. 
En cas de dissolution, l’assemblée générale extraordinaire désigne un ou deux liquidateurs qui seront chargés de la liquidation des biens de l’association conformément à l’art. 9 de la loi du 1er juillet 1901 et à l’art. 15 du décret du 16 août 1901.<br/><br/>


Statuts créés et votés lors de l’assemblée générale constitutive du 14 août 2018 à Rouvenac<br/><br/>
		</div> -->
		<div id="rihva" class="tab-pane row col-xs-10 col-xs-offset-1">
			<center> <b> Procédure</b> </center><br/>
			<ul>
				<li>Créez <b>votre compte personnel</b> en cliquant sur le bouton ci dessous. Identifiez vous avec votre adresse courriel personnelle.</li>
				<li>Vous créez ainsi votre page à renseigner comme vous le souhaitez.</li>
				<li>En vous rendant sur la page « Répertoire », cliquez sur le bouton vert en haut à droite de la page «<b>créer une organisation</b>» </li>
				<li>Une fois validée, <b>attendez 5 secondes</b> qu'une page plus complète s'ouvre enfin de renseigner description longue et bannière et validez.</li>
				<li>Après réception et <b>acceptation</b> de votre proposition par notre équipe, vous serez invité à <b>administrer</b> votre structure qui apparaîtra en ligne dans le répertoire.</li>
				<li>Ensuite, quand vous vous connecterez, vous verrez apparaître, en haut à droite du bandeau "agenda, répertoire, annonces", <b>les boutons "mon chat, mes notifications, mon réseau, ma page et menu".</b></li>
				<li>Vous pourrez ainsi, dans la page "agenda",<b>créér un événement</b> par votre organisation, en cliquant sur le bouton "créer un évènement".</li>
				<li>Une fois validée, <b>attendez 5 secondes</b> qu'une page plus complète s'ouvre.</li>
				<li>Vous pourrez renseigner description longue et bannière mais aussi modifier/compléter les chapitres en cliquant sur les boutons "éditer".</li>
				<li>Vous pourrez aussi dans la page "annonces", <b>proposer des annonces</b> par votre organisation, en cliquant sur le bouton "publier une annonce".</li>
				<li>Dans la page "répertoire", Il vous sera toujours possible de <b>modifier votre organisation</b> en cliquant sur les boutons à droite "éditer"</li>
				<li>Pensez à <b>actualiser les pages</b> afin d'en faciliter les enregistrements</li>

			</ul>  <br/>
			
			<center>
				<?php
				$elt = Slug::getElementBySlug("hva", array("_id", "name") );
				?>
				<button id="connectHVA" class="letter-green font-montserrat btn-menu-connect margin-left-10 margin-right-10 menu-btn-top" data-toggle="modal" data-target="#modalLogin" style="font-size: 17px; background-color: #5b2649 !important; color: white !important; padding: 8px 15px !important;">
		                <i class="fa fa-sign-in"></i> 
		                <span class="hidden-xs"><small style="width:70%;">Créer un compte</small></span>
		        </button>
			</center>
		</div>
	</div>
</div>
