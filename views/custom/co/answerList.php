<div id="results" class="col-md-12 col-lg-12  no-padding "><br/>
	<div class="col-md-12 col-sm-12 col-xs-12 " style="background-color: white; ">
	  <h1 class="margin-top-20 monTitle  text-red padding-20 text-center cbyg" >Réponses</h1>
	</div>

    <div class="row margin-top-20   padding-20" style="background-color: white; ">

	    <div class="col-md-12">
	    	<?php 
				
				if(!empty($allAnswers))
				{
					echo "<br/><table class='table table-striped table-bordered table-hover' >";
					echo "<tr>";
						echo "<th class='text-center'>User</th>";
						foreach ($form['inputs'] as $k => $inp) {
							if( stripos( $inp["type"] , "tpls.forms." ) === false )
								echo "<th class='text-center'>".$inp["label"]."</th>";
						}
						if(isset(Yii::app()->session["costum"]["form"]["showDate"]))
							echo "<th class='text-center'>Date</th>";
						if(isset(Yii::app()->session["costum"]["form"]["canVote"]))
							echo "<th class='text-center'>Vote</th>";
						if(isset(Yii::app()->session["costum"]["form"]["canFund"]))
							echo "<th class='text-center'>Fund</th>";
						echo "<th class='text-center'>Action</th>";
					echo "</tr>";
					$sum = [];
					foreach ($allAnswers as $id => $a) {
						
						if(isset($a["answers"])){
							$user = PHDB::findOne( Person::COLLECTION , ["_id"=>new MongoId($a["user"]) ],['username'] );
							echo "<tr id='line".$a["_id"]."'  data-id='".$a["_id"]."' data-type='".Form::ANSWER_COLLECTION."' >";

								echo "<td>".$user["username"].".</td>";
								foreach ($form['inputs'] as $k => $inp) {
									if( stripos( $inp["type"] , "tpls.forms." ) === false )
										echo "<td class='text-center'>".@$a["answers"][$k]."</td>";
									if(isset(Yii::app()->session["costum"]["form"]["sumColumns"]) && 
										in_array($k, Yii::app()->session["costum"]["form"]["sumColumns"]) && 
										isset($a["answers"][$k]) &&
										is_numeric($a["answers"][$k]) ){
										if(!isset($sum[$k]))
											$sum[ $k ] = 0;
										$sum[$k] = $sum[$k] + $a["answers"][$k];
									}
								}

								if(isset(Yii::app()->session["costum"]["form"]["showDate"]))
									echo "<td>".date('Y-m-d',$a['created'])."</td>";
								
								if(isset(Yii::app()->session["costum"]["form"]["canVote"])){
									echo "<td class='text-center'>";										
										echo "<a href='javascript:;' class='voteBtn btn btn-default'><i class='fa text-red fa-thumbs-up'></i> </a>";
										echo "<a href='javascript:;' class='voteListBtn btn btn-default'>".@$a["voteUpCount"]." <i class='fa text-red fa-bars'></i> </a>";
									echo "</td>";	
								}
								
								if(isset(Yii::app()->session["costum"]["form"]["canFund"])){
									echo "<td class='text-center'>";
										$totalFund = "";
										if(isset($a["fund"])){
											$totalFund = 0;
											foreach ($a["fund"] as $key => $f) {
												$totalFund = $totalFund+$f["detail"]["amount"]; 
											}
											$totalFund = "<br/>".$totalFund." €";
										}
										echo "<a href='javascript:;' class=' fundBtn btn btn-default'><i class='fa text-red fa-money'></i> </a>";
										echo "<a href='javascript:;' class=' fundListBtn btn btn-default'>".@$a["fundCount"]." <i class='fa text-red fa-bars'></i> </a>";
										echo $totalFund;		
									echo "</td>";	
								}
								
								echo "<td class='bg-red'>";
									
									if(isset(Yii::app()->session["costum"]["form"]["canModifyAnswer"]))
										echo "<a href='".Yii::app()->createUrl("/costum/co/index/slug/".$slug."/answer/".$a["_id"])."'  class='btn btn-default'><i class='fa text-red fa-pencil'></i> </a>";

									echo "<a href='javascript:;' data-id='".$a['_id']."' class='deleteAnswer btn btn-default'><i class='fa text-red fa-trash'></i> </a>";

								echo "</td>";
							echo "</tr>";
						}
					}
					
					if(isset(Yii::app()->session["costum"]["form"]["sumColumns"])){
						echo "<tr>";
							echo "<td class='bold'>SOMME</td>";
							foreach ($form['inputs'] as $k => $inp) {
								if( stripos( $inp["type"] , "tpls.forms." ) === false ){
									echo "<td class='bold'>";
									if( isset($sum[$k]) )
										echo $sum[$k];
									echo "</td>";
								}
							}
							if(isset(Yii::app()->session["costum"]["form"]["showDate"]))
								echo "<td></td>";
							if(isset(Yii::app()->session["costum"]["form"]["canVote"]))
								echo "<td></td>";
							if(isset(Yii::app()->session["costum"]["form"]["canFund"]))
								echo "<td></td>";
							echo "<td></td>";
						echo "</tr>";
					}

					echo "</table>";

				} else {
					?>
					<div class='col-xs-12 text-center'>
						<h2 class='text-center'>Aucune Réponse </h2>
					</div>
				<?php }
			 ?>
	    </div>
	</div>
</div>

<div class="formFund" style="display:none;">
	<form class="inputFund" role="form">
		<?php 
          if(isset(Yii::app()->session["costum"]["cms"]["fundMsg"])){
            echo htmlentities(Yii::app()->session["costum"]["cms"]["fundMsg"]);
          } else { ?> 
          Ceci n'est qu'une promesse pour l'instant <br>
			Si le montant est atteint, nous vous recontacterons, pour passer à la caisse <br/>
			merci de votre contribution<br> 
		<?php } 
          if($canEdit)
            echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='fundMsg' data-type='textarea' data-markdown='1'  data-path='costum.cms.fundMsg' data-label='Expliquez les objectifs de ce formulaire ? '><i class='fa fa-pencil'></i></a>";
          ?>
		<input type="fundAmount" name="fundAmount" id="fundAmount">
	</form>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {
    mylog.log("render","/modules/costum/views/custom/co/answerList.php");
	
<?php if($canEdit) { ?>
	

	  $('.deleteAnswer').off().click( function(){
      id = $(this).data("id");
      bootbox.dialog({
          title: "Confirmez la suppression",
          message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
          buttons: [
            {
              label: "Ok",
              className: "btn btn-primary pull-left",
              callback: function() {
                getAjax("",baseUrl+"/survey/co/delete/id/"+id,function(){
                	//urlCtrl.loadByHash(location.hash);
                	$("#line"+id).remove();
                },"html");
              }
            },
            {
              label: "Annuler",
              className: "btn btn-default pull-left",
              callback: function() {}
            }
          ]
      });
    });


<?php } ?>

<?php if( isset(Yii::app()->session["userId"])) { ?>
var elParent = null;
var fundedAmount = null;
	$('.voteBtn').click(function() { 
			//alert( "vote : "+$(this).parent().parent().data('type')+" : "+$(this).parent().parent().data('id') );
			elParent = $(this).parent();

			$.ajax({ type: "POST",
		        url: baseUrl+"/co2/action/addaction/",
		        data: {
		        	"action" : "voteUp",
					"collection" : elParent.parent().data('type'),
					"id" : elParent.parent().data('id')
		        },
				type: "POST",
		    }).done(function (data) {
		    	elParent.html("<i class='fa fa-thumbs-up'></i>")
				toastr.success('vote enregistré, merci!!');
				location.reload();
		    });
		});

		$('.voteListBtn').click(function() { 
			getModal({ title : "Liste des voteurs de ce projet." ,
					   icon : "thumbs-up" }, "");
		});

		$('.fundBtn').click(function() { 
			//alert( "vote : "+$(this).parent().parent().data('type')+" : "+$(this).parent().parent().data('id') );
			elParent = $(this).parent();
			fundedAmount = $($("input#fundAmount")[1]).val();
			fundModal = bootbox.dialog( {
		        message : $(".formFund").html(),
		        title : "Participez à financer ce projet",
		        show: false,
		        onEscape: function() {
		          fundModal.modal("hide");
		        },
		        buttons: {
		            success: {
		                label: "CoFund",
		                className: "btn-primary",
		                callback: function () {
		                    $.ajax({ type: "POST",
						        url: baseUrl+"/co2/action/addaction/",
						        data: {
						        	"action" : "fund",
									"collection" : elParent.parent().data('type'),
									"id" : elParent.parent().data('id'),
									"detail" : {
										amount : $($("input#fundAmount")[1]).val()
									}
						        },
								type: "POST",
						    }).done(function (data) {
						    	elParent.html("<i class='fa fa-thumbs-up'></i>")
						    	toastr.success('participation enregistré, merci!!');
						    	location.reload();
						    });
		                }
		            },
		            cancel: {
		            	label: trad["cancel"],
		            	className: "btn-secondary",
		            	callback: function() {
		    				fundModal.modal("hide");        		
		            	}
		            }
		        }
		    });
		    fundModal.modal("show");
		});

		$('.fundListBtn').click(function() { 
			getModal({ title : "Liste des financeurs de ce projet.",
					   icon : "money" }, "");
		});
		<?php } ?>
});

</script>