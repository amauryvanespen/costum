<?php  
$cssAnsScriptFilesTheme = array(
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js'            
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 
    $sectionColor = @Yii::app()->session["costum"]["cms"]["sectionColor"];
    $sectionTitleColor = @Yii::app()->session["costum"]["cms"]["sectionTitleColor"];
    $bannerImg = @Yii::app()->session["costum"]["banner"] ? Yii::app()->baseUrl.Yii::app()->session["costum"]["banner"] : null;
    $logo = @Yii::app()->session["costum"]["logo"] ? Yii::app()->session["costum"]["logo"] : null;
    $title = @Yii::app()->session["costum"]["title"] ? Yii::app()->session["costum"]["title"] : null;
    ?>

<div style="background-image: url(<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/smarterritoireautre/backgroundlink.jpg);">
<div class="header">
    <center>
        <a href="javascript:;" data-hash="#@<?= Yii::app()->session['costum']['slug']; ?>">
            <img id="hexa-haut" style="position: absolute;margin-top: 22vw;width: 14vw;margin-left: 50vw;z-index: 10000;" class="img-responsive img-hexa" src="<?php echo $bannerImg ?>">
        </a>

        <img id="banner" style="width: 100%;margin-top: -5.1vw;" class="img-responsive" src="<?php echo $bannerImg ?>">

        <div class="imgHaut">
            <?xml version="1.0" encoding="utf-8"?>
            <!-- Generator: Adobe Illustrator 22.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
            <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 564.6 562.4" style="enable-background:new 0 0 564.6 562.4;" xml:space="preserve">
            <style type="text/css">
                @font-face{
                font-family: "futura";
                src: url("<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/font/smarterritoireautre/futur2.ttf") format("ttf");
                }
                .st0haut{opacity:0.4;fill:none;stroke:#FFFFFF;stroke-width:6.4174;stroke-miterlimit:10;}
                .st1haut{opacity:0.4;fill:#FFFFFF;}
                .st2haut{fill:none;}
                .st3haut{fill:#FFFFFF;}
                .st4haut{font-family: 'futura' !important;}
                .st5haut{font-size:20.983px;}
                .st6haut{fill:#00B2BA;}
                .st7haut{fill:#93C021;}
                .st8haut{fill:#1D1D1B;}
            </style>
            <polygon class="st0haut" points="149.8,100.8 274.1,23.9 402,95.1 402.5,229.3 521.1,299 524.4,447.6 400.7,518.9 280,453.7 
                162.3,521.6 35.9,448.7 33.6,305.7 153.3,232.1 "/>
            <polygon class="st1haut" points="275.4,38.9 162.7,105.7 164.2,236.7 278.4,300.9 391.1,234.1 389.6,103.1 "/>
            <polygon class="st1haut" points="397.5,243.6 284.8,310.4 286.3,441.4 400.4,505.6 513.2,438.8 511.7,307.8 "/>
            <polygon class="st1haut" points="159,244.7 46.3,311.5 47.8,442.5 162,506.7 274.7,440 273.2,309 "/>
            <g>
                
                    <!-- <image style="overflow:visible;opacity:0.32;" width="174" height="152" xlink:href="98955EF1AD59A468.png"  transform="matrix(1 0 0 1 314.6704 301.9138)">
                </image> -->
                <g>
                    
                       <!--  <image style="overflow:visible;" width="527" height="458" xlink:href="98955EF1AD59A469.png"  transform="matrix(0.3129 0 0 0.3129 315.9659 303.5446)">
                    </image> -->
                </g>
            </g>
            <rect x="174.2" y="156.3" class="st2haut" width="228.1" height="47.4"/>
            <text transform="matrix(1 0 0 1 174.1616 176.0896)" class="st3haut st4haut st5haut"><?php echo $title ?></text>
            <g>
                <a href="#smarterre">
                    <!-- <image style="overflow:visible;opacity:0.32;" width="161" height="163" xlink:href="98955EF1AD59A46F.png"  transform="matrix(1 0 0 1 80.5312 297.7746)">
                    </image> -->
                </a>
                <g>
                    <g>
                        <g>
                            <path class="st6haut" d="M181.3,443.8c-0.5,0.2-1,0.3-1.5,0.5c-9.7,2.1-18.5,2.8-26.4,2.5c0,0,0,0,0,0
                                c-53.3-2.1-65.4-52.3-65.4-52.3c42.2-4.5,65.4,3.7,78.1,14.7C182.4,423.3,181.5,441.8,181.3,443.8z"/>
                        </g>
                        <g>
                            <path class="st7haut" d="M227.7,395.5c0,0-11.3,34.1-41.8,47.2c0.1-1.8,1.2-20.7-14.1-33.9C181.3,400.1,198.2,393.6,227.7,395.5z"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="225.7" cy="359.9" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="89.5" cy="359.9" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="102.2" cy="336.2" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="129.4" cy="313.7" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="127.5" cy="340" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="161.1" cy="325.4" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="158.4" cy="306.8" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="191.1" cy="315.6" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="218.7" cy="340" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="204.3" cy="347.5" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="187.3" cy="332.3" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="177.2" cy="360.8" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="155.1" cy="359.9" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="121.9" cy="361.8" r="1.9"/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="89.7,360 89.4,359.8 102,336.1 102.4,336.3              "/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="122.1,361.8 121.7,361.7 127.3,339.9 127.7,340              "/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="127.7,340 127.3,340 129.2,313.7 129.6,313.7                "/>
                        </g>
                        <g>
                            
                                <rect x="140.6" y="342.4" transform="matrix(0.172 -0.9851 0.9851 0.172 -206.597 439.4619)" class="st8haut" width="35" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="114.6" y="325.3" transform="matrix(0.1493 -0.9888 0.9888 0.1493 -236.5772 401.1839)" class="st8haut" width="0.4" height="25.6"/>
                        </g>
                        <g>
                            
                                <rect x="105.5" y="344.6" transform="matrix(5.902462e-02 -0.9983 0.9983 5.902462e-02 -260.7166 445.0365)" class="st8haut" width="0.4" height="32.4"/>
                        </g>
                        <g>
                            
                                <rect x="111.8" y="332.8" transform="matrix(0.7929 -0.6094 0.6094 0.7929 -189.4475 140.5434)" class="st8haut" width="0.4" height="32.3"/>
                        </g>
                        <g>
                            
                                <rect x="126" y="332.5" transform="matrix(0.9173 -0.3982 0.3982 0.9173 -120.5302 84.9735)" class="st8haut" width="36.7" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="173" y="300.2" transform="matrix(0.6798 -0.7334 0.7334 0.6798 -178.7316 229.3089)" class="st8haut" width="0.4" height="38.2"/>
                        </g>
                        <g>
                            
                                <rect x="159.5" y="306.3" transform="matrix(0.9896 -0.1441 0.1441 0.9896 -43.8641 26.3271)" class="st8haut" width="0.4" height="19.2"/>
                        </g>
                        <g>
                            
                                <rect x="129" y="309.8" transform="matrix(0.9692 -0.2462 0.2462 0.9692 -71.9064 44.974)" class="st8haut" width="29.9" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="98.2" y="324.7" transform="matrix(0.772 -0.6356 0.6356 0.772 -180.1279 147.6792)" class="st8haut" width="35.3" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="141.1" y="332.9" transform="matrix(0.5845 -0.8114 0.8114 0.5845 -225.2124 260.0605)" class="st8haut" width="0.4" height="34"/>
                        </g>
                        <g>
                            
                                <rect x="167.1" y="346.4" transform="matrix(0.3326 -0.9431 0.9431 0.3326 -205.2222 403.1595)" class="st8haut" width="30.2" height="0.4"/>
                        </g>
                        <g>
                            <rect x="174.9" y="294.4" transform="matrix(0.28 -0.96 0.96 0.28 -172.4396 392.083)" class="st8haut" width="0.4" height="33.2"/>
                        </g>
                        <g>
                            
                                <rect x="204.7" y="309.4" transform="matrix(0.6585 -0.7525 0.7525 0.6585 -176.6705 266.1194)" class="st8haut" width="0.4" height="36.8"/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="225.5,359.9 218.5,339.9 219,339.8 226,359.8                "/>
                        </g>
                        <g>
                            
                                <rect x="203.4" y="343.4" transform="matrix(0.8848 -0.4659 0.4659 0.8848 -135.7533 138.1276)" class="st8haut" width="16.3" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="214.8" y="341.3" transform="matrix(0.5 -0.8661 0.8661 0.5 -198.7712 363.0674)" class="st8haut" width="0.4" height="24.8"/>
                        </g>
                        <g>
                            
                                <rect x="180.6" y="323.8" transform="matrix(0.2223 -0.975 0.975 0.2223 -168.7598 436.4172)" class="st8haut" width="17.1" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="195.6" y="328.5" transform="matrix(0.6643 -0.7475 0.7475 0.6643 -188.3468 260.4563)" class="st8haut" width="0.4" height="22.8"/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="187.2,332.5 161.1,325.6 161.2,325.2 187.3,332.1                "/>
                        </g>
                        <g>
                            
                                <rect x="175.6" y="353.9" transform="matrix(0.8975 -0.441 0.441 0.8975 -136.6156 120.4022)" class="st8haut" width="30.2" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="177.2" y="360.1" transform="matrix(0.9998 -1.952753e-02 1.952753e-02 0.9998 -6.998 4.003)" class="st8haut" width="48.6" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="165.9" y="349.3" transform="matrix(4.319710e-02 -0.9991 0.9991 4.319710e-02 -201.0227 510.7661)" class="st8haut" width="0.4" height="22.1"/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="121.9,362 121.9,361.6 155.1,359.6 155.1,360.1              "/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="177,360.9 160.9,325.5 161.3,325.3 177.4,360.7              "/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="161.1,325.6 129.4,313.9 129.5,313.5 161.2,325.2                "/>
                        </g>
                        <g>
                            <path class="st8haut" d="M87.3,381.8l0.8-0.7c0.8,1.2,1.9,1.8,3.4,1.8c1.7,0,3-1.1,3-2.7c0-1.7-1.6-2.2-3.2-2.8
                                c-1.7-0.6-3.5-1.2-3.5-3.4c0-1.8,1.6-3.2,3.7-3.2c1.7,0,2.9,0.7,3.7,1.7l-0.8,0.6c-0.7-0.9-1.6-1.5-2.9-1.5
                                c-1.5,0-2.7,1-2.7,2.3c0,1.6,1.5,2.1,3.1,2.6c1.8,0.6,3.6,1.3,3.6,3.6c0,2-1.7,3.6-4.1,3.6C89.6,383.9,88.2,383.1,87.3,381.8z"
                                />
                            <path class="st8haut" d="M103.2,371.1h1l5.1,7.1l5.3-7.1h0.9v12.6h-1v-11l-5.3,7.1l-5.1-7v10.9h-1V371.1z"/>
                            <path class="st8haut" d="M127.9,371.1h1.1l4.9,12.6h-1.1l-1.4-3.5h-6l-1.3,3.5h-1.1L127.9,371.1z M125.9,379.3h5.2l-2.6-6.8
                                L125.9,379.3z"/>
                            <path class="st8haut" d="M141.3,371.1h3.9c2.4,0,4.1,1.8,4.1,3.9c0,1.7-1.1,3.2-2.8,3.7l2.8,5h-1.1l-2.6-4.7h-3.4v4.7h-1V371.1z
                                 M142.3,372v6.1h2.9c1.9,0,3.2-1.5,3.2-3.1c0-1.7-1.3-3-3.2-3H142.3z"/>
                            <path class="st8haut" d="M159.7,373.4h-3.4v-2.3h9.1v2.3h-3.4v10.3h-2.4V373.4z"/>
                            <path class="st8haut" d="M172.1,371.1h7.5v2.3h-5.1v2.8h5.1v2.3h-5.1v2.9h5.1v2.3h-7.5V371.1z"/>
                            <path class="st8haut" d="M186.3,371.1h4c3,0,4.8,1.8,4.8,4.4c0,1.5-0.7,2.9-2.1,3.7l2.5,4.5h-2.7l-2-3.8h-2.1v3.8h-2.4V371.1z
                                 M188.7,373.4v4.4h1.4c2,0,2.6-1,2.6-2.3s-0.8-2.1-2.6-2.1H188.7z"/>
                            <path class="st8haut" d="M202,371.1h4c3,0,4.8,1.8,4.8,4.4c0,1.5-0.7,2.9-2.1,3.7l2.5,4.5h-2.7l-2-3.8h-2.1v3.8H202V371.1z
                                 M204.4,373.4v4.4h1.4c2,0,2.6-1,2.6-2.3s-0.8-2.1-2.6-2.1H204.4z"/>
                            <path class="st8haut" d="M217.6,371.1h7.5v2.3H220v2.8h5.1v2.3H220v2.9h5.1v2.3h-7.5V371.1z"/>
                        </g>
                    </g>
                </g>
            </g>
            </svg>
        </div>
    </center>
</div>

<!-- SEARCHBARRE -->
<div class="hidden-xs searchbar col-md-12">
<!-- DEBUT SVG -->
<div class=" text-center">
    <?xml version="1.0" encoding="utf-8"?>
    <!-- Generator: Adobe Illustrator 22.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
    <svg class="search" version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 80 1006 95" style="enable-background:new 0 0 1006 217; margin-top: -2%; width: 50%;" xml:space="preserve">
    <style type="text/css">
        .st0{fill:#FFFFFF;}
        .st1{fill:<?php echo $sectionColor ?>;}
    </style>
    <g>
        
            <!-- <image style="overflow:visible;opacity:0.29;" width="956" height="124" xlink:href="BD554E7F0366A2C6.png"  transform="matrix(1 0 0 1 20.4646 63.5639)">
        </image> -->
        <g style="filter: drop-shadow(0 0px 10px rgba(0, 0, 0, 0.5));">
            <polygon class="st0" points="943.5,117.4 922.2,80.5 908.1,80.5 879.6,80.4 77,80.4 77,80.5 58.9,80.5 37.5,117.4 58.9,154.4 
                77,154.4 77,154.5 737.5,154.5 879.6,154.4 922.2,154.4       "/>
        </g>
    </g>
    <g>
    <a data-type="filters" href="javascript:;" id="second-search-bar-addon-smarterre">
            <polygon class="st1" points="921.7,80.2 878.5,80.2 857.8,116.7 879.4,154 922.6,154 944.2,116.7  "/>
            <path class="st0" d="M919,116.9l-2.3-2.3l0,0l-6.3-6.3l-2.3,2.3l4.5,4.4h-28v3.2h28.5l-5,5l2.3,2.3l6.3-6.3l0,0L919,116.9
            L919,116.9L919,116.9z M914.2,117.2v-0.5l0.3,0.3L914.2,117.2z"/>
    </a>
    </g>
    <g>
        <text x="125" y="130"></text>
    </g>
    <path class="st1" d="M110.7,138.8c-0.1-0.1-0.1-0.2-0.2-0.2c-3.3-3.3-6.6-6.6-9.9-9.9c-0.1-0.1-0.1-0.1-0.2-0.2
        c-4,2.8-8.3,3.7-13.1,2.5c-3.8-0.9-6.8-3.1-9.1-6.3c-4.4-6.3-3.4-15.1,2.4-20.4c5.7-5.2,14.6-5.4,20.5-0.5
        c6.1,5.1,7.7,14.3,2.7,21.3c0.1,0.1,0.1,0.1,0.2,0.2c3.3,3.3,6.5,6.5,9.8,9.8c0.1,0.1,0.2,0.1,0.3,0.2c0,0,0,0.1,0,0.1
        C113,136.5,111.9,137.7,110.7,138.8C110.7,138.8,110.7,138.8,110.7,138.8z M80.4,115.9c0,6,4.9,10.9,10.8,10.9
        c6,0,10.8-4.9,10.8-10.8c0-6-4.8-10.9-10.8-10.9C85.3,105,80.4,109.9,80.4,115.9z"/>
    </svg>
</div>

<div class="hidden-xs" style="margin-top: -4.5%; position: absolute; width: 450px; border: none; margin-left: 31%;">
    <input type="text" class="main-search-bar barS" id="second-search-bar" placeholder="Rechercher un territoire">

    <div id="dropdown" class="dropdown-result-global-search hidden-xs col-sm-5 col-md-5 col-lg-5 no-padding" style="top: 16px;max-height: 100%;background-color:white;width: 110%;margin-left: 0%;z-index: 1000;">
    </div>
</div>
<!-- FIN SVG -->
</div>

<div class="agenda-carousel">
    <div style="margin-top: 6vw" class="col-xs-10 col-sm-12 col-md-12 no-padding carousel-border" >
        <div id="docCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner itemctr">
            </div>
            <div id="arrow-caroussel">
                <a class="left carousel-control" href="#docCarousel" data-slide="prev">
                    <img style="width:11vw;margin-top: 16vw;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/fleche_slide_gauche.svg" class="img-responsive">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#docCarousel" data-slide="next">
                     <img style="width:11vw;margin-top: 15vw;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/fleche_slide_droite.svg" class="img-responsive">
                    <span class="sr-only">Next</span>
                </a>
            </div>   
        </div>
    </div> 
</div>

<div class="explication row">
    <div style="margin-top: 3vw;border-radius: 10px 10px 10px 10px;background-color:<?php echo $sectionColor ?>;" class="explication-title col-xs-10 col-sm-12 col-lg-12">
        <div>
            <h1 style="color:<?php echo $sectionTitleColor ?>;font-family: 'Covered By Your Grace', cursive !important;">Actualité</h1>
        </div>
    </div>
    <!-- NEWS -->
    <div id="newsstream" class="col-xs-10 col-sm-12">
        <div style="background-color: white;">
        </div>
    </div>

    <div style="background-color: <?php echo $sectionColor ?>;border-radius: 10px 10px 10px 10px;" class="text-center container col-lg-12 col-sm-12 col-xs-10">
        <a style="color: white;" href="javascript:;" data-hash="#live" class="lbh-menu-app" style="text-decoration : none; font-size:2rem;">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/fleche.svg" class="img-responsive" style="width: 15%;margin-top: -3vw;"><br>
            <p style="margin-top: -4vw;">Voir plus d'actualités<p> 
        </a>
    </div>
</div>

<div class="explication row">
    <div style="margin-top: 3vw;border-radius: 10px 10px 10px 10px;background-color:<?php echo $sectionColor ?>;" class="explication-title col-xs-10 col-sm-12 col-lg-12">
        <div>
            <h1 style="color:<?php echo $sectionTitleColor ?>;font-family: 'Covered By Your Grace', cursive !important;">Évènements</h1>
        </div>
    </div>
        <p class="text-center">Évènement à venir</p>
            <div style="margin-top: 4vw;" id="containEvent">  
            </div>
    

    <div style="margin-top:2vw;background-color: <?php echo $sectionColor ?>;border-radius: 10px 10px 10px 10px;" class="text-center container col-lg-12 col-sm-12 col-xs-10">
        <a style="color: white;" href="javascript:;" data-hash="#agenda" class="lbh-menu-app" style="text-decoration : none; font-size:2rem; color:white;">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/fleche.svg" class="img-responsive plus-m" style="width: 12vw;margin-top: -3vw"><br>
            <p style="margin-top: -4vw;">Voir plus d'évènement<p> 
        </a>
    </div>
</div>

    <div style="margin-top: 2vw;">
        <img style="width: 100%;position: absolute;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/smarterritoireautre/phototerritoire.jpg">
        <?php echo $this->renderPartial("costum.assets.images.smarterritoireautre.bloc_smarterre"); ?> 
    </div>   
<script>
jQuery(document).ready(function() {
    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });

    logo = costum.logo;

    $("#hexa-haut").attr("src",logo);

        globalSearchFilieres();
        afficheEvent();

    urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";

    ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html");
});

 function globalSearchFilieres(){
    console.log("----------------- Affichage community");
    var params = {
        contextId : costum.contextId,
        contextType : costum.contextType
    };

    $.ajax({
        type : "POST",
        data : params,
        url : baseUrl + "/costum/smarterritoireautre/getcommunityaction",
        dataType : "json",
        async : false,
        success : function(data){
            console.log("success : ",data);
            var str = "";
            var ctr = "";
            var itr = "";
            var assetsUrl = "<?php echo Yii::app()->getModule('costum')->assetsUrl;?>";
            // Icones en svg
            var acteurs = assetsUrl+"/images/smarterritoireautre/acteur_icone.svg";
            var agenda = assetsUrl+"/images/smarterritoireautre/actu_icone.svg";
            var actus = assetsUrl+"/images/smarterritoireautre/agenda_icone.svg";
            var projet = assetsUrl+"/images/smarterritoireautre/projet_icone.svg";

            var fleches = assetsUrl+"/images/smarterritoireautre/fleche.svg";

            //Pour charger l'image 
            var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.events.defaultImg;
            var ph = "<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>";

            var i = 1;

            if(data.result == true){
                // console.log("data.element",data.elt);
                $(data.elt).each(function(key,value){
                    console.log(value);
                    var startDate = (typeof value.startDate != "undefined") ? "Du "+dateToStr(value.startDate, "fr", false, false) : null;

                    var imgMedium = (value.imgMedium != "none") ? baseUrl + value.imgMedium : url;
                    var img = (value.img != "none") ? baseUrl + value.img : url;
                    var imgBanner = (value.imgBanner != "none") ? baseUrl + value.imgBanner : url;
                    var filiereGenerique = baseUrl+"/costum/co/index/slug/"+value.slug ;
                    

                    str += "<div class='col-md-4 card-mobile'>";
                    str += "<div class='card'>";
                    str += "<img src='"+imgMedium+"' class='img-responsive img-event'>";
                    str += "<div class='col-xs-12 col-sm-12 col-md-12 card-c'>";
                    str += "</button></div></div></div></div>";

                    if(i == 1) ctr += "<div class='item active'>";
                    else ctr += "<div class='item'>";

                    ctr += "<div style='position:absolute;text-align:center;' class='resume col-xs-11 col-sm-11 col-md-12'>";
                    ctr += '<svg class="hexaName" version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 227.1 46" style="enable-background:new 0 0 227.1 46;" xml:space="preserve">';
                    ctr += '<style type="text/css">.st0{fill:#FFFFFF;}.st1{fill:<?php echo $sectionColor ?>;}</style>';
                    ctr += '<g><g><image style="overflow:visible;opacity:0.29;" width="237" height="44"  transform="matrix(1 0 0 1 -2.0847 2.548)"></image><g><polygon class="st0" points="222.6,21.6 215.1,8.6 210.1,8.6 200.1,8.6 17.8,8.6 17.8,8.6 11.4,8.6 3.9,21.6 11.4,34.6 17.8,34.6 17.8,34.6 150.1,34.6 200.1,34.6 215.1,34.6"/></g></g><g><g><path class="st1" d="M37.5,16.2v-2.5H20v13.7c0,0,0,2.5,2.5,2.5h15.6c0,0,1.9,0,1.9-2.5V16.2H37.5z M22.5,28.7c-1.2,0-1.2-1.2-1.2-1.2V14.9h15v12.5c0,0.6,0.2,1,0.4,1.2H22.5z"/><rect x="22.5" y="17.4" class="st1" width="12.5" height="1.2"/><rect x="29.4" y="24.9" class="st1" width="4.4" height="1.2"/><rect x="29.4" y="22.4" class="st1" width="5.6" height="1.2"/><rect x="29.4" y="19.9" class="st1" width="5.6" height="1.2"/><rect x="22.5" y="19.9" class="st1" width="5.6" height="6.2"/><text class="nameFil" x="48px" y="26px"><a target"_blank" href="'+filiereGenerique +'">'+value.name+'</text></a></g></g></g>';
                    ctr += '</svg>';    
                    ctr += "<ul class='dataSlide' style=''>";
                    ctr += "<a href='#'><li style='font-size: 26px;color:white;font-weight:bold;' class='inline-block count'><img class='imgSlide' src="+acteurs+"><br>"+value.countActeurs+"<br>Acteur(s)"+"<img src='"+fleches+"'></li></a>";
                    ctr += "<a href='#live' target='_blank'><li style='font-size: 26px;color:white;font-weight:bold;' class='inline-block count'><img class='imgSlide' src="+actus+"><br>"+value.countActus+"<br> Actu(s)"+"<img src='"+fleches+"'></li></a>";
                    ctr += "<a href='#agenda' target='_blank'><li style='font-size: 26px;color:white;font-weight:bold;' class='inline-block count'><img class='imgSlide' src="+agenda+"><br>"+value.countEvent+"<br> évènement(s)"+"<img src='"+fleches+"'></li></a>";
                    ctr += "<a><li style='font-size: 26px;color:white;font-weight:bold;' class='inline-block count'><img class='imgSlide' src="+projet+"><br>"+value.countProjet+"<br> Projet(s)"+"<img src='"+fleches+"'></li></a>";
                    ctr += "</ul>";
                    ctr += "</div>";

                    ctr += "<img style='width:100%;height:43vw;' src='"+imgBanner+"' class='ctr-img img-responsive'>";
                    ctr += "<div style='border-style: solid;height: 1vw;border-color:transparent;background-color:#00B2BA;'>";
                    ctr += "</div>";
                    ctr += "</div>";

                    i++;
                });
                $(".itemctr").html(ctr);
            }
            else{
                str += "<div class='col-xs-12 col-sm-12 col-md-12'><b class='p-mobile-description' >Aucune filière n'éxiste</b></div>";
                $(".agenda-carousel").hide();
            }

            // if(i != 2) $("#arrow-caroussel").show();
            // else $("#arrow-caroussel").hide();
            // console.log("i truc -------------", i );
            // $("#event-affiche").html(str);
            // console.log(str);
        },
        error : function(e){
            console.log("error : ",e);
        }
    });
}


function afficheEvent(){
    console.log("----------------- Affichage évènement");

    var params = {
        contextId : costum.contextId,
        contextType : costum.contextType,
        source : costum.contextSlug
    };

    $.ajax({
        type : "POST",
        data : params,
        url : baseUrl + "/costum/smarterritoireautre/geteventaction",
        dataType : "json",
        async : false,
        success : function(data){
            console.log("success : ",data);
            var str = "";
            var ctr = "";
            var itr = "";
            var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.events.defaultImg;
            var ph = "<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>";

            var fleches = ph + "/images/smarterritoireautre/fleche-plus-noire.svg";
            
            var i = 1;
            
            if(data.result == true){
                
                $(data.element).each(function(key,value){
                    console.log("data.element",data.element);
                    
                    var startDate = (typeof value.startDate != "undefined") ? "Du "+dateToStr(value.startDate, "fr", false, false) : null;  

                     /**
                    Phase de dev
                     **/
                    var imgMedium = (value.imgMedium != "none") ? "/ph/"+value.imgMedium : url;
                    var img = (value.img != "none") ? "/ph/" + value.img : url;
                    /**
                    Phase de prod
                    **/
                    // var imgMedium = (value.imgMedium != "none") ? value.imgMedium : url;
                    // var img = (value.img != "none") ? value.img : url;
                    
                    str += '<div class="card">';
                    str += '<div id="event-affiche" class="card-color col-md-4">';
                    str += '<div id="affichedate" class="info-card text-center">';
                    str += '<div id="afficheImg" class="img-hexa">';
                    str += '<a href="#">';
                    str += '<div class="hexagon hexagon1"><div class="hexagon-in1"><div class="hexagon-in2" style="background-image: url('+imgMedium+');"><div style=";display:none;color:black;position: absolute;margin-left: -113px;margin-top: 6vw;" class="text-in1">'+value.name+'<br><img width="5vw;" src="'+fleches+'"></div></div></div></div>';
                    str += '</a>';
                    str += '</div>';
                    str += ''+startDate+'</br>'+value.type;
                    str += '</div>';
                    str += '</div>';
                    str += '</div>';
                });
            }
            else{
                str += "<div class='col-xs-12 col-sm-12 col-md-12'><b class='p-mobile-description' >Aucun évènement n'est prévu</b></div>";
            }
            $("#containEvent").html(str);
            console.log(str);
        },
        error : function(e){
            console.log("error : ",e);
        }
    });
}
</script>