<?php 
$cssAnsScriptFilesTheme = array(
        // SHOWDOWN
        '/plugins/showdown/showdown.min.js',
        //MARKDOWN
        '/plugins/to-markdown/to-markdown.js',              
    );
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

$articles_une=Poi::getPoiByWhereSortAndLimit(array("rank"=>"true", "source.key"=>"mayenneDemain"),array("updated"=>-1), 3, 0);
?>
<style type="text/css">
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: #71b62c;
    border-radius: 20px;
    padding: 20px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    /*min-height:100px;*/
  }
  .btn-main-menu:hover{
    border:2px solid #8a2f88;
    background-color: white;
    color: #8a2f88;
  }
  .ourvalues img{
    height:70px;
  }
  .main-title{
    color: #8a2f88;
  }

  .ourvalues h3{
    font-size: 36px;
  }
  .box-register label.letter-black{
    margin-bottom:3px;
    font-size: 13px;
  }
  .bullet-point{
      width: 5px;
    height: 5px;
    display: -webkit-inline-box;
    border-radius: 100%;
    background-color: #71b62c;
  }
  .text-explain{
    color: #555;
    font-size: 25px;
  }
  .blue-bg {
  background-color: white;
  color: #8a2f88;
  height: 100%;
  padding-bottom: 20px !important;
}

.circle {
  font-weight: bold;
  padding: 15px 20px;
  border-radius: 50%;
  background-color: #71b62c;
  color: white;
  max-height: 50px;
  z-index: 2;
}
.circle.active{
      background: #8a2f88;
    border: inset 3px #8a2f88;
    max-height: 70px;
    height: 70px;
    font-size: 25px;
    width: 70px;
}
.support-section{
  background-color: white;
}
.support-section h2{
  text-align: center;
    padding: 60px 0px !important;
    background: #8a2f88;
    font-size: 40px;
    color: white;
    margin-bottom: 20px;
}
.timeline-ctc h2{
 text-align: center;
    padding: 105px 0px 60px 0px !important;
    background: #8a2f88;
    font-size: 40px;
    color: white;

}
.how-it-works.row {
  display: flex;
}
.row.timeline{
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
  padding:0;
  margin-bottom: 0;
}
.how-it-works.row .col-2 {
  display: inline-flex;
  align-self: stretch;
  position: relative;
  align-items: center;
  justify-content: center;
}
.how-it-works.row .col-2::after {
  content: "";
  position: absolute;
  border-left: 3px solid #cfc2ae;
  z-index: 1;
}
.pb-3, .py-3 {
    padding-bottom: 1rem !important;
}
.pt-2, .py-2 {
    padding-top: 0.5rem !important;
}
.how-it-works.row .col-2.bottom::after {
  height: 50%;
  left: 50%;
  top: 50%;
}
.how-it-works.row.justify-content-end .col-2.full::after {
  height: 100%;
  left: calc(50% - 3px);
}
.how-it-works.row .col-2.full::after {
    height: 100%;
    left: calc(50% - 0px);
}
.how-it-works.row .col-2.top::after {
  height: 50%;
  left: 50%;
  top: 0;
}

.timeline-ctc .timeline div {
  padding: 0;
  height: 40px;
}
.timeline-ctc .timeline hr {
  border-top: 3px solid #cfc2ae;
  margin: 0;
  top: 17px;
  position: relative;
}
.timeline-ctc .timeline .col-2 {
  display: flex;
  overflow: hidden;
  flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.align-items-center {
    -ms-flex-align: center !important;
    align-items: center !important;
}
.justify-content-end {
    -ms-flex-pack: end !important;
    justify-content: flex-end !important;
}
.row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 15px;
    margin-left: -15px;
}
.how-it-works.row .col-6 p{
  color: #444;
}
.how-it-works.row .col-6 h5{
font-size: 17px;
    text-transform: inherit;
}
.col-2 {
    -ms-flex: 0 0 16.666667%;
    flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.col-6 {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}
.timeline-ctc .timeline .col-8 {  
    flex: 0 0 66.666667%;
    max-width: 66.666667%;
}
.timeline-ctc .timeline .corner {
  border: 3px solid #cfc2ae;
  width: 100%;
  position: relative;
  border-radius: 15px;
}
.timeline-ctc .timeline .top-right {
  left: 50%;
  top: -50%;
}
.timeline-ctc .timeline .left-bottom {
  left: -50%;
  top: calc(50% - 3px);
}
.timeline-ctc .timeline .top-left {
  left: -50%;
  top: -50%;
}
.timeline-ctc .timeline .right-bottom {
  left: 50%;
  top: calc(50% - 3px);
}

  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){

  }
	
</style>

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
    
  <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-12 no-padding">
    <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mayenneDemain/banner.jpg'> 
  </div>

  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#8a2e88; max-width:100%; float:left;">
        <div class="col-xs-12 no-padding" style="margin-top:100px;"> 
             <div class="col-xs-12 no-padding">
                 <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top:-80px;margin-bottom:-80px;background-color: #fff;font-size: 14px;z-index: 5;">
 
                            <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mayenneDemain/suspension-site.png'>
             
                    </div>
                </div>
            </div>
        </div>
    </div>






        

    
        

      