<style type="text/css">
  #home-container{
    background-color:black;
  }
  .playBtn{
    color: black !important;
    font-size: 22px;
    padding: 5px 10px !important;
    margin-top: 20px;
    margin-bottom: 20px;
    font-weight: 800;
  }
</style>
<div id="home-container" class="col-xs-12 no-padding">
    <h3 class="main-title padding-20"><span class="text-yellow">Ch'Titube</span>, le jeu est fini !!<br/><br/></h3>
    <a href="#search" class="lbh btn bg-yellow col-xs-6 col-xs-offset-3 playBtn">Cartographie du 62</a><br/><br/>
    <a href="#agenda" class="lbh btn bg-yellow col-xs-6 col-xs-offset-3 playBtn">Agenda du 62</a><br/><br/>
<?php 
  $countPerson=PHDB::count(Person::COLLECTION, array('$or'=>array(
      array("source.key"=>"chtitube"),
      array("reference.costum"=>array('$in'=>["chtitube"]))))); 
if(Yii::app()->session["userId"]!=""){
  $name=(!empty(Yii::app()->session["user"]["username"])) ? Yii::app()->session["user"]["username"] : Yii::app()->session["user"]["name"];
  $infosGammifications=Element::getElementById(Yii::app()->session["userId"], Person::COLLECTION, null,  array("gamification"));
  $totalPoint=0;
  if(!empty($infosGammifications["gamification"]) 
    && isset($infosGammifications["gamification"]["source"])
    && isset($infosGammifications["gamification"]["source"]["chtitube"])
    && isset($infosGammifications["gamification"]["source"]["chtitube"]["total"])){
    $totalPoint=$infosGammifications["gamification"]["source"]["chtitube"]["total"];
  }
  $rangeInTournament=PHDB::count(Person::COLLECTION, array(
    '$or'=>array(
      array("source.key"=>"chtitube"),
      array("reference.costum"=>array('$in'=>["chtitube"]))),
      "gamification.source.chtitube.total"=>array('$gte'=>$totalPoint),
      "_id"=>array('$ne'=> new MongoId(Yii::app()->session["userId"]))));
?>
<div class="chtitube-container col-xs-12 no-padding text-center">
  <h3 class="main-title padding-20">Salut <span class="text-yellow"><?php echo  $name ?></span></h3>
  <span style="font-size: 20px;">Vous avez <b class="text-yellow"><?php echo $totalPoint ?></b> points</span><br/><br/>
  <span style="font-size: 20px;">Votre classement :<b class="text-yellow"><?php echo $rangeInTournament+1 ?></b> / <?php echo $countPerson ?></span><br/><br/>
  <?php if($rangeInTournament+1 < 6){ ?> 
    <span style="font-size:22px;"><b class="text-yellow">Bravo !!! Vous êtes sur le podium des 5 meilleur⋅es chtitubeur⋅se⋅s !!</b></span><br/><br/>
  <?php }else{ ?>
    <span style="font-size:22px;"><b class="text-yellow">Navré, vous n'êtes pas arrivé dans les 5 premier⋅es chtitubeur⋅se⋅s !!</b></span><br/><br/>
  <?php } ?>
  <a href="#play" class="lbh btn bg-yellow col-xs-6 col-xs-offset-3 playBtn">Participer</a><br/><br/>
  <a href="#references" class="lbh btn text-yellow col-xs-12" style="font-size: 22px;">Retrouvez vos ajouts</a>
   <div class="btnShare col-xs-12 margin-top-20 margin-bottom-20">
      <img class="co3BtnShare" width="60" src="<?php echo $this->module->getParentAssetsUrl() ?>/images/social/facebook-icon-64.png" onclick="window.open('https://www.facebook.com/sharer.php?u=www.communecter.org/costum/co/index/id/chtitube','_blank')"/>
      
      <img class="co3BtnShare margin-left-10" width="60" src="<?php echo $this->module->getParentAssetsUrl() ?>/images/social/twitter-icon-64.png" onclick="window.open('https://twitter.com/intent/tweet?url=www.communecter.org/costum/co/index/id/chtitube','_blank')"/>
    </div>
   
 
</div>
<?php } else { ?>
<div class="col-xs-12 no-padding text-center" style="max-height: 350px;">
    <img class="img-responsive start-img" style="max-height: 350px;margin:auto;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/chtitube/logo.png'>
  </div>
  <br/>
  <h3 class="main-title padding-20">L’équipe du <span class="text-yellow">Ch'Titube</span> vous a proposé de faire monter la <span class="text-yellow">la pression</span> <br/>avec ce jeu 100%  <span class="text-yellow">Libre</span> et 100% <span class="text-yellow">Ouvert</span></h3>
  <span class="col-xs-12 text-center">Pendant deux mois, 19 chtitubeurs se sont affrontés pour gagner de bonnes <a href="#lots" class="lbh text-yellow">bières artisanales du 62</a> tout en contribuant à un bien commun <br/><a href="#regles" class="lbh text-yellow">+ en savoir plus</a></span>
  <span class="col-xs-12 text-center margin-top-20">C'est fini !! Mais vous pouvez continuer à participer à la cartographie et l'agenda du 62</span>
  <!--<span class="col-xs-12 text-center">Déjà <?php echo $countPerson ?> Joueur⋅se⋅s</span>-->
  <div class="col-xs-12 text-center text-white margin-top-20">
    <button class="btn-register col-xs-12" data-toggle="modal" data-target="#modalLogin">Participer</button>
    <button class="btn-register col-xs-12" data-toggle="modal" data-target="#modalRegister">Inscrivez-vous dès maintenant en cliquant ici</button>
    <span class="italic col-xs-12">Et surtout partagez à vos ami⋅e⋅s</span>
    <div class="btnShare col-xs-12 margin-top-20 margin-bottom-20">
      <img class="co3BtnShare" width="60" src="<?php echo $this->module->getParentAssetsUrl() ?>/images/social/facebook-icon-64.png" onclick="window.open('https://www.facebook.com/sharer.php?u=www.communecter.org/costum/co/index/id/chtitube','_blank')"/>
      
      <img class="co3BtnShare margin-left-10" width="60" src="<?php echo $this->module->getParentAssetsUrl() ?>/images/social/twitter-icon-64.png" onclick="window.open('https://twitter.com/intent/tweet?url=www.communecter.org/costum/co/index/id/chtitube','_blank')"/>
    </div>
   <!-- <a href="#search" class="lbh" class="text-yellow">Carto</a></span>
    <a href="#agenda" class="lbh" class="text-yellow">Agenda</a></span>-->
    
  </div>
</div>
<?php } ?>
<script type="text/javascript">
 
  jQuery(document).ready(function() {
      // Lancement du compte à rebours au chargement de la page
      setTitle("Ch'Titube, fais monter la pression");
      setTimeout(function(){
        coInterface.initHtmlPosition();
      },1000)
      //$(".openRegister").click(function(){
        //Login.openRegister();
      //});
});
</script>


