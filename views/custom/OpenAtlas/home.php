<?php 

$cssAnsScriptFilesTheme = array(
			 '/plugins/jsonview/jquery.jsonview.js',
		'/plugins/jsonview/jquery.jsonview.css',
		'/plugins/jQuery/jquery-2.1.1.min.js',
		'/plugins/jQuery/jquery-1.11.1.min.js',
		'/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
		'/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js',
		'/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
		'/plugins/jQuery-Knob/js/jquery.knob.js',
			'/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
			//'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
			// SHOWDOWN
			'/plugins/showdown/showdown.min.js',
			// MARKDOWN
			'/plugins/to-markdown/to-markdown.js',
		);

$cssAndScriptFilesModule = array(
		'/js/default/profilSocial.js',
	);
HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());

HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 

$poiList = array();

if(isset(Yii::app()->session["costum"]["contextType"]) && isset(Yii::app()->session["costum"]["contextId"])){
		$el = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );

		$poiList = PHDB::find(Poi::COLLECTION, 
										array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
													 "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
													 "type"=>"cms") );
}
?>

<style type="text/css">

.disconnectConnection{
	position: absolute;
	bottom: 170px !important;
	margin-left: -4vw;
	display: none;
}

#newsstream .loader{
    display: none;
}

#donation-btn{
	display: none;
}

@font-face{
        font-family: "CoveredByYourGrace";
        src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/CoveredByYourGrace.ttf")
}

.titre{
	font-family: 'CoveredByYourGrace'!important;
}

.entityProfil{
	display: none;
}

</style>

	<div style="padding: 0px 1px;" class="col-xs-12 col-lg-12">
    	<div id="docCarousel" class="carousel slide" data-ride="carousel">

			<div class="carousel-inner">
        		<div class="item active">
        			<img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/OpenAtlas/Bandeau.png'> 
        		</div>

	        	<div class="item">
	        		<img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/OpenAtlas/Bandeau-Haut-Bateau.svg'>
	        	</div>
    		</div>
    			<a class="left carousel-control" href="#docCarousel" data-slide="prev">
    				<span class="glyphicon glyphicon-chevron-left"></span>
    				<span class="sr-only">Previous</span>
  				</a>
  				<a class="right carousel-control" href="#docCarousel" data-slide="next">
    				<span class="glyphicon glyphicon-chevron-right"></span>
    				<span class="sr-only">Next</span>
  				</a>
    	</div>
  	</div>

	<div class="col-xs-12 no-padding" style="margin-top: 13vw;">
		<div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: <?php echo Yii::app()->session["costum"]["colors"]["grey"]; ?>; ">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="font-family: montserrat; margin-top:-200px;background-color: #fff;font-size: 14px;z-index: 5;">
						<div class="col-xs-12  ourvalues" style="text-align:center;">
							<h2 class="mst col-xs-12 text-center" style="color:<?php echo Yii::app()->session["costum"]["colors"]["blue"]; ?>;font-family: 'CoveredByYourGrace' !important;font-size: 5vw;">
							<br>Aventurier des communs
							</h2>
							<div style="margin-left: 12vw;" class="col-xs-12 col-sm-4">
	                			Créateur de solution libre
		            		</div>

				            <div class="col-xs-12 col-sm-4">
			                	Incubateur de projets sociaux Innovants 
				            </div>
						</div><br/>
				</div>
		</div>
	</div>

	<div id="AssoLibre" class="text-center col-xs-10 col-lg-8" style="margin-left: 15vw;">
		<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/OpenAtlas/ligne verte.svg">
		<h1 class="titre col-xs-12 col-lg-12" style="color:<?php echo Yii::app()->session["costum"]["colors"]["green"]; ?>;position: absolute;margin-top: -5vw;font-family: 'CoveredByYourGrace' !important; font-size: 5vw; " class="col-lg-9 padding-20 text-center" > Une assocation << Libre >> </h1>

		<div class="col-xs-12 margin-top-20">
			<?php 
			$params = array(
				"poiList"=>$poiList,
				"listSteps" => array("1","2","3","4","5","6"),
													"el" => $el,
													"color1" => Yii::app()->session["costum"]["colors"]["green"]
			);
			echo $this->renderPartial("costum.views.tpls.wizard",$params); ?>
		</div>
	</div>

	<div class="text-center col-xs-8 col-lg-8" style="margin-left: 15vw;">
		<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/OpenAtlas/ligne bleu.svg">
		<h1 class="titre col-xs-12 col-lg-12" style="position: absolute;margin-top: -5vw; font-size: 5vw;font-family: 'CoveredByYourGrace' !important;" class="padding-20 text-center col-lg-5" > Nos projets</h1>
	</div>

	<style type="text/css">
		#avance > p > img{
			width:3vw;
		}

		#Projet > p {
			position: absolute;
		}

		.numVal{
			font-size: 30px
		}

		.borderInv{
			border-bottom: 1px dashed white;
		}

		@media all and (max-width: 980px){
		#avance{
			margin-left: -27vw;
		}

		#AssoLibre{
			margin-left:8vw;
		}
	}
	</style>

	<div id="Projet" class="col-xs-12 col-lg-12" >
		<div id="avance">
			<p class="col-lg-5 text-center" style="margin-left: 28vw;">
				<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/OpenAtlas/PictoEnCours.svg">
				En cours
			
				<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/OpenAtlas/PictoArchives.svg">
				Archivés

				<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/OpenAtlas/PictoStoppes.svg">
				Stoppés

				<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/OpenAtlas/PictoAVenir.svg">
				A venir
			</p>
		</div>

		<div class="col-xs-7 col-lg-10 text-center" id='content-results-project' style="margin-top:3vw;margin-left: 8vw;">
			<!-- içi que s'affiche les projets --> 
		</div>

		<a href="javascript:;" class="addProjects">
			<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/OpenAtlas/bouton plus.svg" class="img-responsive plus-m" style="margin-left: 45vw;width: 4vw;margin-top: -5vw"><br>
			<p class="col-lg-4 text-center" style="margin-left: 31.3vw;">
				Proposer un projet pour OpenAtlas
			</p>
		</a>
	</div>

	<div class="col-xs-8 col-lg-8" style="margin-top: 5vw;margin-left: 15vw;">

			<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/OpenAtlas/ligne rose.svg">
			<img style="position: absolute;width: 38vw;margin-left: -50vw;" class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/OpenAtlas/nous rejoindre.png">

			<div class="col-lg-12 text-center" id='content-results-persons' style="margin-top:2vw;">
				<!-- içi que s'affiche les persons --> 
			</div>
			
			<a href="javascript:;" data-hash="#citoyens" class="lbh-menu-app btn btn-redirect-home btn-small-orange">
				<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/OpenAtlas/bouton plus.svg" class="img-responsive plus-m" style="margin-top: 1vw;margin-left: 30vw;width: 4vw;"><br/>
			</a>
			
			<div class="text-center">
				<p style="margin-top: 2vw;margin-left: 2vw;">
					Open Atlas, ce sont déjà plusieurs dizaines de développeurs, animateurs <br/>
					coachs, communiquants et électrons libres.<br/>
					<br/>
					Nous sommes toujours à la recherche de nouvelles énergies<br/>
					alors n'hésitez pas à nous contacter : <br/>
					contact@communecter.org<br/>
					<br/>
					<b>Vision carto alternative si la personne veut voir l'acteur le plus proche de chez elle</b>
				</p>

				<a href="https://www.helloasso.com/associations/open-atlas/collectes/communecter/don">
				<button style="margin-left: 1vw;" type="button" class="btn btn-danger">Adhérer</button>
				</a>
			
				<a href="https://www.helloasso.com/associations/open-atlas/collectes/communecter/don">
				<button type="button" class="btn btn-danger">Donner</button>
				</a>

				<br/><br/>
			</div>
	</div>

	<div class="text-center col-sm-8 col-xs-8 col-lg-8" style="margin-left: 15vw;">
		<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/OpenAtlas/ligne bleu.svg">
		<h1 style="position: absolute;margin-top: -5vw;font-family: 'CoveredByYourGrace' !important;font-size:5vw;" class="text-red titre col-xs-12 col-lg-12" > ACTUS</h1>

		<div id="newsstream">
			<div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="background-color: white; ">
				
			</div>
		</div>

		<div class="text-center" style="margin-top: -16vw;">
			<a href="javascript:;" data-hash="#live" class="lbh-menu-app" style="text-decoration : none; font-size:2rem;">
				<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/OpenAtlas/bouton plus.svg" class="img-responsive plus-m" style="margin: 1%;width: 4vw;"><br/>
					Voir plus d'actualités
			</a>
		</div>
	</div>

	<div class="col-xs-12 col-lg-12" style="background-color: <?php echo Yii::app()->session["costum"]["colors"]["grey"]; ?>">

		<h1 style="position: absolute;margin-left: 35vw;margin-top: 3vw;font-family: 'CoveredByYourGrace' !important;font-size:71px;" class="text-dark text-center cbyg uppercase" >NOTRE ECOSYSTÈME <br/> <span style="position: absolute;margin-left: -13.7vw;margin-top: -1vw;font-family: 'CoveredByYourGrace' !important;font-size:47px;">EN SCHÉMA</span></h1>

			<img style="width: 13vw;margin-left: 15vw;margin-top: 3vw;position: absolute;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/OpenAtlas/LegendeSchemaEcosysteme.svg" class="img-responsive plus-m">
			<?php echo $this->renderPartial("costum.views.custom.OpenAtlas.schema"); ?>
	</div>

		<div class="col-xs-12 col-lg-12 margin-top-20">
			<h2 class="text-dark text-center" >Merci a nos partenaires</h2><br/>
			<div class="col-xs-9 col-lg-9" style="margin-left: 12vw;">
				<h2 class="text-center text-dark">Financeurs</h2>
				<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/OpenAtlas/partenaires/LigneLogos.png">
			</div>
			<br/>
			<br/>

			<h2 class="text-dark text-center">CONTRIBUTEURS</h2>
			<br/>
			<p class="text-center">
				W3C Social Working Group . Unisscn, ChezNous . Zero Waste France, Open Source Politics . <br/>
				IAAC . FabLab Barcelona . Assemblée des <br/>
				communs . European Commons Assembly . Open Apps Ecosystem . Université du nous . Les <br/> 
				Colibris et Près de chez nous . La Myne ...
			</p>
		</div>


<script type="text/javascript">
	jQuery(document).ready(function() {

		pageProfil.views.directoryProjects = function(){
				var donnee = {
					"pageProfil.params.dir" : "projects",
					"links" : {
						"citoyens" : "follows",
						"organizations" : "members",
						"event" : "attendees",
						"projects" : "contributors"
					},
					"contextData.type" : "organizations",
					"limit" : 3
				} ;

					var dataIcon = (!notEmpty(donnee["pageProfil.params.dir"])) ? "users" : $(".smma[data-type-dir="+donnee["pageProfil.params.dir"]+"]").data("icon");

					donnee["pageProfil.params.dir"]=(!notEmpty(donnee["pageProfil.params.dir"])) ? donnee["links"][donnee["contextData.type"]] : donnee["pageProfil.params.dir"];

						getAjax('', baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+donnee["contextData.type"]+'/id/'+costum.contextId+'/dataName/'+donnee["pageProfil.params.dir"]+'/limit/'+donnee["limit"]+'?tpl=json',
							function(data){ 
								var type = ($.inArray(donnee["pageProfil.params.dir"], ["poi","ressources","vote","actions","discuss"]) >=0) ? donnee["pageProfil.params.dir"] : null;

								if(typeof donnee["pageProfil.params.dir"] != "undefined" && donnee["pageProfil.params.dir"])
									canEdit=donnee["pageProfil.params.dir"];

								mylog.log("DATA HERE", data);
								var html = "";
								html = html + directory.showResultsDirectoryHtml(data, null, null, canEdit);

								$("#content-results-project").html(html);
								if(typeof mapCO != "undefined"){
									mapCO.clearMap();
									mapCO.addElts(data);
									mapCO.map.invalidateSize();
								}
								coInterface.bindButtonOpenForm();
							}
						,"html");
			};

			pageProfil.views.directoryPersons = function(){
				var donnee = {
					"pageProfil.params.dir" : "members",
					"links" : {
						"citoyens" : "follows",
						"organizations" : "members",
						"event" : "attendees",
						"projects" : "contributors"
					},
					"contextData.type" : "organizations",
					"limit" : 3
				} ;

				var dataIcon = (!notEmpty(donnee["pageProfil.params.dir"])) ? "users" : $(".smma[data-type-dir="+donnee["pageProfil.params.dir"]+"]").data("icon");

				donnee["pageProfil.params.dir"]=(!notEmpty(donnee["pageProfil.params.dir"])) ? donnee["links"][donnee["contextData.type"]] : donnee["pageProfil.params.dir"];

				getAjax('', baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+donnee["contextData.type"]+'/id/'+costum.contextId+'/dataName/'+donnee["pageProfil.params.dir"]+'/limit/'+donnee["limit"]+'?tpl=json',
						function(data){ 
							var type = ($.inArray(donnee["pageProfil.params.dir"], ["poi","ressources","vote","actions","discuss"]) >=0) ? donnee["pageProfil.params.dir"] : null;

							if(typeof donnee["pageProfil.params.dir"] != "undefined" && donnee["pageProfil.params.dir"])
								canEdit=donnee["pageProfil.params.dir"];
								// mylog.log("DATA HERE", data);
							var html = "";
							html = html + directory.showResultsDirectoryHtml(data, null, null, canEdit);

							mylog.log("html -----------",html);
							$("#content-results-persons").html(html);
							if(typeof mapCO != "undefined"){
								mapCO.clearMap();
								mapCO.addElts(data);
								mapCO.map.invalidateSize();
							}
							coInterface.bindButtonOpenForm();
						}
					,"html");
				};
		
		pageProfil.views.directoryProjects();

		pageProfil.views.directoryPersons();

		var urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";

		ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html");

		$.each($(".markdown"), function(k,v){
				descHtml = dataHelper.markdownToHtml($(v).html()); 
				$(v).html(descHtml);
		});

		contextData = {
				id : costum.contextId,
				type : costum.contextType,
				name : costum.title,
				profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
		};

		$(".addProjects").click(function(){
			dyFObj.openForm("project");
		});

	});
</script>