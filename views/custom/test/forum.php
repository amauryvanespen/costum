<?php 
$cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
  // SHOWDOWN
  '/plugins/showdown/showdown.min.js',
  // MARKDOWN
  '/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl); 

if(isset(Yii::app()->session["costum"]["contextType"]) && isset(Yii::app()->session["costum"]["contextId"])){
    $el = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );
    $poiList = PHDB::find(Poi::COLLECTION, 
                    array( 
                      "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                      "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                      "type"=>"forum") );
} else {?>
  
  <div class="col-xs-12 text-center margin-top-50">
    <h2>Ce COstum est un template <br/>doit etre associé à un élément pour avoir un context.</h2>
  </div>

<?php exit;}?>


<div class="col-xs-12 margin-top-50" style="padding:0px;">
    
    <div class="col-xs-12 text-center margin-bottom-20" style="padding:0px;">
      <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
      <bloquote style="font: 10 25px/1 'Pacifico', Helvetica, sans-serif;
    color: #2b2b2b;
    text-shadow: 4px 4px 0px rgba(0,0,0,0.1);">
     « <span class="text-red">FAUX</span> RHUM CMS»</bloquote>
  </div>
  <br/>
              
    <div class="col-md-10 col-md-offset-1 col-xs-12">
      <?php 
      echo $this->renderPartial("costum.views.tpls.multiElementBlock", array(
          "poiList"   => $poiList,
          "blockName" => "el",
          "titlecolor"=> "#e6344d",
          "blockCt"   => count($poiList)+1
        ), true );
       ?>
    </div>

  
</div>  
        



<?php 
/*
- search filter by src key 
- admin candidat privé only
- app carte 
*/
 ?>


<script type="text/javascript">
  jQuery(document).ready(function() {
    
    setTitle("CMS Forum tests");
    contextData = {
        id : "<?php echo Yii::app()->session["costum"]["contextId"] ?>",
        type : "<?php echo Yii::app()->session["costum"]["contextType"] ?>",
        name : "<?php echo $el['name'] ?>",
        slug : "<?php echo $el['slug'] ?>",
        profilThumbImageUrl : "<?php echo $el['profilThumbImageUrl'] ?>"
    };
    if(contextData.id == "")
      alert("Veuillez connecter ce costum à un élément!!")
    
    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });
    
});

</script>

