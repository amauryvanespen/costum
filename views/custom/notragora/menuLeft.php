<?php

	// $cssAnsScriptFilesModule = array(
	// 	'/css/menu.css',
	// 	'/css/menuLeft.css',
	// 	'/js/menu.js',
	// );
	// HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( "notragora" )->getAssetsUrl());
?>

<style>
	.footer-menu-left{
		/*background-image: url("<?php echo $this->module->assetsUrl; ?>/images/bg/footer_menu_left.png");*/
		/*background-image: url("<?php echo $this->module->assetsUrl; ?>/images/people.jpg");*/
	}

	.title-menu-left{
		color:grey;
		font-weight:800;
	}
	.menu-left-container a{
		color: #b4b4b4;
		font-weight: 700;
	}
	.menu-left-container a:hover{
		color:grey;
	}
	.carousel-indicators{
		bottom:-10px;
	}

	#menuLeftNA{
		position: absolute;
	    bottom: 0px;
	    border-right: 1px solid rgba(150,150,150,0.3);
	    z-index: 150;
	    left: 0px;
	    padding: 0px !important;
	    z-index: 1;
	    box-shadow: 0px 1px 3px -1px rgba(0,0,0,0.5);
	    top: 197px;
	    width: 230px;
	    background-color : white;
	}

	/*#bg-homepage, .social-main-container{
		margin-left: 230px;
	}*/
	
	.pageContent{
		margin-left: 230px !important;
	} 


</style>
<?php 
	$whereO = array("name"=>array('$exists'=>1),"profilMediumImageUrl"=>array('$exists'=>1), "source.key" => "notragora");
	$projects = PHDB::findAndSortAndLimitAndIndex( Organization::COLLECTION, $whereO , array("updated" => -1), 3, 0);
	?>
<div id="menuLeftNA" class="hidden-xs main-menu-left col-md-2 col-sm-2 padding-10"  data-tpl="menuLeft" <?php if (@$emptyTop && $emptyTop==true) { ?> style="top:50px !important;" <?php } ?>>

	<div class="menu-left-container">
	<?php if (@$projects && !empty($projects)){ ?>
		<div class="col-md-12">
			<span class="title-menu-left text-brown">FOCUS<i class="fa fa-angle-down pull-right"></i>
				<br>
				<hr>
			</span>
		</div>
		<div class="col-md-12">
			<div id="docCarousel" class="carousel slide" data-ride="carousel">
	  <!-- Indicators -->
			  <ol class="carousel-indicators">
				<?php
					$i=0;
					foreach ($projects as $data){
						if(@$data["profilMediumImageUrl"] && !empty($data["profilMediumImageUrl"])){ ?>
						<li data-target="#docCarousel" data-slide-to="0" class=" <?php if($i==0) echo "active" ?>"></li>
						<?php $i++;
						}
					} ?>
			  </ol>

	  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
				<?php
					$inc=0;
					foreach ($projects as $data){
						if(@$data["profilMediumImageUrl"] && !empty($data["profilMediumImageUrl"])){ ?>
						<div class="item <?php if($inc==0) echo "active" ?>">

							<?php $urlO = "#page.type.".Organization::COLLECTION.".id.".(String)$data['_id']; ?>
							<a href="<?php echo  $urlO; ?>" class="lbh">
								<img src="<?php echo Yii::app()->createUrl('/'.$data["profilMediumImageUrl"]); ?>" class="col-sm-12 img-responsive no-padding">
							</a>
						</div>
				<?php $inc++;
					} }?>
			  </div>
			  <!-- Left and right controls -->
			</div>
		</div>
	<?php } ?>
		<div class="col-md-12" id="poiParent">
			<!--<img src="<?php echo $this->module->assetsUrl?>/images/velo.png" class="img-responsive">-->
		</div>
    <a class="col-md-12 margin-top-15" href="javascript:directory.showAll('.favSection','.searchPoiContainer');">Voir tout</a><br>
		<div class="col-md-12 margin-top-15">
			<span class="title-menu-left text-brown">
				LES COLLECTIONS
				<i class="fa fa-angle-down pull-right"></i><br>
				<hr>
			</span>
			<div class="collectionsList">
				<?php
				if(!empty(Yii::app()->session["costum"]["collections"])){
					foreach (Yii::app()->session["costum"]["collections"] as $keyC => $valC) {
						$slugVal = InflectorHelper::slugify2($valC) ;
						echo '<a href="javascript:addItemsToSly(\''.$slugVal.'\')" class="favElBtn '.$slugVal.'Btn" data-tag="'.$slugVal.'">'.$valC.'</a><br>';
					}
				}
				?>
			</div>
		</div>

		<div class="col-md-12 margin-top-15">
			<span class="title-menu-left text-brown">
				LES GENRES
				<i class="fa fa-angle-down pull-right"></i><br>
				<hr>
			</span>
			<div class="genresList">
				<?php
				if(!empty(Yii::app()->session["costum"]["genres"])){
					foreach (Yii::app()->session["costum"]["genres"] as $keyC => $valC) {
						$slugVal = InflectorHelper::slugify2($valC) ;
						echo '<a href="javascript:addItemsToSly(\''.$slugVal.'\')" class="favElBtn '.$slugVal.'Btn" data-tag="'.$slugVal.'">'.$valC.'</a><br>';
					}
				}
				?>
			</div>
		</div>

  </div>
</div>

<div class="visible-xs" id="menu-bottom">
	<input type="text" class="text-dark input-global-search visible-xs" id="input-global-search-xs" placeholder="rechercher ..."/>
	<?php
	if(isset(Yii::app()->session['userId']) && false){ ?>
		<button class="menu-button menu-button-title btn-menu btn-menu-add" onclick="">
		<span class="lbl-btn-menu-name">Ajouter</span></span>
		<i class="fa fa-plus-circle"></i>
		</button>
	<?php } ?>
</div>


<script type="text/javascript">

var timeoutCommunexion = setTimeout(function(){}, 0);

var urlLogout = "<?php echo Yii::app()->createUrl('/'.$this->module->id.'/person/logout'); ?>";

var menuExtend = ["organization", "project", "people", "vote", "action"];
jQuery(document).ready(function() {
	// var genresHtml="";
	// $.each(costum.genres, function(i, v) {
	// 	//"javascript:addItemsToSly('".InflectorHelper::slugify2($val)."')"
	// 	genresHtml+='<a href="javascript:addItemsToSly(\''+slugify(v)+'\')" class="favElBtn '+slugify(v)+'Btn" data-tag="'+slugify(v)+'">'+v+'</a><br>';
	// });
	// $(".genresList").html(genresHtml);
	// var collectionsHtml="";
	// $.each(costum.collections, function(i, v) {
	// 	collectionsHtml+='<a href="javascript:addItemsToSly(\''+slugify(v)+'\')" class="favElBtn '+slugify(v)+'Btn" data-tag="'+slugify(v)+'">'+v+'</a><br>';
	// });
	// $(".collectionsList").html(collectionsHtml);

	$(".carousel-control").click(function(){
    var top = $("#docCarousel").position().top-30;
    $(".my-main-container").animate({ scrollTop: top, }, 300 );
  });

  $(".btn-carousel-previous").click(function(){ //toastr.success('success!'); 
      var top = $("#docCarousel").position().top-30;
      $(".my-main-container").animate({ scrollTop: top, }, 100 );
      setTimeout(function(){ $(".carousel-control.left").click(); }, 500);
    });
   $(".btn-carousel-next").click(function(){ //toastr.success('success!'); 
      var top = $("#docCarousel").position().top-30;
      $(".my-main-container").animate({ scrollTop: top, }, 100 );
      setTimeout(function(){ $(".carousel-control.right").click(); }, 500);
    });

	//bindEventMenu();

	$(".menu-button-left").mouseenter(function(){
		$(this).addClass("active");
	});
	$(".menu-button-left").mouseout(function(){
		$(this).removeClass("active");
	});
	$(".menu-button-left").click(function(){
		$(".menu-button-left").removeClass("selected");
		$(this).addClass("selected");
	});

	<?php if(!empty($myCity)){ ?>
		$(".visible-communected").hide(400);
		$(".hide-communected").show(400);
	<?php } ?>

	extendMenu(true);
});

function extendMenu(open){
	if(!notEmpty(open)) open = $("#menu-extend").hasClass("hidden");
	if(open){
		$("#menu-extend").removeClass("hidden", 300);
		$(".lbl-extends-menu i").removeClass("fa-angle-down").addClass("fa-angle-up");

	}else{
		$("#menu-extend").addClass("hidden", 300);
		$(".lbl-extends-menu i").addClass("fa-angle-down").removeClass("fa-angle-up");
	}
}

function openParams(){
	$("#modalParams").modal("show");
}



</script>
