<?php 
$bannerImg = @Yii::app()->session["costum"]["banner"] ? Yii::app()->baseUrl.Yii::app()->session["costum"]["banner"] : Yii::app()->getModule("costum")->assetsUrl."/images/filiereCostum/no-banner.jpg";
$logo = @Yii::app()->session["costum"]["logo"] ? Yii::app()->session["costum"]["logo"] : null;
$shortDescription = @Yii::app()->session["costum"]["description"] ? Yii::app()->session["costum"]["description"] : null;
$bkagenda = @Yii::app()->session["costum"]["tpls"]["blockevent"]["background"] ? Yii::app()->session["costum"]["tpls"]["blockevent"]["background"] : "white";
$titlenews = @Yii::app()->session["costum"]["tpls"]["news"]["title"] ? Yii::app()->session["costum"]["tpls"]["news"]["title"] : "Actualité";

$cssAnsScriptFilesTheme = array(
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    // MARKDOWN
    '/plugins/to-markdown/to-markdown.js'            
  );
  HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 

  $poiList = array();
  if(isset(Yii::app()->session["costum"]["contextType"]) && isset(Yii::app()->session["costum"]["contextId"])){
      $el = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );
  
      $poiList = PHDB::find(Poi::COLLECTION, 
                      array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                             "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                             "type"=>"cms") );
  } 
?>
<style>
section {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/templateCostumDesTiersLieux/background.jpg);
}
.header{
    background: #001146;
    background-image : url(<?= $bannerImg; ?>);
    width: 101%;
    height: auto;
    background-size: cover;
    padding-bottom: 58%;
}
.logofn{
    width: 50%;
margin-top: 5%;
position: absolute;
margin-left: 3%;
}
.carto{
    margin-top : -7.9%;
}
.carto-h1 {
    color : white;
}
.carto-d {
    box-shadow: 0px 0px 20px -2px black;
    width: 80%;
    left: 10%;
    background : white;
    padding: 2%;
    font-size : 1.5vw;
}
.blockwithimg {
    margin-top : 2%;
    margin-bottom: 2%;
}
.actu{
    margin-top:3%;

}
.agenda{
    /* background-image : url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/fond/fond.png); */
    color : white; 
    margin-top :3%;
    background : <?= $bkagenda; ?>;
}
.agenda-h1{
    margin-left :12%;
    left: 4%;
    margin-top: 2%;
}
.plus-m {
    width : 2%;
}
.img-event{
    height: 250px;
    width: 500px;
}
.carto-p-m{
    font-size: 2.85vw;
    margin-top: 6%;
    line-height: 42px;
}
.carto-n{
    margin-top: 2%;
}
.description{
    padding : 5%; 
    top: 5%;
}
.title-carto{
    font-size : 2.81em;
}
button {
    background-color: Transparent;
    background-repeat:no-repeat;
    border: none;
    cursor:pointer;
    overflow: hidden;
    outline:none;
}
#newsstream .loader{
    display: none;
}
@media (max-width:768px){
    .plus-m {
    width : 5%;
    }
    .carto-description{
        font-size: 2.5vw !important;
    }
    .img-event {
        height: 75px;
        width: 250px;
    }
    .carto-p-m{
        font-size: 2.5rem;
        margin-top: 2%;
        line-height: 19px;
    }
    .p-mobile-description{
        font-size : 1.5rem;
    }
    .description{
        padding : 5%; 
        margin-top: -3%;
    }
    .title-carto{
        font-size : 2.81vw;
    }
    .btn {
        padding: 3px 5px;
        font-size: 7px;
    }
    .card-mobile{
        margin-top : 3%;
    }
    .logofn{
        width: 100%;
        margin-top: 13%;
    }
}
</style>



<div class="header row">
    <?php 
$params = [  "tpl" => "filiereCostum","slug"=>Yii::app()->session["costum"]["slug"],"canEdit"=>$canEdit,"el"=>$el ];
echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params,true );  
?>
    <div class="logofn col-md-9">
        <?php
        if(@$logo && $logo != null) { ?>
        <img src="<?= $logo; ?>" id="logoBanner" class="img-responsive" style="width: 65%;">
        <?php } ?>
    </div>
</div>




<!-- Description -->

<div class="carto row">
    <div class="carto-h1 col-xs-6 col-sm-6 no-padding">
        <p class="title-carto" style="margin-left: 19.5%;" id="shortDescription"><?php echo $shortDescription; ?></p>
    </div>
    <div class="carto-img col-xs-6 col-sm-6" >
       <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/filigram.png" class="img-responsive" style="margin-top: -60%; margin-left:3%; position: absolute; opacity: 0.7;">
    </div>
    <div class="carto-d col-xs-12 col-sm-12">
        <p class="p-mobile-description" id="descriptions"> <?= $this->renderPartial("costum.views.tpls.text", array("poiList" => $poiList, "tag" => "descriptionsfil1"));
    ?></p>
        </div>

  
</div>

<!-- Carto NEWS -->

<div class="carto-n row">
    <div class="c-description container">
    <?= $this->renderPartial("costum.views.tpls.blockwithimg", array("poiList" => $poiList, "canEdit" => $canEdit)); ?>
    </div>
</div>


<!-- Join -->

<?php 
if(isset(Yii::app()->session["costum"]["custom"]["btnother"]))
        echo $this->renderPartial("costum.views.custom.filiereCostum.tpls.btn-other", array("poiList"   =>  $poiList, "canEdit" => $canEdit));
    else
        echo $this->renderPartial("costum.views.custom.filiereCostum.tpls.btn", array("poiList" => $poiList));
?>


<?php if(isset(Yii::app()->session["costum"]["tpls"]["news"])) { ?>
<!-- Actualité -->
<div class="actu row">
    <div class="col-xs-12 col-sm-12">
    <div class="actu-titre col-xs-6 col-sm-6" style="padding-left: 16%;">
        <h1><?= $titlenews; ?></h1>
  
    </div>
    <div class="col-xs-12">
        <?php 
            echo $this->renderPartial("costum.views.tpls.news", array("canEdit" => $canEdit));
        ?>
    </div>
    </div>
</div>
<?php } ?>

<!-- Agenda --> 
<?php if(isset(Yii::app()->session["costum"]["tpls"]["blockevent"])) { ?>
<div class="agenda row">
    <div class="agenda-h1 col-xs-6 col-sm-6">
    <h1><?php if(!empty(Yii::app()->session["costum"]["tpls"]["blockevent"]["title"])) echo Yii::app()->session["costum"]["tpls"]["blockevent"]["title"]; else "Agenda"; ?> <br></h1>
    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/trait-blanc.svg" class="img-responsive" style="padding-right: 69%;">
    <p>Évènement à venir</p>
    </div>
    <!-- <div class="event col-xs-12 col-sm-12">
        <div id="event-affiche"></div>

    </div> -->

    <!-- TEST EVENT -->
    <div class="col-xs-12">
    <?php echo $this->renderPartial("costum.views.tpls.blockevent",  array("canEdit" => $canEdit)); ?>
    </div>
    <div class="event-lien col-xs-12 col-sm-12 text-center">
            <p>
            <a href="javascript:;" data-hash="#agenda" class="lbh-menu-app " style="text-decoration : none; font-size:2rem; color:white;">
           <center> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/plus-blanc.svg" class="img-responsive plus-m" style="margin: 1%;"></center>          Voir plus<br>
            d'évènement.</a></p>
        </div>
</div>
<?php } ?>

<!-- Carousel --> 
    <?= $this->renderPartial("costum.views.tpls.eventCarousel",  array("canEdit" => $canEdit)); ?>


    <?php if($canEdit){ ?> 
<!-- ESPACE ADMIN --> 

<hr>
<div class="container">
<a href="javascript:;" class="addTpl btn btn-primary" data-key="blockevent" data-id="<?= Yii::app()->session["costum"]["contextId"]; ?>" data-collection="<?= Yii::app()->session["costum"]["contextType"]; ?>"><i class="fa fa-plus"></i> Ajouter une section</a>
</div>
<?php } ?>

<script>
jQuery(document).ready(function(){
    mylog.log("render","/modules/costum/views/custom/filiereCostum/home.php");
    setTitle(costum.title);

    contextData = {
        id : "<?php echo Yii::app()->session["costum"]["contextId"] ?>",
        type : "<?php echo Yii::app()->session["costum"]["contextType"] ?>",
        name : '<?php echo htmlentities($el['name']) ?>',
        profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
    };
});
</script>