<?php
$cs = Yii::app()->getClientScript();
$cssAnsScriptFilesModule = array(
		'/plugins/jsonview/jquery.jsonview.js',
		'/plugins/jsonview/jquery.jsonview.css',
		'/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
		'/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule,Yii::app()->request->baseUrl);

$colorSection1 = @Yii::app()->session["costum"]["cms"]["colorSection1"];
    $colorSection2 = @Yii::app()->session["costum"]["cms"]["colorSection2"];
?>
<style type="text/css">
	#text-search-menu #dropdown-apps{
  background-color: <?php echo $colorSection2 ?> !important;
}

.citoyen{
	margin-top: 2%;
	position: absolute;
    display: block;
    width: 600px;
}

.citation{
	text-align: center;
	background-color: <?php echo $colorSection2; ?>;
	margin-left: 13%;
	margin-top: -1%;
	position: absolute;
	z-index: 2;
	border: 5px solid transparent;
  	box-shadow: 0px 0px 24px 3px grey;
}

.cls-1{
	fill:none;
	stroke:#9D9D9C;
	stroke-width:0.5;
	stroke-miterlimit:10;
}

.cls-2{
	fill:<?php echo $colorSection2 ?>;
}

.cls-3{
	fill:#020203;
}

.cls-1:hover{
	fill: <?php echo $colorSection1 ?>;
}

.hexa{
	width: 44%;
	float: left;
	margin-left: -5%;
	margin-top: 0%;
    position:absolute;
}

</style>

	<div class="titleEduc">
		<h1 style="font-family: 'fb';font-size: revert;text-align: center;">EDUCATION</h1>
		<p style="font-family: 'fb';font-size: smaller;text-align: center;">
			Territoire Qu’on Emprunte à Nos Enfants
		</p>
			<div class="hidden-xs" style="margin-top: 19%;">
				<input style="position: absolute;opacity: 0.5;z-index: 1;margin-left: 3vw;width: 31vw;" type="text" class="form-control pull-left text-center main-search-bar barS" id="second-search-bar" placeholder="Que recherchez-vous ?">
			
				<a data-type="filters" href="javascript:;">
					<span id="second-search-bar-addon-smarterre" style="position: absolute;z-index: 10000;cursor: pointer;background-color: <?php echo $colorSection2 ?>;margin-left: 32vw;border-radius: 32px;width: 5VW;height: 40px;" class="text-white input-group-addon pull-left main-search-bar-addon-educ">
						<i style="margin-top: -6%;font-size: 34px" class="fa fa-search">
						</i>
					</span>
				</a>

				<div id="dropdown" class="dropdown-result-global-search hidden-xs col-sm-5 col-md-5 col-lg-5 no-padding" style="top: 39px;max-height: 70%;display: none;background-color: whitesmoke;width: 81%;margin-left: 11%;z-index: 1000;">
				</div>
			</div>
		</div>	

		<img class="header img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl;?>/images/smarterre/education/education.jpg" style="margin-top: -9%;">

	<div class="description">
		<p style="font-family: 'ml';font-size: 1.5vw;color: #5d8797" align="center">
			Les qualités d’un territoire et d’une société se révèle dans son éducation<br>
			libre, créative, productive, artistique, et virale
		</p>

<div class="visible-xs hidden-lg">
<?php echo $this->renderPartial("costum.assets.images.smarterre.hexaXS.hexaeducation"); ?>
</div>

	<div class="col-lg-5 hidden-xs">
		<?xml version="1.0" encoding="utf-8"?>
		<!-- Generator: Adobe Illustrator 22.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
		<svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 595.3 769.9" style="enable-background:new 0 0 595.3 841.9;" xml:space="preserve">
			<polygon onMouseOver="mouse(this.id)" id="2" class="cls-1" points="301.8,244.7 267.2,244.7 249.9,274.7 267.2,304.6 301.8,304.6 319.1,274.7 "/>
			<polygon onMouseOver="mouse(this.id)" id="3" class="cls-1" points="301.8,309 267.2,309 249.9,339 267.2,368.9 301.8,368.9 319.1,339 "/>
			<polygon onMouseOver="mouse(this.id)" id="4" class="cls-1" points="301.8,372.5 267.2,372.5 249.9,402.5 267.2,432.4 301.8,432.4 319.1,402.5 "/>
			<polygon onMouseOver="mouse(this.id)" id="5" class="cls-1" points="301.8,436.5 267.2,436.5 249.9,466.5 267.2,496.5 301.8,496.5 319.1,466.5 "/>
			<polygon onMouseOver="mouse(this.id)" id="1" class="cls-1" points="301.6,180.4 267,180.4 249.7,210.3 267,240.3 301.6,240.3 318.9,210.3 "/>
			<polygon onclick="floor('up');" id="up" class="cls-2" points="335.7,289.7 316,289.7 306.1,306.8 316,323.9 335.7,323.9 345.6,306.8 "/>
			<polygon onclick="floor('down');" id="down" class="cls-2" points="335.7,353.6 316,353.6 306.1,370.7 316,387.8 335.7,387.8 345.6,370.7 "/>
			<g>
				<path class="cls-3" d="M335.3,309.5c-1.8-0.6-3.5-1.3-5.2-2.1c-0.9-0.4-1.7-0.8-2.5-1.2c-0.8-0.4-1.6-0.9-2.4-1.3l1,0
					c-0.8,0.5-1.6,0.9-2.4,1.4c-0.8,0.4-1.6,0.9-2.4,1.3c-1.6,0.8-3.3,1.6-5.1,2.2c1.3-1.3,2.8-2.4,4.3-3.5c0.7-0.6,1.5-1,2.2-1.6
					c0.8-0.5,1.5-1,2.3-1.5l0.5-0.3l0.5,0.3c0.8,0.5,1.6,0.9,2.4,1.4c0.8,0.5,1.6,1,2.3,1.5C332.4,307.1,333.9,308.2,335.3,309.5z"/>
			</g>
			<g>
				<path class="cls-3" d="M316.3,369.5c1.8,0.6,3.5,1.3,5.2,2.1c0.9,0.4,1.7,0.8,2.5,1.2c0.8,0.4,1.6,0.9,2.4,1.3l-1,0
					c0.8-0.5,1.6-0.9,2.4-1.4c0.8-0.4,1.6-0.9,2.4-1.3c1.6-0.8,3.3-1.6,5.1-2.2c-1.3,1.3-2.8,2.4-4.3,3.5c-0.7,0.6-1.5,1-2.2,1.6
					c-0.8,0.5-1.5,1-2.3,1.5l-0.5,0.3l-0.5-0.3c-0.8-0.5-1.6-0.9-2.4-1.4c-0.8-0.5-1.6-1-2.3-1.5C319.2,371.9,317.7,370.8,316.3,369.5z
					"/>
			</g>
			<g id="g1">
				<title>sante</title>
				<path class="cls-3" d="M294.2,206.4v1c0,0,0,0.1,0,0.1c-0.1,1-0.4,1.9-0.8,2.8c-0.7,1.4-1.6,2.7-2.8,3.8c-1.8,1.8-3.9,3.2-6.2,4.4
					c-0.1,0-0.1,0-0.2,0c-1.9-1.1-3.8-2.3-5.4-3.8c-1.3-1.2-2.5-2.5-3.3-4c-0.8-1.4-1.2-2.9-1-4.5c0.2-1.8,0.9-3.2,2.4-4.3
					c1.5-1,3.1-1.3,4.7-0.6c1.1,0.5,1.9,1.2,2.5,2.2c0.1,0.1,0.1,0.2,0.2,0.3c0.1-0.1,0.1-0.1,0.1-0.2c0.3-0.5,0.7-1,1.2-1.4
					c1.3-1.1,2.7-1.6,4.4-1.1c2.1,0.6,3.3,2,3.9,4C294.1,205.6,294.1,206,294.2,206.4z"/>
			</g>
			<g id="g2">
				<title>nouvellemethode</title>
				<path class="cls-3" d="M295.1,275c0,0.2-0.1,0.4-0.1,0.5c-0.4,1.1-1.4,1.9-2.7,1.9c-1.5,0-3,0-4.5,0h-0.3v0.3c0,1.5,0,3,0,4.6
					c0,1.3-0.8,2.4-2.1,2.7c-1.7,0.4-3.4-0.9-3.4-2.7c0-1.5,0-3,0-4.5v-0.3h-0.2c-1.5,0-3,0-4.5,0c-1.3,0-2.4-0.8-2.7-2
					c-0.5-1.7,0.8-3.4,2.7-3.4c0.5,0,1,0,1.5,0h3.3v-0.2c0-1.5,0-3,0-4.5c0-1.2,0.7-2.3,1.9-2.7c0.2-0.1,0.4-0.1,0.6-0.2h0.6
					c0,0,0,0,0.1,0c1.3,0.1,2.4,1.3,2.4,2.8c0,1.5,0,3,0,4.4v0.3h0.3c1.5,0,3,0,4.5,0c1.2,0,2,0.6,2.6,1.6c0.1,0.2,0.2,0.5,0.2,0.8
					L295.1,275z"/>
			</g>
			<g id="g3">
				<title>miseenapplication</title>
				<path class="cls-3" d="M289.7,327.1c0.4,0.1,0.9,0.2,1.3,0.5c1.2,0.9,1.5,2.5,0.6,3.8c-0.2,0.3-0.5,0.6-0.7,0.9l-4.2-3.4
					c0.6-0.8,1.2-1.7,2.3-1.8c0,0,0,0,0.1,0L289.7,327.1z"/>
				<path class="cls-3" d="M279.6,346.4l-4.2-3.4c3.6-4.5,7.2-9,10.7-13.4l5,4c0.2-0.2,0.3-0.4,0.5-0.6c0.2-0.2,0.5-0.3,0.8-0.2
					c0.3,0.1,0.5,0.3,0.5,0.6c0,0.3-0.1,0.5-0.2,0.7c-1,1.2-2,2.4-2.9,3.7c-0.5,0.6-1,1.3-1.5,1.9c-0.2,0.2-0.4,0.4-0.7,0.4
					c-0.6-0.1-0.9-0.8-0.5-1.3c0.4-0.6,0.9-1.1,1.3-1.7c0.6-0.7,1.2-1.5,1.8-2.2l-0.8-0.6L279.6,346.4z"/>
				<path class="cls-3" d="M277.7,349.6c-0.4,0-0.6-0.2-0.6-0.5c0-0.3,0.1-0.5,0.4-0.6c0.4-0.1,0.9-0.2,1.3-0.2c1.9-0.3,3.9-0.6,5.8-0.9
					c1.5-0.2,3-0.5,4.5-0.7c0.3,0,0.5,0.1,0.6,0.3c0.1,0.2,0.1,0.4,0,0.6c-0.1,0.1-0.1,0.2-0.2,0.4c-0.1,0.1-0.1,0.3,0,0.4
					c0.1,0.1,0.3,0.2,0.4,0.2c0.7-0.1,1.3-0.2,2-0.2c1.3-0.2,2.6-0.3,3.9-0.5c0.1,0,0.2,0,0.2,0c0.3,0,0.5,0.2,0.5,0.5
					c0,0.3-0.2,0.6-0.5,0.6c-0.5,0.1-1,0.1-1.6,0.2l-5.1,0.6c-0.5,0.1-1,0.1-1.5,0.2c-0.2,0-0.4,0-0.5-0.2c-0.1-0.2-0.1-0.5,0-0.7
					c0-0.1,0.1-0.2,0.2-0.3c0.1-0.1,0.1-0.3,0-0.4c-0.1-0.2-0.2-0.2-0.4-0.2c-0.9,0.1-1.8,0.3-2.7,0.4c-1.9,0.3-3.9,0.6-5.8,0.9
					C278.4,349.5,278,349.5,277.7,349.6z"/>
				<path class="cls-3" d="M274.9,343.9l4,3.2c-0.1,0.1-0.2,0.1-0.2,0.1c-1.4,0.6-2.7,1.2-4.1,1.9c-0.1,0.1-0.3,0-0.5,0
					c-0.2-0.1-0.2-0.2-0.1-0.4C274.3,347,274.6,345.5,274.9,343.9z"/>
			</g>
			<g id="g4">
				<title>attention</title>
				<path class="cls-3" d="M296.4,408.9v0.6c0,0,0,0,0,0.1c-0.3,1.1-1.1,1.8-2.3,1.8c-5.9,0-11.7,0-17.6,0c-0.2,0-0.5,0-0.7-0.1
					c-1.4-0.4-2-1.9-1.2-3.2c2.9-5.1,5.9-10.2,8.9-15.4c0.2-0.4,0.5-0.7,0.9-0.9c0.9-0.5,2.3-0.3,3,0.9c2.8,4.8,5.5,9.6,8.3,14.4
					C295.8,407.7,296.2,408.2,296.4,408.9z M283.8,399.5v3.2c0,0.3,0.1,0.7,0.3,1c0.5,0.6,1.4,0.8,2,0.3c0.4-0.3,0.6-0.7,0.6-1.2
					c0-2.2,0-4.3,0-6.5c0-0.5-0.3-0.7-0.7-0.7c-0.4,0-0.9,0-1.3,0c-0.6,0-0.8,0.2-0.8,0.8C283.8,397.4,283.8,398.5,283.8,399.5z
					 M285.3,408.8c0.8,0,1.4-0.6,1.4-1.4c0-0.8-0.6-1.4-1.4-1.4c-0.8,0-1.4,0.6-1.4,1.4C283.9,408.2,284.5,408.8,285.3,408.8z"/>
			</g>
			<image style="overflow:visible;" width="512" height="512" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAMAAADDpiTIAAAAA3NCSVQICAjb4U/gAAAACXBIWXMA
			ABefAAAXnwFLnDNSAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAwBQTFRF
			////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
			AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
			AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
			AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
			AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
			AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
			AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
			AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
			AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
			AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
			AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
			AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
			AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
			AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACyO34QAAAP90Uk5TAAECAwQFBgcICQoLDA0ODxAR
			EhMUFRYXGBkaGxwdHh8gISIjJCUmJygpKissLS4vMDEyMzQ1Njc4OTo7PD0+P0BBQkNERUZHSElK
			S0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6e3x9fn+AgYKD
			hIWGh4iJiouMjY6PkJGSk5SVlpeYmZqbnJ2en6ChoqOkpaanqKmqq6ytrq+wsbKztLW2t7i5uru8
			vb6/wMHCw8TFxsfIycrLzM3Oz9DR0tPU1dbX2Nna29zd3t/g4eLj5OXm5+jp6uvs7e7v8PHy8/T1
			9vf4+fr7/P3+6wjZNQAAHu9JREFUeNrtnWdgVkUWhie90qtIkbLgCiwdlCIdaUtbkKKitHVBBURU
			UFnAgsgqKLh0kMhakA4GgixSBBQQQkcwYEJHREoKIW32B7pSku+75Zy5Z+49719I7rznfXLL3Dlz
			hWCxWCwWi8VisVgsFovFYrFYLBaLxWKxWCwWi8UyqYBy7Z9+8c1pMSs27k64mHIhIX5L7OdzJo99
			smExro3ro2838qOdyTIvXdn16fjH64Vyodyo6M7Td+Qd/a1KXf9KwxBCI48oWaVeq27dOrRsVLda
			pTIFOEoLqjZyww1pRinrRj0Y7PDpqnSzgW8vjv8l486h/bAhZsIznesW4ViNKX/X2SelFV2Z2zzQ
			oTGX7vHetjR/40taPqbDPZyvbwV1WZshrevUpBrKh1xm8JLTxkd4LvaNrsU55zxU6p+npF0dHFVO
			4Wm/wRt7zQ8xZ8/E5nznencxWy3NlBDKWd1QzYgLjz5teZApscPu58xvLeYLxyScNj+CP+LK01Nt
			jvLk3G7hnPxN9b8uYbWnB+4NYb1VORDDvDLn4QBOXwRNlvA6OgBxcqAtHLCJb/G1YLhE0dE2aCNe
			BTrQXUO9/WAQeEIiaUkZpCE3uAQ70MzYR4O9C0BniabU0UjPW/lGHMiBHeqplwp6FYBnJKLwrgOF
			Ok7cmg451OSpFbwJQG+JqiUl8YYe1mjkF4lwQ81e2siLALTGBUCeb407/uIdX18Ldk+wo6f3bgZC
			jyITkDMBv6gVe0/x/z7IkJJeyM+nAGhtLaPCR3CtpxdfBRjtr6MiPUbAXHQCLnVSZCWkxXs/2B/u
			2cEhngIg6D/oBMgp6l7AVRz21Q27w03oE8AEwGqLyufs6C5zztoc776OTACs9pdSaimg9jvnbN65
			NGECQJVYWbGp4E4r7K1xWFOTCYDUxXrKbZV48YitJ9jPyjEBkLOtbRww1nBuso0hp74cwgTAKaO3
			E86i+m2zs8yxCRMAOCv4pDPeHt5gY8zzizIBcOeAlg6Za7TOxjzWwAAmAEpXqjrl7sE11ke9rToT
			AKWkko7Zq7fa+qKhd6OZACB9H+Wcv9orrS8j78oEAGlVoIMG21pfAvllWSYARtOcNBgx0fL04JUn
			mAAYDXfUYfVvLQ/8i8JMAMjquy6OOgwYfMXqyM+0ZgIglFbfWYv3LLI8LfRBOBMAoNNOb97SxfJJ
			4FAtJgBAs532+KcDlmczRwUyAfbfCjR32mPU55YH/819TIBtJTi/9vZ5yw+E155iAmzrPedNNj1v
			efRLizABdp8F6ztv8l7rUwJnmzMBNnWQwF5N4XGWh581kgmwqXFCawLkF9FMgL3VIdU0J+BQZSbA
			lnYGaU7A1c5MgC29IDQnIOfNQCbAhn6N1p0AGUf4BWFQt2d6tQwiTQCJU4CI2GvDwgmq/UPBI5Ok
			lHJfI8oEnKaxbW+Fy3ZebfalCcCHv1+mJgcQJmAAjWJ1sLXr2IcU24cG/jG+GYQJOErkJmq8LRfb
			6H2KoOit26lSJqA7jXoFrrXl4lxjagC8etv4CBPwPZGCFf7Jlo0bxBaMBt+xmT5hAqissmtoc/PR
			MaQA6Hnn8OgSsIFKzRbYNDKf0q3g3Q3RdAmoT6RmJexuMreezhaDdXIZHlkCllGp2vN2nRwoQ8VK
			jNSIgBwqLVfBB+1aOUtkxXBU7h/UmUmUgKFU/m6a27aS3J7mLSBtAr4mc+lcadtL1j8o+FgmtSIg
			sxAVAFoAuHnH+e1E8uX9USWaBNCZRDkM4GaR481jj/sYHUkClpABAOT7KVudXjLucycUigSkkGm4
			zHcNws+xSo6aKOB7s2yKBNDZmfnfIH4uPuSkh65+RkeQgDlkAKgOY+h6Bwc9TPU3ulnkCDhPZ2nl
			SaAl7w6+5vbf90yPgIZkAPgIyFGWYwvFihsYHTkCJpEB4DGwGW6npoQelRoSsJ8MACXhTI1wxsFM
			qSEBmaFkCDgI58qZNSIGP5tFjIBqZACYBuhqogPjjza6sokWAb3IADAU0tY09S8G6hseHCkC3iAD
			QFdQsOcpf8DtL7UkYDkZAOrCXts+Vf014ikmBjfbJgHL4er0IxkASgA/4KxQfH+7XqojIGovWJmy
			I6gAEJAOTECcWmvmPpdok4ByF8DKVJvMKeA09CzXpnwKR1/E5OBsEtAoE6pKdFpsL4JPdH+ncMlT
			LamWgJlQRXqHDACp8G8744spG317qZaAkilANYolcw+QAw+APKTsHDBAKibgDaASJVIBIAJl2eNX
			qvbDek0qJiD/ZZgKpVIBoCgKAHKyouFbWtI0xw4BnwC9PaUCQAMcAKSij6YulaoJ6AVUoDAiAAxB
			AiC9gZLhb5eqCSgI9CRYkAgAc5EAkGdLqRj+fqmcgE0w9aGyzU48FgByh4qT3BGpnIDpMOWpQCP/
			sAw0AOQMBeP/USonYAxMdR6gAUAniahu+ONPlMoJGAhTnDo0AFiGCcCv+Fsh2HmTYZGA9jDFobHR
			WtEbmADIb9Dng2y9nptriYCmMLWhsVvYUImr17ENXJLKCegJU5pOFPIPOogMQHZTZAfnpXIChsOU
			picFAJ6V2DqN3Dv+o1ROwCSYyjxGIP/il9EBkCtxLeyWygn4xD33AAukAj2HasH+tJxpAs7A1KWq
			8/k/mKMCgPQamB5WSdUE1AaqC4F3AZukEh3B/GouxPnYHAH/hKlKmvP5PyIVaR6iiRkgAzRDwC6Y
			ohBoDFiiCgDMJ55xUjEBDwPVZLPzABxUBsCV8mgm+krFBHwHVJPPnJ8EuqEMAPkdWsNYY6mWgL9B
			leRdxwEIzlIHgHwby0UpqZSA0KNQxxvh/CXglEIAsrBefgakKSVgvg73RUa1VSEAcjfWe8FDUiEB
			I+AK0sR5AMaqBEAOR3KxWqojoF023MEIrAirqhSA5NI4LiYCjnGR7ymrdtfgDnWdwi5R3yolAGlP
			jG6QY9zn63n1RcC/f7mRwsvgxkoBkJ1RTNwLOsZLeb6jC18IeqDxFAAQy5UCcDIaxcQZ0EHmLPpz
			rpMmT52ALUYLEgBUyVRKwGQtKM5eeNfu94F9jgIfJCOSBACKHwSyUD4yNhp8nDm7xtX/44GgSO+Y
			8+CH2EYjfxG4WikB2zE8tEQZ6s+bF30wethb82L3ZWP8+reJACDCYpUS0BbBQmS61E9tBRkClJ4D
			vsOwEKdf/ln5BB0CklQ6b4fgYKh+AOyik3/AdKXOdyBYqKgfAO96NX8pMb41e1Q7ADqRyX+Gaus7
			EVxM0Q6Aezybv5QI3xdrpR0Aa8M8mz/GKSDoHBOgTf4op4BJkgnQJn8ZB2/lAckEaJO/zC4Db2an
			hgSsCfNm/lK+Bu9miGQCtMlfHof/slShdCZAm/ylbARv6FPJBGiTP8ZyqNqSCdAmf7kVwdN6PQmI
			DfNg/jITYXFgK8kE6JI/zkvh3UyANvmjbBv0qGQCdMkfZUVcUIKuBHwZ5rX85ccY1vpIbQkI9Vj+
			GK8DhAjYwQRokj/S9igNJROgR/5YS+K+YAL0yB9rr4Dy6foSsBqfgICZZNw+hWRxkmQCdMgfDYB8
			SUyADvnLD7BctpFMgAb5y1MBWD5n60zAqlCP5C9lSyyj+ZOYAA3yR2yP1/oigEUAufxxOsRccBHA
			IYBg/nI32l1AvsNMAP38pfwb2imgylWtCVgZ6on85aFANAK65DABoPmnofh8Gu+Z902pNwEhtPI/
			Unoxhs3k+9AACFzLBADmX1IEoxCwKZf7wBqjY9bt3bZ0ajd7e+gUOq43AStCSOUvkAgYduf9+7g/
			vmCQseZBO8YrXWACAPNHIiCtym2Dffbn2/95mZ0N1WtdZQIA80ci4LtbPiKRb+Vd/3zZzmZ6Ta97
			ngDI/IUInoPgcvT/f32F3KZvsp+38zCY5XECYPMXQvS/Bm7yRvXfr9l5fL+ol40C9NcbALk8xOn8
			D5e8/VfesxDcZHyIz/xlak0bJRic7WECAmaB5y+EaBwPbfJ1n/lLedjOt6a6p3uWAJz8hQh6bPMt
			l+jtkybPt/n+PbOuz/xtrh5rftWjBEDkXyKvdy2jV12UUiYv7VtECCECGk61tUnb4XCf+cufbF0J
			a57Tm4BlIeTyv7nuJjryllNzUMtlNo600M/3K+0tHip/zIMEoOefy1P3HrQS2PzOTLFdniPAgfyF
			COx3FqkCR2w+Dkev8BgBjuQvhIh+E2fuLdP2woHhN7QmYGmIFvkLIf6Es12j/Z216+r9cnBpsB75
			CxEyAWPqpaZtAESBxV4hwNH8hRDNED5JD7JwZEi6JwhwOn8hCoH/reXALJGrqfXz4JJgTfIXQvRP
			hfV+FmhxTMT4624ngET+QtRMBLW+EGx9XIXV7iaASP5CFN0I6byXgFPH4y4mgEz+QgRPg/OdUhBy
			vXD42OtuJQAg/0MlwArdH2zu5S3gJePlF2m7SmBxsDb5C/EQ0Mzw5YICWpXn6jozuECf/IUo9R2I
			56cEgkpPSdEx/+TGGuUvRNhHAJ6nIvUOFRn/K+ePm78QYmim3VF9FSywFD1sj3vyn00yfyFa/GJv
			VBsiBaYemJDI+WPmL0T5fbbyjxDICnh49mXOHy9/IaKWWh/Vf9HzF0KIsG4LLJwHzsf0Lq2yBTm5
			CWr+xRH/xsbnkM7/5omq38cnjQ8sc8srtQOEEGFrOX8j6pFKPf+bLwoGLNznd6QZ8XOHNAj//8lj
			LedvRLVOWhjVesX536zmvc0GTVp+KJfZ4vSErxe8PqDeHZ/2UESA5vkLUXybHvn/v6oFyzzQoGWX
			J4a8/NqIf/Tt3qF53ZJ57GmmhADt8xcidL5O+Zu6hVzL+RvScFO92l/pkr8CAtyRvxB/N5N/uBBM
			wE2loOZ/UFn+lU65NH9kAjh/HQhYw/kD5r9Ot/wRCeD8vU1AysPuyL94ksvzRyLALfmHfmN8VHF6
			5o9CgFvyF9M9kD8CAa7Jv2q2F/IHJ8A1+Yvlhke1NkwIJsBt+VfxSv6gBPjKf45W+YuXPJO/EGGx
			nP9d2mpwVGv0zx+MADflH5TlofyBCHBT/qKkp/IHIQA5/2JqC1LL0Khi3ZI/AAEpTTHzP6A4f1HP
			Y/nbJsBl+YsyXsvfJgFuy1+E+h/Vl+7K3xYBrstfiCN+8w8Vgglwbf7iX97LX4iwLzn/39XM96hW
			uzF/iwS4Mn8RuNeD+QsR+iXn/5vaezF/CwS4NX8h1uc5qlXuzd80Ae7NXxRL9GL+JglIdW/+QlRP
			9mL+pghIbebi/IVokNunu2aGCMEEeCJ/IUrftSFX5hDhARkkwPX5CxE27Pxtg1peVXhCoas5/98U
			NWzT77sGnp5dT3hFBgjwlf9c2/nvL0anGAU7DBoz8fleNYSX5JcA7+TvUfkhgPP3NgGcv7cJ4Py9
			TQDn720CUpuj5l+UC0+bAM7fSwSs4vyZAM6fCeD8mQDOnwng/D1OAOfvbQI4f28TgJv/Ps6fOAGc
			v7cJ+LwF589yIP9i74Vyib2c/365kgkgnf885PwlE+Dx/JkAr+fPBHg9fybA6/kzAV7Pnwnwev5M
			gNfzZwJcmH8RM/kzAV7PnwlwV/57TefPBHg9fybA6/kzAV7Pnwnwev5MgNfzZwK8nj8T4PX8mQCv
			588EOKeZJPJnAhzTQydJ5M8EOKaicSTyZwKcuwsYk00hfybAObW8QCF/JsA5VTplLf940PyZAOdU
			8RSF/JkAvQiAz58J0IkAjPyZAH0IwMmfCUBV4H2tBr83vd8DAbkScJJC/kwAnpqvSv+txlfXvxxt
			j4D4wlj5MwFIqvLtbVU+P/ju00CFkxTyZwJQNCTtzjKvyGeZANz8mQCEi/+0XMp8qLRFArDzZwKg
			FbY41zInWCMAP38mAFYFN+dR5twISKKQPxMAqXsP5FnmXAgon0QhfyYATn/2dVY3TYCq/JkAKDX6
			1WeZTRKgLn8mAEZd0vyUOaGMCQL2KMyfCYDQ01l+y3zcOAFq82cC7Gu8kTLnRkAihfyZAJsKMvjN
			91wIuC+RQv5MgC1FrjJaZkMEOJE/E2BDRb41XmYDBDiTPxNgWeV+MFPm42X9EOBU/kyARdU4a67M
			J3wT4Fz+TIAlNb9qtsy5EXCSQv5MgAX1vGG+zLkQUD+DQv5MgGkNy7FS5lwIGEYifybAnAImWSxz
			LgQsJZE/E2BGIQstl/lEuTt/WYHjJPJnAowrep2NMv90FwG1t5PInwkwquLf2yrz3QQE0MifCTCm
			Sgk2y3w3AUTy90FA8TrN2nfv+48Rr4569qnubRvXrFTCs6zU/dl2mY0SoDz/uwkIqtrnlVlxP+Sy
			4iErYc37Q1qVDfBa/m1TAMpsjAAH8r+VgOAa/T7cnurv/6ftXziggofy75sJUubjRYjm/zsBfxm5
			/rrxnzn5cb/y3sh/FFSZNwT7PdZ06YxW3ttnwVnzP5a4oFuY2+MPnApX5nF+jxa5QWqmK3ObB7o5
			/7AvAIuVUsKFBEh5+l81XZt/gU2gpZoiXEmAlIdeLujK/EsB35MlGDhmhJYEyGvvlHRf/n9Ogi5T
			cfcSIK/PcNtTQcNL4EXqJFxMgMz6TzU35d85Db5EbwtXEyBzVlZ2Tf5/z0Io0CbhcgLkjbci3ZH/
			OJTybBGuJ0AmdnFB/EGzcYoz3+gAIv6rLwFyTSXd849YiVSa0cITBKS/HqF1/oW3Y1Wmh/AGAfJY
			LY3zL3cErS5m5kz1JiD9WW3z/8sZtKpcNPXuTG8C5DJNZ4ebXcGryXiTtyJ6E/BTAx3zfzQdryLJ
			xYWnCMh4Ub/FY0OzEQsy0PzjiN4EyNj8esUfMBGzGiutPJBqTkC8Vu8IQz7GrMW+AsKDBBzXaFIo
			Og6zEomlLE5KrdebgAu1dcnfZvOPH1263/K0pOYEXGuhR/4VEzCrkPaQjYlpzQm40UOH/OtcwKxB
			Vic7Y9OdgOzB9PN/JBm1BINsvpzSnADZn3r+T2Sg+h9r+/Wk5gRkdaOd/0s5qPZnAbyg1pyAdMp3
			goEf4JpfGSSYgOS6ZPMPW4RrfRvM4gjdCbh4P9H8C2zENX64MNQyJc0JSCxKMv9S+3BtnykLNtSI
			r/QmYH0QwfzvT8Q1faU64GB1J2AivfwfuoRrOb0Z6HDDNSeA3MNgpzRcw9nQk6CaE3CN2I3goCxk
			w8+BD1lzAo7ko5T/WB2veZoTMJtO/EGzsM3GoKyI05yANlTyj1iBbTUuBGfkehOQRGSVYOFt2E53
			RWONXW8CaFwEyh7G9vljcbzR600AhYtA9TPYLi9UxBy/1gQkOf8k0PQKtsnkOrgOwtdpTMAkp/Pv
			kY5tMQP9NKczATcqOpv/c9nYDnOewHehMwFLHc3/bXyDL6nwoTMBTZ2LPzgG394HaqxoTMAex7YX
			jlqL726RqpZYjQno51D+xXbhe/ta3Qbq+hJwMsSR/Cv8iG/NWguo5wjo60T+dc7jG7PaAuo1Ag46
			sHVEm2R8X5dUr3jQloCOyvN/PAPflZ0WUKsExOkJwDeqC/ViDr6prL86cGHTlQC1fysB76vwNMiJ
			OxtdCVimskahn6uwNFYIJsCwMoupq1D+r1U4miUEE2BCw5TV5569KvysdLDvRUsC9qiqTpVEFXa2
			Obo/tpYEVFdTmwd/UWEGrAXUQwS8q6Qyf01V4QWwBdQ7BJxTcdEcmKXCypXqQjABptUKvyr/VGIE
			uAXU6r2udgBMwS5J0EwlPrJp7IPXSTsAjmKfFJer8fEcifzFu/rdBuKuDi20VY0LKtse7NIPgKGY
			9ShzSI2JGCIfRciXpR8AcYj1qHZakYcQIieAthpOBaXjfWX04ctqLOC1gJoVynL3nANr5k8YNvqD
			RZtxtlJuiVWN7ulq8sdsATUp+Hbnnxc+9scbu4A6r36TCX6I0UjFeDZbTf4XKpLJPxJ6wVNC37sm
			6krPgD7IcpxiTFB0CcNuATWjFrDWkgYG53aU++bBngXOYJQieIGi/DPa0MlfjAe19lmed2cNYZdW
			3wtfiag1ivJX0QJqXBshpzZH+Xq83g1ZRPidA4vtVPUM8xKl/EOvwxm71t7noSIg19eBz6JVOKYq
			//cp5S8qAP79t/NzrIB5cAdbDVyH2udV5b+I1ldRm8A5G+H3YIAEHIItQ+trqvJX2AJqSD3BnM03
			cDQ4AtJA/44ey1CVv9IWUCMaAfaKNlQoJQCyl3Jkjqr8FbeAqnwXbHBtAxgBjcFKEDBZ2TuMS/S+
			fvIZkLXvjZ6SoQgA6xMP/UxZ/g60gPrVZiBvrY3/wcEQMA6oAPk3KMvfkRZQfwLa+uKAmVMuCAEz
			YPzfE6/uJfYggvkLoC9fvGnqojsX4IifgNiv8pO6/MdSzL8gkLl6QjUBqyDsq2n+ualZFPMXVWHM
			nTX5VA5AwCYA9x1T1eW/IogkAK1h3Jk+H9snAKBFdIDC1ZDOtoD6mAKDsWd+G2fbBPxo2/tr6uJ3
			vAUUeyZ4uFBOwAWbzgNnKMzf+RbQvATUFdRTKCfgkj3j4csU5k+hBRT3HsDSJs72CLB3Bij0jcL8
			abSA5q7GMBbbW5uEn2PjkKft2FbV/HNznUQPuvmLOjAeBwrlBCTacF3tlML8qbSA5q4HYDyOEcoJ
			sPEUoKr556YmUs4fakXYdKGcgCOWPXe7rjL/GNL5Q20OsVkoJ2C/1SM+k60y/7hg2gAAvQvIKqyc
			gO0Wj/eWyvgJtYDmoTAgozZaHSwSYO0DUsEfKc2fUAtoXgJaDbdYqCbg31YOFRWrNH9CLaB5Cuh9
			WHIhOwTMtnDE1ywcqOgOpflTagHNU1C7odr6qqcVAgaYP0z5Y0rzJ9UCmqegzonXSysmwPzkY61z
			SvOn1QKap96B8jtPqCWgltlDtLqmNH9aLaB5qy+U36xmagkoYvIAfTLU5v++HvmL2mCOf6mgkoBz
			Jn/9Czlq8yfWApq3IuDmxQ7lV0jAV+Z+93tq45dfhwpdBPhNzNhgdQRMNvObQz9VnD+5FlAfgtwW
			d7FNAmYZP1R/E783338V50+vBdSH3pBaEmCiEaFkvOL8f7lfo/xFL6kjATlRhn9n5ROK86fYAupD
			1aSOBBhfDdDgouL8SbaA+rpBAt7G8wslBBhegdIhVXH+NFtAfWm/1JAAo+ss+2Wqzn+sbvmLSVI/
			AnIMfj30VdXxE20B9amGUj8CjG1HEPhv5fkTbQH1Xabz+hEw1civCVmiPH+qLaC+NUeSI8DfR7u6
			GAF7kfL8ybaA+lZHqRsBN4zMtKo//9NtAfWt8BTdCDCyO0hT5fkTbgH1I5RrJSYBj/n/+aAjqvOn
			3ALqR0/gvBFHI+B6Pv8//ojq/Em3gPpRoUy9CDDSEhCjGoDnhMb6WmpFgJG/tbOK85+oc/5iqNSJ
			gBQDHw2MVJx/jNb5i7I5RAnIdR8fI/vS3682f+otoH6FtmEOBgE1DfzgfUrzJ98C6lf10WqzKAia
			gC1Gfq6gyvw1aAH1qw36ENDd0M8pvAnUoQXUr1pLXQhIMvbrPlaWvxYtoP71PV6FPgcl4GVjP9VD
			Vf56tID6V3epBwGpBl+4BSraCE6TFlADBTuqBwFPGv2hv6kB4CXhFg2QhAmYnGNmJchNTVeR//uu
			yV+EniZMgGi0X0opT7xgYlohWEE/kDYtoEb0gqRMQHCTrgM6BJr6kWj0/YA1agE1Uq9fSRNgxREy
			ATq1gDp/CnAfAVq1gBpR0E4mwIT0agE1pGrIu6h85gABW7DMaNYCakzjJBNgULq1gBp8FDzIBBjU
			IOFK1c9mAgxprHCp0DdTcgcBs9yav4hMcCEBUdAE6NgCalTNc5gAf9KzBdSoZks3ErAZcPyatoAa
			VYHT6AR8qjUBuraAGn8SSGMCfEjfFlDD6pLNBOSp9KbC/RoumYA8pHMLqAlNYwLy0HOeyF8ErmIC
			ctVE4RFFfc8E5KIY4RmVTHInAZvsDFj7FlAzqnqFCbhD+reAmlJLBd/Y+UQnAtzQAmpKT+YwAbfI
			FS2g5tSbzwF/yCUtoObUJpkJ+E1uaQE1qXoXmQAppYtaQM2qciITIKWbWkDNqtR+VxIQudHUCN8X
			3lXBLUyAq1pATSt0itcJcFcLqAV1uoROwFxBlwC3tYBaUJmt6AS0JkuA61pArSh4Avas4NFQogS4
			sAXUklqfd98pwAgBrmwBtaSSyF9g6u2EqUh/26S7swXU6sQw5j5i8hlBkYBBHPutD4SjET/D2lkQ
			JGAsh367yqJdB04ECnoEzOLElV0HhjvmKG8C3NwCal0hA44h5D/ZwVrnRYC7W0BtKPDReOD4r/d3
			1FDuBLi8BdSe2kNuvnV0hNOlzo0A17eA2lSTNTDpZyxqTsBN5Lo7x5X0J87Yj8q/eth2/D+NLkHD
			TPAdHxo+VIYDNqBa79rZS+D6inaBdLz0OXFLB/C4cA7X4A1hi3nWGkhOfNiB2E12yMC4mxsj/DCm
			HAdr5uz54Kh15j4/fmP981VIWgmv0bJrg6IcqYW/noavrDc2TZy8Y3qnKC6YGxVa7/Hxn+7K+4KQ
			vPOjke3KBXChXK5iDZ8cO3nO57Fb4hMupFxM2L1xRcy0N198uj1Hz2KxWCwWi8VisVgsFovFYrFY
			LBaLxWKxWCwWy7z+B5qjEA1QCDehAAAAAElFTkSuQmCC" transform="matrix(4.129772e-02 0 0 4.129772e-02 273.9434 456.0936)">
			</image>
		</svg>
	</div>



	<div class="col-lg-5">
		<div class="col-lg-12">
			<img class="engage img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl;?>/images/smarterre/education/FirstImg.jpg">
		</div>

		<div class="contenu col-lg-12">
			<h3 style="font-family: 'fb';font-size:2vw;color: #1ea1a4" class="titre"></h3>
			<p style="font-size: 1.4vw;">
            <ul style="font-family: 'ml';" class="description_li col-lg-12">
            </ul>
			</p>
		</div>
	</div>


<!-- Petit bonhomme -->
<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 22.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="190 100 900 296.7" style="enable-background:new 0 0 1280 510.7;" xml:space="preserve">
     <style type="text/css">
  .st0{fill:none;stroke:#95B2BB;stroke-width:0.5;stroke-miterlimit:10;stroke-dasharray:1,3;}
  .st1{fill:none;stroke:#4F8AA0;stroke-width:0.5;stroke-miterlimit:10;stroke-dasharray:1,3;}
  .st2{fill:#FFFFFF;}
  .st3{opacity:0.7;}
  .st4{fill:#72C4C6;}
  .st5{fill:#B4DA9A;}
  .st6{fill:none;stroke:#95B2BB;stroke-width:0.5;stroke-miterlimit:10;}
  .st7{fill:none;}
  .st8{fill:#95C11F;}
  .st9{  font-family: 'fb';}
  .st10{font-size:8.4926px;}
  .st11{ font-family: 'fb'; fill:#00B2BB;}
  .st12{  font-family: 'fb';}
  .st13{font-size:18px;}
  .st14{fill:#00B2BB;}
  .st15{fill:#00817C;}
  .st16{fill:#C6C6C5;}
</style>
<g>
  <line class="st0" x1="615.1" y1="263.9" x2="601.9" y2="289.4"/>
  <line class="st1" x1="574.9" y1="212.8" x2="601.9" y2="194.3"/>
  <line class="st1" x1="623.6" y1="184.9" x2="658.8" y2="182.4"/>
  <line class="st1" x1="680.5" y1="189.6" x2="703" y2="205.3"/>
  <line class="st1" x1="718.3" y1="223.6" x2="727" y2="254.8"/>
  <line class="st1" x1="727" y1="269.2" x2="723.8" y2="297.9"/>
  <line class="st1" x1="709.3" y1="315.7" x2="696.8" y2="328.2"/>
  <line class="st1" x1="684.1" y1="340.6" x2="664.1" y2="347.8"/>
  <line class="st1" x1="619.1" y1="347.8" x2="590.2" y2="337.6"/>
  <line class="st1" x1="577.1" y1="324" x2="562.4" y2="304.3"/>
  <line class="st1" x1="558.2" y1="285.6" x2="554.8" y2="266.5"/>
  <line class="st1" x1="558.8" y1="240.8" x2="568.6" y2="219.1"/>
  <polyline class="st0" points="574.9,216.1 596.2,239.5 562.2,254.8   "/>
  <line class="st0" x1="573.5" y1="271.6" x2="558.6" y2="256.3"/>
  <line class="st0" x1="571.8" y1="282.6" x2="566.2" y2="289"/>
  <polyline class="st0" points="567.9,298.7 609.6,305.5 587.8,324.3   "/>
  <polyline class="st0" points="582.8,276.7 613.4,263.1 594.5,240.9   "/>
  <line class="st0" x1="616" y1="260.9" x2="640.6" y2="227.4"/>
  <line class="st0" x1="630.4" y1="223.6" x2="603.2" y2="235.5"/>
  <line class="st0" x1="619.3" y1="196.4" x2="601.1" y2="233.3"/>
  <line class="st0" x1="634.2" y1="211.7" x2="621.9" y2="195.1"/>
  <line class="st0" x1="644.8" y1="227.8" x2="651.6" y2="246.1"/>
  <line class="st0" x1="634.2" y1="287.3" x2="620.2" y2="269.9"/>
  <line class="st0" x1="632.9" y1="290.2" x2="615.9" y2="297"/>
  <line class="st0" x1="647.3" y1="287.3" x2="674.1" y2="300"/>
  <line class="st0" x1="644.8" y1="281.3" x2="652.4" y2="257.5"/>
  <line class="st0" x1="628.2" y1="262.2" x2="644.8" y2="252.9"/>
  <line class="st0" x1="677.9" y1="229.1" x2="651.2" y2="220.6"/>
  <line class="st0" x1="683.9" y1="221.9" x2="674.1" y2="192.2"/>
  <line class="st0" x1="646.9" y1="210.8" x2="659.7" y2="191.3"/>
  <line class="st0" x1="704.2" y1="221" x2="693.6" y2="229.9"/>
  <line class="st0" x1="687.3" y1="290.2" x2="716.6" y2="266.5"/>
  <line class="st0" x1="682.2" y1="238" x2="663.1" y2="249.5"/>
  <polyline class="st0" points="683,237.2 684.7,255.4 645.2,287.3   "/>
  <line class="st0" x1="687.3" y1="263.9" x2="683" y2="297"/>
  <line class="st0" x1="705.5" y1="221.5" x2="688.9" y2="253.5"/>
  <line class="st0" x1="718.1" y1="259.2" x2="691.4" y2="254.8"/>
  <line class="st0" x1="683.9" y1="308.9" x2="688.9" y2="327"/>
  <line class="st0" x1="689.4" y1="305.5" x2="716.1" y2="304.3"/>
</g>
<g>
  <circle class="st2" cx="595.6" cy="240.8" r="9.2"/>
  <circle class="st2" cx="642.5" cy="218.3" r="9.2"/>
  <circle class="st2" cx="685.8" cy="233.5" r="9.2"/>
  <circle class="st2" cx="685.4" cy="254.1" r="9.2"/>
  <circle class="st2" cx="654.3" cy="250.4" r="9.2"/>
  <circle class="st2" cx="614.8" cy="264.1" r="9.2"/>
  <circle class="st2" cx="640" cy="288.2" r="9.2"/>
  <circle class="st2" cx="577.4" cy="280" r="9.2"/>
  <circle class="st2" cx="583.7" cy="331.1" r="9.2"/>
</g>
<g class="st3">
  <path class="st4" d="M584.5,282.6c0,0.1,0,0.3,0,0.4c0,0,0,0,0,0c-0.1,0.7-0.7,1.2-1.4,1.2c-3.8,0.1-7.6,0.2-11.3,0.4
    c-0.2,0-0.3,0-0.5,0c-0.9-0.2-1.3-1.2-0.9-2.1c1.8-3.4,3.6-6.7,5.4-10.1c0.1-0.2,0.3-0.4,0.5-0.6c0.6-0.4,1.5-0.2,1.9,0.5
    c1.9,3,3.8,6,5.6,9.1C584.2,281.8,584.4,282.2,584.5,282.6z M576.3,276.8c0,0.7,0,1.4,0.1,2c0,0.2,0.1,0.4,0.2,0.6
    c0.3,0.3,0.7,0.4,1,0.3c0.4-0.1,0.6-0.5,0.6-0.9c0-1.4-0.1-2.8-0.1-4.2c0-0.3-0.2-0.5-0.5-0.5c-0.3,0-0.6,0-0.9,0
    c-0.4,0-0.5,0.2-0.5,0.5C576.2,275.5,576.3,276.2,576.3,276.8z M577.4,282.8c0.5,0,0.9-0.4,0.9-0.9c0-0.5-0.4-0.9-0.9-0.9
    c-0.5,0-0.9,0.4-0.9,0.9C576.5,282.4,576.9,282.8,577.4,282.8z"/>
  <g>
    <path class="st4" d="M648.9,249.3c0-0.1,0-0.3,0-0.4c0,0,0-0.1,0-0.1c0.1-0.3,0.1-0.5,0.2-0.8c0.5-1.1,1.3-1.8,2.5-1.9
      c1.1-0.1,2,0.3,2.7,1.1c0,0,0.1,0.1,0.1,0.1c0.1-0.1,0.2-0.2,0.3-0.3c0.5-0.6,1.2-0.9,2-0.9c0.7-0.1,1.4,0.1,2,0.5
      c0.7,0.5,1.1,1.2,1.2,2c0.1,0.7,0,1.4-0.4,2c0,0,0,0.1-0.1,0.1c-0.3,0-0.5,0-0.8,0c0-0.6,0-1.1,0-1.7c-1,0-1.9,0-2.9,0
      c0,0.6,0,1.1,0,1.7c-0.6,0-1.1,0-1.7,0c0,1,0,1.9,0,2.9c0.6,0,1.1,0,1.6,0c0,0,0,0.1,0,0.1c0,0.5,0,1,0,1.4c0,0.1,0,0.1-0.1,0.2
      c-0.4,0.5-0.8,0.9-1.1,1.4c0,0,0,0.1-0.1,0.1c-0.2-0.2-0.4-0.5-0.6-0.7c-1.4-1.7-2.8-3.4-4.2-5.1c-0.3-0.3-0.5-0.7-0.6-1.1
      C649,249.7,649,249.5,648.9,249.3z"/>
    <path class="st4" d="M654.7,252.8c0-0.5,0-1,0-1.4c0.5,0,1.1,0,1.7,0c0-0.6,0-1.1,0-1.7c0.5,0,1,0,1.4,0c0,0.5,0,1.1,0,1.7
      c0.6,0,1.1,0,1.7,0c0,0.5,0,1,0,1.4c-0.5,0-1.1,0-1.7,0c0,0.6,0,1.1,0,1.7c-0.5,0-1,0-1.4,0c0-0.5,0-1.1,0-1.7
      C655.8,252.8,655.3,252.8,654.7,252.8z"/>
  </g>
  <g>
    <title>argentnoncomme</title>
    <path class="st4" d="M639.9,281c0.4,0.1,0.7,0.2,0.9,0.5c0.4,0.5,0.3,1.2-0.1,1.6c-0.5,0.4-1.2,0.5-1.7,0.1
      c-0.4-0.3-0.6-0.7-0.5-1.2c0.1-0.5,0.5-0.8,1.1-0.9c0,0,0,0,0.1,0L639.9,281z"/>
    <path class="st4" d="M638.4,294.2c0,0.2,0,0.3,0,0.4c0,0.3-0.3,0.6-0.7,0.6c-0.2,0-0.4,0-0.6,0c-0.4,0-0.7-0.3-0.7-0.7
      c0-0.4,0-0.7,0-1.1c0-0.1,0-0.1-0.1-0.2c-1.2-0.9-1.9-2.1-2.1-3.5c-0.1-0.7,0-1.5,0.3-2.2c0-0.1,0-0.1,0.1-0.2c0,0,0,0,0,0
      c-0.2-0.1-0.4-0.1-0.6-0.2c-0.5-0.2-0.7-0.6-0.7-1.1c0-0.5,0.3-0.9,0.8-1.1c0.2-0.1,0.4,0,0.5,0.1c0.1,0.2,0,0.4-0.2,0.5
      c-0.3,0.1-0.4,0.4-0.4,0.6c0,0.2,0.2,0.3,0.4,0.4c0.2,0.1,0.4,0,0.6-0.1c0.1-0.1,0.1-0.1,0.2-0.2c1-1.3,2.3-2.1,4-2.3
      c0.8-0.1,1.6,0,2.3,0.2l0.2,0.1c0.1-0.1,0.1-0.2,0.2-0.3c0.4-0.5,0.9-0.8,1.6-0.8c0.1,0,0.2,0,0.2,0.2c0,0.6,0,1.2,0,1.8
      c0,0.1,0,0.2,0.1,0.2c0.7,0.6,1.2,1.4,1.5,2.2c0,0.1,0.1,0.1,0.2,0.1c0.1,0,0.2,0,0.3,0c0.5,0,0.8,0.3,0.8,0.7c0,0.6,0,1.1,0,1.7
      c0,0.4-0.3,0.7-0.8,0.7c-0.1,0-0.3,0-0.4,0c-0.1,0.1-0.1,0.2-0.1,0.3c-0.4,0.8-0.9,1.6-1.7,2.1c-0.1,0.1-0.2,0.2-0.2,0.4
      c0,0.3,0,0.6,0,1c0,0.5-0.3,0.7-0.8,0.7c-0.1,0-0.3,0-0.4,0c-0.5,0-0.8-0.3-0.8-0.7c0-0.1,0-0.2,0-0.3
      C640.2,294.4,639.3,294.4,638.4,294.2z M642.1,288.2c0.4,0,0.8-0.3,0.8-0.7c0-0.4-0.3-0.7-0.8-0.7c-0.4,0-0.8,0.3-0.8,0.7
      C641.4,287.9,641.7,288.2,642.1,288.2L642.1,288.2z"/>
  </g>
  <g>
    <path class="st4" d="M601.1,238C601.1,238,601.1,238.1,601.1,238c0,0.1,0,0.1,0,0.1c0,0.4-0.1,0.7-0.4,1c0,0.1,0,0.1,0,0.2
      c0.1,0.3,0.1,0.5,0.1,0.8c0,0.3-0.1,0.5-0.3,0.8c0,0,0,0,0,0c0.1,0.3,0.1,0.6,0,0.9c-0.1,0.3-0.2,0.5-0.4,0.7c0,0,0,0,0,0.1
      c0,0.2,0,0.4,0,0.7c-0.1,0.5-0.4,0.9-0.9,1.2c-0.3,0.1-0.6,0.2-1,0.2c-0.4,0-0.7,0-1.1,0c-0.2,0-0.4,0-0.6,0c-0.3,0-0.7-0.1-1-0.2
      c-0.4-0.1-0.7-0.2-1.1-0.3c-0.4-0.1-0.7-0.2-1.1-0.4c-0.2-0.1-0.4-0.1-0.6-0.1c-0.2,0-0.5-0.2-0.5-0.5c0-1.7,0-3.4,0-5.1
      c0-0.2,0.2-0.5,0.4-0.5c0.1,0,0.2,0,0.2-0.1c0.1-0.1,0.2-0.2,0.3-0.3c0.3-0.3,0.6-0.7,0.9-1c0.2-0.3,0.5-0.6,0.7-0.9
      c0.1-0.1,0.1-0.1,0.2-0.2c0.1-0.2,0.2-0.4,0.3-0.6c0.1-0.2,0.1-0.5,0.2-0.7c0-0.3,0.1-0.5,0.3-0.8c0.1-0.1,0.1-0.2,0.2-0.3
      c0.1-0.1,0.2-0.1,0.3-0.2c0.1,0,0.1,0,0.2,0c0,0,0,0,0,0c0.1,0,0.3,0,0.4,0.1c0.2,0.1,0.4,0.1,0.6,0.2c0.2,0.2,0.4,0.4,0.5,0.7
      c0.1,0.3,0.1,0.7,0.1,1.1c0,0.3-0.1,0.6-0.2,0.9c-0.1,0.2-0.2,0.4-0.3,0.7c-0.1,0.1-0.1,0.2-0.1,0.4c0,0,0,0,0,0
      c0.7,0,1.4,0,2.2,0c0.1,0,0.3,0,0.4,0.1c0.3,0.1,0.5,0.2,0.7,0.4c0.2,0.2,0.3,0.4,0.4,0.6C601.1,237.8,601.1,237.9,601.1,238z"/>
    <path class="st4" d="M588.4,238c0-0.1,0-0.2,0.1-0.2c0.1-0.1,0.2-0.2,0.4-0.2c0.8,0,1.5,0,2.3,0c0.3,0,0.5,0.2,0.5,0.4
      c0,0,0,0,0,0.1c0,1.7,0,3.4,0,5.1c0,0.3-0.2,0.5-0.5,0.5c-0.8,0-1.5,0-2.3,0c-0.2,0-0.5-0.2-0.5-0.4c0,0,0,0,0,0
      C588.4,241.5,588.4,239.7,588.4,238z M589.9,241.6c-0.3,0-0.5,0.2-0.5,0.5c0,0.3,0.2,0.5,0.5,0.5c0.3,0,0.5-0.2,0.5-0.5
      C590.4,241.8,590.2,241.6,589.9,241.6z"/>
  </g>
  <g>
    <title>Conception</title>
    <path class="st4" d="M686.6,333c0-0.2,0-0.4,0-0.6c0-0.2,0.1-0.3,0.3-0.3c0.3,0,0.5-0.1,0.7-0.2c0.6-0.3,0.8-1.1,0.4-1.7
      c0-0.1-0.1-0.1-0.2-0.2c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.1-0.3,0-0.4c0.3-0.3,0.6-0.6,0.9-0.9c0.1-0.1,0.3-0.1,0.4,0
      c0.2,0.3,0.6,0.4,0.9,0.4c0.6,0,1.1-0.4,1.2-0.9c0-0.1,0-0.2,0-0.3c0-0.2,0.1-0.3,0.3-0.3c0.4,0,0.8,0,1.2,0
      c0.2,0,0.3,0.2,0.3,0.3c0,0.7,0.5,1.2,1.1,1.3c0.4,0.1,0.8-0.1,1.1-0.3c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.2,0,0.3,0
      c0.3,0.3,0.6,0.6,0.9,0.9c0.1,0.1,0.1,0.3,0,0.4c-0.5,0.4-0.5,1.1-0.2,1.6c0.2,0.3,0.6,0.5,1,0.5c0.1,0,0.2,0,0.3,0.1
      c0.1,0.1,0.2,0.2,0.2,0.3c0,0.3,0,0.7,0,1c0,0.1,0,0.1,0,0.2c0,0.2-0.2,0.3-0.3,0.3c-0.3,0-0.6,0.1-0.9,0.3
      c-0.5,0.4-0.6,1.2-0.2,1.7c0,0,0.1,0.1,0.1,0.1c0.1,0.1,0.2,0.3,0,0.5c-0.3,0.3-0.5,0.6-0.9,0.9c-0.2,0.1-0.3,0.1-0.5,0
      c-0.4-0.4-1-0.5-1.4-0.3c-0.5,0.2-0.7,0.6-0.7,1.1c0,0,0,0.1,0,0.1c0,0.2-0.1,0.3-0.3,0.3c-0.4,0-0.8,0-1.3,0
      c-0.2,0-0.3-0.2-0.3-0.3c0-0.3-0.1-0.6-0.3-0.8c-0.4-0.5-1-0.6-1.5-0.3c-0.1,0.1-0.3,0.2-0.4,0.3c-0.1,0.1-0.3,0.1-0.4,0
      c-0.3-0.3-0.6-0.6-0.9-0.9c-0.1-0.1-0.1-0.3,0-0.4c0.2-0.2,0.3-0.4,0.4-0.7c0.1-0.7-0.3-1.3-1-1.4c-0.1,0-0.1,0-0.2,0
      c-0.1,0-0.2-0.1-0.3-0.2c-0.1-0.1-0.1-0.2-0.1-0.2C686.6,333.4,686.6,333.2,686.6,333z M694.1,333.1c0-1-0.8-1.9-1.9-1.9
      c-1,0-1.9,0.8-1.9,1.8c0,1.1,0.8,1.9,1.9,1.9C693.3,334.9,694.1,334.1,694.1,333.1z"/>
  </g>
</g>
<g class="st3">
  <g>
    <path class="st5" d="M584.8,339c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2-0.1-0.3-0.1c-0.5-0.3-1-0.7-1.5-1.1c-0.8-0.7-1.7-1.5-2.5-2.3
      c-1.2-1.2-2.1-2.5-2.6-4.2c-0.3-1-0.3-2-0.1-3.1c0.2-0.9,0.6-1.7,1.2-2.4c0.7-0.7,1.4-1.3,2.4-1.6c0.2-0.1,0.3-0.1,0.5-0.2
      c0.4-0.1,0.8-0.1,1.1-0.2c0.1,0,0.1,0,0.2,0c0.7,0,1.4,0.1,2.1,0.3c1.2,0.4,2.1,1.1,2.8,2.2c0.6,0.9,0.9,1.9,1,2.9
      c0.1,1.5-0.3,2.9-0.9,4.3c-0.5,1.3-1.2,2.5-2,3.7C585.6,338,585.2,338.5,584.8,339z M582.7,327c-1.5,0.2-2.5,1.6-2.3,3.1
      c0.2,1.5,1.6,2.5,3.1,2.3c1.5-0.2,2.5-1.6,2.3-3.1C585.6,327.8,584.2,326.8,582.7,327z"/>
    <path class="st5" d="M582.7,329.8c-0.1,0-0.2-0.1-0.3-0.1c0.2-0.7,0.3-0.8,0.9-0.6c0.1-0.2,0-0.3-0.1-0.4
      c-0.1-0.1-0.2-0.2-0.2-0.2c0-0.1,0.1-0.2,0.1-0.3c0.1,0.1,0.2,0.1,0.3,0.2c0.1,0.1,0.1,0.2,0.1,0.3c0.1-0.2,0.2-0.5,0.5-0.5
      c0.2,0.3-0.1,0.5-0.3,0.7c0.5,0,0.6,0.1,0.5,0.4c-0.2,0.2-0.4,0.1-0.6-0.1c-0.1,0-0.2,0-0.3,0c0.1,0.2,0.4,0.4,0.2,0.7
      c0,0.1-0.2,0.1-0.3,0.1c0,0.3-0.2,0.5-0.5,0.5c0,0.2-0.5,0.7-0.9,0.8c0.1-0.2,0.1-0.4,0.1-0.6c0-0.2,0.1-0.3,0.2-0.4
      C582.3,329.9,582.4,329.8,582.7,329.8C582.7,329.9,582.7,329.8,582.7,329.8z"/>
  </g>
  <g>
    <path class="st5" d="M633.3,211.9c0.1-0.2,0.3-0.3,0.6-0.4c0.8-0.2,1.6-0.3,2.4-0.2c1.5,0.2,2.8,0.8,3.8,1.8
      c0.8,0.8,1.4,1.7,1.8,2.8c0.1,0.4,0.2,0.8,0.2,1.2c-1.4-1.6-3.1-2.8-5.2-3.3c0,0,0,0,0,0c0.1,0,0.2,0.1,0.2,0.1
      c0.9,0.5,1.7,1,2.4,1.7c0.5,0.5,0.9,1,1.2,1.6c0,0,0,0.1,0.1,0.2c-0.1,0-0.1,0-0.2,0c-1,0-1.9,0-2.9,0c-0.4,0-0.8-0.1-1.1-0.3
      c-1.7-0.9-2.7-2.4-3.2-4.2c-0.1-0.3-0.1-0.5-0.2-0.8C633.3,212,633.3,211.9,633.3,211.9z"/>
    <path class="st5" d="M643,217.9c2.3,0,4.7,0,7,0c0.4,0,0.6,0.2,0.6,0.6c-0.1,2.6-1.3,4.5-3.5,5.8c-0.9,0.5-2,0.8-3.1,0.8
      c-0.8,0-1.7,0-2.5,0c-1.2-0.1-2.2-0.5-3.2-1.2c-1-0.7-1.8-1.7-2.3-2.8c-0.4-0.9-0.6-1.8-0.6-2.7c0-0.3,0.2-0.4,0.4-0.5
      c0.1,0,0.1,0,0.2,0C638.4,217.9,640.7,217.9,643,217.9z"/>
    <path class="st5" d="M642.9,217.4c0.8-0.7,1.5-1.4,2.2-2.1c-0.8,0.6-1.6,1.1-2.4,1.7c0-0.3-0.1-0.5-0.1-0.7
      c-0.1-0.3-0.2-0.5-0.3-0.8c0-0.1,0-0.2,0-0.2c0.6-1,1.6-1.6,2.7-1.6c0.5,0,1.1,0,1.6,0.1c0.3,0,0.5,0.3,0.4,0.5
      c-0.3,1.3-1,2.4-2.2,3.1c-0.1,0-0.1,0.1-0.2,0.1C644,217.4,643.5,217.4,642.9,217.4C642.9,217.4,642.9,217.4,642.9,217.4z"/>
  </g>
  <path class="st5" d="M692,251.5c0,0.1,0.1,0.2,0.1,0.2c0,0.1,0,0.3,0,0.4c0,0.3-0.2,0.5-0.5,0.7c-1.4,0.7-2.8,1.5-4.1,2.2
    c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.2,0.2-0.1,0.4c0.1,0.2,0.2,0.2,0.4,0.1c1.1-0.6,2.2-1.2,3.2-1.8c0.1,0,0.1-0.1,0.2-0.1
    c0.4-0.2,0.9,0.1,1,0.6c0.1,0.3-0.1,0.6-0.5,0.8c-1.1,0.6-2.1,1.2-3.2,1.7c-0.1,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.1,0.3
    c0.1,0.2,0.3,0.3,0.5,0.2c0.8-0.4,1.5-0.8,2.3-1.2c0.1,0,0.2-0.1,0.2-0.1c0.3-0.1,0.5,0,0.7,0.3c0.1,0.2,0.1,0.5-0.1,0.7
    c-0.1,0.1-0.3,0.3-0.5,0.4c-2.2,1.2-4.3,2.4-6.5,3.5c-0.4,0.2-0.8,0.3-1.2,0.5c-0.3,0.1-0.7,0.1-1,0.1c-0.5-0.1-0.9-0.3-1.2-0.8
    c-0.3-0.5-0.5-1-0.8-1.5c-0.4-0.7-0.7-1.3-1.1-2c-0.2-0.3-0.3-0.7-0.2-1.1c0.1-0.2,0.1-0.4,0.2-0.6c0.6-1.1,1.1-2.1,1.7-3.2
    c0.4-0.7,0.7-1.4,1.1-2.1c0.1-0.3,0.4-0.4,0.7-0.5c0.5-0.1,0.9,0.3,0.9,0.8c0,0.2-0.1,0.3-0.1,0.5c-0.2,0.5-0.5,1-0.7,1.5
    c0,0.1-0.1,0.2-0.1,0.2c0,0,0,0.1,0,0.2c0,0,0.1,0,0.2,0c0.1,0,0.2-0.1,0.2-0.1c1.9-1,3.8-2.1,5.8-3.1c0.1,0,0.1-0.1,0.2-0.1
    c0.4-0.2,1,0.1,1.1,0.5c0.1,0.4-0.1,0.8-0.5,1c-1.2,0.6-2.4,1.3-3.6,1.9c-0.1,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.1,0.3
    c0.1,0.1,0.2,0.2,0.3,0.1c0,0,0.1,0,0.1-0.1c1.4-0.7,2.8-1.5,4.2-2.2c0.1-0.1,0.3-0.1,0.5-0.1C691.6,251.2,691.8,251.4,692,251.5z"
    />
  <g>
    <path class="st5" d="M610.5,271.6c0-0.1,0-0.2,0-0.2c0.1-0.5,0.2-0.9,0.4-1.4c0.1-0.4,0.3-0.8,0.5-1.2c0.2-0.3,0.3-0.5,0.5-0.7
      c0.1-0.1,0.3-0.2,0.4-0.3c0.4-0.3,0.8-0.7,1.2-1c0.9-0.8,1.6-1.8,2.2-2.8c0.5-0.8,0.9-1.7,1.2-2.6c0.1-0.2,0.2-0.5,0.2-0.7
      c0,0,0,0,0,0c0,0.1,0,0.1-0.1,0.2c-0.2,0.7-0.5,1.3-0.9,1.9c-0.5,0.8-1,1.6-1.6,2.3c-0.2,0.3-0.5,0.6-0.8,0.8
      c-0.6,0.6-1.2,1.1-1.8,1.6c-0.2,0.1-0.3,0.2-0.5,0.4c-0.2,0.2-0.5,0.5-0.7,0.8c-0.1,0.1-0.2,0.3-0.3,0.4c0,0,0,0,0,0c0,0,0,0,0,0
      c-0.1-0.1-0.1-0.2-0.2-0.4c-0.3-0.5-0.4-1.1-0.6-1.6c-0.1-0.4-0.1-0.8-0.1-1.2c0-0.9,0.2-1.8,0.6-2.6c0.2-0.5,0.6-0.9,0.9-1.3
      c0.5-0.5,1-1,1.6-1.3c0.7-0.5,1.6-0.9,2.4-1.3c0.3-0.1,0.6-0.2,0.9-0.3c0.4-0.1,0.7-0.3,1.1-0.6c0.5-0.3,0.9-0.6,1.3-1
      c0,0,0,0,0,0c0,0,0,0,0,0c0.2,0.2,0.3,0.5,0.4,0.7c0.4,0.7,0.6,1.4,0.8,2.1c0.1,0.4,0.2,0.7,0.2,1.1c0.1,0.5,0.1,0.9,0.1,1.4
      c0,0.3,0,0.6-0.1,0.9c-0.1,0.7-0.3,1.4-0.6,2c-0.3,0.6-0.6,1.1-1,1.6c-0.5,0.6-1.2,1.2-1.9,1.5c-0.6,0.3-1.2,0.5-1.8,0.6
      c-0.4,0.1-0.7,0.1-1.1,0.1c-0.4,0-0.7,0-1.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c-0.2,0.2-0.3,0.5-0.4,0.8c-0.1,0.4-0.2,0.7-0.3,1.1
      c0,0,0,0,0,0.1c-0.3,0.1-0.5,0.2-0.8,0.3C610.6,271.6,610.5,271.6,610.5,271.6z"/>
  </g>
  <g>
    <title>Innovation</title>
    <path class="st5" d="M693.2,231.2c0,0.1,0,0.2,0,0.2c0,0.2-0.1,0.5-0.1,0.7c0,0,0,0,0,0c-0.1,0-0.1,0-0.2,0
      c-0.2,0-0.4-0.1-0.6-0.1c-0.3,0-0.6,0.2-0.7,0.5c-0.1,0.2,0,0.5,0.1,0.7c0.1,0.2,0.3,0.4,0.4,0.6l0,0l-0.8,0.6
      c-0.1-0.1-0.2-0.2-0.3-0.4c-0.1-0.1-0.1-0.2-0.2-0.3c-0.2-0.2-0.5-0.3-0.7-0.2c-0.3,0.1-0.4,0.3-0.5,0.6c0,0.2-0.1,0.5-0.1,0.7
      c0,0,0,0,0,0l-1-0.1c0-0.1,0-0.1,0-0.2c0-0.2,0.1-0.4,0.1-0.6c0-0.3-0.2-0.7-0.6-0.8c-0.2-0.1-0.4,0-0.6,0.1
      c-0.2,0.1-0.4,0.3-0.5,0.4l0,0l-0.6-0.8l0,0l0.5-0.4c0.3-0.2,0.4-0.7,0.1-1c-0.1-0.2-0.3-0.3-0.5-0.3l-0.7-0.1c0,0,0,0,0,0
      c0-0.3,0.1-0.6,0.1-1l0.2,0c0.2,0,0.4,0.1,0.6,0.1c0.4,0,0.7-0.2,0.8-0.6c0-0.2,0-0.4-0.1-0.6l-0.4-0.5c0,0,0,0,0,0l0.8-0.6
      c0,0,0,0,0,0c0.1,0.2,0.3,0.4,0.4,0.5c0.2,0.2,0.4,0.3,0.7,0.3c0.3-0.1,0.5-0.3,0.6-0.6c0-0.2,0.1-0.4,0.1-0.5c0-0.1,0-0.1,0-0.2
      l1,0.1c0,0.1,0,0.2,0,0.3c0,0.2-0.1,0.3-0.1,0.5c0,0.4,0.2,0.8,0.6,0.8c0,0,0,0,0,0c0.2,0,0.3,0,0.5-0.1l0.5-0.4c0,0,0,0,0,0
      l0.6,0.8c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.4c-0.3,0.2-0.4,0.7-0.2,1c0.1,0.2,0.3,0.3,0.5,0.3
      C692.7,231.1,693,231.1,693.2,231.2L693.2,231.2z M691,231.1c0-0.8-0.7-1.5-1.5-1.5c-0.8,0-1.4,0.6-1.4,1.4c0,0.8,0.6,1.5,1.5,1.5
      c0,0,0,0,0,0C690.4,232.6,691,232,691,231.1z"/>
    <path class="st5" d="M686.6,228.2C686.6,228.2,686.6,228.2,686.6,228.2l-0.1,0.1c-0.1,0.1-0.1,0.2-0.1,0.4
      c0.2,0.3,0.4,0.5,0.6,0.8c0.1,0.1,0.1,0.3,0,0.3c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c-0.3,0-0.6-0.1-0.8-0.1
      c-0.1,0-0.3,0.1-0.3,0.2c0,0,0,0,0,0c-0.1,0.5-0.1,1-0.2,1.5c0,0.1,0.1,0.2,0.2,0.3c0.3,0,0.5,0.1,0.8,0.1c0,0,0.1,0,0.1,0
      c0.1,0,0.2,0.1,0.2,0.3c0,0.1,0,0.1-0.1,0.2c-0.3,0.2-0.5,0.4-0.8,0.6c-0.1,0-0.1,0.1-0.1,0.2c0,0,0,0.1,0,0.1
      c0.3,0.4,0.6,0.8,0.9,1.2c0.1,0.1,0.2,0.1,0.3,0c0.2-0.1,0.4-0.3,0.5-0.4l0.2-0.2c0.1-0.1,0.2-0.1,0.3,0c0.1,0.1,0.1,0.2,0.1,0.3
      c0,0.3-0.1,0.6-0.1,0.9c0,0.1,0.1,0.2,0.2,0.3c0.3,0,0.6,0.1,0.9,0.1c0.2,0,0.4,0.1,0.5,0.1c0.1,0,0.1,0,0.2,0
      c-0.1,0.2-0.3,0.3-0.4,0.4c-0.2,0.2-0.4,0.3-0.6,0.5c-0.4,0.3-0.6,0.7-0.7,1.1c0,0.3,0,0.7,0,1c0,0.4-0.3,0.7-0.7,0.7c0,0,0,0,0,0
      h-2c-0.4,0-0.7-0.3-0.7-0.7c0-0.3,0-0.7,0-1c0-0.5-0.2-0.9-0.6-1.1c-0.2-0.2-0.5-0.3-0.7-0.5c-0.5-0.5-0.9-1.1-1.1-1.7
      c-0.2-0.5-0.3-1.1-0.2-1.7c0.1-1,0.4-1.9,1-2.6c0.6-0.7,1.3-1.2,2.2-1.4c0.3-0.1,0.6-0.1,0.9-0.1
      C686.4,228.2,686.5,228.2,686.6,228.2z M686.7,237.2C686.7,237.2,686.7,237.2,686.7,237.2c0-0.2,0-0.3,0-0.4
      c-0.2-0.7-0.6-1.3-1.3-1.5c-0.7-0.3-1.2-0.8-1.6-1.4c-0.3-0.5-0.4-1.2-0.3-1.8c0.1-0.8,0.5-1.4,1.1-1.9c0.1-0.1,0.1-0.1,0.2-0.1
      l-0.3-0.4c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.6,0.5c-0.7,0.7-1,1.5-0.9,2.5c0,0.5,0.1,1,0.4,1.4c0.4,0.8,1,1.3,1.8,1.6
      c0.2,0.1,0.4,0.2,0.6,0.4c0.2,0.2,0.4,0.5,0.4,0.8c0,0.1,0,0.2,0,0.3H686.7z M686.7,238.4v-0.7h-0.5v0.7H686.7z"/>
    <path class="st5" d="M683.9,237.6l-0.3-0.2l-0.2-0.1c-0.2-0.2-0.5-0.1-0.7,0.1c0,0.1-0.1,0.1-0.1,0.2c0,0.2-0.1,0.4-0.1,0.6
      c0,0,0,0,0,0c-0.2,0-0.3,0-0.5,0c0,0,0,0,0,0c0-0.2-0.1-0.4-0.1-0.6c-0.1-0.3-0.3-0.4-0.6-0.4c-0.1,0-0.1,0-0.2,0.1l-0.5,0.3
      c0,0,0,0,0,0l-0.4-0.4l0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3c0.2-0.2,0.1-0.5-0.1-0.7c-0.1,0-0.1-0.1-0.2-0.1c-0.2,0-0.4-0.1-0.6-0.1
      v-0.6l0.2,0c0.1,0,0.3-0.1,0.4-0.1c0.3-0.1,0.4-0.3,0.4-0.6c0-0.1,0-0.1-0.1-0.2c-0.1-0.2-0.2-0.3-0.3-0.5c0,0,0,0,0,0l0.4-0.4
      l0.2,0.2c0.1,0.1,0.2,0.1,0.3,0.2c0.3,0.2,0.7,0,0.8-0.3c0-0.1,0-0.1,0-0.2c0,0.3,0.1,0.5,0.2,0.8c-0.7,0.1-1.3,0.7-1.3,1.4
      c0,0.8,0.6,1.5,1.4,1.5c0.6,0,1.1-0.3,1.3-0.8c0,0,0,0,0,0c0.1,0.1,0.2,0.2,0.4,0.3c0.2,0.1,0.4,0.3,0.5,0.6c0,0,0,0,0,0.1
      C684.2,237.3,684.1,237.4,683.9,237.6C683.9,237.6,683.9,237.6,683.9,237.6z"/>
    <path class="st5" d="M687.4,240.4c0,0,0,0.1,0,0.1c0,0.1,0,0.2,0,0.3c-0.1,0.2-0.2,0.4-0.5,0.4c-0.3,0-0.7,0-1,0
      c-0.3,0-0.5-0.2-0.5-0.5c0-0.1,0-0.2,0-0.2c0,0,0,0,0,0L687.4,240.4z"/>
    <path class="st5" d="M681.7,229.4c-0.1,0.1-0.2,0.3-0.3,0.4l-1.7-1.2c0.1-0.1,0.2-0.3,0.3-0.4L681.7,229.4z"/>
    <path class="st5" d="M682.3,226.9c0.2,0.7,0.3,1.3,0.5,2l-0.5,0.1c-0.2-0.7-0.3-1.3-0.5-2L682.3,226.9z"/>
    <path class="st5" d="M679.4,231.6c0-0.2-0.1-0.3-0.1-0.5l2-0.5l0.1,0.5L679.4,231.6z"/>
    <path class="st5" d="M685.5,239.9v-0.5h2v0.5H685.5z"/>
    <path class="st5" d="M690.7,237.6l0.3-0.4l1.5,1.2l-0.3,0.4L690.7,237.6z"/>
    <path class="st5" d="M691.1,236.5V236h1.7v0.5H691.1z"/>
    <path class="st5" d="M689.6,237.9h0.5v1.7h-0.5V237.9z"/>
    <path class="st5" d="M690,235.2c0-0.2,0-0.3,0.1-0.5c0-0.2,0-0.3,0.1-0.5c0-0.1,0.1-0.2,0.2-0.2c0.1,0,0.2,0,0.3,0.1
      c0,0,0,0,0,0.1C690.4,234.5,690.2,234.8,690,235.2C690,235.2,690,235.2,690,235.2z"/>
  </g>
</g>
<g>
  <line class="st6" x1="732.9" y1="313.6" x2="763.5" y2="313.6"/>
  <line class="st6" x1="771.2" y1="323.8" x2="763.5" y2="313.6"/>
</g>
<g>
  <line class="st6" x1="742.4" y1="268.2" x2="773" y2="268.2"/>
  <line class="st6" x1="780.6" y1="278.4" x2="773" y2="268.2"/>
</g>
<g>
  <line class="st6" x1="732.3" y1="212.5" x2="762.9" y2="212.5"/>
  <line class="st6" x1="770.6" y1="202.3" x2="762.9" y2="212.5"/>
</g>
<g>
  <line class="st6" x1="554.6" y1="212.5" x2="524" y2="212.5"/>
  <line class="st6" x1="516.4" y1="202.3" x2="524" y2="212.5"/>
</g>
<g>
  <line class="st6" x1="601.9" y1="175.2" x2="584.5" y2="162.9"/>
  <line class="st6" x1="565.6" y1="162.4" x2="584.5" y2="162.9"/>
</g>
<g>
  <line class="st6" x1="679" y1="168.4" x2="696.4" y2="156.1"/>
  <line class="st6" x1="715.3" y1="155.6" x2="696.4" y2="156.1"/>
</g>
<g>
  <line class="st6" x1="543" y1="303.8" x2="512.4" y2="303.8"/>
  <line class="st6" x1="504.8" y1="314" x2="512.4" y2="303.8"/>
</g>
<g>
  <line class="st6" x1="541" y1="265.6" x2="510.4" y2="265.6"/>
  <line class="st6" x1="502.8" y1="275.8" x2="510.4" y2="265.6"/>
</g>
<rect x="779.5" y="284.5" class="st7" width="71" height="11.7"/>
<a href="#economie" class="lien"><text transform="matrix(1 0 0 1 779.4899 291.509)" class="st8 st9 st10">ECONOMIE</text></a>
<rect x="448.1" y="191.1" class="st7" width="71" height="11.7"/>
<a href="#construction" class="lien"><text transform="matrix(1 0 0 1 448.1012 198.0826)" class="st8 st9 st10">CONSTRUCTION</text></a>
<rect x="729.8" y="148.8" class="st7" width="71" height="11.7"/>
<a href="#dechets" class="lien"><text transform="matrix(1 0 0 1 729.8234 155.7701)" class="st8 st9 st10">DECHETS</text></a>
<rect x="469.7" y="318.8" class="st7" width="71" height="11.7"/>
<a href="#transport" class="lien"><text transform="matrix(1 0 0 1 469.7169 325.8416)" class="st8 st9 st10">ENERGIE</text></a>
<rect x="393.5" y="232.6" class="st7" width="116.9" height="26.2"/>
<text transform="matrix(1 0 0 1 424.9308 247.5091)" class="st11 st12 st13" id="humain">HUMAIN</text>
<rect x="767.7" y="232.6" class="st7" width="116.9" height="26.2"/>
<text transform="matrix(1 0 0 1 767.735 247.5092)" class="st8 st12 st13 " id="territoire">TERRITOIRE</text>
<rect x="770" y="189.4" class="st7" width="71" height="11.7"/>
<a href="#education" class="lien"><text transform="matrix(1 0 0 1 769.9972 196.356)" class="st11 st9 st10">EDUCATION</text></a>
<rect x="511.9" y="155.6" class="st7" width="71" height="11.7"/>
<a href="#commun" class="lien"><text transform="matrix(1 0 0 1 511.9459 162.564)" class="st11 st9 st10">COMMUNS</text></a>
<rect x="446.2" y="280.6" class="st7" width="71" height="11.7"/>
<a href="#citoyennete" class="lien"><text transform="matrix(1 0 0 1 446.1583 287.5829)" class="st11 st9 st10">CITOYENNETE</text></a>
<rect x="772.1" y="329.1" class="st7" width="97.8" height="11.7"/>
<a href="#sante" class="lien"><text transform="matrix(1 0 0 1 772.0912 336.0812)" class="st11 st9 st10">ALIMENTATION/SANTE</text></a>
<g>
  <a href="#construction"><g class="clterritoire construction-on">
    <title>Construction</title>
    <path class="st8" d="M562.3,222.6c0.1-0.4,0.2-0.7,0.3-1c0.4-1.2,0.7-2.4,1.1-3.5c0-0.1,0-0.2,0.1-0.3c0-0.6,0.1-1.2,0.1-1.9
      c0-0.7,0.1-1.5,0.1-2.2c0-0.4,0.1-0.9,0.1-1.3v-0.1l-2.3-0.6c0,0,0-0.1,0-0.1v-3.6c0-0.1,0-0.2,0.1-0.2c0.9-0.8,1.8-1.6,2.7-2.4
      c0,0,0.1-0.1,0.2-0.1c0.4,0,0.8,0.1,1.1,0.1c0.1,0,0.1,0,0.1,0c0.3-0.3,0.6-0.5,1-0.8c-0.9-0.7-1.2-1.6-1.1-2.7l5.1,0.7
      c0.1,0.4-0.2,1.1-0.6,1.6c-0.4,0.4-0.9,0.7-1.4,0.9c0.1,0.3,0.2,0.5,0.3,0.8c0.6,0,1.1,0,1.7,0.1c0,0.1,0,0.3,0.1,0.4
      c0.1,0.5,0.1,1.1,0.2,1.6c0.1,0.5,0.1,0.9,0.2,1.4c0,0.4,0.1,0.8,0.1,1.2c0,0.2,0.1,0.3,0.1,0.5c0.2,0.6,0.4,1.3,0.5,1.9
      c0,0,0,0.1,0,0.1c0,0-0.1,0-0.1,0c-0.5-0.3-1-0.6-1.5-0.8c-0.1,0-0.1-0.1-0.1-0.1c-0.1-0.3-0.2-0.6-0.3-0.9
      c-0.1-0.2-0.1-0.3-0.1-0.5c-0.1-0.6-0.1-1.1-0.2-1.7c0,0,0,0.1,0,0.1c-0.2,0.7-0.4,1.5-0.5,2.2c0,0.1,0,0.1,0,0.2
      c0.1,0.3,0.2,0.6,0.3,1c0,0.1,0.1,0.1,0.1,0.2c1.5,0.8,2.9,1.6,4.4,2.5l1.5,0.9c0,0,0.1,0,0.1,0.1l1-1.7l2.3,1.3l-2.4,4.2
      l-2.3-1.3l1-1.7l-5.3-3c0,0.1,0,0.1,0,0.1c0.2,1,0.6,1.9,0.9,2.9c0.1,0.3,0.2,0.5,0.3,0.8c0,0.1,0,0.1,0,0.2l-0.9,4.9
      c0,0,0,0,0,0.1h-1.9c0,0,0-0.1,0-0.1c0.1-0.6,0.2-1.2,0.3-1.9c0.2-0.8,0.3-1.6,0.5-2.4c0-0.1,0-0.1,0-0.2
      c-0.4-1.2-0.9-2.3-1.3-3.5c0,0,0-0.1,0-0.1c-0.2,0-1.4,0-1.5,0c0,0.1,0,0.1,0,0.2c-0.1,1-0.3,2.1-0.4,3.1c0,0.2,0,0.3-0.1,0.5
      c-0.5,1.5-1,2.9-1.5,4.4c0,0,0,0.1,0,0.2H562.3z M564.3,210.4c0-0.2,0-2.1,0-2.2c0,0,0,0,0,0c-0.3,0.3-0.6,0.5-0.9,0.8
      c0,0-0.1,0.1-0.1,0.2c0,0.3,0,0.6,0,0.9v0.2L564.3,210.4z"/>
    <path class="st8" d="M576.6,222.5C576.6,222.5,576.6,222.5,576.6,222.5c1.4-2.6,2.8-5.1,4.2-7.7c0,0,0.1-0.1,0.1-0.1l1.2,0.5
      l1.8-2.9c0,0,0.1,0,0.1,0c0.5,0.1,0.9,0.2,1.4,0.4c0.1,0,0.2,0.1,0.2,0.2c0.8,2.1,1.7,4.2,2.5,6.3c0.4,1.1,0.9,2.1,1.3,3.2
      c0,0.1,0,0.1,0.1,0.2c0,0.1-0.1,0.1-0.1,0.1s-1.1,0-1.6,0c0,0,0,0,0,0c-0.7-1-1.3-1.9-2-2.9c0-0.1-0.1-0.1-0.2-0.1
      c-0.6,0-1.1,0-1.7,0c-0.1,0-0.1,0-0.2,0.1c-0.7,0.9-1.4,1.8-2.1,2.7c0,0,0,0.1-0.1,0.1H576.6z"/>
    <path class="st8" d="M587.1,222.5h-4.9c0.6-0.8,1.2-1.6,1.8-2.3c0-0.1,0.1-0.1,0.2-0.1c0.4,0,0.8,0,1.2,0c0.1,0,0.1,0,0.2,0.1
      c0.2,0.3,0.5,0.7,0.7,1L587.1,222.5z"/>
    <path class="st8" d="M571.9,202.1l-6.4-0.9c0,0,0-0.1,0-0.1c0.1-0.6,0.3-1.1,0.7-1.7c0.3-0.4,0.6-0.8,1-1.1
      c0.6-0.3,1.2-0.5,1.8-0.4c0.4,0,0.8,0.1,1.2,0.3c0.6,0.3,1.1,0.9,1.3,1.5c0.2,0.4,0.3,0.9,0.3,1.4c0,0.2,0,0.4,0,0.7
      C571.9,201.9,571.9,202,571.9,202.1z"/>
  </g></a>
<a href="#transport"><g class="clterritoire transport-on">
    <title>energie</title>
    <path class="st8" d="M559.4,284.5c0.4,0.1,0.8,0.1,1.2,0.2c2.5,0.4,4.5,1.7,6.2,3.5c0.1,0.1,0.1,0.1,0.2,0.2l0.1,0
      c0-0.2,0-0.4,0-0.6c0-0.4,0.3-0.7,0.6-0.7s0.6,0.3,0.6,0.6c0,0.9,0,1.8,0,2.8c0,0.3-0.3,0.6-0.6,0.6c0,0,0,0,0,0
      c-0.8-0.1-1.7-0.1-2.5-0.2c-0.4,0-0.6-0.3-0.6-0.7s0.2-0.6,0.7-0.6c0.4,0,0.8,0,1.2,0.1c0,0,0-0.1,0-0.1c-1.5-1.8-3.3-3.2-5.7-3.6
      c-3.7-0.7-8.8,0.6-10.9,5.9c-0.4,1.1-0.7,2.2-0.7,3.4c0,0.5-0.3,0.8-0.7,0.8c-0.3,0-0.6-0.3-0.6-0.7c0.1-3.2,1.2-5.9,3.6-8.1
      c1.6-1.4,3.4-2.3,5.5-2.7c0.4-0.1,0.8-0.1,1.2-0.2L559.4,284.5z"/>
    <path class="st8" d="M559.6,297.1c0.4,0.7,0.7,1.4,1,2.1c0.4,0.7,0.7,1.4,1.1,2.1c0,0,0,0.1,0,0.1c0.2,0.4,0.2,0.8-0.1,1
      c-0.3,0.2-0.7,0.1-1-0.2c-2-2-4.1-4-6.1-6.1c-0.1-0.1-0.1-0.1-0.2-0.2c-0.4-0.5-0.3-0.9,0.3-1.1c0.5-0.2,1.1-0.4,1.6-0.5l1.5-0.5
      c-0.1-0.1-0.1-0.3-0.2-0.4c-0.7-1.3-1.3-2.6-1.9-3.9c-0.1-0.1-0.1-0.3-0.1-0.4c0-0.2,0-0.5,0.3-0.6c0.2-0.1,0.5-0.1,0.7,0
      c0.1,0.1,0.2,0.2,0.3,0.2c2,2,4,4,6.1,6.1c0,0,0.1,0.1,0.1,0.1c0.4,0.5,0.3,1-0.3,1.2c-0.7,0.2-1.4,0.5-2.1,0.7
      C560.2,296.9,559.9,297,559.6,297.1z"/>
    <path class="st8" d="M550.3,302.5c0,0.2,0,0.4,0,0.6c0,0.4-0.3,0.7-0.6,0.7c-0.4,0-0.6-0.3-0.6-0.7c0-0.9,0-1.8,0-2.6
      c0-0.4,0.3-0.7,0.8-0.7c0.8,0.1,1.6,0.1,2.4,0.2c0.4,0,0.7,0.3,0.7,0.7c0,0.4-0.3,0.6-0.7,0.6c-0.4,0-0.8,0-1.2,0
      c0.2,0.2,0.4,0.5,0.6,0.7c1.5,1.7,3.4,2.8,5.6,3.1c4,0.5,7.2-0.9,9.5-4.2c1.1-1.6,1.6-3.3,1.6-5.2c0-0.6,0.5-0.9,0.9-0.7
      c0.3,0.1,0.4,0.3,0.4,0.6c-0.1,3.2-1.2,5.9-3.6,8c-1.6,1.4-3.4,2.4-5.6,2.7c-3.3,0.5-6.2-0.3-8.8-2.5
      C551.2,303.4,550.8,303,550.3,302.5z"/>
  </g></a>
  <a href="#economie"><g>
    <g>
      <g class="clterritoire economie-on">
        <path class="st8" d="M719.8,262.1c0.7,0,1.3,0,1.9-0.1v-6.6c-0.6-0.1-1.2-0.1-1.9-0.1c-3.7,0-6.8,1.4-6.8,3.1v0.7
          C713,260.8,716,262.1,719.8,262.1z"/>
        <path class="st8" d="M719.8,265.6c0.7,0,1.3,0,1.9-0.1v-1.9c-0.6,0.1-1.2,0.1-1.9,0.1c-3.3,0-6-1-6.6-2.4
          c-0.1,0.2-0.1,0.4-0.1,0.6v0.7C713,264.2,716,265.6,719.8,265.6z"/>
        <path class="st8" d="M721.6,267.1v-0.1c-0.6,0.1-1.2,0.1-1.9,0.1c-3.3,0-6-1-6.6-2.4c-0.1,0.2-0.1,0.4-0.1,0.6v0.7
          c0,1.7,3,3.1,6.8,3.1c0.7,0,1.5-0.1,2.1-0.2c-0.2-0.3-0.3-0.7-0.3-1.1V267.1z"/>
        <path class="st8" d="M729.5,250.1c-3.7,0-6.8,1.4-6.8,3.1v0.7c0,1.7,3,3.1,6.8,3.1c3.7,0,6.8-1.4,6.8-3.1v-0.7
          C736.3,251.5,733.2,250.1,729.5,250.1z"/>
        <path class="st8" d="M729.5,258.4c-3.3,0-6-1-6.6-2.4c-0.1,0.2-0.1,0.4-0.1,0.6v0.7c0,1.7,3,3.1,6.8,3.1c3.7,0,6.8-1.4,6.8-3.1
          v-0.7c0-0.2,0-0.4-0.1-0.6C735.5,257.4,732.8,258.4,729.5,258.4z"/>
        <path class="st8" d="M729.5,261.9c-3.3,0-6-1-6.6-2.4c-0.1,0.2-0.1,0.4-0.1,0.6v0.7c0,1.7,3,3.1,6.8,3.1c3.7,0,6.8-1.4,6.8-3.1
          v-0.7c0-0.2,0-0.4-0.1-0.6C735.5,260.8,732.8,261.9,729.5,261.9z"/>
        <path class="st8" d="M729.5,265.5c-3.3,0-6-1-6.6-2.4c-0.1,0.2-0.1,0.4-0.1,0.6v0.7c0,1.7,3,3.1,6.8,3.1c3.7,0,6.8-1.4,6.8-3.1
          v-0.7c0-0.2,0-0.4-0.1-0.6C735.5,264.4,732.8,265.5,729.5,265.5z"/>
        <path class="st8" d="M729.5,268.9c-3.3,0-6-1-6.6-2.4c-0.1,0.2-0.1,0.4-0.1,0.6v0.7c0,1.7,3,3.1,6.8,3.1c3.7,0,6.8-1.4,6.8-3.1
          v-0.7c0-0.2,0-0.4-0.1-0.6C735.5,267.9,732.8,268.9,729.5,268.9z"/>
      </g>
    </g>
  </g></a>
  <a href="#dechets"><g class="clterritoire dechets-on">
    <path class="st8" d="M665.5,172.8c0.2,0,0.3,0,0.5,0c0.9,0,1.9,0,2.8,0c0.6,0,1.1,0,1.7,0c0.6,0,1.1,0,1.7,0h0.3v0c0,0,0,0,0,0
      c-1.1,0.3-1.9,0.9-2.4,1.8c-0.3,0.5-0.6,1-0.8,1.5c-0.9,1.6-1.8,3.2-2.7,4.8c0,0,0,0,0,0.1c-1.7-0.9-3.4-1.9-5.2-2.9
      c0.1-0.1,0.1-0.2,0.2-0.3c0.6-1.1,1.2-2.1,1.8-3.2c0.2-0.4,0.5-0.8,0.8-1.1C664.5,173.3,664.9,173,665.5,172.8
      C665.5,172.9,665.5,172.8,665.5,172.8L665.5,172.8z"/>
    <path class="st8" d="M657.2,185.4c0-0.5,0.3-0.9,0.6-1.3c0.4-0.7,0.7-1.4,1.1-2.1c0,0,0,0,0-0.1c-0.5-0.4-1-0.8-1.5-1.2
      c0,0,0,0,0,0c0.1,0,0.2,0,0.2,0c0.4,0,0.7,0,1,0c0.8,0,1.5,0,2.3,0c0.6,0,1.2,0,1.9,0c0.1,0,0.1,0,0.1,0.1
      c0.9,1.6,1.8,3.3,2.7,4.9c0,0,0,0,0,0c-0.5-0.3-1-0.6-1.6-0.8c-0.1,0.2-0.3,0.4-0.4,0.7c-0.5,0.9-1.1,1.8-1.6,2.7
      c0,0.1-0.1,0.1-0.1,0.1c-0.8,0-1.6,0-2.4-0.2c-1-0.3-1.7-0.9-2.1-1.8c-0.1-0.2-0.1-0.4-0.2-0.6
      C657.2,185.5,657.2,185.5,657.2,185.4z"/>
    <path class="st8" d="M672.9,197c-0.1-0.2-0.2-0.4-0.4-0.6c-0.8-1.3-1.7-2.7-2.5-4c0-0.1,0-0.1,0-0.2c0.9-1.6,1.8-3.2,2.7-4.8
      c0,0,0,0,0.1-0.1c0,0.6,0.1,1.2,0.1,1.8c0.1,0,0.2,0,0.4,0c0.5,0,1,0,1.5,0c0.6,0,1.2,0,1.9,0c0.2,0,0.3,0,0.3,0.2
      c0.4,0.6,0.7,1.2,0.9,1.8c0.4,0.9,0.3,1.8-0.1,2.6c-0.2,0.5-0.5,0.8-1,1c-0.2,0.1-0.4,0.1-0.6,0.1c-0.9,0.1-1.9,0.1-2.8,0.2
      c-0.1,0-0.2,0-0.3,0c0,0.3-0.1,0.6-0.1,0.9C673,196.3,673,196.6,672.9,197C673,196.9,673,197,672.9,197C673,197,673,197,672.9,197
      z"/>
    <path class="st8" d="M657.6,187.6c0.2,0.2,0.5,0.4,0.7,0.6c0.5,0.4,1.1,0.6,1.7,0.6c0.4,0,0.7,0,1.1,0c0.6,0,1.1,0,1.7,0
      c0.6,0,1.2,0,1.8,0c0.8,0,1.6,0,2.4,0c0.2,0,0.5,0,0.7,0c0,0,0,0,0,0v5.9c0,0,0,0,0,0c-1.3,0-2.7,0-4,0c-0.6,0-1.3-0.1-1.9-0.3
      c-0.4-0.1-0.7-0.3-0.9-0.6c-0.1-0.1-0.1-0.2-0.2-0.2C659.8,191.7,658.7,189.7,657.6,187.6C657.6,187.7,657.6,187.6,657.6,187.6z"
      />
    <path class="st8" d="M679.4,180.6C679.5,180.6,679.5,180.6,679.4,180.6c0.7,1.1,1.4,2.2,2.1,3.3c0.3,0.5,0.6,1,0.7,1.6
      c0.1,0.4,0.1,0.9,0,1.3c-0.1,0.2-0.2,0.3-0.3,0.5c-1.1,1.9-2.1,3.8-3.2,5.6c0,0,0,0,0,0c0-0.1,0-0.2,0.1-0.3c0.2-1,0-1.9-0.5-2.8
      c-0.3-0.6-0.7-1.1-1.1-1.7c-0.9-1.5-1.8-3-2.7-4.4c0,0,0-0.1-0.1-0.1C676.1,182.7,677.8,181.6,679.4,180.6z"/>
    <path class="st8" d="M677,176.8c0.6-0.3,1.2-0.5,1.8-0.8c-0.1,0.1-0.1,0.3-0.2,0.4c-0.8,1.5-1.6,3-2.4,4.4c0,0-0.1,0.1-0.1,0.1
      c-0.6,0-1.2,0-1.8,0c-0.6,0-1.3,0-1.9,0c-0.5,0-1,0-1.5,0c-0.2,0-0.4,0-0.5,0c0,0,0,0,0,0c0.5-0.3,1-0.6,1.5-0.9
      c-0.3-0.5-0.5-1-0.8-1.4c-0.4-0.7-0.7-1.3-1.1-2c0,0,0-0.1,0-0.1c0.3-0.6,0.7-1.2,1.1-1.8c0.6-0.8,1.4-1.2,2.4-1.3
      c0.5,0,1,0.1,1.4,0.4c0.1,0.1,0.2,0.2,0.3,0.4C675.8,175,676.3,175.9,677,176.8C676.9,176.8,676.9,176.8,677,176.8z"/>
  </g></a>
</g>
<g id="pBonhomme">
  <g>
    <path class="st14" d="M619,357.6l10-6c-5-5.6-11.4-36-11.4-36c-1.5-6.6-2.3-13.2,1.4-19.5c0.2-0.3,0.2-0.8,0.3-1.3
      c-2.8-0.3-4,1.2-4.9,3.4c-0.6,1.3-1.9,2.7-3.1,3c-0.8,0.2-2.3-1.3-3.1-2.3c-1.8-2.3-3.3-4.9-5.1-7.2c-0.8-1-2-1.6-3-2.4
      c-0.3,0.3-0.5,0.5-0.8,0.8c2.1,3.8,4.2,7.7,6.3,11.5c-4.4-2.8-6.8-7.8-12.1-9.9c-0.8,3.3,1.6,4.3,3,5.8c1.4,1.6,2.9,3.1,4.8,5
      c-3.5-1.1-6.2-2.2-9-2.8c-0.9-0.2-2.1,0.6-3.2,1c0.8,0.6,1.6,1.4,2.5,1.7c2.8,1.2,5.7,2.2,8.6,3.3c-3.1,1.2-6.3-0.5-9,2.9
      c1.4,0.3,2.5,0.3,3.4,0.7c3.8,1.6,7.7,3,11.3,4.9c1.1,0.6,2,2.2,2.3,3.5c0,0.2,0.1,0.3,0.1,0.5L619,357.6z"/>
    <path class="st14" d="M629,351.6c0,0,11.5-4.1,6.5-14c0,0-14.6-19.1,5.2-23.4c0,0,18.9-3,9.5,21.3c0,0-2.8,9,0.2,12.7
      c3,3.7,19.9-30.7,20.7-33.5c1.5-6.9,2.5-13.9-2.2-20.4c2.6-0.7,3.9,0.2,4.9,1.9c0.5,0.9,0.8,2,1.3,2.8c1.5,2.3,3.3,2.3,4.9,0
      c1.8-2.5,3.5-5.2,5.4-7.7c0.8-1.1,2-1.9,3-2.8c0.4,0.2,0.7,0.5,1.1,0.7c-2.1,3.9-4.2,7.8-6.3,11.7c4.4-2.8,6.8-7.8,12-9.5
      c-0.4,4.8-5.3,6.4-7.2,10.3c2.8-0.9,5.4-2,8.1-2.6c1.1-0.2,2.4,0.5,3.6,0.8c-0.9,0.6-1.7,1.4-2.7,1.9c-2.8,1.1-5.6,2.1-8.4,3.2
      c0,0.3,0.1,0.5,0.1,0.8c1.5,0,3-0.1,4.4,0c1.4,0.2,2.7,0.7,4.1,1.1c0,0.4-0.1,0.7-0.1,1.1c-0.9,0.2-1.9,0.4-2.8,0.6
      c-2.5,0.7-5.1,1.2-7.4,2.3c-3.6,1.6-6,4.1-7,8.2c0,0-21.2,42.9-20.8,44.8c1.1,4.9-38.9,1.4-38.9,1.4l-0.7-3.7l-0.7-3.9"/>
      <path class="st14" d="M620.4,365c0,0,2.5,12.8,1.6,30.4h35c0,0,1.6-28.6,3-34.3c1.4-5.7-0.2-5,1.4-5.7"/>
  </g>
</g>
<a href="#sante"><g class="clhumain sante-on">
  <path class="st11" d="M715,321.9c-0.1-0.1-0.3-0.2-0.4-0.3c-0.5-0.4-0.6-0.8-0.5-1.7c-0.1,0-0.2-0.1-0.3-0.1c-2-1-4-1.9-6-2.9
    c-0.1-0.1-0.2-0.1-0.3-0.2c-0.2-0.1-0.2-0.3-0.2-0.6c0.1-0.2,0.3-0.4,0.5-0.3c0.1,0,0.3,0.1,0.4,0.1c2,1,4.1,2,6.1,2.9
    c0.2,0.1,0.3,0.1,0.4-0.1c0.5-0.5,1-0.6,1.6-0.4c0.2,0.1,0.4,0.1,0.7,0.2c0.9,0.4,1.8,0.9,2.7,1.3c0.1,0,0.2,0.1,0.2,0.1
    c0.1,0.1,0.1,0.2,0.1,0.3c0,0.1-0.2,0.1-0.3,0.1c-0.1,0-0.2-0.1-0.2-0.1c-0.9-0.4-1.8-0.9-2.7-1.3c-0.1,0.2-0.1,0.3-0.2,0.5
    c0.1,0,0.2,0.1,0.3,0.1c0.8,0.4,1.6,0.8,2.4,1.2c0.1,0,0.2,0.1,0.2,0.2c0,0.1,0.1,0.2,0,0.3c0,0.1-0.2,0.1-0.3,0.1
    c-0.1,0-0.2-0.1-0.3-0.1c-0.8-0.4-1.6-0.8-2.4-1.2c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0.2-0.2,0.3-0.2,0.5c0.3,0.1,0.6,0.3,0.8,0.4
    c0.6,0.3,1.3,0.6,1.9,0.9c0.2,0.1,0.3,0.2,0.2,0.4c-0.1,0.2-0.3,0.2-0.5,0.1c-0.8-0.4-1.7-0.8-2.5-1.2c-0.1,0-0.2-0.1-0.3-0.1
    c-0.1,0.2-0.2,0.3-0.2,0.5c0.1,0,0.2,0.1,0.2,0.1c0.8,0.4,1.6,0.8,2.5,1.2c0.1,0.1,0.3,0.2,0.3,0.3c0,0.1-0.2,0.2-0.2,0.3
    C717.1,323,716.1,322.5,715,321.9z"/>
  <path class="st11" d="M713.9,317c-2.6-1.3-4.4-3.4-5.3-6.4c-0.8-2.8-0.3-5.5,1.4-7.9c2-2.7,4.7-4,8-4c1.9,0,3.8,0.5,5.4,1.6
    c2.9,1.8,4.5,4.4,4.8,7.8c0.2,2.2-0.4,4.2-1.7,6c-1.7,2.4-4.1,3.7-7.1,3.9C717.6,318.2,715.7,317.9,713.9,317z M722.1,301
    c-2.4-1.2-4.9-1.3-7.3-0.4c-2.6,1-4.3,2.9-5,5.6c-0.6,2.6-0.1,5,1.5,7.1c1.8,2.3,4.2,3.5,7.2,3.5c2.8,0,5.2-1.1,6.9-3.4
    c1.3-1.8,1.8-3.9,1.4-6.1C726.2,304.3,724.5,302.2,722.1,301z"/>
  <path class="st11" d="M725,298.1c1.3-1.1,3.5-0.3,4.2,1.1c0.6,1.1,0.1,2.2-1.1,2.5c-1.3,0.3-3-0.6-3.4-1.8
    c-0.1-0.1-0.1-0.3-0.1-0.4c0-0.3-0.2-0.4-0.4-0.5c-2.2-1-4.3-2.1-6.5-3.1c-0.1-0.1-0.2-0.1-0.3-0.2c-0.2-0.2-0.2-0.4-0.1-0.6
    c0.1-0.2,0.4-0.3,0.6-0.2c0.1,0,0.2,0.1,0.3,0.1C720.5,295.9,722.8,297,725,298.1z"/>
  <path class="st11" d="M715.1,315.4c-2.9-1.5-4.5-3.7-4.8-6.8c-0.2-2.9,1-5.2,3.4-6.8c2.2-1.4,4.6-1.7,7.1-0.8
    c2.9,1.1,4.8,3.2,5.4,6.2c0.4,2,0,3.9-1.2,5.5c-2.1,2.8-4.9,3.7-8.3,3.1C716.1,315.8,715.5,315.6,715.1,315.4z M720.5,309.5
    c0.7,0,1.3-0.2,1.8-0.7c0.8-0.7,1.3-1.6,1-2.8c-0.2-0.8-0.8-1.4-1.5-1.8c-1.1-0.7-2.4-0.8-3.7-0.5c-1.7,0.4-3.1,1.2-4.5,2.3
    c-0.1,0.1-0.1,0.2-0.1,0.3c0,1.7,0.2,3.4,1,5c0.7,1.4,1.8,2.4,3.3,2.7c0.8,0.2,1.5,0.1,2.1-0.4C721,312.8,721.5,310.7,720.5,309.5z
    "/>
  <path class="st11" d="M722.4,307c-1-1.9-2.6-2.4-4.6-2.4c0,0,0,0,0-0.1c0.4-0.1,0.7-0.2,1.1-0.2c1-0.1,2,0.1,2.8,0.7
    C722.4,305.5,722.7,306.3,722.4,307z"/>
</g></a>
<a href="#education"><g class="clhumain education-on">
  <title>education</title>
  <path class="st11" d="M725.3,211.7c0.5,0.7,0.5,1.5,0.5,2.3c0,0.4-0.1,0.9-0.3,1.3c-0.2,0.7-0.5,1.1-1.1,1.5l0.3,0.1l0.2,0.1
    c0.2,0,0.2,0.2,0.2,0.4c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.2,0.1-0.3,0.2c-0.1,0.1-0.2,0.1-0.3,0.1c-0.3,0.1-0.5,0.3-0.7,0.5
    c-0.2,0.3-0.3,0.6-0.3,1c0,0.4,0,0.8-0.1,1.2c0,0.1,0,0.2,0.1,0.2c0.5,0.2,1,0.4,1.5,0.7c0.1,0.1,0.2,0.1,0.3,0.2
    c0.1,0,0.1,0.1,0.1,0.2c0,0.1-0.1,0.1-0.2,0.1c-0.2,0-0.4,0.1-0.7,0.1l-6.3,1l-8,1.3c-0.2,0-0.4,0.1-0.6,0.1
    c-0.2,0-0.4-0.1-0.6-0.2c-3.1-1.3-6.2-2.5-9.3-3.8c-0.7-0.3-1-0.8-1.3-1.4c-0.2-0.6-0.2-1.2-0.2-1.7c0-0.5,0.1-1.1,0.3-1.5
    c0.1-0.3,0.4-0.6,0.7-0.9c-0.2-0.1-0.4-0.1-0.5-0.2c0,0-0.1,0-0.1-0.1c-0.1-0.1-0.1-0.2,0-0.3c0.1-0.1,0.2-0.1,0.3-0.2
    c0.5-0.2,1-0.3,1.6-0.4c0.1,0,0.1,0,0.1-0.1c0.1-0.5,0.1-1.1,0.1-1.6c0-0.3-0.1-0.6-0.3-0.9c-0.1-0.2-0.3-0.3-0.5-0.4
    c-0.2-0.1-0.4-0.2-0.5-0.3c-0.1-0.1-0.2-0.3-0.1-0.4c0,0,0,0,0,0c0-0.1,0.1-0.1,0.2-0.1c0.4-0.1,0.7-0.2,1.1-0.2l0.1,0
    c-0.1-0.2-0.3-0.4-0.4-0.7c-0.2-0.4-0.3-0.9-0.3-1.4c-0.1-0.6,0-1.2,0.1-1.7c0.1-0.4,0.3-0.7,0.5-1.1c0.3-0.4,0.9-0.7,1.4-0.7
    c0.8,0,1.5-0.1,2.3-0.1l2.1-0.1l2.4-0.2l2.1-0.1l2.4-0.2l2.5-0.2c0.5,0,0.9-0.1,1.4-0.1c0.1,0,0.1,0,0.2,0l9.2,3.5
    c0.2,0.1,0.2,0.2,0.1,0.4c0,0,0,0,0,0c-0.1,0.1-0.1,0.1-0.2,0.2c-0.1,0.1-0.3,0.1-0.5,0.2c-0.4,0.1-0.6,0.3-0.7,0.6
    c-0.2,0.3-0.3,0.7-0.3,1c0,0.4,0,0.8-0.1,1.1c0,0.1,0,0.1,0.1,0.1c0.5,0.2,1,0.4,1.4,0.6c0.1,0.1,0.2,0.1,0.3,0.2
    c0.1,0.1,0.1,0.1,0.1,0.2c0,0.1-0.1,0.1-0.2,0.1c-0.3,0.1-0.6,0.1-1,0.2L725.3,211.7z M713.6,218C713.7,218,713.7,218,713.6,218
    l9.2-1.9c0.3-0.1,0.5-0.1,0.8-0.2c0.2,0,0.3-0.1,0.5-0.2c0.2-0.2,0.4-0.4,0.5-0.7c0.2-0.6,0.3-1.3,0.3-2c0-0.3-0.1-0.5-0.2-0.8
    c-0.2-0.2-0.4-0.4-0.7-0.3l-5.5,1.4l-4.6,1.1c-0.1,0-0.1,0-0.1,0.1c0,0.8-0.1,1.7-0.1,2.5C713.7,217.5,713.7,217.7,713.6,218
    L713.6,218z M709.6,223c0,0,0-0.1,0-0.1c0.1-1,0.2-2.1,0.3-3.1c0-0.1,0-0.1-0.1-0.1c0,0,0,0,0,0c-0.6-0.3-1.3-0.5-1.9-0.8l-7.7-3.1
    c-0.3-0.1-0.6,0-0.8,0.2c-0.1,0.2-0.2,0.4-0.3,0.6c-0.1,0.6-0.2,1.1-0.1,1.7c0,0.3,0.1,0.7,0.3,1c0.1,0.2,0.3,0.3,0.5,0.4
    c0.2,0.1,0.4,0.1,0.6,0.2l8.1,2.9L709.6,223z M711.2,212.5C711.2,212.5,711.2,212.5,711.2,212.5c0.1-1.1,0.2-2.2,0.4-3.3
    c0-0.1,0-0.1-0.1-0.1c0,0,0,0,0,0c-1.8-0.7-3.6-1.5-5.5-2.2l-4.1-1.7c-0.3-0.1-0.6,0-0.8,0.2c-0.1,0.1-0.2,0.2-0.2,0.4
    c-0.3,0.8-0.3,1.6-0.1,2.4c0.1,0.5,0.4,0.8,0.9,0.9l9.4,3.4C711.1,212.5,711.2,212.5,711.2,212.5z M711.8,212.6l12.6-2.2
    c0-1,0-1.9,0.5-2.8c0,0-0.1,0-0.1,0l-4.1,0.5l-5.2,0.7c-1.1,0.1-2.2,0.3-3.2,0.4c-0.1,0-0.1,0-0.1,0.1c0,0.2,0,0.4-0.1,0.6
    c-0.1,0.6-0.1,1.2-0.2,1.8C711.8,212,711.8,212.3,711.8,212.6L711.8,212.6z M713.1,218c0-1.1,0.1-2.2,0.1-3.3l-12.3-3.5
    c0.4,1,0.2,1.9,0.1,2.9L713.1,218z M723.2,218.1C723.2,218.1,723.1,218.1,723.2,218.1l-3.2,0.4c-1.3,0.2-2.5,0.3-3.8,0.5
    c-0.5,0.1-1,0.1-1.5,0.2c-0.3,0.1-0.7,0.2-1.1,0.3c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2-0.1-0.4-0.1c-0.8,0.1-1.7,0.2-2.5,0.3
    c-0.1,0-0.1,0-0.1,0.1c-0.1,0.8-0.2,1.6-0.3,2.4c0,0.3,0,0.6-0.1,0.8l12.6-2.2C722.7,219.9,722.7,218.9,723.2,218.1z"/>
</g></a>
<a href="#commun"><g class="clhumain commun-on">
  <title>commun</title>
  <path class="st11" d="M599.6,188.9v-0.8c0-0.1,0.1-0.2,0.1-0.3c0.4-3,1.9-5.1,4.7-6.3c0.5-0.2,0.7-0.4,0.7-1c0-3.5,2.3-6.5,5.6-7.4
    c0.3-0.1,0.7-0.2,1-0.3h1.9c0,0,0.1,0.1,0.1,0.1c3.6,0.5,6.8,3.8,6.5,8.1c0,0.3,0.1,0.4,0.4,0.6c2.1,1,3.5,2.7,4.1,5
    c0.1,0.5,0.2,1,0.3,1.5v0.8c0,0.1-0.1,0.2-0.1,0.3c-0.3,3.3-2.6,5.9-5.8,6.7c-0.5,0.1-0.9,0.2-1.4,0.3H617c-0.7-0.1-1.4-0.2-2-0.5
    c-0.9-0.4-1.8-0.9-2.8-1.4c-0.9,0.8-2,1.3-3.2,1.6c-0.5,0.1-0.9,0.2-1.4,0.3h-0.8c-0.1,0-0.2-0.1-0.3-0.1c-3.3-0.3-5.9-2.6-6.7-5.8
    C599.7,189.8,599.7,189.4,599.6,188.9z M605.2,182.1c-2.7,0.8-4.5,3.3-4.7,6.2c-0.1,2.8,1.7,5.5,4.3,6.5c2.5,1,5.3,0.2,6.7-1.2
    c0,0,0-0.1,0-0.1c-1.3-1.6-2-3.5-1.8-5.6c0-0.3-0.1-0.4-0.3-0.5c-1-0.4-1.8-1.1-2.5-1.9C606.1,184.5,605.5,183.3,605.2,182.1z
     M620.1,182.4c-0.8,2.6-2.4,4.4-5,5.3c-0.1,0-0.2,0.3-0.2,0.4c0,1-0.1,2-0.5,3c-0.4,0.9-0.9,1.7-1.3,2.6c1.6,1.6,4.6,2,7,1
    c2.5-1.1,4.2-3.7,4-6.4C624,185.6,622.2,183.1,620.1,182.4L620.1,182.4z M612.3,182.7c0.9-0.8,2.1-1.4,3.3-1.7
    c1.2-0.3,2.5-0.2,3.7,0c0.4-2.4-1-5.1-3.3-6.4c-2.4-1.4-5.4-1.2-7.5,0.5c-1.8,1.5-2.6,3.4-2.6,5.7
    C609.5,180.9,609.9,181.1,612.3,182.7L612.3,182.7z M613.1,183.3c0.6,1.2,1.2,2.3,1.8,3.5c2.2-0.8,3.6-2.4,4.3-4.7
    C617.6,181.4,614.7,181.8,613.1,183.3L613.1,183.3z M609.8,186.5c0.6-1.1,1.1-2.1,1.7-3.2c-1.5-1.2-3.3-1.8-5.4-1.4
    C606.6,184.1,607.9,185.6,609.8,186.5L609.8,186.5z M612.3,192.9c1.2-1.1,2-3.7,1.6-4.8l-3.2-0.1
    C610.6,189.8,611.1,191.4,612.3,192.9L612.3,192.9z"/>
</g></a>
<a href="#citoyennete"><g class="clhumain citoyennete-on">
  <path class="st11" d="M567.5,248.5c0,0,0,0.1,0.1,0.1c0.1,0.2,0.1,0.4-0.1,0.6c-1.7,1.9-3.5,3.8-5.2,5.6c0,0,0,0-0.1,0.1
    c-3.4-3.1-6.8-6.3-10.2-9.4c0,0,0.1-0.1,0.1-0.1c1.7-1.9,3.5-3.7,5.2-5.6c0.2-0.2,0.4-0.3,0.7-0.1c0,0,0,0,0,0
    c0.1,0.1,0.1,0.1,0.2,0.2c0,0.1,0.1,0.2,0.1,0.3c0.6,2,1.3,4.1,1.9,6.1c0.1,0.3,0.3,0.4,0.5,0.5c1.7,0.4,3.4,0.8,5.1,1.2
    c0.5,0.1,1,0.3,1.5,0.4C567.4,248.4,567.4,248.4,567.5,248.5z"/>
  <path class="st11" d="M540,252.2c0.1,0,0.2-0.1,0.2-0.1c0.2,0,0.3,0,0.4,0.1c0.2,0.2,0.4,0.4,0.7,0.6c0,0,0.1,0.1,0.1,0.1
    c1.9-2,3.8-4.1,5.7-6.1c0,0,0,0,0,0c0,0.1,0,0.2-0.1,0.3c-0.3,1.5-0.6,2.9-1,4.4c0,0.2,0,0.4,0.2,0.6c0.2,0.2,0.3,0.3,0.5,0.5
    c2.5,2.3,4.9,4.6,7.4,6.9c0,0,0.1,0.1,0.1,0.1c-0.9,1-1.8,2-2.7,2.9c0,0,0.1,0.1,0.1,0.1c2.3,2.2,4.6,4.3,7,6.5
    c0.3,0.3,0.2,0.7-0.1,0.8c-0.2,0.1-0.3,0.1-0.5,0c0,0-0.1,0-0.1,0c-6-5.6-12.1-11.2-18.1-16.8c-0.1-0.2-0.1-0.4-0.1-0.6
    C539.9,252.3,540,252.2,540,252.2z"/>
  <polygon class="st15" points="547.1,246.9 546,251.9 555.6,260.8 558.6,257.6   "/>
  
    <rect x="549.7" y="246.4" transform="matrix(0.6805 -0.7327 0.7327 0.6805 -8.6144 486.975)" class="st16" width="8.7" height="13.9"/>
</g></a>
</svg>


<div>
	<div class="citation col-xs-8">
		<p style="font-size: 2vw;color: white; margin: 5%;" >
			Personne n'éduque autrui, personne ne s'éduque seul, <br>
			les hommes s'éduquent ensemble par l'intermédiaire du monde.<br>
			<span style="font-size: 1.4vw;">Paulo FREIRE (1974)</span>
		</p>
	</div>
		<img style="height: auto;width: 100%;margin-top: 6%;" height="250" src="<?php echo Yii::app()->getModule("costum")->assetsUrl;?>/images/smarterre/education/pied_page.jpg">
		<!--C'est notre petite secret à nous -->
		<input type="hidden" id="urlImage" value="<?php echo Yii::app()->getModule("costum")->assetsUrl;?>">

</div>

<script type="text/javascript">
 file_json = costum.themes.education;

$(document).ready(function(){
  
  reset_polygon();
  reset_file();
  color();
//   file_json = costum.themes.education;
});

// Petit bonhomme


$(".lien").mouseover(function(){
    
  //    blColor(this);

      var a = this.href.animVal;
      var url = a.split("#");

      
      blColor(url[1]);
      
});

$(".lien").mouseleave(function(){
    
    blReset();
});

    //Passage sur humain
    $("#humain").mouseover(function(){
      
      bMenuColor("humain");
  });
  $("#humain").mouseleave(function(){
      
      bResetColor("humain");
  });


  //Passage sur territoire
  $("#territoire").mouseover(function(){
      
      bMenuColor("territoire");
  });
  $("#territoire").mouseleave(function(){
      
      bResetColor("territoire");
  });

  function bMenuColor(c){
      if(c == "humain"){
          $(".clterritoire").each(function(){
              $(this).children().each(function(){
                  $(this).animate({
                      opacity:"0.2"
                  },200);
              });
          });

      // $(".clhumain").each(function(){
      //     $(this).children().each(function(){
      //         $(this).css({
      //             "transform-origin": "489px 200px",
      //             "transform": "scale(1)",
      //             "-ms-transform": "scale(1)",
      //             "-webkit-transform": "scale(1.5)"
      //         });
      //     });
      // });
          // $(".clhumain").css("fill", "<?php echo Yii::app()->session['costum']['css']['color']['turquoise']; ?>");
          // $(".clterritoire").css("fill","grey");
      }
      else if(c == "territoire"){
          $(".clhumain").each(function(){
              $(this).children().each(function(){
                  $(this).animate({
                      opacity:"0.2"
                  },200);
              });
          });

          // $(".clhumain").css("fill","grey");
          // $(".clterritoire").css("fill", "<?php echo Yii::app()->session['costum']['css']['color']['green']; ?>");
      }
  }

  function bResetColor(c){
      if(c == "humain"){
          $(".clterritoire").each(function(){
              $(this).children().each(function(){
                  $(this).animate({
                      opacity:"1"
                  },200);
              });
          });

          // $(".clhumain").each(function(){
          //     $(this).children().each(function(){
          //         $(this).css("transform-origin", "0px 0px");
          // });
      }
      else if(c == "territoire"){
          
          $(".clhumain").each(function(){
              $(this).children().each(function(){
                  $(this).animate({
                      opacity:"1"
                  },200);
              });
          });
      }
  }

  function blColor(url){
      
      $("g").each(function(k,v){
          if(url+"-on" != v.classList[1] && $(v).attr("class") != "st3" && $(v).attr("class")){
            
            $(this).children().each(function(){
              $(this).animate({
                  opacity: "0.2"
              }, 200);
            });
          }
      });
  }

  function blReset(){
          
          $("g").each(function(){
              $(this).children().each(function(){
                  $(this).animate({
                      opacity: "1"
                  }, 200);
              });
          });
      }

      $("#second-search-bar").off().on("keyup",function(e){ 
            $("#input-search-map").val($("#second-search-bar").val());
            $("#second-search-xs-bar").val($("#second-search-bar").val());
            if(e.keyCode == 13){
                searchObject.text=$(this).val();
                searchObject.category = window.location.href.split("#")[1];
                myScopes.type="open";
                myScopes.open={};
               startGlobalSearch(0, indexStepGS);
           		$("#dropdown").css('display','block');
             }	
});

$("#second-search-xs-bar").off().on("keyup",function(e){ 
            $("#input-search-map").val($("#second-search-xs-bar").val());
            $("#second-search-bar").val($("#second-search-xs-bar").val());
            if(e.keyCode == 13){
                searchObject.text=$(this).val();
                searchObject.category = window.location.href.split("#")[1];
                myScopes.type="open";
                myScopes.open={};
                startGlobalSearch(0, indexStepGS);
           		$("#dropdown").css('display','block');            
            }
});

$("#second-search-bar-addon-smarterre, #second-search-xs-bar-addon").off().on("click", function(){
            $("#input-search-map").val($("#second-search-bar").val());
            searchObject.text=$("#second-search-bar").val();
            searchObject.category = window.location.href.split("#")[1];
            myScopes.type="open";
            myScopes.open={};
           	startGlobalSearch(0, indexStepGS);
           	$("#dropdown").css('display','block');
});
</script>