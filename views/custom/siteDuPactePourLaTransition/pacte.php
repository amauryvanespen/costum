
<div class="">


<style type="text/css">
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: #1b7baf;
    border-radius: 20px;
    padding: 20px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    /*min-height:100px;*/
  }
  .btn-main-menu:hover{
    border:2px solid #1b7baf;
    background-color: white;
    color: #1b7baf;
  }
  .ourvalues img{
    height:70px;
  }
  .main-title{
    color: #450e33;
  }

  .ourvalues h3{
    font-size: 36px;
  }
  .box-register label.letter-black{
    margin-bottom:3px;
    font-size: 13px;
  }
  .bullet-point{
      width: 5px;
    height: 5px;
    display: -webkit-inline-box;
    border-radius: 100%;
    background-color: #fbae55;
  }
  .text-explain, .text-partner{
    color: #555;
    font-size: 18px;
  }
  .blue-bg {
  background-color: white;
  color: #5b2549;
  height: 100%;
  padding-bottom: 20px !important;
}

.circle {
  font-weight: bold;
  padding: 15px 20px;
  border-radius: 50%;
  background-color: #fea621;
  color: white;
  max-height: 50px;
  z-index: 2;
}
.circle.active{
      background: #ea4335;
    border: inset 3px #ea4335;
    max-height: 70px;
    height: 70px;
    font-size: 25px;
    width: 70px;
}
.support-section{
  background-color: white;
}
.support-section h2{
  text-align: center;
    padding: 60px 0px !important;
    background: #450e33;
    font-size: 40px;
    color: white;
    margin-bottom: 20px;
}
.timeline-ctc h2{
 text-align: center;
    padding: 105px 0px 60px 0px !important;
    background: #450e33;
    font-size: 40px;
    color: white;
    margin-bottom: 20px;
}
.how-it-works.row {
  display: flex;
}
.row.timeline{
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
}
.how-it-works.row .col-2 {
  display: inline-flex;
  align-self: stretch;
  position: relative;
  align-items: center;
  justify-content: center;
}
.how-it-works.row .col-2::after {
  content: "";
  position: absolute;
  border-left: 3px solid #0091c6;
  z-index: 1;
}
.pb-3, .py-3 {
    padding-bottom: 1rem !important;
}
.pt-2, .py-2 {
    padding-top: 0.5rem !important;
}
.how-it-works.row .col-2.bottom::after {
  height: 50%;
  left: 50%;
  top: 50%;
}
.how-it-works.row.justify-content-end .col-2.full::after {
  height: 100%;
  left: calc(50% - 3px);
}
.how-it-works.row .col-2.full::after {
    height: 100%;
    left: calc(50% - 0px);
}
.how-it-works.row .col-2.top::after {
  height: 50%;
  left: 50%;
  top: 0;
}

.timeline div {
  padding: 0;
  height: 40px;
}
.timeline hr {
  border-top: 3px solid #0091c6;
  margin: 0;
  top: 17px;
  position: relative;
}
.timeline .col-2 {
  display: flex;
  overflow: hidden;
  flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.align-items-center {
    -ms-flex-align: center !important;
    align-items: center !important;
}
.justify-content-end {
    -ms-flex-pack: end !important;
    justify-content: flex-end !important;
}
.row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}
.how-it-works.row .col-6 p{
  color: #444;
  margin:inherit;
}
.how-it-works.row .col-6 h5{
  font-size: 17px;
  text-transform: inherit;
  margin:inherit;
}
.col-2 {
    -ms-flex: 0 0 16.666667%;
    flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.col-6 {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}
.timeline .col-8 {  
    flex: 0 0 66.666667%;
    max-width: 66.666667%;
}
.timeline .corner {
  border: 3px solid #0091c6;
  width: 100%;
  position: relative;
  border-radius: 15px;
}
.timeline .top-right {
  left: 50%;
  top: -50%;
}
.timeline .left-bottom {
  left: -50%;
  top: calc(50% - 3px);
}
.timeline .top-left {
  left: -50%;
  top: -50%;
}
.timeline .right-bottom {
  left: 50%;
  top: calc(50% - 3px);
}
.btn-redirect-home {
        font-size: 22px;
      border-radius: 2px;
      color: white !important;
      padding: 8px 10px;
      background-color: #5b2649 !important;
  }
  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){

  }
</style>
<div id="start" class="section-home section-home-video">
    <div class="col-xs-12 content-video-home no-padding">
      <div class="col-xs-12 no-padding container-video text-center" style="max-height: 450px;">
        <img class="img-responsive start-img" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/image_pacte.jpg' style="margin:auto;">
      </div>
    </div>
</div>
<div class="col-xs-12 section-separtor no-padding">
  <div class="col-xs-4 bg-orange"></div>
  <div class="col-xs-4 bg-blue"></div>
  <div class="col-xs-4 bg-orange"></div>
</div>
<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">-->
  <div class="col-xs-12 no-padding"> 
      <div class="col-xs-12 col-sm-10 col-sm-offset-1  padding-20 text-explain text-center" style="font-size: 22px;">
       L’échelle locale est primordiale pour la transition écologique, sociale et démocratique, et les élections municipales de mars 2020 seront un moment clé pour encourager cette transition partout en France.<br/><br/>
      Nous avons donc conçu le Pacte pour la Transition, 32 mesures concrètes pour construire des communes plus écologiques et plus justes. Notre objectif est d’inspirer et d’accompagner les habitant·es et les candidat·es locaux qui souhaitent œuvrer à transformer nos communes.<br/><br/>
      Les 32 mesures du Pacte ont été rédigées par 60 organisations, une large <a href="https://www.pacte-transition.org/#actualites?preview=poi.5ccaf87340bb4e86326aa995" target="_blank" class="text-orange">consultation citoyenne</a> et un <a href="https://www.pacte-transition.org/#actualites?preview=poi.5cb5b19f40bb4e4636a55804" target="_blank" class="text-orange">comité d’expert·es</a>. Elles s'accompagnent notamment de fiches techniques comprenant des retours d'expérience de communes qui ont déjà mis en œuvre les mesures.
<!--Le Pacte pour la Transition vise à proposer 32 mesures concrètes pour construire des communes plus écologiques et plus justes.<br/><br/>

Ces mesures ont été rédigées par une cinquantaine d'organisations, une large consultation citoyenne et un comité d’expert·es.<br/><br/>

Les citoyen⋅e⋅s pourront ensuite suivre et accompagner les municipalités dans la mise en œuvre des engagements pris, tout au long.<br/><br/>-->
      </div>
      <div class="col-xs-12 text-center">
        <a href="javascript:;" data-hash="#mesures" class="lbh-menu-app btn-redirect-home" style="display: inline-block;padding: 10px 25px;border-radius: 4px;">
            <div class="text-center">
                <div class="col-md-12 no-padding text-center">
                    <h4 class="no-margin">
                      Voir les mesures
                    </h4>
                </div>
            </div>
          </a>
        </div>
    </div>
    <div class="col-xs-12 no-padding"> 
      <div class="col-xs-12 col-sm-10 col-sm-offset-1  padding-20 text-explain text-center" style="font-size: 22px;">
        Ensuite, dans chaque commune, les habitant·es définissent leurs priorités et sensibilisent leurs candidat·es. Les candidat·es qui le souhaitent peuvent rejoindre la dynamique en signant un Pacte pour la Transition.
      </div>
      <div class="col-xs-12 text-center">
        <a href="https://www.pacte-transition.org/#communiquer?preview=poi.5de7cb0d69086484548b4731" target="_blank" class="btn-redirect-home" style="display: inline-block;padding: 10px 25px;border-radius: 4px;">
            <div class="text-center">
                <div class="col-md-12 no-padding text-center">
                    <h4 class="no-margin">
                      Comment signer
                    </h4>
                </div>
            </div>
          </a>
        </div>
    </div>
    <div class="col-xs-12 no-padding"> 
      <div class="col-xs-12 col-sm-10 col-sm-offset-1  padding-20 text-explain text-center" style="font-size: 22px;">
         Les habitant·es engagé·es pourront ensuite suivre et accompagner les municipalités dans la mise en œuvre des engagements pris, tout au long des mandats.
      </div>
    </div>
    <div class="col-xs-12 section-separtor no-padding no-margin">
        <div class="col-xs-4 bg-orange"></div>
        <div class="col-xs-4 bg-blue"></div>
        <div class="col-xs-4 bg-orange"></div>
    </div>
    <div id="search" class="section-home col-xs-12 bg-purple padding-20" style="margin-top:0px;color:white; padding-bottom: 40px;">
      <div class="col-xs-12">
        <h3 class="text-center"><i class="fa fa-search"></i> Que se passe-t-il dans <span class="text-orange">ma commune</span> ?</h3>
        <div class="col-xs-12 text-center content-input-scope-pacte"></div>
      </div>
    </div>
    <div class="col-xs-12 section-separtor no-padding no-margin">
        <div class="col-xs-4 bg-orange"></div>
        <div class="col-xs-4 bg-blue"></div>
        <div class="col-xs-4 bg-orange"></div>
    </div>
    <div class="col-xs-12 no-padding support-section margin-top-20">
        <!--<h2 style="padding: 22px 0px !important;font-size: 30px;color: white;">50+ organisations</h2>-->
        <!--<span class="col-xs-12 col-sm-10 col-sm-offset-1 text-partner" style="font-size: 22px;">Le Pacte est impulsé par le  <span class="text-orange">Collectif pour</span>  <span class="text-purple">une Transition Citoyenne</span> en
partenariat avec une  <span class="text-purple">cinquantaine</span>  <span class="text-orange">d'organisations</span></span>-->
          <!--<div class="col-xs-12 padding-20">
          <a href="mailto:pacte@transitioncitoyenne.org" target="_blank" class="btn-main-menu col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 margin-top-20"  >
            <div class="text-center">
                <div class="col-md-12 no-padding text-center">
                    <h4 class="no-margin uppercase">
                      <i class="fa fa-hand-point-right faa-pulse"></i>
                      <?php echo Yii::t("home","Lire l’appel des partenaires") ?>
                    </h4>
                </div>
            </div>
          </a>
        </div>-->
        <div class="col-xs-12 no-padding">
           <img class="img-responsive hidden-xs" style="margin: auto;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/partenaires_pacte.png'>
          <img class="img-responsive visible-xs" style="margin: auto;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/partenaires_pacte_xs.png'>
          
        </div>
        <div class="col-xs-12 text-center text-explain" style="text-align: center;font-size: 25px !important;"><span>Et des milliers de citoyen⋅nes</span></div>
        <div class="col-xs-12 text-center margin-top-20">
        <a href="https://www.pacte-transition.org/#faq" target="_blank" class="btn-redirect-home" style="display: inline-block;padding: 10px 25px;border-radius: 4px;">
            <div class="text-center">
                <div class="col-md-12 no-padding text-center">
                    <h4 class="no-margin">
                      Encore une question ?
                    </h4>
                </div>
            </div>
          </a>
        </div>
      </div>
    
     <!-- <div class="timeline-ctc col-xs-12 no-padding" style="font-weight: 300;height: 100%;text-align: inherit;">
             
              <div class="container-fluid blue-bg no-padding">
                <div class="container col-xs-12 no-margin no-padding">
                  <h2 class="pb-3 pt-2">
                    <i class="fa fa-calendar"></i> L'agenda du pacte
                  </h2>
                  <div class="row align-items-center how-it-works">
                    <div class="col-2 text-center bottom">
                      <div class="circle">1</div>
                    </div>
                    <div class="col-6">
                      <h5>Octobre à Décembre 2018</h5>
                      <p>Consultation des organisations engagées dans la transition écologique et citoyenne</p>
                    </div>
                  </div>
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner top-right"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner left-bottom"></div>
                    </div>
                  </div>
                  <div class="row align-items-center justify-content-end how-it-works">
                    <div class="col-6 text-right">
                      <h5>Du 25 janvier au 28 février 2019</h5>
                      <p>Consultation citoyenne</p>
                    </div>
                    <div class="col-2 text-center full">
                      <div class="circle">2</div>
                    </div>
                  </div>
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner right-bottom"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner top-left"></div>
                    </div>
                  </div>
                  <div class="row align-items-center how-it-works">
                    <div class="col-2 text-center full">
                      <div class="circle">3</div>
                    </div>
                    <div class="col-6">
                      <h5>Mars 2019</h5>
                      <p>Validation des 32 mesures définitives du Pacte pour la Transition par un comité d'expert.e.s</p>
                    </div>
                  </div>
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner top-right"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner left-bottom"></div>
                    </div>
                  </div>
                  <div class="row align-items-center justify-content-end how-it-works">
                    <div class="col-6 text-right">
                      <h5>Avril 2019 à Mars 2020</h5>
                      <p>Dialogue des collectifs locaux porteurs du Pacte avec les candidats aux élections municipales de 2020</p>
                    </div>
                    <div class="col-2 text-center full">
                      <div class="circle active">4</div>
                    </div>
                  </div>
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner right-bottom"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner top-left"></div>
                    </div>
                  </div>
                  <div class="row align-items-center how-it-works">
                    <div class="col-2 text-center full">
                      <div class="circle">5</div>
                    </div>
                    <div class="col-6">
                      <h5>Mars 2020</h5>
                      <p>Elections municipales en France</p>
                    </div>
                  </div>
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner top-right"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner left-bottom"></div>
                    </div>
                  </div>
                  <div class="row align-items-center justify-content-end how-it-works">
                    <div class="col-6 text-right">
                      <h5>A partir d'avril 2020</h5>
                      <p>Suivi de la mise en place des mesures avec les élus</p>
                    </div>
                    <div class="col-2 text-center top">
                      <div class="circle">6</div>
                    </div>
                  </div>
                 </div>
            </div>

      </div>-->
      
      </div>
  </div>

<script type="text/javascript">
  function lazySearchPacte(time){
    if(typeof pacte != "undefined"){
      pacte.initScopeObj();
    }else
      setTimeout(function(){
        lazyWelcome(time+200)
      }, time);
  }
  jQuery(document).ready(function() {
    setTitle("Pacte pour la Transition");
    lazySearchPacte(0);
  });
</script>


