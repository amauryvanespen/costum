<?php 
$cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
  // SHOWDOWN
  '/plugins/showdown/showdown.min.js',
  // MARKDOWN
  '/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl); 

if(isset(Yii::app()->session["costum"]["contextType"]) && isset(Yii::app()->session["costum"]["contextId"])){
    $el = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );
    $poiList = PHDB::findAndSort(Poi::COLLECTION, 
                    array( 
                      //"parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                      //"parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                      "source.key"=>Yii::app()->session["costum"]["slug"],
                      "type"=>"forum"), array("updated"=>-1) );
} else {?>
  
  <div class="col-xs-12 text-center margin-top-50">
    <h2>Ce COstum est un template <br/>doit etre associé à un élément pour avoir un context.</h2>
  </div>

<?php exit;}?>

<style type="text/css">
 .addForumBtn {
    background-color: #18a47c;
    border: 0px;
    padding: 5px 20px;
    width: 50%;
    margin-left: 25%;
    border-radius: 2px;
    margin-bottom: 25px;
  }
</style>
<div class="col-xs-12 margin-top-50" style="padding:0px;">
    
    <div class="col-xs-12 text-center margin-bottom-20" style="padding:0px;">
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <bloquote style="font: 10 25px/1 'Pacifico', Helvetica, sans-serif;
      font-size: 30px;color: #18a47c;
      text-shadow: 2px 2px 0px rgba(0,0,0,0.1);">
        <span style="color: #221d4e;">FORUM</span> du Contrat de Transition Écologique</bloquote>
    </div>
      <?php if(Authorisation::isInterfaceAdmin()){ ?>
      <div class="col-xs-12"><button class="btn-primary addForumBtn" onclick="dyFObj.openForm('forum');">Ajouter un forum</button></div>
      <?php } ?>
  <br/>
              
    <div class="col-sm-10 col-sm-offset-1 col-xs-12">
      <?php 
      foreach($poiList as $k => $v){  
        $imgPath=(isset($v["profilMediumImageUrl"]) && !empty($v["profilMediumImageUrl"])) ? Yii::app()->createUrl($v["profilMediumImageUrl"]) : Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/PictoCTE.png"; 
        $desc=(isset($v['shortDescription'])) ? $v['shortDescription'] : "";
        if(empty($desc) && isset($v["description"])){
          $desc=/*(strlen($v["description"]) > 200) ? substr($v["description"], 0, 200)."..." :*/ $v["description"];
        }

        ?>
        <div id="entity<?php echo $k ?>" class="searchEntityContainer searchEntity shadow2 col-xs-12" style="margin-bottom: 15px !important;padding-top: 5px !important;padding-bottom: 5px !important;">
            <a href="#page.type.poi.id.<?php echo $k ?>" class="lbh-preview-element col-xs-12 no-padding">
              <div class="container-img col-xs-3 col-md-2"><img src="<?php echo $imgPath ?>" class="img-responsive"/></div>
              <div class="container-info col-xs-9 col-xs-10">
                <h3 style="font-size: 22px;text-transform: initial;color:#18a47c;"><?php echo $v["name"] ?></h3>
                <span style="margin-bottom:15px !important;font-weight: 100 !important;width: 100%;float: left;"><?php echo $desc; ?></span>
              </div>
            </a>
        </div>
      <?php } 
      //var_dump($poiList);exit;
      /*echo $this->renderPartial( "costum.views.tpls.multiElementBlock", array(
          "poiList"   => $poiList,
          "blockName" => "el",
          "titlecolor"=> "#e6344d",
          "blockCt"   => count($poiList),
          "typeApp"=>"forum"
        ), true );*/
       ?>
    </div>

</div>  
        


<script type="text/javascript">
  jQuery(document).ready(function() {
    setTitle("Forum CTE");
    /*contextData = {
        id : "<?php echo Yii::app()->session["costum"]["contextId"] ?>",
        type : "<?php echo Yii::app()->session["costum"]["contextType"] ?>",
        name : "<?php echo $el['name'] ?>",
        slug : "<?php echo $el['slug'] ?>",
        profilThumbImageUrl : "<?php echo @$el['profilThumbImageUrl'] ?>"
    };*/
    //if(contextData.id == "")
      //alert("Veuillez connecter ce costum à un élément!!");
    
    $.each( $(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    } ); 
    
});

</script>

