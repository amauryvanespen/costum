<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<script type="text/javascript">

	var COLORS = [
	"#0A2F62",
	"#0B2D66",
	"#064191",
	"#2C97D0",
	"#16A9B1",
	"#0AA178",
	"#74B976",
	"#0AA178",
	"#16A9B1",
	"#2A99D1",
	"#064191",
	"#0B2E68"
	];
</script>


<div id="container" style="margin:20px auto;width:100%">
	<canvas id="canvas-bar"></canvas>
</div>
<?php 

$badgeCounts = [];
$actionIds= [];
$badges = [];
$ssBadgeFamily = [];
$answers = PHDB::find( "answers", array("source.key"=>"ctenat","priorisation" => ['$in'=> Ctenat::$validActionStates ] ));
//var_dump(Yii::app()->session["costum"]["lists"]["domainAction"]);exit;
foreach ( Yii::app()->session["costum"]["lists"]["domainAction"] as $pb => $cbs) 
{
	$actionIds[$pb] = [];
	$badges[] = $pb;
	foreach ($cbs as $key => $bName) {
		$ssBadgeFamily[$bName] = $pb;
	}
}

foreach ( $answers as $id => $ans ) {
	$formId = $ans["formId"];
	if(    isset( $ans["answers"][$formId]["answers"]["project"]["id"] ) 
		&& isset( $ans["answers"][$formId]["answers"]["caracter"]["actionPrincipal"] ) )
	{
		$daPrinci = $ans["answers"][ $formId ]["answers"]["caracter"]["actionPrincipal"];
		if( isset( $ssBadgeFamily[$daPrinci] ))
		{
			$projectId = $ans["answers"][$formId]["answers"]["project"]["id"];
			$pBadge = $ssBadgeFamily[$daPrinci];
			if( isset($actionIds[$pBadge]) && !in_array($projectId, $actionIds[$pBadge])) 
				$actionIds[$pBadge][] = $projectId;
		}
	}
}

foreach (Yii::app()->session["costum"]["lists"]["domainAction"] as $pb => $cbs) {
	if( isset($actionIds[$pb]))
		$badgeCounts[] = count($actionIds[$pb]);
}
// var_dump($badgeCounts);
// var_dump($badges);
// exit;
 ?>
<script>
	var randomScalingFactor = function() {
		return Math.round(Math.random() * 100);
	};
	jQuery(document).ready(function() {
		mylog.log("render","/dev/modules/costum/views/custom/ctenat/graph/barPorteurbyDomaine.php",<?php echo json_encode($badges); ?>,<?php echo json_encode($badgeCounts); ?>);
		var barChartData = {
			labels: <?php echo json_encode($badges); ?> ,
			datasets: [{
				backgroundColor: COLORS,
				borderWidth: 1,
				data: <?php echo json_encode($badgeCounts); ?>
			}]

		};

			var ctxContainer = document.getElementById('canvas-bar');
			var ctx = ctxContainer.getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					responsive: true,
					legend : {display:false}
				}
			});

			ctxContainer.onclick = function(evt) {
		      var activePoints = myBar.getElementsAtEvent(evt);
		      if (activePoints[0]) {
		        var chartData = activePoints[0]['_chart'].config.data;
		        var idx = activePoints[0]['_index'];

		        var label = chartData.labels[idx];
		        var value = chartData.datasets[0].data[idx];

		        var url = "label=" + label + "&value=" + value;
		        //alert("todo sub graphs"+url);
		        smallMenu.openAjaxHTML( baseUrl+'/costum/ctenat/dashboard/tag/'+label);
		      }
		    };

});
	</script>
