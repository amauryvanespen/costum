<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>



<div id="container"  style="margin:0px auto;width:<?php echo ($size == "S") ? "40" : "100";?>%;">
	<canvas id="<?php echo $id?>-canvas-bar"></canvas>
</div>

<script>
	var randomScalingFactor = function() {
		return Math.round(Math.random() * 100);
	};
	jQuery(document).ready(function() {
		if(typeof <?php echo $id?>Data == "undefined")
			alert("<?php echo $id?>Data is undefined");
		if(!id)
			alert("<?php echo $id?> id cannot be null");


		var <?php echo $id?>barChartData = {
			labels: <?php echo $id?>Data.labels ,
			datasets: [{
				backgroundColor: <?php echo json_encode( Ctenat::$COLORS )  ?>,
				borderWidth: 1,
				data: <?php echo $id?>Data.data 
			}]

		};
		mylog.log("render","/modules/costum/views/custom/ctenat/graph/barMany.php", "<?php echo $id?>barChartData",<?php echo $id?>barChartData);

		var ctxContainer = document.getElementById('<?php echo $id?>-canvas-bar');
		var ctx = ctxContainer.getContext('2d');
		window.myBar<?php echo $id?> = new Chart(ctx, {
			type: 'bar',
			data: <?php echo $id?>barChartData,
			options: {
				responsive: true,
				legend : {display:false}
			}
		});

		ctxContainer.onclick = function(evt) {
	      var activePoints = myBar<?php echo $id?>.getElementsAtEvent(evt);
	      if (activePoints[0]) {
	        var chartData = activePoints[0]['_chart'].config.data;
	        var idx = activePoints[0]['_index'];

	        var label = chartData.labels[idx];
	        var value = chartData.datasets[0].data[idx];

	        var url = "label=" + label + "&value=" + value;
	        
	        
	        var classSel = slugify( label ).toLowerCase();
	        $(".dashElem").removeClass("hide");
	        $(".dashElem:not(."+classSel+")").addClass("hide");
	        $("#dashElemTitle").html("Projets "+label +" ("+$("."+classSel).length+")");
	        //alert(label+"||"+slugify( label ).toLowerCase() +"||"+classSel);
	      }
	    };

});
	</script>
