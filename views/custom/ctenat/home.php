<?php 
$cssJS = array(
      '/plugins/jQuery-Knob/js/jquery.knob.js',
      '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
      //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    // MARKDOWN
    '/plugins/to-markdown/to-markdown.js',
    "/plugins/jquery-counterUp/waypoints.min.js",
    "/plugins/jquery-counterUp/jquery.counterup.min.js",
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl); 

//echo "<script type='text/javascript'> alert(' home ".@Yii::app()->session["costum"]["slug"]." : ".@Yii::app()->session["costum"]["contextType"]." : ".@Yii::app()->session["costum"]["contextId"]."'); </script>";


if(Yii::app()->session["costum"]["contextType"] && Yii::app()->session["costum"]["contextId"]){
    $el = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );

    $poiList = PHDB::find(Poi::COLLECTION, 
                    array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                           "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                           "type"=>"cms") );
}

?>
<div class="">



<?php 
/*
if(false ){//!@$el["costum"]){
  $tagBarList = array(
    array("icon"=>"fa-user","color"=>"#fff"),
    array("icon"=>"fa-group","color"=>"#fff"),
    array("icon"=>"fa-calendar","color"=>"#fff"),
    array("img"=>Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/badge.png"));
    echo $this->renderPartial("costum.views.tpls.tagBar",
                        array(  "list" => $tagBarList,
                                "borderColor" => "white",
                                "bgColor" => "#22252A",
                                 ),true ); 
}
?>  

<div class="col-xs-12 no-padding support-section text-center">
  <br/><br/>
  <h2 id="carteCTE"><i class="fa fa-group"></i> acteurs publics</h2>
  <div class="col-xs-12 padding-20 text-explain" >
    <?php 
      $params = array(
          "poiList" => $poiList,
          "listSteps" => array("1","2","3","4","5","6"),
          "color1" => "#354fff"
      );
      //var_dump($params);
      echo $this->renderPartial("costum.views.tpls.wizard",$params);
      ?>

  </div>
</div>

<div class="col-xs-12 no-padding support-section text-center">
  <br/><br/>
  <h2 id="carteCTE"><i class="fa fa-money"></i> acteurs sociaux économique</h2>
  <div class="col-xs-12 padding-20 text-explain" >
    <?php 
      $params = array(
          "poiList" => $poiList,
          "listSteps" => array("one","two","three","four","five","six")
      );
      //var_dump($params);
      echo $this->renderPartial("costum.views.tpls.wizard",$params,true);
      ?>
    
  </div>
</div>
*/?>

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  
    <div class="col-xs-12 text-center margin-bottom-50" style="padding:0px;">
    <?php 
    $banner = Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/banner.jpg";
    if(@Yii::app()->session["costum"]["metaImg"]){
      if(strrpos(Yii::app()->session["costum"]["metaImg"], "http" ) === false && strrpos( Yii::app()->session["costum"]["metaImg"], "/upload/" ) === false )
        $banner = Yii::app()->getModule("costum")->getAssetsUrl().Yii::app()->session["costum"]["metaImg"] ;
      else 
        $banner = Yii::app()->session["costum"]["metaImg"];
    }
    ?>
    <img class="img-responsive"  style="margin:auto;background-color: black;" src='<?php echo $banner ?>'/> 
  <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">-->
  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#24284D; max-width:100%; float:left;">
    <div class="col-xs-12 no-padding" style="margin-top:100px;"> 
      <div class="col-xs-12 no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top:-80px;margin-bottom:-80px;background-color: #fff;font-size: 14px;z-index: 5;">
            <div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
              <h3 class="col-xs-12 text-center">
                
                <small style="text-align: left">
                <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
                  <bloquote style="font: 10 25px/1 'Pacifico', Helvetica, sans-serif;
  color: #2b2b2b;
  text-shadow: 4px 4px 0px rgba(0,0,0,0.1);">


<!--
<span class="main-title" style="font-family:'Pacifico', Helvetica, sans-serif;color:#65BA91;font-size: 50px; font-style: italic;">#iciOnAccélère</span><br>
<br/>
-->

  « Le contrat de transition écologique illustre la méthode souhaitée par le
Gouvernement pour accompagner les territoires : une coconstruction avec les
élus, les entreprises et les citoyens qui font le pari d’une transition écologique
génératrice d’activités économiques et d’opportunités sociales. »</bloquote>

              </h3>
              <div  style="text-align: right; font-size: 1.4em;padding-right:30px;">
                <b>Emmanuelle Wargon</b>,<br/>
                secrétaire d’Etat auprès de la ministre de la Transition écologique et solidaire
                  </small>
              </div>
              <br/>


              
              <hr style="width:40%; margin:20px auto; border: 1px dashed #6CC3AC;">


              
<iframe width="560" height="315" src="https://www.dailymotion.com/embed/video/x720ifv" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                




                <hr style="width:40%; margin:20px auto; border: 1px dashed #6CC3AC;">
              





              <div class="col-md-10 col-md-offset-1 col-xs-12">
                
                <span class="text-explain">
                  <br/>
                  <h2 style="color:#24284D">UN CONTRAT ADAPTE AU TERRITOIRE</h2>


                  Lancés en 2018, les <b>contrats de transition écologique</b>
                  (CTE) traduisent les engagements environnementaux
                  pris par la France (Plan climat, COP21, One Planet
                  Summit) au niveau local. Ce sont des outils au
                  service de la transformation écologique de territoires
                  volontaires, autour de projets durables et concrets.

                  <br/><span class="bullet-point"></span><br/>

                  Mis en place par une ou plusieurs intercommunalités,
                  le CTE est coconstruit à partir de projets locaux, entre
                  les collectivités locales, l’État, les entreprises, les
                  associations... Les territoires sont accompagnés aux
                  niveaux technique, financier et administratif, par les
                  services de l’État, les établissements publics et les
                  collectivités. Signé après six mois de travail, le CTE fixe
                  un programme d’actions avec des engagements précis
                  et des objectifs de résultats.

                <hr style="width:40%; margin:20px auto; border: 1px dashed #6CC3AC;">
                <br/>
                <h2 style="color:#24284D">TROIS OBJECTIFS</h2>
                <br/>
                <b>1) Démontrer  par  l’action  que  l’écologie  est  un  moteur  de  l’économie,  et  développer  l’emploi  local  par  la  transition  écologique</b>  (structuration  de filières, création de formations).

                <br/><span class="bullet-point"></span><br/>

                <b>2) Agir avec tous les acteurs du territoire, publics comme privés</b> pour traduire concrètement la transition écologique.

                <br/><span class="bullet-point"></span><br/>

                <b>3) Accompagner de manière opérationnelle les
                situations de reconversion industrielle d’un ter-
                ritoire</b> (formation professionnelle, reconversion de
                sites).



                </span>

                
            

              </div>
            </div>
            <div class="text-center col-xs-12">
            <br/><br/>
            <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/city.png"></div>
        </div>
          </div>

        </div>

      </div>
      

      <style type="text/css">
  .counterBlock{border: 2px solid white; background-color: white; color:#24284D; border-radius: 20px;height:250px;}
</style>


      <div class="col-xs-12 no-padding support-section text-center">
        <br/><br/><br/><br/>
        <h1 id="whereIsMyMind" class="text-white" style="margin-bottom:40px;"><i class="fa fa-2x fa-map-marker"></i> <br/>Où en est-on ?</h1>
      </div>

          
          <div class="col-xs-12 text-explain  bg-white" style="padding-bottom: 20px;" >

              <div class="col-md-4  text-center text-white border- padding-10"  >
                <div class="counterBlock"  >
                    <h1><span class="counter"><?php echo PHDB::count(Project::COLLECTION, array(
                                                          "category" => Ctenat::CATEGORY_CTER,
                                                          "source.status.ctenat"=>Ctenat::STATUT_CTER_LAUREAT)) ?></span></h1>
                    <h3>CTE Lauréats</h3>
                    <div class="col-md-4 col-sm-offset-4">
                    <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/PictoCTE.jpg" ?>">
                    </div>
                  </div>
              </div> 

              <div class="col-md-4  text-center  padding-10"  >
                <div class="counterBlock" style="border-left:1px dashed #6CC3AC;border-right:1px dashed #6CC3AC;" >
                  <h1><span class="counter"><?php $cters = PHDB::find(Project::COLLECTION, [
                                                          "category" => Ctenat::CATEGORY_CTER,
                                                          "source.status.ctenat"=>Ctenat::STATUT_CTER_SIGNE ],["nbHabitant","name", "slug", "scope"]);
                                                      echo count($cters) ?></span></h1>
                  <h3>CTE signés</h3>
                  <div class="col-md-4 col-sm-offset-4">
                    <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/PictoCTE.jpg" ?>">
                  </div>
                </div>
              </div>
              
              <?php 
              $answers = PHDB::find( Form::ANSWER_COLLECTION, array("source.key"=>"ctenat",
                      "priorisation" => ['$in'=> Ctenat::$validActionStates ]) );
              $ssBadgeFamily = [];
              foreach (Yii::app()->session["costum"]["lists"]["domainAction"] as $pb => $bChild) {
                foreach ($bChild as $key => $bName) {
                  $ssBadgeFamily[$bName] = $pb;
                }
              }
              $financeTotal = 0;
              $actionCount = 0;
              foreach ( $answers as $id => $ans ) {

                $daPrinci = @$ans["answers"][ $ans["formId"] ]["answers"]["caracter"]["actionPrincipal"];
                if( isset( $ssBadgeFamily[$daPrinci] )
                  && isset($ans["answers"][$ans["formId"]]["answers"]["project"]["id"]) ) 
                  $actionCount++;

                //indicator counts
                $financeTotalSlug = Ctenat::chiffreFinancementByType([$id=>$ans],$ans["formId"]);
                $financeTotal += $financeTotalSlug['total'];
              }
              ?>

              <div class="col-md-4  text-center  padding-10"  >
                <h1><span class="counter"><?php echo $financeTotal ?></span></h1>
                <h3> millions €</h3>
                <div class="counterBlock" id="millionPie" >
                
                </div>
              </div>


            </div>

            <div class="col-xs-12 " style="background-color:#24284D;height:40px;">
            </div>

          <div class="col-xs-12 text-explain bg-white" style="margin-bottom:40px; padding-bottom: 20px;" >
              <div class="col-md-4  text-center  padding-10"  >
                <div class="counterBlock"  >
                    <?php 
                        $idScope =  array();
                        $allcter = PHDB::find(Project::COLLECTION, [ 
                                                "category" => Ctenat::CATEGORY_CTER,
                                                "source.status.ctenat"=> ['$in'=>Ctenat::$validCter ] ],
                                              ["nbHabitant","name", "slug", "scope"]);
                        foreach ($allcter as $keyCTER => $cter) {
                        if(!empty($cter["scope"])){
                          foreach ($cter["scope"] as $key => $val) {
                            if(!empty($val["id"]) && !in_array($val["id"], $idScope))
                              $idScope[] = new MongoId($val["id"]) ;;
                          }
                        }
                      }
                        
                        $epci = 178;//PHDB::count(Zone::COLLECTION, array(
                                            // "_id" => array('$in' => $idScope), 
                                            // "epci" => array('$exists' => 1)), array("name"));
                       ?>
                    <h1><span class="counter"><?php echo $epci; ?></span></h1>
                    <h3>EPCI</h3>
                    <div class="col-md-6 col-sm-offset-3">
                    <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/PictoEPCI.jpg" ?>">
                    </div>
                  </div>
              </div> 

              <div class="col-md-4  text-center  padding-10"  >
                <div class="counterBlock" style="border-left:1px dashed #6CC3AC;border-right:1px dashed #6CC3AC;" >
                <h1><span class="counter"><?php 
                    $population = 0;
                    foreach ($allcter as $id => $cter) {
                      if(isset($cter["nbHabitant"]))
                        $population += intval($cter["nbHabitant"]);
                    }
                    echo number_format($population, 0, ',', '');
                 ?></span></h1>
                <h3> Francais concernés</h3>
                <div class="col-md-6 col-sm-offset-3">
                  <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/Francais-Acteurs.jpg" ?>">
                </div>
                </div>
              </div>

              <div class="col-md-4  text-center  padding-10"  >
                <div class="counterBlock"  >
                <h1><span class="counter"><?php echo $actionCount ?></span></h1>
                <h3>Actions</h3>
                <div class="col-md-6 col-sm-offset-3">
                <i class="fa fa-lightbulb-o" style="font-size:18em;color:#134292"></i>
                </div></div>
              </div>
<?php /* ?>
              <div class="col-md-3  text-center   padding-10"  >
                <div class="counterBlock" style="border-left:1px dashed #6CC3AC;" >
                <h1><span class="counter">12,218</span></h1>
                <h3>Contributeurs</h3>
                <div class="col-md-6 col-sm-offset-3">
                <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/Contributeurs.jpg" ?>">
                </div></div>
              </div>
*/?>
          </div>

          <div class="col-xs-12 " style="background-color:#24284D;height:40px;">
          </div>

          <div class="col-xs-12 text-explain bg-white" style="padding-top:40px;margin-bottom:40px; padding-bottom: 20px;" >
            <div class="col-xs-12 no-padding">
              <div class="col-xs-offset-3 col-xs-6 col-sm-offset-0 col-sm-4 col-md-2 col-md-offset-2">
                <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/images/ctenat/light-bulb.png"/>
              </div>
              <span class="col-xs-12 col-sm-8 col-md-6 text-left">La plateforme des CTE se <b style="color:#24284D;">co-construit</b> en mode agile <b style="color:#24284D;">avec les territoires CTE</b> pionniers et se déploie progressivement de concert avec l’ensemble de la démarche CTE. La plateforme va continuer à évoluer et s’améliorer tout l’été !<br/><br/>
              Pour découvrir pas-à-pas le portail et toutes ses fonctionnalités : <a href="<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/files/ctenat/Tutoriel_Premier_pas_PortailCTENat.pdf" target="_blank" class="text-blue">lien tuto</a><br/><br/>
              N'hésitez pas à nous faire part de vos remarques sur le portail via le forum dédié à cet effet !</span>
            </div>
            <div class="col-xs-12">
              <style type="text/css">
                .timeline{
  list-style-type: none;
  align-items: center;
  justify-content: center;
}
.timeline .li{
  transition: all 200ms ease-in;
}

.timeline .li .timestamp{
  margin-bottom: 20px;
  padding: 0px 40px;
  flex-direction: column;
  align-items: center;
  font-weight: 100;
}
.timeline .li .status{
  padding: 0px 40px;
  justify-content: center;
  border-top: 2px solid #70bd79;
  position: relative;
  transition: all 200ms ease-in;
  }  
  .timeline .li .status h4{
    font-weight: 600;
    font-size: 16px;
    text-transform: initial;
  }
  .timeline .li .status h4:before{
    content: '';
    width: 25px;
    height: 25px;
    background-color: white;
    border-radius: 25px;
    border: 1px solid #ddd;
    position: absolute;
    top: -25px;
    left: 42%;
    transition: all 200ms ease-in;
  } 
  .li.complete .author{
    font-size: 14px;
  }
  .li.complete .status{
    border-top: 2px solid #66DC71;
  }
    .li.complete .status:before{
      background-color: #66DC71;
      border: none;
      transition: all 200ms ease-in ;
    }
    h4{
      color: #66DC71;
    }

@media (min-device-width: 320px) and (max-device-width: 700px){
  .timeline{
    list-style-type: none;
    display: block;
  }
  .li{
    transition: all 200ms ease-in;
    display: flex;
    width: inherit;
  }
  .timestamp{
    width: 100px;
  }
  .status:before{
      left: -8%;
      top: 30%;
      transition: all 200ms ease-in;
  } 

  button{
    position: absolute;
    width: 100px;
    min-width: 100px;
    padding: 20px;
    margin: 20px;
    border: none;
    color: white;
    font-size: 16px;
    text-align: center;
  }
  #toggleButton{
    position: absolute
    left: 50px
    top: 20px
    background-color: #75C7F6
  }
}
</style>
              <ul class="timeline col-xs-12 no-padding margin-top-20 col-md-10 col-md-offset-1" id="timeline">
                <li class="li complete col-xs-4 no-padding">
                  <div class="timestamp">
                    <span class="author">Mars 2019</span>
                  </div>
                  <div class="status">
                    <h4 class="col-xs-12 no-padding"> Déclarer un territoire candidat </h4>
                     <div class="col-xs-12">
                     <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/mars2019.png" ?>">
                     </div>
                  </div>
                </li>
                <li class="li complete col-xs-4 no-padding">
                  <div class="timestamp">
                    <span class="author">9 Juillet 2019</span>
                  </div>
                  <div class="status">
                    <h4 class="col-xs-12 no-padding"> Naviguer dans les territoires, les projets et les chiffres du CTE  </h4>
                    <div class="col-xs-12">
                     <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/Naviguer.png" ?>">
                     </div>
                  </div>
                </li>
                <li class="li complete col-xs-4 no-padding">
                  <div class="timestamp">
                    <span class="author">15 Septembre 2019</span>
                  </div>
                  <div class="status">
                    <h4 class="col-xs-12 no-padding"> Co-construire, co-financer et valoriser ses propres actions </h4>
                    <div class="col-xs-12">
                     
                     <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/coconstruire.png" ?>">
                     </div>
                  </div>
                </li>
               
               </ul>
            </div>
          </div>


     
      </div>

<div class="col-xs-12 " style="background-color:#24284D; padding-bottom:20px">
  <span class="text-white" style="font-size: 34px; font-weight: bold;"><i class="fa fa-book"></i> <br/>TUTORIELS</span>
</div>

 <div class="col-xs-12 no-padding support-section margin-top-20">
        <style type="text/css">
          .docTitle{color: #18a47d}
          .docBlock{height:300px;}
        </style>
        <div class="col-xs-12 no-padding">
          
           
                  <div class=" docBlock col-xs-4">
                    <h4 class="docTitle col-xs-12 no-padding"> Pour tous - 1 Premiers pas </h4>
                     <div class="col-xs-12">
                     <a href="https://docs.google.com/presentation/d/e/2PACX-1vSes_XSrJiuBU8RhE42RdbeDMzKbYTWLxyLV5j483DmEkfr5QwgKX0p0nW6QeBHdg/pub?start=false&loop=false&delayms=3000" target='_blank'><img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/mars2019.png" ?>"></a>
                     </div>
                  </div>
                
                  <div class=" docBlock col-xs-4">
                    <h4 class="docTitle col-xs-12 no-padding"> Pour tous - 2 fonctionnalités communes </h4>
                     <div class="col-xs-12">
                     <a href="https://docs.google.com/presentation/d/e/2PACX-1vSAbVg5lGS_tCh_hQdwe0sKoT9jVP4ysoT_WTFLFCYxb-vWlGu8y1teJ8RGGkMREW0QQxlcf8wgTnIJ/pub?start=false&loop=false&delayms=3000" target='_blank'><img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/mars2019.png" ?>"></a>
                     </div>
                  </div>

                  <div class=" docBlock col-xs-4">
                    <h4 class="docTitle col-xs-12 no-padding"> pour tous - 3 Les rôles dans le CTE </h4>
                     <div class="col-xs-12">
                     <a href="https://docs.google.com/presentation/d/e/2PACX-1vSSIRVht49NdQwHAfd2Bj2uXG7z5MQDKZ1YBbOS32LK9KxNBMx4vBwyvKm5R0szWw/pub?start=false&loop=false&delayms=3000" target='_blank'><img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/mars2019.png" ?>"></a>
                     </div>
                  </div>

                  <div class=" docBlock col-xs-4">
                    <h4 class="docTitle col-xs-12 no-padding"> pour tous - 4 espaces territoire, action et organisation </h4>
                     <div class="col-xs-12">
                     <a href="https://docs.google.com/presentation/d/e/2PACX-1vRNX1XouRGF1oAsf6R_EK5J5jjt4csev2rrj6heo0j2jy309JqOe55Qu3xkc-WxPM9rybNPkKjeeee_/pub?start=false&loop=false&delayms=3000" target='_blank'><img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/mars2019.png" ?>"></a>
                     </div>
                  </div>

                  <div class=" docBlock col-xs-4">
                    <h4 class="docTitle col-xs-12 no-padding"> référent/porteur - 1 gestion de la communauté </h4>
                     <div class="col-xs-12">
                     <a href="https://drive.google.com/file/d/1olFDu4yEFSd7OR5C6SAx9M1Hu5flp8Iw/view" target='_blank'><img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/mars2019.png" ?>"></a>
                     </div>
                  </div>

                  <div class=" docBlock col-xs-4">
                    <h4 class="docTitle col-xs-12 no-padding"> référent/porteur - 2 fiche action </h4>
                     <div class="col-xs-12">
                     <a href="https://docs.google.com/presentation/d/e/2PACX-1vRqQZzSfcfSEz8jXK1ZuL9JWwGLrsTrG1ShcBgH6AgIZ-lmSeIteh3AM4wSwgYkvsPQuhZc_HIb759C/pub?start=false&loop=false&delayms=3000" target='_blank'><img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/mars2019.png" ?>"></a>
                     </div>
                  </div>

                  <div class=" docBlock col-xs-4">
                    <h4 class="docTitle col-xs-12 no-padding"> Tutoriel Portail CTENat Organismes publics et CTE </h4>
                     <div class="col-xs-12">
                     <a href="https://docs.google.com/presentation/d/e/2PACX-1vQDN4Crbm6TskF53nlw9TT50XNZNC9EVrJQmkQcp_O95KuuWz1tcusGx4bWp0V0wg/pub?start=false&loop=false&delayms=3000" target='_blank'><img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/mars2019.png" ?>"></a>
                     </div>
                  </div>


        </div>

      </div>

<!-- <div class="col-xs-12 " style="background-color:#24284D; padding-bottom:20px">
  <span class="text-white" style="font-size: 34px; font-weight: bold;"><i class="fa fa-book"></i> <br/>CARACTÉRISATION</span>
</div>

 <div class="col-xs-12 no-padding support-section margin-top-20">
        <style type="text/css">
          .docTitle{color: #18a47d}
          .docBlock{height:300px;}
        </style>
        <div class="col-xs-12 no-padding">
          
           
                  


        </div>

      </div> -->

<div class="col-xs-12 " style="background-color:#24284D;height:40px;">
          </div>

      <div class="col-xs-12 no-padding support-section">
        
        <div class="col-xs-12 no-padding">
          
          
          <h3 style="color:#24284D">Toutes les informations et la carte de France des CTE <a href="http://www.ecologique-solidaire.gouv.fr/contrat-transition-ecologique" target="_blank">ici</a> </h3>
          
          <img width=200 src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/planClimat.png">

          <img width=200 src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/1200px-Ministère_de_la_Transition_Écologique_et_Solidaire_(depuis_2017).svg.png">

          <br/><br/>
        <img style="margin:auto" class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/city.png">

        </div>

      </div>
    </div>
  </div>
</div>


    </form>
</div>


<?php 
/*
- search filter by src key 
- admin candidat privé only
- app carte 
*/
 ?>

<script type="text/javascript">

  jQuery(document).ready(function() {
    //alert("homectenat");
    
    mylog.log("render","/modules/costum/views/custom/ctenat/home.php");
    
    setTitle("CTE : Contrat de Transition écologique");

    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });
    contextData = {
        id : "<?php echo Yii::app()->session["costum"]["contextId"] ?>",
        type : "<?php echo Yii::app()->session["costum"]["contextType"] ?>",
        name : "<?php echo $el['name'] ?>",
        slug : "<?php echo $el['slug'] ?>",
        profilThumbImageUrl : "<?php echo $el['profilThumbImageUrl'] ?>"
    };
    $('.counter').counterUp({
      delay: 10,
      time: 2000
    });
    $('.counter').addClass('animated fadeInDownBig');
    $('h3').addClass('animated fadeIn');

    ajaxPost('#millionPie', baseUrl+'/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMillion/size/S', 
          null,
          function(){  },"html");
});

</script>

