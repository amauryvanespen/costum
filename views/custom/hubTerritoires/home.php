<?php  
$cssAnsScriptFilesTheme = array(
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js'            
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

     $cssAndScriptFilesModule = array(
        '/js/default/profilSocial.js',
    );
HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());

$colorSectionAgenda = @Yii::app()->session["costum"]["cms"]["colorSectionAgenda"];
$colorSectionActu = @Yii::app()->session["costum"]["cms"]["colorSectionActu"];
$colorCardEvent = @Yii::app()->session["costum"]["cms"]["colorCardEvent"];
$colorPathPlus = @Yii::app()->session["costum"]["cms"]["colorPathPlus"];
$colorTitleAgenda = @Yii::app()->session["costum"]["cms"]["colorTitleAgenda"];
$colorTitleActu = @Yii::app()->session["costum"]["cms"]["colorTitleActu"];
$colorSearchicon = @Yii::app()->session["costum"]["cms"]["colorSearchicon"];
$bannerImg = @Yii::app()->session["costum"]["banner"] ? Yii::app()->baseUrl.Yii::app()->session["costum"]["banner"] : Yii::app()->getModule("costum")->assetsUrl."/images/hubterritoires/header.jpg";
?>

<style type="text/css">
    .hexagon {
    overflow: hidden;
    visibility: hidden;
    -webkit-transform: rotate(120deg);
       -moz-transform: rotate(120deg);
        -ms-transform: rotate(120deg);
         -o-transform: rotate(120deg);
            transform: rotate(120deg);
    cursor: pointer;
}

.hexagon1 {
    width: 485px;
    height: 250px;
    margin: 0 0 0 -80px;
}

.hexagon-in1 {
    overflow: hidden;
    width: 100%;
    height: 100%;
    -webkit-transform: rotate(-60deg);
       -moz-transform: rotate(-60deg);
        -ms-transform: rotate(-60deg);
         -o-transform: rotate(-60deg);
            transform: rotate(-60deg);
}

.hexagon-in2 {
    width: 100%;
    height: 100%;
    background-repeat: no-repeat;
    background-position: 50%;
    visibility: visible;
    -webkit-transform: rotate(-60deg);
       -moz-transform: rotate(-60deg);
        -ms-transform: rotate(-60deg);
         -o-transform: rotate(-60deg);
            transform: rotate(-60deg);
}
    .img-hexa{
        margin-top: -25%;
    }

    .info-card{
        border-radius: 15px;
        background: <?php echo $colorCardEvent ?> ;
        padding: 3%;
        font-size: 25px;
        color : white;
        box-shadow: 4px 3px 7px 3px #dadada;
        margin-left: 13%;
        margin-right: 13%;
    }
</style>
<?php 
//  $params = [  "tpl" => "hubTerritoires","slug"=>Yii::app()->session["costum"]["slug"],"canEdit"=>$canEdit,"el"=>$el ];
// if(isset($test))
//     $params["test"]=$costumSlug;
//echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params,true);  
?>
<center>
	<img class="img-responsive ultra" src=" <?= @Yii::app()->session["costum"]["logo"]; ?>">
    <p class="txtHeader text-center col-lg-12" style="">Le réseau des acteurs ultramarins de la </br> MÉDIATION NUMÉRIQUE </p>
	<img style="margin-top: -3.3vw;width: 100%" class="img-responsive" src='<?php echo $bannerImg ?>'>

	<div class="hidden-xs col-lg-12" id="searchBar" style="margin-left: 18vw;">
		<a data-type="filters" href="javascript:;">
				<span id="second-search-bar-addon-mednum" class="text-white input-group-addon pull-left main-search-bar-addon-mednum">
					<i style="color:<?php echo $colorSearchicon ?>" class="fa fa-search searchIcone"></i>
				</span>
			</a>
		<input type="text" class="form-control pull-left text-center main-search-bars" id="second-search-bar" placeholder="Je recherche (mot clé, une association, un point d'interet...)">
	</div> 

	<div id="dropdown" class="hidden-xs dropdown-result-global-search hidden-xs col-sm-5 col-md-5 col-lg-5 no-padding">
	</div>
</center>

<div style="margin-top: 2.4vw;background-color: white" class="col-lg-12">
    <div style="margin-top: 7vw;">
        <?php echo $this->renderPartial("costum.assets.images.hubterritoires.backLeft"); ?>
        <div class="contain">
            <div id="containerCard" class="col-lg-3">
                <center>
                    <h2 style="margin-top: 1vw;color:#300080">Projets</h2>
                    <?xml version="1.0" encoding="utf-8"?>
                    <!-- Generator: Adobe Illustrator 23.0.4, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                    <img class="img-responsive logoApp" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/hubterritoires/logo-fusee.svg"><br>
                    <a href="#projects" class="lbh-menu-app">
                        <svg class="plus" style="border-radius: 38px;box-shadow: 0px 0px 8px gray;margin-top: 1.5vw;" fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="64px" height="64px">
                            <path fill="white" d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/>
                            <path fill="<?php echo $colorPathPlus ?>" d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/>
                        </svg>
                    </a>
                </center>
            </div>
            <div id="containerCard" class="col-lg-3">
                <center>
                    <h2 style="margin-top: 1vw;color:#300080">Participants</h2>
                    <img class="img-responsive logoApp" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/hubterritoires/logo-participants.svg"><br>
                    <a href="#organizations" class="lbh-menu-app">
                        <svg class="plus" style="border-radius: 38px;box-shadow: 0px 0px 8px gray;margin-top: 3.5vw;" fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="64px" height="64px">
                            <path fill="white" d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/>
                            <path fill="<?php echo $colorPathPlus ?>" d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/>
                        </svg>
                    </a>
                </center>
            </div>
            <div id="containerCard" class="col-lg-3">
                <center>
                    <h2 style="margin-top: 1vw;color:#300080">Formations</h2>
                    <img class="img-responsive logoApp" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/hubterritoires/logo-formation.svg"><br>
                    <a href="#Jobs" class="lbh-menu-app">
                        <svg class="plus" style="border-radius: 38px;box-shadow: 0px 0px 8px gray;margin-top: 2.9vw;" fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="64px" height="64px">
                            <path fill="white" d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/>
                            <path fill="<?php echo $colorPathPlus ?>" d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/>
                        </svg>
                    </a>
                </center>
            </div>
        </div>
        <?php echo $this->renderPartial("costum.assets.images.hubterritoires.backRight"); ?>
    </div>
</div>

<div style="margin-top: 45%;background-color: #fafafa;">
	<div class="explication row">
	    <div style="background-color:<?php echo $colorSectionActu ?>;" class="explication-title col-xs-12 col-sm-12 col-lg-12">
	        <div>
	            <h1 style="color: <?php echo $colorTitleActu ?>" class="titleBandeau"><i class="fa fa-newspaper-o" aria-hidden="true"></i>  Actualité</h1>
	        </div>
	    </div>
	    <!-- NEWS -->
	    <div style="margin-left: -5vw;width: 89vw;margin-top: 5vw;background-color: white" id="newsstream" class="col-xs-10 col-sm-12">
	        <div style="background-color: white;">
	        </div>
	    </div>

	    <div style="margin-top: 3vw;margin-bottom: 2vw;" class="text-center container col-lg-12 col-sm-12 col-xs-12">
	        <a href="javascript:;" data-hash="#live" class="lbh-menu-app btn btn-redirect-home btn-small-orange">
                <svg style="border-radius: 38px;box-shadow: 0px 0px 8px gray;" fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="64px" height="64px">
                    <path fill="transparent" d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/>
                    <path fill="<?php echo $colorPathPlus ?>" d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/>
                </svg>
            </a>
	    </div>
	</div>
</div>

<div style="background-color: white;">
	<div class="explicationAgenda row">
	    <div style="background-color:<?php echo $colorSectionAgenda ?>" class="explication-title col-xs-12 col-sm-12 col-lg-12">
	        <div>
	            <h1 style="color:<?php echo $colorTitleAgenda ?>" class="titleBandeau"><i class="fa fa-calendar" aria-hidden="true"></i>  Agenda</h1>
	        </div>
	    </div>

        <div style="margin-top: 7vw;" id="containEvent" class="col-lg-12 col-xs-12">
            
        </div>

	    <div style="margin-top: 3vw;" class="text-center container col-lg-12 col-sm-12 col-xs-12">
	        <a href="javascript:;" data-hash="#agenda" class="lbh-menu-app btn btn-redirect-home btn-small-orange">
                <svg style="border-radius: 38px;box-shadow: 0px 0px 8px gray;" fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="64px" height="64px">
                    <path fill="transparent" d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/>
                    <path fill="<?php echo $colorPathPlus ?>" d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/>
                </svg>
            </a> 
	    </div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {

    urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";

    ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:true, scroll:false}, function(news){}, "html");
    afficheEvent();
});



	function afficheEvent(){
    mylog.log("----------------- Affichage évènement");

    var params = {
       source : costum.contextSlug
    }

    $.ajax({
        type : "POST",
        url : baseUrl + "/costum/hubterritoires/geteventaction",
        data : params,
        dataType : "json",
        async : false,
        success : function(data){
            mylog.log("success : ",data);
            var str = "";
            var ctr = "";
            var itr = "";
            var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.events.defaultImg;
            var ph = "<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>";
            
            var i = 1;
            
            if(data.result == true){
                $(data.element).each(function(key,value){
                    mylog.log("data.element",data.element);
                    
                    var startDate = (typeof value.startDate != "undefined") ? "Du "+dateToStr(value.startDate, "fr", false, false) : null;  

                    var imgMedium = (value.imgMedium != "none") ? baseUrl + value.imgMedium : url;
                    var img = (value.img != "none") ? baseUrl + value.img : url;
                    
                    str += '<div style="color:white" class="card">';
                    str += '<div id="event-affiche" class="card-color col-md-4">';
                    str += '<div id="affichedate" class="info-card text-center">';
                    str += '<div id="afficheImg" class="img-hexa">';
                    str += '<a class="entityName bold text-dark add2fav  lbh-preview-element" href="#page.type.events.id.'+value.id+'">';
                    str += '<div class="hexagon hexagon1"><div class="hexagon-in1"><div class="hexagon-in2" style="background-image: url('+imgMedium+');"></div></div></div>';
                    str += '</a>';
                    str += '</div>';
                    str += ''+value.name+'<br>';
                    str += ''+startDate+'</br>'+value.type;
                    str += '</div>';
                    str += '</div>';
                    str += '</div>';
                });
            }
            else{
                str += "<div class='col-xs-12 col-sm-12 col-md-12'><b class='p-mobile-description' >Aucun évènement n'est prévu</b></div>";
            }
            $("#containEvent").html(str);
            mylog.log(str);
        },
        error : function(e){
            mylog.log("error : ",e);
        }
    });
}
</script>