<?php 
$keyTpl = "app";

     $paramsData = [
            "search"        =>  false,
            "live"          =>  false,
            "annonces"      =>  false,
            "agenda"        =>  false,
            "dda"           =>  false,
            "map"           =>  false,
            "pages"       => ["" => ""] 
     ];
    $y = 0;
    //  if( isset(Yii::app()->session["costum"][$keyTpl]) ) {
        foreach($paramsData as $i => $v) {
        // foreach (Yii::app()->session["costum"][$keyTpl] as $i => $v) {
            // var_dump(Yii::app()->session["costum"][$keyTpl][$i]);
            if(isset(Yii::app()->session["costum"][$keyTpl]["#".$i]["inMenu"])) 
                $paramsData[$i] =  Yii::app()->session["costum"][$keyTpl]["#".$i]["inMenu"] == "1" ? true : false;      
            }
        
        if(isset(Yii::app()->session["costum"][$keyTpl])){
            foreach(Yii::app()->session["costum"][$keyTpl] as $k => $v){
                if(isset(Yii::app()->session["costum"][$keyTpl][$k]["staticPage"])){
                    $paramsData["pages"][$v["icon"]] = $v["subdomainName"];
                }
            }
        }
            // $k = str_replace("#","",$i);
            // if(@$v["viewCustom"] == "true"){
            //     $paramsData["pages"][$y] = $k;
            //     $y++;
            // }
            // else
            //     $paramsData[$k] = $v;
        // }
        // if(isset(Yii::app()->session["costum"][$keyTpl]["pages"]["viewCustom"]))
        //     $paramsData["pages"] = @Yii::app()->session["costum"][$keyTpl]["pages"];
        
        // var_dump($paramsData);exit;
?>
<?php if($canEdit){ ?>
    <a class='btn btn-default edit<?php echo $keyTpl ?>Params' href='javascript:;' data-id='<?= Yii::app()->session["costum"]["contextId"]; ?>' data-collection='<?= Yii::app()->session["costum"]["contextType"]; ?>' data-key='<?php echo $keyTpl ?>' data-path='costum.<?= $keyTpl ?>'>
    <i class="fa fa-desktop" aria-hidden="true"></i> App
</a><?php }?>


<script type="text/javascript">
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {
    mylog.log("sectionParamsData----------------", sectionDyf.<?= $keyTpl ?>ParamsData);

    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo $keyTpl ?> config",
            "description" : "Liste de question possible",
            "icon" : "fa-cog",
            "properties" : {
                "search" : {
                            "inputType" : "checkboxSimple",
                            "label" : "Activer la recherche",
                            "params" : {
                                "onText" : "Oui",
                                "offText" : "Non",
                                "onLabel" : "Oui",
                                // "inputTrue" : ".amendementDateEnddatetime"
                                "offLabel" : "Non",
                                "labelText" : "Activer la recherche"
                            },
                            "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.search
                        },
                "live" : {
                        "inputType" : "checkboxSimple",
                        "label" : "Activer le live",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "Activer le live"
                        },
                        "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.live
                    },
                "annonces" : {
                    "inputType" : "checkboxSimple",
                        "label" : "Activer les annonces",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "Activer les annonces"
                        },
                        "checked" :  sectionDyf.<?php echo $keyTpl ?>ParamsData.annonces
                },
                "agenda" : {
                    "inputType" : "checkboxSimple",
                        "label" : "Activer l'agenda",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "Activer l'agenda"
                        },
                        "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.agenda
                },
                "dda" : {
                    "inputType" : "checkboxSimple",
                        "label" : "Activer les sondages",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "Activer les sondages"
                        },
                        "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.dda
                },
                "map" : {
                    "inputType" : "checkboxSimple",
                        "label" : "Activer la map",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "Activer la map"
                        },
                        "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.map
                },
                pages : {
                    inputType : "properties",
                    label : "Page Statique(s)",
                    labelKey : "Icon",
                    labelValue : "Titre",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.pages
                }
            },
            save : function () { 
                // mylog.log(document.body.getElementsByTagName("input"));
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    // mylog.log($("#"+k).val());
                    if($("#"+k).val() === "true" || $("#"+k).val() === "false"){
                        // mylog.log("qu'est ce que s'est trop bien la vie d'artiste");
                        var inMenu = $("#"+k).val() == "true" ? true : false;
                        tplCtx.value["#"+k] = { inMenu : $("#"+k).val() == "true" ? 1 : 0 } ;
                    }
                    if(val.inputType == "properties"){
                        pages = getPairs('.'+k+val.inputType);
                        ct = 0;
                        $.each(pages,function(icon,va){
                            tplCtx.value["#page"+ct] = {
                                hash : "#app.view",
                                icon : icon,
                                urlExtra : "/page/page"+ct+"/url/costum.views.tpls.staticPage",
                                useHeader : true,
                                useFooter : true,
                                useFilter : false,
                                inMenu : true,
                                subdomainName : va.value,
                                staticPage : true
                            }
                            ct++;
                        });
                        mylog.log("------------ array", pages);
                  
                    }
              
                        
                 });
                mylog.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>