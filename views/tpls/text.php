<?php 
$structField = "structags";

if( count(Poi::getPoiByStruct($poiList,$tag,$structField))!=0 )
{   
    $p = Poi::getPoiByStruct($poiList,$tag,$structField)[0];
    echo '<h1 style="color:'.@$color.'">'.@$p["name"].'</h1>';
    echo "<div class='markdown'>".@$p["description"]."</div>";
    //var_dump($p);

    if(isset($p["documents"])){
          echo "<br/><h4>Documents</h4>";
          foreach ($p["documents"] as $key => $value) {
            $dicon = "fa-file";
            $fileType = explode(".", $p["name"])[1];
            if( $fileType == "png" || $fileType == "jpg" || $fileType == "jpeg" || $fileType == "gif" )
              $dicon = "fa-file-image-o";
            else if( $fileType == "pdf" )
              $dicon = "fa-file-pdf-o";
            else if( $fileType == "xls" || $fileType == "xlsx" || $fileType == "csv" )
              $dicon = "fa-file-excel-o";
            else if( $fileType == "doc" || $fileType == "docx" || $fileType == "odt" || $fileType == "ods" || $fileType == "odp" )
              $dicon = "fa-file-text-o";
            else if( $fileType == "ppt" || $fileType == "pptx" )
              $dicon = "fa-file-text-o";
            echo "<a href='".$p["path"]."' target='_blanck'><i class='text-red fa "+$dicon+"'></i> ".$p["name"]."</a><br/>";
          }
    }
              
    $edit ="update";
} 
else 
    $edit ="create";


echo $this->renderPartial("costum.views.tpls.openFormBtn",
                                array(
                                    'edit' => $edit,
                                    'tag' => $tag,
                                    'id' => (isset($p["_id"])) ? (string)$p["_id"] : null
                                 ),true);

echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
?>

<script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render","costum.views.tpls.text",'tag : <?php echo $tag ?>');
    //OPTIM : ce code est répété autant de fois qu'il y a de btn 
    //il devrait etre sur l'appelant mais du il sera répété un peu partout 
    
    
    // /alert("costum.views.tpls.text");
    $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumCMS)
    });
    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dyFObj.openForm('poi',null,{structags:$(this).data("tag") ,type:'cms'},null,dynFormCostumCMS)
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                  $.ajax({
                        type: "POST",
                        url: urlToSend,
                        dataType : "json"
                    })
                    .done(function (data) {
                        if ( data && data.result ) {
                          toastr.info("élément effacé");
                          $("#"+type+id).remove();
                        } else {
                           toastr.error("something went wrong!! please try again.");
                        }
                    });
                }
            });

    });
});
</script>