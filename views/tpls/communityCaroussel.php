<?php
$defaultcolor = "black";
$structags = "structags";
$keyTpl = "communityCaroussel";

$paramsData = [ "title" => "",
                "color" => "",
                "background" => "",
                "defaultcolor" => "white",
                "tags" => "structags"
                ];

if( isset(Yii::app()->session["costum"]["tpls"][$keyTpl]) ) {
    foreach ($paramsData as $i => $v) {
        if( isset(Yii::app()->session["costum"]["tpls"][$keyTpl][$i]) ) 
            $paramsData[$i] =  Yii::app()->session["costum"]["tpls"][$keyTpl][$i];      
    }
}
?>
<style>
.carousel-inner{
    height : 500px;
}
.border{
    background: white;
    border-radius: 10px;
    padding: 1%;
    box-shadow: 2px 2px 3px 1px
    black;
    font-size: 3vw;
}
.p-mobile{
    font-size: 2vw;
}
.arrow-f{
    font-size : 6rem;
}
@media (max-width:768px){
    .carousel-inner{
        height : 200px;
    }
    .arrow-f{
    font-size : 3rem;
}
}
</style>
<div id="community-caroussel" class="communityCms-caroussel" style="margin-top:2%;">
    <?php if(isset(Yii::app()->session["costum"]["tpls"]["communityCaroussel"])) { ?>
        <div id="carousel"  class="carousel slide" data-ride="carousel">
            <?php 
            $data = Element::getCommunityByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"], "all", null, $roles,null);
             // var_dump($data);exit;
            if($data){
            ?>
            <div class="carousel-inner">
            <?php
                    foreach($data as $k => $v){
                        $element[$k] = Element::getElementSimpleById($k, $v["type"], null, array("name", "profilRealBannerUrl","links","slug"));
                    }
                    end($element);
                    $lk = key($element);
                    foreach($element as $key => $value){ 

                        $bannerImg = (!empty($value["profilRealBannerUrl"])) ? "/ph".$value["profilRealBannerUrl"] : Yii::app()->getModule("costum")->assetsUrl."/images/templateCostum/no-banner.jpg"; ?>
                        <div class="<?php if($key === $lk) echo "item active"; else echo "item"; ?>">
                            <div class="col-xs-12 text-center" style="position : absolute; margin-top: 2%">
                            <span class="border"><?= $value["name"]; ?></span>

                            <div class="container" style="margin-top:5.5%;">
                                <div class="col-xs-3">
                                    <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/acteur_icone.svg">
                                    <span class="p-mobile text-white"><?= count(@$value["links"]["members"]); ?> <br> Acteur(s)</span>
                                </div>
                                <div class="col-xs-3">
                                    <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/agenda_icone.svg">
                                    <span class="p-mobile text-white"><?= count(@$value["links"]["projects"]); ?><br> Projet(s)</span>
                                </div>
                                <div class="col-xs-3">
                                    <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/actu_icone.svg">
                                    <span class="p-mobile text-white"><?= @PHDB::count(News::COLLECTION,array("type" => "news","source.key" => $value["slug"])); ?><br> Actu(s)</span>
                                </div>
                                <div class="col-xs-3">
                                    <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/projet_icone.svg">
                                    <span class="p-mobile text-white"><?= @PHDB::count(Event::COLLECTION,array("source.key" => $value["slug"])); ?><br> Évènement(s)</span>
                                </div>
                            </div>
                            </div>
                            <img src="<?= $bannerImg; ?>"  alt="<?= $value["name"]; ?>" style="width:100%">
                        </div>
                        <?php
                    } ?>
                </div>
                <!-- ARROW -->
                <?php if(count($element) > 1) { ?>
                <div class="arrow col-xs-12 no-padding" style="position : absolute; z-index: 1000000; margin-top: -20%;">
                    <div class="col-xs-6" style="text-align : left;">
                        <a class="arrow-f carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                        <i class="fa fa-arrow-left" style="color: white;"></i>
                            <!-- <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span> -->
                        </a>
                    </div>
                    <div class="col-xs-6" style="text-align : right;">
                        <a class="arrow-f carousel-control-next" href="#carousel" role="button" data-slide="next">
                        
                        <i class="fa fa-arrow-right" style="color: white;"></i>
                            <!-- <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span> -->
                        </a>
                    </div>
                </div> <?php } ?>
        </div>
                <?php 
                }
                else echo "<div class='container'>Vous n'avez pas encore crée de communauté</div>";

                echo $this->renderPartial("costum.views.tpls.editTplBtns", ["canEdit" => $canEdit, "keyTpl"=>$keyTpl]); 
            ?>
<script type="text/javascript">
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo $keyTpl ?> config",
            "description" : "Liste de question possible",
            "icon" : "fa-cog",
            "properties" : {
                "title" : {
                    label : "Titre",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.title
                },
                "color" : {
                    label : "Couleur du texte",
                    "inputType" : "colorpick",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.color
                },
                "background" : {
                    label : "Couleur du fond",
                    "inputType" : "colorpick",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.background
                },
                defaultcolor : {
                    label : "Couleur",
                    "inputType" : "colorpick",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.defaultcolor
                },
                tags : {
                    inputType : "tags",
                    label : "Tags",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.tags
                },
                "statut" : {
                    "inputType" : "select",
                            "order":5,
                            "label" : "Services complémentaires",
                            "placeholder" : "Choisir un services",
                            "list" : {
                                "projects" : "Projects",
                                "organizations" : "Organizations",
                                "event" : "Event"
                            },
                            "groupOptions" : false,
                            "groupSelected" : false,
                            "optionsValueAsKey":true,
                            "select2" : {
                                "multiple" : true
                            }
                    
                }
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                 });
                console.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>
            <?php } ?>
</div>