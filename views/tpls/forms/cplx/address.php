<?php

$cssAnsScriptFilesModule = array( 
    '/js/address.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl() );


$value = "";
if(!empty($answer) && isset($answer["answers"][$form["id"]][$kunik])) 
	$value =  " value='".$answer["answers"][$form["id"]][$kunik]."' ";
else if(!empty($answer) && isset($answer["answers"][$kunik])) 
	$value =  " value='".$answer["answers"][$kunik]."' ";
$inpClass = "form-control";

// if($saveOneByOne)
// 	$inpClass .= " saveOneByOne";
?>
<div class="form-group" id="<?php echo $kunik ?>">
    <label for="<?php echo $kunik ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn ?></h4></label>
    <div id="addressForm<?php echo $kunik ?>"></div>

    
    <?php if(!empty($info)){ ?>
    	<small id="<?php echo $kunik ?>Help" class="form-text text-muted"><?php echo $info ?></small>
    <?php } ?>
</div>

<script type="text/javascript">
   // 137 rue des villages, Cangey
var cplxAddObj ={};
var keyAdd ="<?php echo $kunik ?>";
var valueAdd =<?php echo json_encode( $value ); ?>;
jQuery(document).ready(function() {
    mylog.log("render form input","/modules/costum/views/tpls/forms/cplx/address.php");
    
    var paramsInitAddress = {
        container : "#addressForm"+"<?php echo $kunik ?>",
        input : {
            id : 'inputAdd',
            class : "<?php echo $inpClass ?>",
            placeholder : "<?php echo (isset($placeholder)) ? $placeholder : '' ?>",
            "aria-describedby" : "<?php echo $kunik ?>Help"
        }
    };

    cplxAddObj = addressObj.init(paramsInitAddress);

    cplxAddObj.saveAuto = function(data){

        var answer = {
            collection : "answers",
            id : answerObj._id.$id,
            path : "answers."+keyAdd
        };

        answer.value =  cplxAddObj.result;

        mylog.log("addressObj.saveAuto", answer );

        dataHelper.path2Value( answer , function(params) {
            toastr.success('saved');
        } );
    };

 //    allMaps.maps[keyAdd] = addressObj.init(paramsInitAddress);

 //    allMaps.maps[keyAdd].saveAuto = function(data){

	// 	var answer = {
	// 		collection : "answers",
	// 		id : answerObj._id.$id,
	// 		path : "answers."+keyAdd
	// 	};

	// 	answer.value =  allMaps.maps[keyAdd].result;

	// 	mylog.log("addressObj.saveAuto", answer );

	// 	dataHelper.path2Value( answer , function(params) {
	// 		toastr.success('saved');
	// 	} );
	// };
});
</script>


