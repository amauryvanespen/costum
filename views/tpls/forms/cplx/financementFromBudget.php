<?php if($answer){ 
	
$copy = "opalProcess1.depense";

if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["budgetCopy"]) ) 
	$copy = Yii::app()->session["costum"]["form"]["params"][$kunik]["budgetCopy"];
//var_dump($copy);
$copyT = explode(".", $copy);
$copyF = $copyT[0];
$budgetKey = $copyT[1];
$answers = null;	
//var_dump($budgetKey);
if($wizard){
	if( $budgetKey ){
		if( isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey])>0)
		$answers = $answer["answers"][$copyF][$budgetKey];
	} else if( isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey])>0 )
		$answers = $answer["answers"][$copyF][$kunik];
} else {
	if($budgetKey)
		$answers = $answer["answers"][$budgetKey];
	else if(isset($answer["answers"][$kunik]))
		$answers = $answer["answers"][$kunik];
}
	
$editBtnL =  "";

$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$el["_id"]."' data-collection='".Yii::app()->session["costum"]["contextType"]."' data-path='costum.form.params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";



$paramsData = [ 
	"financerTypeList" => Ctenat::$financerTypeList,
	"limitRoles" =>["Financeur"],
	"openFinancing" => true 
];

if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]) ) {
	if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["tpl"]) ) 
		$paramsData["tpl"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["tpl"];
	if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["budgetCopy"]) ) 
		$paramsData["budgetCopy"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["budgetCopy"];
	if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["financerTypeList"]) ) 
		$paramsData["financerTypeList"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["financerTypeList"];
	if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["limitRoles"]) ) 
		$paramsData["limitRoles"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["limitRoles"];
	if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["openFinancing"]) ) 
		$paramsData["openFinancing"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["openFinancing"];
}

$communityLinks = Element::getCommunityByTypeAndId(Yii::app()->session["costum"]["contextType"],Yii::app()->session["costum"]["contextId"]);
$organizations = Link::groupFindByType( Organization::COLLECTION,$communityLinks,["name","links"] );

$orgs = [];

foreach ($organizations as $id => $or) {
	$roles = $or["links"]["memberOf"][Yii::app()->session["costum"]["contextId"]]["roles"];
	if( $paramsData["limitRoles"] && !empty($roles))
	{
		foreach ($roles as $i => $r) {
			if( in_array($r, $paramsData["limitRoles"]) )
				$orgs[$id] = $or["name"];
		}		
	}
}
//var_dump($orgs);exit;
$listLabels = array_merge(Ctenat::$financerTypeList,$orgs);

$properties = [
      // "financerType" => [
      //   "placeholder" => "Type de Financement",
      //       "inputType" => "select",
      //       "list" => "financerTypeList",
      //       "rules" => [
      //           "required" => true
      //       ]
      //   ],
		"poste" => [
            "inputType" => "text",
            "label" => "Contexte",
            "placeholder" => "Contexte",
            "size" => 4,
            "rules" => [ "required" => true ]
        ],
        "financer" => [
            "placeholder" => "Quel Financeur",
            "inputType" => "select",
            "list" => "financersList",
            "subLabel" => "Si financeur public, l’inviter dans la liste ci-dessous (au cas où il n’apparait pas demandez à votre référent territoire de le déclarer comme partenaire financeur",
            "size" => 6,
        ]
];
$amounts = (isset(Yii::app()->session["costum"]["form"]["params"][$budgetKey]["amounts"])) ? Yii::app()->session["costum"]["form"]["params"][$budgetKey]["amounts"] : ["price" => "Price"] ; 
foreach ( $amounts as $k => $l) {
	$properties[$k] = [ "inputType" => "text",
			            "label" => $l,
			            "placeholder" => $l,
			            "propType" => "amount"
			        ];
}


	?>	
<div class="form-group">
<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info; 
				if( !isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["budgetCopy"]) ) 
					echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>";?>
			</td>
		</tr>	
		<?php if($answers){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		$totalFunded = 0;
		if($answers){
			foreach ($answers as $q => $a) {

				//prepare td content
				$trStyle = "";
				$tds = "";
				foreach ($properties as $i => $inp) 
				{
					$size = (isset($inp["size"])) ? "col-xs-".$inp["size"] : "";
					$tds .= "<td class='".$size."'>";

					if( $i == "price" && isset($a[$i]) ){
						$tds .= $a[$i]."€";
						if(isset($percentDiv)){
							$tds .= $percentDiv;
							$tds .= "<br/> <span class='label label-primary'>".floor($percentAmountFunded)."%</span>";
							$percentDiv = "";
							$percentAmountFunded = 0;
						}
					}
					else if( $i == "financer"  )
					{
						
						$totalAmountFunded = 0;
						if( isset($a["financer"]) )
						{
							//if(count($a["financer"])>1){}
							$totalAmountFunded = 0;
							$tds .= "<table class='table table-bordered table-hover  directoryTable'>";
							foreach ($a["financer"] as $ix => $fin) {
								$tds .= "<tr>";
								
									$tds .= "<td class='col-xs-4'>";
										if(!empty($fin["id"])){
											$o = PHDB::findOne(Organization::COLLECTION,[ "_id" => new MongoId($fin["id"]) ],
																					 	["name","slug"]);
											$tds .= $o["name"];
										} else if( isset( $fin["email"] ) && isset( $fin["name"] ) ){
											$tds .= $fin["name"]."<br/>".$fin["email"];
										}else 
											$tds .= "unkknown";	
									$tds .= "</td>";

									$tds .= "<td  class='col-xs-5'>";
										if(!empty($fin["line"]))
											$tds .= $fin["line"];
									$tds .= "</td>";

									$tds .= "<td class='col-xs-2'>";
										if(!empty($fin["amount"])){
											$tds .= $fin["amount"]."€";
											$totalAmountFunded += (int)$fin["amount"];
										}
									$tds .= "</td>";

									$tds .= "<td class='col-xs-2'>";
										if(!empty($fin["amount"]) && isset($a["price"])){
											$percent = $fin["amount"] * 100 / (int)$a["price"];
											$tds .= " <span class='pull-right label label-primary'>".floor($percent)."%</span>";
										}
									$tds .= "</td>";
								
								$tds .= "</tr>";
							}
							$totalFunded += $totalAmountFunded;
							$tds .= "</table>";

							if($totalAmountFunded>0 && isset( $a["price"] )){
								$percentAmountFunded =  $totalAmountFunded * 100 / (int)$a["price"];
								$percol = "danger";
								$trStyle = "background-color:#ffdada";
								if( $percentAmountFunded >= 50 ){
									$percol = "warning";
									$trStyle = "background-color:#ffe7d1";
								}
								if( $percentAmountFunded >= 100 ){
									$percol = "success";
									$trStyle = "background-color:#e5ffe5";
								}

								$percentDiv = '<div class="progress btnFinancer"  data-id="'.$answer["_id"].'" data-budgetpath="'.$copy.'" data-form="'.$copyF.'" data-pos="'.$q.'"  style="cursor:pointer;margin-bottom: 0px;">'.
											  '<div class="progress-bar progress-bar-'.$percol.'" style="width:'.$percentAmountFunded.'%">'.
												    '<span class="sr-only">'.$percentAmountFunded.'% Complete</span>'.
											  '</div>'.
											'</div>';
							}
						}


						
						$tds .= "<div class='col-xs-12 text-center'>".
									
									" <div class='col-xs-12'><a href='javascript:;' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class='btn btn-xs btn-primary btnFinancer '><i class='fa fa-plus'></i> Ajouter un financeur</a></div></div>";
					}
					else if( isset($a[$i]) )
						$tds .= $a[$i];
					// else if( $i == "amount" && isset( $a["financer"]["amount"] ) ) 
					// 	$tds .= $a["financer"]["amount"];
					// else if( $i == "line" && isset( $a["financer"]["line"] ) ) 
					// 	$tds .= $a["financer"]["line"];
					
					$tds .= "</td>";
				} 
					
			

			echo "<tr id='".$kunik.$q."' class='".$kunik."Line text-center' style='".$trStyle."'>";
			echo $tds;		
			?>

			<td>

				<?php 
					$color = "default";
					$valbl = "?";
					$tool= "En attente de validation";
					if( isset($a["financing"]["valid"]) ){
						if( $a["valid"]["valid"] == "validated" ){
							$color = "success";
							$valbl = "V";
							$tool="Validé sans réserve";
							$totalFunded += 300;
						} else if( $a["financing"]["valid"] == "reserved" ){
							$color = "warning";
							$valbl = "R";
							$tool="Validé avec réserves";
						} else if( $a["financing"]["valid"] == "refused" ){
							$color = "danger";
							$valbl = "NV";
							$tool="Non validé";
						}
					}

					echo "<a href='javascript:;' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class='btnValidFinance btn btn-xs btn-".$color." margin-left-5 padding-10 tooltips' data-toggle='tooltip' data-placement='left' data-original-title='".$tool."'>".$valbl."</a>";

				
					?>
				<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
			</td>

			<?php 
				$ct++;
				echo "</tr>";
			}
		}


$total = (isset(Yii::app()->session["totalBudget"])) ? Yii::app()->session["totalBudget"] : 0;

if($totalFunded > 0){

	echo "<tr class='bold'>";
	echo 	"<td colspan=".count($properties)." style='text-align:right'>TOTAL FINANCÉ: </td>";
	echo 	"<td colspan=2>".trim(strrev(chunk_split(strrev($totalFunded),3, ' ')))." €</td>";
	echo "</tr>";

}

if($total > 0){

	echo "<tr class='bold'>";
	echo 	"<td colspan=".count($properties)." style='text-align:right'>TOTAL À FINANCER : </td>";
	echo 	"<td colspan=2>".trim(strrev(chunk_split(strrev($total),3, ' ')))." €</td>";
	echo "</tr>";

}




?>
		</tbody>
	</table>
</div>


<?php 
$percol = "warning";
$financedPercentage = (!empty($total)) ? $totalFunded * 100 / $total: 0;
if( $financedPercentage == 100 ){
	$percol = "success";
}
echo "<h4 style='color:".(($titleColor) ? $titleColor : "black")."'>Pourcentage de Financement Globale</h4>".
'<div class="progress " style="cursor:pointer" >'.
  '<div class="progress-bar progress-bar-'.$percol.'" style="width:'.$financedPercentage.'%">'.
	    '<span class="sr-only">'.$financedPercentage.'% Complete</span>'.
  '</div>'.
'</div>'; ?>

<table class="table table-bordered table-hover  ">
	<tbody class="">
		<tr>
			<td>Total à financer</td>
			<td><?php echo trim(strrev(chunk_split(strrev($total),3, ' '))) ?> €</td>
		</tr>
		<tr>
			<td>Total financé</td>
			<td><?php echo trim(strrev(chunk_split(strrev($totalFunded),3, ' '))) ?> €</td>
		</tr>
		<tr>
			<td>Pourcentage financé</td>
			<td><?php echo floor($financedPercentage) ?> %</td>
		</tr>
		<tr>
			<td>Reste à financer </td>
			<td><?php echo floor($total-$totalFunded) ?> €</td>
		</tr>
		<tr>
			<td>Resta à financer </td>
			<td><?php echo floor(100-$financedPercentage)+1 ?> %</td>
		</tr>		
	</tbody>
</table>


<div class="form-prioritize" style="display:none;">
  	<select id="validWork" style="width:100%;">
		<option> Valider ces travaux </option>
		<?php foreach (["validated" => "Validé sans réserve",
						"reserved" => "Validé avec réserves",
						"refused" => "Non validé" ] as $v => $f) {
			echo "<option value='".$v."'>".$f."</option>";
		} ?>
		</select>
 
</div>

<div class="form-financer" style="display:none;">
	<span class="bold">Financeur existant dans la communauté</span><br/>
	<select id="financer" style="width:100%;">
		<option value=0 >Quel financeur</option>
		<?php foreach ($orgs as $v => $f) {
			echo "<option value='".$v."'>".$f."</option>";
		} ?>
	</select>
	<br><span class="bold">ou Financeur inexistant dans la communauté</span><br/>
	<?php //if(!(boolean)$paramsData["openFinancing"]) { ?>
	<span class="bold">Nom du Financeur Libre</span>
	<br>
	<input type="text" id="financerName" name="financerName" style="width:100%;">

	<span class="bold">Email</span>
	<br>
	<input type="text" id="financerEmail" name="financerEmail" style="width:100%;">
	<?php //} ?>

	<br><br>
	<span class="bold">Fonds, enveloppe ou budget mobilisé</span>
	<br>
	<input type="text" id="financeLine" name="financeLine" style="width:100%;">
	<span class="bold">Montant Financé</span>
	<br>
	<input type="text" id="financeAmount" name="financeAmount" style="width:100%;">
</div>



<?php 
if( isset(Yii::app()->session["costum"]["form"]["params"]["financement"]["tpl"])){
	//if( Yii::app()->session["costum"]["form"]["params"]["financement"]["tpl"] == "tpls.forms.equibudget" )
		$this->renderPartial( "costum.views.".Yii::app()->session["costum"]["form"]["params"]["financement"]["tpl"] , 
		[ "totalFin"  => $total,
		  "totalBudg" => Yii::app()->session["totalBudget"]["totalBudget"] ] );
	// else 
	// 	$this->renderPartial( "costum.views.".Yii::app()->session["costum"]["form"]["params"]["financement"]["tpl"]);
}
 ?>

<script type="text/javascript">
if(typeof costum.lists == "undefined")
	costum.lists = {};
costum.lists.financerTypeList = <?php echo json_encode(Ctenat::$financerTypeList); ?>;
costum.lists.financersList = <?php echo json_encode($orgs); ?>;


var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$budgetKey])) ? $answer["answers"][$budgetKey] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Plan de Financement",
            "icon" : "fa-money",
            "text" : "Décrire ici les financements mobilisés ou à mobiliser. Les coûts doivent être en <b>hors taxe</b>.",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            financerTypeList : {
	                inputType : "properties",
	                labelKey : "Clef",
	                labelValue : "Label affiché",
	                label : "Liste des type de Fiannceurs",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.financerTypeList
	            } , 
	            limitRoles : {
	                inputType : "array",
	                label : "Liste des roles financeurs",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.limitRoles
	            },
	            tpl : {
	                label : "Sub Template",
	                value :  sectionDyf.<?php echo $kunik ?>ParamsData.tpl
	            },
	            budgetCopy : {
	            	label : "Input Bugdet",
	            	inputType : "select",
	            	options :  costum.lists.budgetInputList
	            },
	            openFinancing : {
	                inputType : "checkboxSimple",
                    label : "Financement Ouvert",
                    subLabel : "Le Finacement est ouvert aux personnes ou organisations non inscrites dans la communauté",
                    params : {
                        onText : "Oui",
                        offText : "Non",
                        onLabel : "Oui",
                        offLabel : "Non",
                        labelText : "Financement Ouvert"
                    },
                    checked : sectionDyf.<?php echo $kunik ?>ParamsData.estimate
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

 

	$('.btnValidFinance').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-validate-work").html(),
	        title: "État du Financement",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	var formInputsHere = formInputs;
			        	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".validFinal";	   
			        	
			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = { 
		        			valid : $(".bootbox #validWork").val(),
		        			user : userId,
		        			date : today
		        		};
		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	mylog.log("btnValidateWork save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, closePrioModalRel );
				  	 }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});
    
    $('.btnFinancer').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-financer").html(),
	        title: "Ajouter un Financeur",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	//var formInputsHere = formInputs;
			        	var financersCount = ( typeof eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer") != "undefined" ) ? eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer").length : 0;
			        	tplCtx.path = "answers."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financersCount;
			        	// if( notNull(formInputs [tplCtx.form]) )
			        	// 	tplCtx.path = "answers."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financersCount;	       

			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = { 
		        			line   : $(".bootbox #financeLine").val(),
		        			amount : $(".bootbox #financeAmount").val(),
		        			user   : userId,
		        			date   : today
		        		};

		        		if($(".bootbox #financer").val() != 0 ){
		        			tplCtx.value.id = $(".bootbox #financer").val();
		        			tplCtx.value.name = $(".bootbox #financer option:selected").text();
		        		}else if($(".bootbox #financerName").val() != "" ){
		        			tplCtx.value.name = $(".bootbox #financerName").val();
		        			tplCtx.value.email = $(".bootbox #financerEmail").val();
		        		}

		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	mylog.log("btnFinancer save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, function(params) { 
				  	 		prioModal.modal('hide');
				  	 		location.reload();
				  	 	} );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: function() {
                  }
                }
              },
		        onEscape: function() {
		          prioModal.modal("hide");
		        }
	    });
	    prioModal.modal("show");
	});
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>