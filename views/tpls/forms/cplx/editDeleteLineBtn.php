<?php 
if ($canEdit){ 
	$keyTpl = (isset($kunik)) ? $kunik : $keyTpl;
	?>
<a href='javascript:;'  data-id='<?php echo $id?>' data-collection='<?php echo $collection?>' data-key='<?php echo $q?>' data-path='<?php echo $path?>' class='edit<?php echo $keyTpl ?> previewTpl btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>

<a href='javascript:;' data-id='<?php echo $id?>' data-collection='<?php echo $collection?>' data-key='<?php echo $keyTpl.$q?>' data-path='<?php echo $path ?>' class='deleteLinex previewTpl btn btn-xs btn-danger'><i class='fa fa-times'></i></a>

<?php } ?>