<style type="text/css">
	#allAnswersList{ list-style: none }
	#allAnswersList li{ padding:5px; border-bottom: 1px solid #ccc; display: block; }
</style>

<div class="col-xs-12 text-center">
     <a href="javascript:;" class='btn btn-xs btn-default' id="showAnswerBtn"><i class="fa fa-bars"></i> Les <?php echo $what ?></a>
     <a href="#dashboard" class='lbh btn btn-xs btn-default'><i class="fa  fa-area-chart"></i> Observatoire Global</a>
     <a href="/costum/co/index/slug/<?php echo $el["slug"] ?>/answer/new" class='btn btn-xs btn-primary' id="showAnswerBtn"><i class="fa fa-plus"></i>  Ajouter <?php echo $what ?></a>
</div>

<div id="allAnswersContainer" class="hide col-xs-12 col-lg-offset-3 col-lg-6 margin-top-20">
	<ul id="allAnswersList">
	<?php 
	$lbl = $what." ";
	$ct = 0;
	if(!empty($allAnswers)){
		foreach ($allAnswers as $k => $ans) {
			$ct++;
			?>

		<li class="answerLi">
			
				<a class="col-xs-3" href="/costum/co/index/slug/<?php echo $el["slug"] ?>/answer/<?php echo $ans["_id"] ?>"> <?php echo $lbl." ".$ct ?></a> 

				<?php 
				$lblp = "";
				$percol = "danger";
				
				if(!isset($ans["answers"])) {
					$lblp = "no answers" ;
					$percent = 0;
				} else {
					$totalInputs = 0;
					$answeredInputs = 0;
					foreach (Yii::app()->session["forms"] as $ix => $f) {
						$totalInputs += count($f["inputs"]);
						//echo "|".$f['id']."-fi=".count($f["inputs"]);
						if( isset( $ans["answers"][$f['id']] ) ){
							$answeredInputs += count( $ans["answers"][$f['id']] );
							//echo "|".$f['id']."-ai=".count( $ans["answers"][$f['id']] )."<br/>";
						}
						
					}
					//echo "tot".$totalInputs."-ans".$answeredInputs;
					$percent = floor($answeredInputs*100/$totalInputs);
					$lblp = $percent."%";
				}

				if( $percent > 50 )
					$percol = "warning";
				if( $percent > 75 )
					$percol = "success";
				?>
				<span class="label label-<?php echo $percol ?>"> <i class="fa fa-pencil-square-o"></i> <?php echo $lblp ?> </span>
				<span class="margin-left-5 label label-primary"> <i class="fa fa-group"></i> 3 Participants</span>
				<span class="margin-left-5 label label-default"> <i class="fa fa-calendar"></i> <?php echo date("d/m/y H:i",$ans["created"]); ?></span>
				<a href="#dashboard.answer.<?php echo $ans["_id"] ?>" class='lbh btn btn-xs btn-default margin-left-5'> <i class="fa  fa-pie-chart "></i> Observatoire Local</a>
				<a class='text-red pull-right' href=""> <i class="fa  fa-trash"></i> </a> 
			
		</li>
		
	<?php }?>
		<li class="text-center"><a href="/costum/co/index/slug/<?php echo $el["slug"] ?>/answer/new" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i>  Ajouter</a></li>
	</ul>
</div>