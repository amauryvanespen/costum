<?php 
if( $canEdit  )
{ 
	if( isset($form['id']) )
	{ ?>

	<div id="copyEquilibreBudgetaire"></div>

	<table class="table table-bordered table-hover  directoryTable" >
		<tbody class="directoryLines">	
		
			<tr>
				<td colspan='2' ><h3 style="color:#16A9B1">Validation</h3>
				</td>
			</tr>
			<?php 
			if( isset( $answer["validation"][ $form['id'] ] ) )
			{ ?>
			<tr>
				<td colspan='2' align="center">
					<button id="generateCopil2" class="generateCopil btn btn-primary" data-id="<?php echo $_GET["id"] ?>" data-date="<?php echo date("Y-m-d") ?>" data-title="Compte Rendu de la réunion de finalisation" data-key="copilReunionFinalisation">Générer le dossier validé</button>
			<?php $pdf=Document::getListDocumentsWhere([ "id"=>(string)$answer["_id"], "type"=>Form::ANSWER_COLLECTION, "doctype"=>"file", "subKey"=>"pdf".$form['id'] ] ,"file");
				 ?>
				</td>
			</tr>
			<?php   } 


			if(isset($answer["validation"][$form['id']]))
			{ 
				$color = "danger";
				$lbl = "Non validé";
				if(isset($answer["validation"][$form['id']]["valid"])){
					if( $answer["validation"][$form['id']]["valid"] == "valid" ){
						$color = "success";
						$lbl = "Validé sans réserves";
					} else if( $answer["validation"][$form['id']]["valid"] == "validReserve" ){
						$color = "warning";
						$lbl = "Validé avec réserves";
					}
				}

				$editState = ( isset( $answer["validation"][$form['id']] ) ) ? "" : " <a href='javascript:;' data-type='".$form['id']."' class='validEdit btn btn-default btn-xs'><i class='fa fa-pencil'></i></a>";
				?>
				<tr>
					<td>Avis</td>
					<td><h4 class="label label-<?php echo $color?>"><?php echo $lbl?></h4> <?php echo $editState ?></td>
				</tr>
				<tr>
					<td>Commentaire global</td>
					<td><?php if( isset($answer["validation"][$form['id']]["description"] ) ) echo $answer["validation"][$form['id']]["description"]; ?></td>
				</tr>
				<tr>
					<td>Date</td>
					<td><?php if( isset($answer["validation"][$form['id']]["date"] ) ) echo $answer["validation"][$form['id']]["date"]; ?></td>
				</tr>
				<?php 
				if(!empty($ficheAction))
					{ 
						//$ficheAction = array_splice($ficheAction, count($ficheAction)-1);
						$histo = 0;
						foreach($ficheAction as $k => $v){ 
							if($histo < 1){?>
							<tr>
								<td>Fiche Action Finale</td>
								<td><a href='<?php echo $v["docPath"] ?>' target='_blank' class="link-files"><i class="fa fa-file-pdf-o text-red"></i> <?php echo $v["name"] ?></a></td>
							</tr>
						<?php	$histo++; }
						} 
					} ?>

			<?php }  ?>
			<tr>
				<td colspan='2' class="text-center" >
					<a href="javascript:;" data-type="<?php echo $form['id'] ?>" class="validEdit btn btn-primary">Valider cette action</a>				
				</td>
			</tr>
		</tbody>
	</table>
	 

	
<?php 
} ?>




<script type="text/javascript">

$(document).ready(function() { 

    mylog.log("render","/modules/costum/views/tpls/forms/cplx/stepValidation.php");
    documentManager.bindEvents();
    typeObj.validation<?php echo $form['id'] ?> = {
		    jsonSchema : {
			    title : "Validation",
			    icon : "gavel",
			    onLoads : {
			    	sub : function(){
		    		 	dyFInputs.setSub("bg-red");
		    		},
			    },
			    save : function() { 	    	
			   
					var formData = $("#ajaxFormModal").serializeFormJSON();
					var today = new Date();
					today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
			    	ctxTpl = {
			    		id : "<?php echo $answer['_id'] ?>",
			    		collection : "answers",
			    		path : "validation."+formData.form,
                		value : {
                			//who : formData.who,
                			valid : formData.valid,
                			description : formData.description,
                			date : today , 
                			user : userId
                		}
			    	};
			    	
			    	mylog.log("validation save",ctxTpl);

			  	 	dataHelper.path2Value( ctxTpl, function(params) { 
			  	 		dyFObj.closeForm();
			  	 		//only increment step form to next step if step valid
			  	 		if( ctxTpl.value.valid == "valid" || ctxTpl.value.valid == "validReserve" ){
				  	 		step = 0;
				  	 		if(costum.form.ids.length == $.inArray( "<?php echo $form['id'] ?>", costum.form.ids )+1)
				  	 			step = "all";
				  	 		else 
				  	 			step = costum.form.ids[$.inArray( "<?php echo $form['id'] ?>", costum.form.ids )+1];

				  	 		ctxTpl = {
					    		id    : "<?php echo $answer['_id'] ?>",
					    		collection : "answers",
					    		path  : "step",
					    		value : step
					    	};
					    	mylog.log("save step save",ctxTpl);
					  	 	dataHelper.path2Value( ctxTpl, function(params) { 
					  	 		location.reload();
					  	 	} );
					  	 } else 
					  	 	location.reload();
			  	 	} );

						
			    },
			    properties : {
			    	info : {
		                inputType : "custom",
		                html:"<p></p>",
		            },
		            valid : {
                        "label" : "Valider",
                        "inputType" : "select",
                        "placeholder" : "---- Selectionner Valider ----",
                        "options" : {
                        	"notValid":"Non validé",
                            "validReserve" : "Validé avec réserves",
                            "valid" : "Validé sans réserve"
                        }
                    },
		            description : dyFInputs.textarea( tradDynForm["description"], "..." ),
		      		//  date : {
				    // 	inputType : "date",
				    // 	label : "Date",
				    // 	placeholder : "Date de Validation",
				    // 	rules : { 
				    // 		required : true,
				    // 		greaterThanNow : ["DD/MM/YYYY"]
				    // 	}
				    // },
		            form : dyFInputs.inputHidden()
			    }
			}
		};
    $('.validEdit').off().on("click", function() {
    	var setVal = { form : $(this).data("type")};
		dyFObj.openForm( typeObj[ 'validation'+setVal.form ], null, setVal );
	});

	
    
});
</script>
<?php } else {?>
	<div class="col-xs-12">
		Seuls les référents du territoire, le porteur de l’action et l’équipe nationale ont accès à cette étape.
	</div>
<?php } ?>