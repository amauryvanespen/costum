<?php if($answer){ ?>
<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
		

	<?php 
	$paramsData = [ 
	 "sectionTitles" => [ 
            "1er<br/>Sem<br/>2018", 
            "2ème<br/>Sem<br/>2018", 
            "1er<br/>Sem<br/>2019", 
            "2ème<br/>Sem<br/>2019", 
            "1er<br/>Sem<br/>2020", 
            "2ème<br/>Sem<br/>2020", 
            "1er<br/>Sem<br/>2021", 
            "2ème<br/>Sem<br/>2021", 
            "1er<br/>Sem<br/>2022", 
            "2ème<br/>Sem<br/>2022"
        ],
        "dateSections" => [ 
            "01/01/2018", 
            "01/07/2018", 
            "01/01/2019", 
            "01/07/2019", 
            "01/01/2020", 
            "01/07/2020", 
            "01/01/2021", 
            "01/07/2021", 
            "01/01/2022", 
            "01/07/2022", 
            "01/01/2023"
        ]];
		$dateSections = (isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["dateSections"])) ? Yii::app()->session["costum"]["form"]["params"][$kunik]["dateSections"] : $paramsData["dateSections"] ;

		$editBtnL = (Yii::app()->session["userId"] == $answer["user"]) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
		
		$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$el["_id"]."' data-collection='".Yii::app()->session["costum"]["contextType"]."' data-path='costum.form.params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
	?>	
	<thead>
		<tr>
			<td colspan='<?php echo count( $dateSections)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info ?>
			</td>
		</tr>	
		<?php if(isset($answer["answers"][$kunik]) && count($answer["answers"][$kunik])>0){ ?>
		<tr>
			<th>Actions<br/>
			</th>
			<?php 
			$sectionTitles = (isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["sectionTitles"])) ? Yii::app()->session["costum"]["form"]["params"][$kunik]["sectionTitles"] : $paramsData["sectionTitles"];
			foreach ($sectionTitles as $i => $lbl) {
				echo "<th>".$lbl."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		
		if(isset($answer["answers"][$kunik])){
			foreach ($answer["answers"][$kunik] as $q => $a) {

				echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>".
					"<td>".@$a["step"]."</td>";
				$bgColor = "white";
				$td = "";
				foreach ($dateSections as $sa => $sv) {
					if( $bgColor == "white"){
						if( isset( $dateSections[$sa+1] ) && strtotime(str_replace('/', '-',@$a["startDate"])) < strtotime(str_replace('/', '-',$dateSections[$sa+1]))  ) {
							$bgColor = "#5EAC87";
							$td = "&bull;";
						}
					} else if( $bgColor == "#5EAC87"){
						if( strtotime(str_replace('/', '-',@$a["endDate"])) < strtotime(str_replace('/', '-',$sv))  ) {
							$bgColor = "";
							$td = "";
						}
					}
					if( $sa != sizeof( $dateSections ) - 1 )
						echo "<td class='text-center' style='border: 1px solid #ddd;background-color:".$bgColor."'>".$td."</td>";
				}
			?>
			<td>
				<?php 
					$this->renderPartial( "costum.views.tpls.forms.cplx.editDeleteLineBtn" , [
						"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
						"id" => $answer["_id"],
						"collection" => Form::ANSWER_COLLECTION,
						"q" => $q,
						"path" => "answers.".$kunik.".".$q,
						"keyTpl"=>$kunik
						] );
					?>
				<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
			</td>
			<?php 
				$ct++;
				echo "</tr>";
			}
		}
		 ?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;

$(document).ready(function() { 

	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Calendrier",
	        "description" : "Etapes clefs de la fiche action",
	        "icon" : "fa-calendar",
	        "properties" : {
	            "step" : {
	                "inputType" : "text",
	                "label" : "Actions",
	                "placeholder" : "Actions",
	                "rules" : { "required" : true }
	            },
	            "startDate" : {
	                "inputType" : "date",
	                "label" : "Début",
	                "placeholder" : "Début de l'action",
	                "rules" : { "required" : true }
	            },
	            "endDate" : {
	                "inputType" : "date",
	                "label" : "Fin",
	                "placeholder" : "Fin de l'action",
	                "rules" : { "required" : true }
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    //$("#ajax-modal").modal('hide');
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "description" : "Labels et dates de la section",
	        "icon" : "fa-cog",
	        "properties" : {
	            sectionTitles : {
	                "inputType" : "array",
	                "label" : "Label de chaque période"
	            },
	            dateSections : {
	                "inputType" : "array",
	                "label" : "Date de chaque période"
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    //$("#ajax-modal").modal('hide');
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( ( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]) ) ? Yii::app()->session["costum"]["form"]["params"][$kunik] : $paramsData); ?>;

    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");       
        tplCtx.path = $(this).data("path")+(notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length :"0");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //if no params config on the element.costum.form.params.<?php echo $kunik ?>
        //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
        //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>