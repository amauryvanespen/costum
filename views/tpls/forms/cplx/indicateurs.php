<?php if($answer){ 
	$keyTpl = "indicateurs";
	$kunik = $keyTpl.$key;
	?>
<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="panelAdmin<?php echo $key?>">
	
<?php 
$editBtnL = (Yii::app()->session["userId"] == $answer["user"]) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='answers.".$kunik.".' class='add".$keyTpl." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";

$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$el["_id"]."' data-collection='".Yii::app()->session["costum"]["contextType"]."' data-path='costum.form.params.".$keyTpl."' class='previewTpl edit".$keyTpl."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

$paramsData = [ "titles" => [ "Indicateur",
							"Objectif / Réalisé",
							"Réf. 2018",
							"Résultat 2019",
							"Résultat 2020",
							"Résultat 2021",
							"Résultat 2022"],
				"keys" => [ "res2018","res2019","res2020","res2021","res2022"] ];

//temporaire car specific au CTE
$indicateurs = Ctenat::getIndicator();

if( isset(Yii::app()->session["costum"]["form"]["params"][$keyTpl]) ) 
	$paramsData =  Yii::app()->session["costum"]["form"]["params"][$keyTpl];

$properties = [
	"indicateur" => [
        "inputType" => "select",
        "label" => "Indicateur",
        "list" => "indicateurs",
        "select2" => [
            "multiple" => false
        ],
        "rules" => [ "required" => true ]
    ]
];

?>	
<style type="text/css">
	.bgGrey{background-color: grey;}
</style>
<thead>
	<tr>
		<td colspan='<?php echo count( $paramsData["titles"])+2?>' >
			<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?>
				<a href='javascript:;' onclick="dyFObj.openForm('indicator');" class='btn btn-default margin-top-5 margin-bottom-5'><i class='fa fa-plus'></i> Ajouter un nouvel indicateur </a>
			</h4>
			<?php echo $info ?>
		</td>
	</tr>	
	
	<tr>
		<?php foreach ($paramsData["titles"] as $key => $t) {
			echo "<th>".$t."</th>";
		} ?>
		<th></th>
	</tr>
</thead>
<tbody class="directoryLines">	
	<!-- <tr> -->
	<?php 
	$ct = 0;
	//always show indicateur emploi 
	$emploiExists = false;
	if(isset($answer["answers"][$kunik])){
	
	//var_dump($indicateurs); //exit;
	foreach ($answer["answers"][$kunik] as $q => $a) {
		if(isset($a["indicateur"])){
			//foreach ($a["indicateur"] as $kA => $valA) {
			echo "<tr>".
				//"<td id='indic".$ct."' rowspan=2 style='vertical-align : middle;text-align:center;'>".( !empty($indicateurs[$valA]) ?  $indicateurs[$valA] : "" )."</td>".
				"<td id='indic".$ct."' rowspan=2 style='vertical-align : middle;text-align:center;'>".( !empty($indicateurs[$a["indicateur"]]) ?  $indicateurs[$a["indicateur"]] : "" )."</td>".
				"<td>Objectif</td>".
				"<td style='vertical-align : middle;text-align:center;background-color:grey;'></td>";
				foreach ($paramsData["keys"] as $i => $k) {
					if($i>0)
						echo "<td class='editContent' data-key='".$k."' data-indic='indic".$ct."' data-pos='".$q."' data-type='objectif'>".(isset($a["objectif"][$k]) ? $a["objectif"][$k]:"")."</td>";	
				}
				?>
				
				<td rowspan=2 >
					
					<?php 
					$this->renderPartial( "costum.views.tpls.forms.cplx.editDeleteLineBtn" , [
						"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
						"id" => $answer["_id"],
						"collection" => Form::ANSWER_COLLECTION,
						"q" => $q,
						"path" => "answers.".$kunik.".".$q,
						"keyTpl"=>$keyTpl
						] );
					?>

					<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
				</td>
			
			<?php echo "</tr>";
			$editableClass = ($isSuivi) ? "editContent" : "bgGrey" ;
			echo "<tr>".
				"<td>Réalisé</td>";
				foreach ($paramsData["keys"] as $i => $k) {
					echo "<td class='editContent' data-key='".$k."' data-indic='indic".$ct."' data-pos='".$q."' data-type='reality'>".(isset($a["reality"][$k]) ? $a["reality"][$k]:"")."</td>";	
				}
			echo "</tr>";

			$ct++;
			//}
		}
	}
}

	 ?>

	</tbody>
</table>
</div>

<script type="text/javascript">
if(typeof costum.lists == "undefined")
	costum.lists = {};
costum.lists.indicateurs = <?php echo json_encode($indicateurs); ?>;
var <?php echo $keyTpl ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	mylog.log("render","/var/www/dev/modules/costum/views/tpls/forms/<?php echo $keyTpl ?>.php");
	
	sectionDyf.<?php echo $keyTpl ?> = {
		"jsonSchema" : {	
	        "title" : "Résultats attendus de l'action",
            "description" : "Décrivez ici la manière dont nous pourrons évaluer, dans 1 an / 2 ans / 3 ans / 4 ans, que le projet aura réussi : quels objectifs chiffrés, et quelle méthode pour mesurer les progrès.",
            "icon" : "fa-calendar-check-o",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $keyTpl ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $keyTpl ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $keyTpl ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            titles : {
	                inputType : "array",
	                label : "Liste des titres",
	                values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.titles
	            },
	            keys : {
	                inputType : "array",
	                label : "Liste des clefs",
	                values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.keys
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};
	//adds a line into answer
    $(".add<?php echo $keyTpl ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $keyTpl ?>Data) ? <?php echo $keyTpl ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?> );
    });

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  	
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //if no params config on the element.costum.form.params.<?php echo $keyTpl ?>
        //then we load default values available in forms.inputs.<?php echo $keyTpl ?>xxx.params
        //mylog.log(".editParams",sectionDyf.<?php echo $keyTpl ?>Params,calData);
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

	$('.editContent').off().click(function() {
		//mylog.log("ping ping");
		tplCtx.id = "<?php echo $answer["_id"] ?>";
		tplCtx.collection = "<?php echo Form::ANSWER_COLLECTION ?>";      
		tplCtx.path = "answers.<?php echo $kunik ?>."+$(this).data('pos')+"."+$(this).data('type')+"."+tplCtx.key;

		mylog.log(".editContent", "<?php echo $kunik ?>","tplCtx",tplCtx);
		var indic = $('#'+$(this).data('indic')).html();
		bootbox.prompt({
				inputType: 'number',
	            title: "Valeur ? <span style='color:red'>(uniquement des chiffres)</span><br/>"+indic, 
	            callback : function(result){ 
	                if (result === null) {
				    	//alert("null");
				    } 
				    else 
				    {
		                tplCtx.value = result;
			    		mylog.log(".editContent", "<?php echo $kunik ?>","tplCtx",tplCtx);
						
						dataHelper.path2Value( tplCtx, function(params) { 
							$("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
	                    	location.reload();
						} );
					}
	            }
	        });

	 	});

    });
  	</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>