<?php 
$value = (!empty($answers)) ? " value='".$answers."' " : "";

$inpClass = "form-control";

if($type == "tags") 
	$inpClass = "select2TagsInput";

if($saveOneByOne)
	$inpClass .= " saveOneByOne";
?>
<div class="form-group">
    <label for="<?php echo $key ?>"><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn ?></h4></label>
    <br/><input type="<?php echo (!empty($type)) ? $type : 'text' ?>" class="<?php echo $inpClass ?>" id="<?php echo $key ?>" aria-describedby="<?php echo $key ?>Help" placeholder="<?php echo (isset($placeholder)) ? $placeholder : '' ?>" data-form='<?php echo $form["id"] ?>'  <?php echo $value ?> >
    <?php if(!empty($info)){ ?>
    	<small id="<?php echo $key ?>Help" class="form-text text-muted"><?php echo $info ?></small>
    <?php } ?>
</div>

<script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render form input","/modules/costum/views/tpls/forms/text.php");
});
</script>