<?php
// var_dump(Yii::app()->session["costum"]);exit;
    if(isset(Yii::app()->session["costum"]["contextType"]) && isset(Yii::app()->session["costum"]["contextId"])){
        $el = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );
    
        $poiList = PHDB::find(Poi::COLLECTION, 
                        array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                               "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                               "type"=>"cms","structags" => new MongoRegex("/".$page."/")) );
    } 

    //  var_dump($poiList);
    // var_dump(Yii::app()->session["costum"]["contextId"]);
    ?>
<div class="container">

<?php
//echo $page;
if( !count($poiList))
    echo "<h1>Explain static pages</h1>".
        "<p>La création d'une page stastique est vraiment simple, cliquer sur le bouton 
        crée un contenu, j'ajoutais votre contenu et c'est tout. <br>
        Vous pouvez utiliser du markdown si vous le souhaitez.</p>";

echo $this->renderPartial("costum.views.tpls.multiblocks",array(
                        "poiList"   => $poiList,
                        "blockName" => $page,
                        "titlecolor"=> Yii::app()->session["costum"]["css"]["loader"]["ring2"]["color"],
                        "blockCt"   => 1,
                        "endHTML"   => '<br/><span class="bullet-point"></span><br/>'
                      ));

    ?>
</div>

<script>
 jQuery(document).ready(function() {
 contextData = {
        id : "<?php echo Yii::app()->session["costum"]["contextId"] ?>",
        type : "<?php echo Yii::app()->session["costum"]["contextType"] ?>",
        name : '<?php echo htmlentities($el['name']) ?>',
        profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
    };
 });
</script>