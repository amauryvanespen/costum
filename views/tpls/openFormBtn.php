<?php if( 
	Authorisation::canEdit(Yii::app()->session["userId"] , Yii::app()->session["costum"]["contextId"], Yii::app()->session["costum"]["contextType"] ) ||
	Authorisation::canEditItem(Yii::app()->session["userId"] , Yii::app()->session["costum"]["contextId"], Yii::app()->session["costum"]["contextType"] ) ||
	Authorisation::canEdit(Yii::app()->session["userId"] , Yii::app()->session["costum"]["contextId"], ucfirst(Element::getControlerByCollection(Yii::app()->session["costum"]["contextType"])) ) )
{ ?>
	<br>
	<?php if( $edit == "create" ){ ?>
	    <a href="javascript:;" data-tag="<?php echo $tag ?>" class="createBlockBtn btn btn-xs btn-primary">Créer du contenu</a>
	<?php } else if ( $edit == "update") { ?>
	    
	    <a href="javascript:;"  data-id="<?php echo (string)$id ?>" data-type="poi" onclick=""  class="editThisBtn btn btn-xs btn-primary">Modifier</a> 

	    <a class="deleteThisBtn btn btn-xs btn-danger " data-id="<?php echo (string)$id ?>" data-type="poi" href="javascript:;" class="btn btn-xs btn-primary"><i class="fa fa-trash"></i></a> 
	<?php } 
?>

<script type="text/javascript">
	jQuery(document).ready(function() {
		mylog.log("render","costum.views.tpls.openFormBtn",'tag : <?php echo $tag ?>');
	});
</script>



<?php
} ?>

