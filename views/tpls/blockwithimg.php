<?php
$default = "black";
$structField = "structags";
// var_dump(count(Poi::getPoiByStruct($poiList,"blockimg0",$structField)));exit;
// $bk = (@Yii::app()->session["costum"]["tpls"]["blockwithimg"]["background"] && !empty(Yii::app()->session["costum"]["tpls"]["blockwithimg"]["background"])) ? Yii::app()->session["costum"]["tpls"]["blockwithimg"]["background"] : "green";
$color = (@Yii::app()->session["costum"]["tpls"]["blockwithimg"]["color"] && !empty(Yii::app()->session["costum"]["tpls"]["blockwithimg"]["color"])) ? Yii::app()->session["costum"]["tpls"]["blockwithimg"]["color"] : "black";

$keyTpl = "blockwithimg";

$paramsData = [
                "color" => "",
                "defaultcolor" => "#354C57",
                "tags" => "structags"
                ];

if( isset(Yii::app()->session["costum"]["tpls"][$keyTpl]) ) {
    foreach ($paramsData as $i => $v) {
        if( isset(Yii::app()->session["costum"]["tpls"][$keyTpl][$i]) ) 
            $paramsData[$i] =  Yii::app()->session["costum"]["tpls"][$keyTpl][$i];      
    }
}

?>
<style>
#blockwithimg{
    color : <?= $color; ?>
}
</style>
<div id="blockwithimg" class="blockwithimg row">

    <?php 
    if(isset(Yii::app()->session["costum"]["tpls"]["blockwithimg"])){
	// foreach($poiList as $k => $v){
	// 	$n = "todo";
		$result = null;
		if(count ( Poi::getPoiByStruct($poiList,"blockimg0",$structField ) ) != 0){
			$result = Poi::getPoiByStruct($poiList,"blockimg0",$structField)[0];
			// var_dump($result["_id"]); exit;
      // $img = (Document::getWhere(array("id" =>(String) $result["_id"])) ? Document::getWhere(array("id" =>(String) $result["_id"])) : null);
      $img = PHDB::findOne(Document::COLLECTION, array("id" => (String) $result["_id"]));
      // $baseUrl = "<script>document.write(baseUrl);</script>";
      // var_dump($img["moduleId"]); exit; 
		?>
		<div class="blockwithimg-txt col-md-6">
			<h1 class="<?php if(isset($default)) echo 'text-'.$default.''; else echo 'text-red'; ?>"><?php echo $result["name"]; ?></h1>
			<span class="markdown"><?= @$result["description"]; ?></span>
		</div>
		<?php if(isset($img)){
			?>
		<div class="blockwithimg-img col-md-6">
      <img src="<?php echo Yii::app()->baseUrl.'/upload/'.$img["moduleId"].'/'.$img["folder"].'/'.$img["name"]; ?>" class="img-responsive">
		</div>
        <?php }
        $edit = "update";
	}
	else { ?>
    <div class="col-xs-12">
			TODO : <br />
			as POI type cms + tag : blockimg0;
		<?php 
		$edit = "create"; 
    echo"</div>";
	} 
	echo $this->renderPartial("costum.views.tpls.openFormBtn",
        array(
            'edit' => $edit,
            'tag' => 'blockimg0',
            'id' => (string)@$result["_id"]),true); ?>
   <?php  
    // }  

    echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
    echo $this->renderPartial("costum.views.tpls.editTplBtns", ["canEdit" => $canEdit, "keyTpl"=>$keyTpl]);
?>
<script type="text/javascript">
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function(){
		mylog.log("---------- Render blockimg","costum.views.tpls.blockwithimg");
		$.each($(".markdown"), function(k,v){
			descHtml = dataHelper.markdownToHtml($(v).html());
			$(v).html(descHtml);
		});

        sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo $keyTpl ?> config",
            "description" : "Liste de question possible",
            "icon" : "fa-cog",
            "properties" : {
                "color" : {
                    label : "Couleur du texte",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.color

                },
                defaultcolor : {
                    label : "Couleur",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.defaultcolor
                },
                tags : {
                    inputType : "tags",
                    label : "Tags",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.tags
                }
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                 });
                console.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});

	$(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumCMS)
    });
    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dyFObj.openForm('poi',null,{structags:$(this).data("tag") ,type:'cms'},null,dynFormCostumCMS)
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                  $.ajax({
                        type: "POST",
                        url: urlToSend,
                        dataType : "json"
                    })
                    .done(function (data) {
                        if ( data && data.result ) {
                          toastr.info("élément effacé");
                          $("#"+type+id).remove();
                        } else {
                           toastr.error("something went wrong!! please try again.");
                        }
                    });
                }
            });

    });

    
</script>
    <?php } ?>
</div>