<?php 
    /**
     * TPLS QUI PERMET AFFICHAGE DES NEWS
     */
    $keyTpl = "news";

    $defaultcolor = "white";
    $tags = "structags";
    $paramsData = [ "title" => "",
                    "color" => "",
                    "background" => "",
                    "defaultcolor" => "white",
                    "tags" => "structags",
                    "nbPost" => 2
                    ];

    if( isset(Yii::app()->session["costum"]["tpls"][$keyTpl]) ) {
        foreach ($paramsData as $i => $v) {
            if( isset(Yii::app()->session["costum"]["tpls"][$keyTpl][$i]) ) 
                $paramsData[$i] =  Yii::app()->session["costum"]["tpls"][$keyTpl][$i];      
        }
    }
    ?>


        <div id="newsstream"></div>
        <div class="container text-center"> 
            <a href="javascript:;" data-hash="#live" class="lbh-menu-app " style="text-decoration : none; font-size:2rem;">
            <center><img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/plus.svg" class="img-responsive" style="width:3%;"> </center>
            <p class="text-center text-black">Voir plus d'actualité</p>
            </a>
        </div>
    <?php  

     echo $this->renderPartial("costum.views.tpls.editTplBtns", ["canEdit" => $canEdit, "keyTpl"=>$keyTpl]);
 ?>

<script type="text/javascript">
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {
    
    
    urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/<?php echo $paramsData["nbPost"]; ?>/scroll/false";
    mylog.log("tplsNews",urlNews);

    ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html");
        // chargeElement();

    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo $keyTpl ?> config",
            "description" : "Liste de question possible",
            "icon" : "fa-cog",
            "properties" : {
                "title" : {
                    label : "Titre",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.title
                },
                "color" : {
                    label : "Couleur du texte",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.color
                },
                "background" : {
                    label : "Couleur du fond",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.background
                },
                defaultcolor : {
                    label : "Couleur",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.defaultcolor
                },
                tags : {
                    inputType : "tags",
                    label : "Tags",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.tags
                },
                nbPost : {
                    label : "Nombre de post",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.nbPost
                },
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                 });
                mylog.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>