<?php
/**
 * 
 */
class hubTerritoires{
  const COLLECTION = "costum";
  const CONTROLLER = "costum";
  const MODULE = "costum";

  public static function getEvent($source){

       $parametres = array(
           "result" => false
       );
       date_default_timezone_set('UTC');

       $date_array = getdate();
       $numdays = $date_array["wday"];

       $endDate = strtotime(date("Y-m-d H:i", time() + ((30 - $numdays) * 24*60*60)));
       $startDate = strtotime(date("Y-m-d H:i"));
    
       
       $where = array(
                 "source" => array(
                    "insertOrign" =>    "costum",
                    "keys"         =>    array(
                                        $source),
                    "key"          =>  $source),            
                 "startDate"   =>  array('$gte'   =>  new MongoDate($startDate)),
                 "endDate"     =>  array('$lt'    => new MongoDate($endDate))
                 );

       // var_dump($where);

       $allEvent = PHDB::findAndLimitAndIndex(Event::COLLECTION, $where,3);
       // var_dump($allEvent);
       if(isset($allEvent)){

           $results = array();
           
           $parametres = array(
               "result" =>  true
           );

           $results = self::createResultEvent($allEvent);
           return array_merge($parametres,$results);
       }
       return $parametres;
   }


   private static function createResultEvent($params){

       $res["element"] = array();
       foreach($params as $key => $value){
        $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
        $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
        $resume = (@$value["shortDescription"] ? $value["shortDescription"] : "");

           array_push($res["element"], array(
               "id"               => (String) $value["_id"],
               "name"             =>  $value["name"], 
               "type"             =>  Yii::t("event",$value["type"]),
               "startDate"        =>  date(DateTime::ISO8601, $value["startDate"]->sec),
               "imgMedium"        =>  $imgMedium,
               "img"              =>  $img,
               "slug"             =>  $value["slug"]
           ));
       }
       return $res;
   }
}
?>