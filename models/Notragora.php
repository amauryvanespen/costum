<?php

class Notragora {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	
	public static function setSourceAllElement($params){
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $colElts = array(Person::COLLECTION, Organisation::COLLECTION, Poi::COLLECTION);
            foreach ($colElts as $keyCol => $col) {
                $source = array(
                    'insertOrign' => "import";
                    'date'=> new MongoDate(time()),
                    "key" => "notragora",
                    "keys" => array("notragora") );
                      
                PHDB::update($col,
                    array() , 
                    array('$set' => array("source" => $source))
                );

            }
        }
        return $params;
    }
}
?>