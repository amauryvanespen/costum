<?php
/**
 * 
 */
class hubMedNum{
  const COLLECTION = "costum";
  const CONTROLLER = "costum";
  const MODULE = "costum";

  public static function getCommunity($id,$type){
        $community = Element::getCommunityByTypeAndId($type, $id, "all", null , "mednum",null);
        
       if (isset($community)) {
          $results = self::getDataCommunity($community);
       }
       return $results;
    }

  public static function getCommunityNews($id,$type){
        $community = Element::getCommunityByTypeAndId($type, $id, "all", null , "mednum",null);
        
       if (isset($community)) {
          $results = self::getDataNews($community);
       }
       return $results;
    }


  public static function getEventCommunity($id,$type){
    $communityForEvent = Element::getCommunityByTypeAndId($type, $id, "all", null , "mednum",null);
        
       if (isset($communityForEvent)) {
          $results = self::getDataEventCommunity($communityForEvent);
       }
       return $results;
  }

  private static function getDataNews($data){
        $params = array(
             "result" => false
         );

        $elements = [];
        $resNews = [];

          foreach ($data as $k => $v) {
            $elements[$k] = Element::getElementSimpleById($k, $v["type"], null, array("name", "profilImageUrl","profilRealBannerUrl","links","slug", "address","geo", "geoPosition", "profilThumbImageUrl", "profilMarkerImageUrl","type" )); 
          }

          if (isset($elements)) {
          
            foreach ($elements as $key => $value) {
              $resNews = PHDB::find(News::COLLECTION,array("source" => array(
                                                            "insertOrign" =>    "costum",
                                                            "keys"         =>    array(
                                                                                  $value["slug"]),
                                                            "key"          =>  $value["slug"])));
            }

          if (isset($resNews)) {
            $params = array(
                "result" => true
            );
            $results  = self::createResultNews($resNews);
            return array_merge($params,$results);
          }
        }
        return $results;
     }

   private static function createResultNews($params){

       $res["element"] = array();
       foreach($params as $key => $value){

           array_push($res["element"], array(
               "id"               => (String) $value["_id"],
               "type"             =>  $value["type"],
               "date"             =>  date(DateTime::ISO8601, $value["date"]->sec),
               "text"             =>  $value["text"],
               "author"           =>  $value["author"],
               "sharedBy"         =>  $value["sharedBy"],
               "target"           =>  $value["target"],
               "created"           => date(DateTime::ISO8601, $value["created"]->sec),
               "scope"             =>  $value["scope"]
           ));
       }
       // var_dump($res);
    return $res;
  }

  private static function getDataEventCommunity($data){
        $params = array(
             "result" => false
         );

        $elements = [];
        $tabres = [];

        foreach ($data as $k => $v) {
          $elements[$k] = Element::getElementSimpleById($k, $v["type"], null, array("name", "profilImageUrl","profilRealBannerUrl","links","slug"));

          $tabres += PHDB::find(Event::COLLECTION,array(
                                                          "source"    => array(
                                                          "insertOrign" =>    "costum",
                                                          "keys"         =>    array(
                                                                              $elements[$k]["slug"]),
                                                          "key"          =>  $elements[$k]["slug"])));
        }

        if (isset($tabres)) {
            $params = array(
                "result" => true
            );
            
          $results  = self::createResultEventCommunity($tabres);

          return array_merge($params,$results);
        }
        return $results;
     }

     private static function createResultEventCommunity($params){

       $res["element"] = array();
       foreach($params as $key => $value){

        $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
        $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
        $resume = (@$value["shortDescription"] ? $value["shortDescription"] : "");

           array_push($res["element"], array(
               "id"               => (String) $value["_id"],
               "name"             =>  $value["name"],
               "startDate"        =>  date(DateTime::ISO8601, $value["startDate"]->sec),
               "type"             =>  Yii::t("event",$value["type"]),
               "sourceKey"        =>  $value["source"]["keys"],
               "imgMedium"        =>  $imgMedium,
               "img"              =>  $img,
               "resume"           =>  $resume,
               "slug"             =>  $value["slug"]
           ));
       }
    return $res;
  }

     private static function getDataCommunity($data){
        $params = array(
             "result" => false
         );

        $elements = [];

        foreach ($data as $k => $v) {
          $elements[$k] = Element::getElementSimpleById($k, $v["type"], null, array("_id","name", "profilImageUrl","profilRealBannerUrl","links","slug", "address","geo", "geoPosition", "profilThumbImageUrl", "profilMarkerImageUrl","type" )); 
        }
        // var_dump($elements);

        if (isset($elements)) {
            $params = array(
                "result" => true
            );
            
          $results  = self::createResultCommunity($elements);
          return array_merge($params,$results);
        }
        return $results;
     }

   private static function createResultCommunity($params){

       $res["elt"] = array();

       foreach($params as $key => $value){
        // var_dump($key);
        $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
        $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
        $imgBanner = (@$value["profilRealBannerUrl"] ? $value["profilRealBannerUrl"] : "none");
        $countActus = PHDB::count(News::COLLECTION,array("source.key" => $value["slug"]));

           $res["elt"][$key] = array(
               "id"               => (String) $value["_id"],
               "name"             =>  $value["name"],
               "imgMedium"        =>  $imgMedium,
               "img"              =>  $img,
               "imgBanner"        =>  $imgBanner,
               "type"             => $value["type"],
               "profilMarkerImageUrl" => @$value["profilMarkerImageUrl"],
               "address"          =>  @$value["address"],
               "geo"              =>  @$value["geo"],
               "geoPosition"      =>  @$value["geoPosition"],
               "profilThumbImageUrl" => @$value["profilThumbImageUrl"],
               "slug"             =>  $value["slug"],  
               "countEvent"       =>  count(@$value["links"]["events"]),
               "countActeurs"     =>  count(@$value["links"]["members"]),
               "countProjet"      =>  count(@$value["links"]["projects"]),
               "countActus"       =>  @$countActus
           );
        }
        // var_dump($res);
    return $res;
  }
}
?>