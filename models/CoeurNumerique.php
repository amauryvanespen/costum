<?php 
class CoeurNumerique{
	const COLLECTION = "costum";
	const CONTROLLER = "costum";
    const MODULE = "costum";
    
    protected static $source = array(
        "insertOrign" =>    "costum",
        "keys"         =>    array(
                            "coeurNumerique"),
        "key"          =>  "coeurNumerique");

   public static function getEvent(){
       $params = array(
           "result" => false
       );
       date_default_timezone_set('UTC');

       $date_array = getdate ();
       $numdays = $date_array["wday"];

    //    $startDate = strtotime(date("Y-m-d", time() - ($numdays * 24*60*60)));
       $endDate = strtotime(date("Y-m-d H:i", time() + ((30 - $numdays) * 24*60*60)));
       $startDate = strtotime(date("Y-m-d H:i"));

    //    var_dump(date(DateTime::ISO8601, $endDte));
    
       
       $where = array(
            "source" => self::$source,
            "startDate"   =>  array('$gte'   =>  new MongoDate($startDate)),
            "endDate"     =>  array('$lt'    => new MongoDate($endDate))
            );
    
    // $where = array(
    //     "source"    =>  self::$source
    // );

        //  var_dump($where);exit;

     $allEvent = PHDB::findAndLimitAndIndex(Event::COLLECTION, $where,3);
       
        // var_dump($allEvent);exit;

       if(@$allEvent){
           //WHAT ENCORE UN EL FAMOSO TABLEAU
           $res = array();
           
           $params = array(
               "result" =>  true
           );

           $res = self::createResultEvent($allEvent);
           return array_merge($params,$res);
       }

       return $params;
   }

   private static function createResultEvent($params){

       $res["element"] = array();
       $typesList=Event::$types;
       foreach($params as $key => $value){

        $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
        $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
        $resume = (@$value["shortDescription"] ? $value["shortDescription"] : "");

           array_push($res["element"], array(
               "id"               => (String) $value["_id"],
               "name"             =>  $value["name"],
               "startDate"        => date(DateTime::ISO8601, $value["startDate"]->sec),
               "type"             =>  Yii::t("category",$typesList[$value["type"]]),
               "imgMedium"        =>  $imgMedium,
               "img"              =>  $img,
               "resume"           =>  $resume,
               "slug"             =>  $value["slug"]
           ));
       }
    //    var_dump($res);
       return $res;
   }

   public static function galery($post){
        $allDocument = PHDB::find(Document::COLLECTION,array("id" => $post["id"]));

        $res["element"] = array();
        foreach($allDocument as $key => $value){
           array_push($res["element"], array(
                                "name"      =>  $value["name"],
                                "doctype"   =>  $value["doctype"],
                                "folder"    =>  $value["folder"],
                                "type"      =>  $value["type"]));
        }

        return $res;
   }
}