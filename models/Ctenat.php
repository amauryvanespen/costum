<?php

class Ctenat {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";   
	
    const CATEGORY_CTER = "cteR";
    const CATEGORY_FICHEACTION = "ficheAction";

    const STATUT_CTER_LAUREAT = "Territoire lauréat";
    const STATUT_CTER_SIGNE = "CTE Signé";
    const STATUT_CTER_CANDIDAT =  "Territoire Candidat";
    const STATUT_CTER_REFUSE = "Territoire Candidat refusé"; 
    const STATUT_CTER_COMPLET = "Dossier Territoire Complet";

    const STATUT_ACTION_COMPLETED = "Action Réalisée";
    const STATUT_ACTION_VALID = "Action validée";
    const STATUT_ACTION_CONTRACT = "Passage en réunion de finalisation";
    const STATUT_ACTION_MATURATION = "Action en maturation";
    const STATUT_ACTION_LAUREAT = "Action lauréate";
    const STATUT_ACTION_REFUSE = "Action refusée";
    const STATUT_ACTION_CANDIDAT = "Action Candidate";
    

    public static $validCter = [ self::STATUT_CTER_LAUREAT,
                                  self::STATUT_CTER_SIGNE];
                                  
    public static $validActionStates = [ self::STATUT_ACTION_VALID,
                                          self::STATUT_ACTION_COMPLETED,
                                          self::STATUT_ACTION_CONTRACT ];

	public static $COLORS = [
        "#0A2F62",
        "#0B2D66",
        "#064191",
        "#2C97D0",
        "#16A9B1",
        "#0AA178",
        "#74B976",
        "#0AA178",
        "#16A9B1",
        "#2A99D1",
        "#064191",
        "#0B2E68"
        ];

    public static $FINANCERS = [ 
        "Communes / intercommunalités / Syndicats mixtes",
        "Départements",
        "Régions",
        "Europe",
        "Etat",   
        "Privé"];
    public static $financerTypeList = [
        "acteursocioeco" => "Acteur Socio-économique",
        "colfinanceur"  => "Communes / intercommunalités / Syndicats mixtes",
        "departement"   => "Départements",
        "region"        => "Région",
        "europe"=>"Europe",
        "etat" => "Etat - Services déconcentrés / préfecture",
        "ademe" => "Etat - Ademe",
        "cerema" => "Etat - Cerema",
        "bdt"=> "Etat - Banque des territoires",
        "bpi"=> "Etat - BPI",
        "agenceLeau"=> "Etat - Agence / Office de l'eau",
        "officeNatForet"=>"Etat - Office national des forêts",
        "agenceBiodiv"=> "Etat - Office de la biodiversité",
        "afd"=> "Etat - Agence Francaise de développement",
        "vn2f"=>"Etat - Voies navigable de France",
        "franceAgirMer"=>"Etat - FranceAgriMer",
        "autre" => "Etat - Autre"
    ];
    public static $financeurTypePublic = ["colfinanceur","departement","region" ,"europe","etat","ademe","cerema","bdt","bpi","agenceLeau","officeNatForet","agenceBiodiv","afd","vn2f","franceAgirMer"];


    public static $listValid = array(
        "valid" => "Validé sans réserves",
        "validReserve" => "Validé avec réserves",
        "notValid" => "Non validé" );

    public static $dataBinding_allProject  = [
        "id"     => ["valueOf" => "id"],
        "type"     => "Project",
        "name"      => ["valueOf" => "name"],
		"siren"     => ["valueOf" => "siren"],
		"why"     => ["valueOf" => "why"],
		"nbHabitant"     => ["valueOf" => "nbHabitant"],
		"dispositif"     => ["valueOf" => "dispositif"],
		"planPCAET"     => ["valueOf" => "planPCAET"],
		"singulartiy"     => ["valueOf" => "singulartiy"],
		"caracteristique"     => ["valueOf" => "caracteristique"],
		"actions"     => ["valueOf" => "actions"],
		"economy"     => ["valueOf" => "economy"],
		"inCharge"     => ["valueOf" => "inCharge"],
		"autreElu"     => ["valueOf" => "autreElu"],
		"contact"     => ["valueOf" => "contact"],
        "url"       => ["valueOf" => [
		                // "pdf"           => [   "valueOf"   => '_id.$id', 
                                //                                 "type"  => "url", 
                                //                                 "prefix"   => "/co2/export/pdfelement/type/projects/id/",
                                //                                 "suffix"   => "" ],
                                "website"       => [   "valueOf" => 'url']]],
        "address"   => ["parentKey"=>"address", 
                             "valueOf" => [
                                    "@type"             => "PostalAddress", 
                                    "streetAddress"     => ["valueOf" => "streetAddress"],
                                    "postalCode"        => ["valueOf" => "postalCode"],
                                    "addressLocality"   => ["valueOf" => "addressLocality"],
                                    "codeInsee"         => ["valueOf" => "codeInsee"],
                                    "addressRegion"     => ["valueOf" => "addressRegion"],
                                    "addressCountry"    => ["valueOf" => "addressCountry"]
                                    ]],
        "startDate"     => ["valueOf" => "startDate"],
        "endDate"       => ["valueOf" => "endDate"],
        "geo"   => ["parentKey"=>"geo", 
                             "valueOf" => [
                                    "@type"             => "GeoCoordinates", 
                                    "latitude"          => ["valueOf" => "latitude"],
                                    "longitude"         => ["valueOf" => "longitude"]
                                    ]],

    ];

    public static $dataHead_allProject  = [
		"type"     => "Type",
		"name"      => "Nom",
		"siren"     => "Siren",
		"why"     => "Pourquoi souhaitez-vous réaliser un CTE ? Quelles sont vos attentes vis-à-vis d’un CTE ?",
		"nbHabitant"     => "Nombre d'habitants concernés?",
		"dispositif"     => "Quels sont les dispositifs actuellement mis en place sur tout ou partie du territoire (contractuels et/ou en lien avec la transition écologique) ?",
		"planPCAET"     => "Avez élaboré un Plan climat-air-énergie territorial (PCAET) sur votre territoire ?",
		"singulartiy"     => "Qu'est ce qui constitue la singularité de votre territoire ? Quel est le contexte économique et quels sont ses enjeux en termes de transition écologique ?",
		"caracteristique"     => "Quelles sont les caractéristiques de votre territoire sur lesquelles vous agissez/vous souhaiteriez agir dans le cadre du CTE ? (exemples : problèmes majeurs/risques auxquels vous êtes confrontés, thèmes sur lesquels vous vous êtes engagés à agir)",
		"actions"     => "Quelles sont les actions envisagées dans votre projet de CTE ? Décrivez au moins 3 pistes d’actions.",
		"economy"     => "Quels acteurs socio-économiques prévoyez-vous de mobiliser pour le portage d’actions du CTE ?",
		"inCharge"     => "Qui serait en charge du projet au sein de la collectivité (chargé de mission, équipe dédiée, élu...) ?",
		"autreElu"     => "Autres élus engagés politiquement dans la réussite du projet (député, sénateur, maires) ?",
		"contact"     => "Nom du référent local, Adresse postale, numéro de téléphone et adresse mail ?",
		// "url.communecter"     => "Url ctenat",
		// "url.pdf"     => "PDF",
		"url.website"     => "Site internet",
		"address.streetAddress"     => "Rue",
		"address.postalCode"     => "Code Postal",
		"address.addressLocality"     => "Ville",
		"address.codeInsee"     => "Insee",
		"address.addressCountry"     => "Code Pays",
		"geo.latitude"     => "Latitude",
		"geo.longitude"     => "Longitude",

    ];

    public static $dataBinding_allPoi  = [
        "id"     => ["valueOf" => "id"],
        "name"      => ["valueOf" => "name"],
        "nameLong" => ["valueOf" => "nameLong"],
        "type" => ["valueOf" => "type"],
        "echelleApplication" => ["valueOf" => "echelleApplication"],
        "definition" => ["valueOf" => "definition"],
        "methodeCalcul" => ["valueOf" => "methodeCalcul"],
        "declinaison1" => ["valueOf" => "declinaison1"],
        "declinaison2" => ["valueOf" => "declinaison2"],
        "declinaison3" => ["valueOf" => "declinaison3"],
        "declinaison4" => ["valueOf" => "declinaison4"],
        "unity1" => ["valueOf" => "unity1"],
        "unity2" => ["valueOf" => "unity2"],
        "unity3" =>["valueOf" => "unity3"],
        "perimetreGeo" => ["valueOf" => "perimetreGeo"],
        "periode" => ["valueOf" => "periode"],
        "sourceData1" => ["valueOf" => "sourceData1"],
        "sourceData2" => ["valueOf" => "sourceData2"],
        "sourceData3" => ["valueOf" => "sourceData3"],
        "origin.citykeys" => ["valueOf" => "origin.citykeys"],
        "origin.ISO" => ["valueOf" => "origin.ISO"],
        "origin.RFSC" => ["valueOf" => "origin.RFSC"],
        "origin.ecoCite" => ["valueOf" => "origin.ecoCite"],
        "origin.CTE" => ["valueOf" => "origin.CTE"],
        "origin.other" => ["valueOf" => "origin.other"],
        "domainAction" => ["valueOf" => "domainAction"],
        "objectifDD" => ["valueOf" => "objectifDD"]
    ];

    public static $dataHead_allPoi  = [
        "id"     => "id",
        "name"      => "name",
        "nameLong" => "nameLong",
        "type" => "type",
        "echelleApplication" => "echelleApplication",
        "definition" => "definition",
        "methodeCalcul" => "methodeCalcul",
        "declinaison1" => "declinaison1",
        "declinaison2" => "declinaison2",
        "declinaison3" => "declinaison3",
        "declinaison4" => "declinaison4",
        "unity1" => "unity1",
        "unity2" => "unity2",
        "unity3" => "unity3",
        "perimetreGeo" => "perimetreGeo",
        "periode" => "periode",
        "sourceData1" => "sourceData1",
        "sourceData2" => "sourceData2",
        "sourceData3" => "sourceData3",
        "origin.citykeys" => "origin.citykeys",
        "origin.ISO" => "origin.ISO",
        "origin.RFSC" => "origin.RFSC",
        "origin.ecoCite" => "origin.ecoCite",
        "origin.CTE" => "origin.CTE",
        "origin.other" => "origin.other",
        "domainAction" => "domainAction",
        "objectifDD" => "objectifDD"
    ];
	
    public static function prepData($params){
		if($params["collection"] == Project::COLLECTION 
			&& isset($params["category"]) && $params["category"]=="cteR"){
			$proj=PHDB::findOne(Project::COLLECTION, array("_id"=> new MongoId($params["id"])));
			$status=(!isset($proj["source"]) || !isset($proj["source"]["status"])|| empty($proj["source"]["status"]) || empty($proj["source"]["status"]["ctenat"])) ? array("ctenat" => "Territoire Candidat") : array("ctenat" => $proj["source"]["status"]["ctenat"]);
				$params["source"]["status"] = $status;
		}
		return $params;
    }

    public static function elementBeforeSave($params){
    	//var_dump("elementBeforeSave");
        $elt=Element::getElementById($params["id"], $params["collection"], null, array("links", "category"));
        if($params["collection"]==Project::COLLECTION && isset($elt["category"]) && $elt["category"]=="cteR"){
            if(isset($elt["links"]["contributors"])){
                foreach($elt["links"]["contributors"] as $k => $v){
                    if(isset($v["roles"]) && in_array("Porteur du CTE", $v["roles"]) && isset($elt["links"]) && isset($elt["parent"][$k])){
                        Link::disconnect($k, $v["type"],$data["id"], $data["collection"], Yii::app()->session["userId"], "projects");
                        Link::disconnect($data["id"], $data["collection"], $k, $v["type"], Yii::app()->session["userId"], "contributors");
                    }
                }
            }
            if(isset($data["params"]["parent"]) && !empty($data["params"]["parent"])){
                foreach($data["params"]["parent"] as $k => $v){
                    Link::connect($data["id"], $data["collection"], $k, $v["type"], Yii::app()->session["userId"], "contributors",false,false,false,false, ["Porteur du CTE"]);
                    Link::connect($k, $v["type"], $data["id"], $data["collection"], Yii::app()->session["userId"], "projects",false,false,false, false, ["Porteur du CTE"]);
                }
            }
        }
		if( !empty($params["collection"]) && 
			$params["collection"] == Badge::COLLECTION && 
			!empty($params["id"]) && 
			!empty($params["elt"]) && 
			!empty($params["elt"]["name"]) && 
            !empty($params["elt"]["category"]) && $params["elt"]["category"] != "strategy"){
			$oldBadge = PHDB::findOneById($params["collection"], $params["id"]);
			if(!empty($oldBadge) && !empty($oldBadge["name"])){
				$where =  array("source.key" => "ctenat",
								"tags" => array('$in' => array($oldBadge["name"]) ) ) ;
				$projects = PHDB::find(Project::COLLECTION, $where, array("name", "tags"));
				if(!empty($projects)){
					foreach ($projects as $key => $project) {
						if(!empty($project["tags"])){
							//unset($project["tags"][array_search($oldBadge["name"], $project["tags"])]);
							//array_push($project["tags"], $params["elt"]["name"]);
                            //$project["tags"][] = $params["elt"]["name"];


                            $tagA = array();
                            foreach ($project["tags"] as $keyTA => $valTA) {
                                if( $valTA != $oldBadge["name"] && !empty($valTA) )
                                    $tagA[] = $valTA;
                            }
                            $tagA[] = $params["elt"]["name"];

							PHDB::update(Project::COLLECTION,
								array("_id"=>new MongoId($key)) , 
								array('$set' => array("tags" => $tagA))
							);
						}
					}
				}

                $whereA =  array("source.key" => "ctenat" ) ;
                $answers = PHDB::find("answers", $whereA, array("answers", "formId") );
                //Rest::json($answers); exit;
                if(!empty($answers)){

                    foreach ($answers as $keyA => $answer) {
                        if( !empty($answer["answers"]) &&
                            !empty($answer["formId"]) && 
                            !empty($answer["answers"][$answer["formId"]]) && 
                            !empty($answer["answers"][$answer["formId"]]["answers"]) && 
                            !empty($answer["answers"][$answer["formId"]]["answers"]["caracter"]) ) {

                            //Rest::json($c); exit;
                            //unset($project["tags"][array_search($oldBadge["name"], $project["tags"])]);
                            $c = $answer["answers"][$answer["formId"]]["answers"]["caracter"];
                            if(!empty($c["actionPrincipal"]) && $c["actionPrincipal"] == $oldBadge["name"])
                                $c["actionPrincipal"] = $params["elt"]["name"] ;

                            if(!empty($c["actionSecondaire"]) && array_search($oldBadge["name"], $c["actionSecondaire"]) !== false ) {
                                $aS = array();
                                foreach ($c["actionSecondaire"] as $keyDA => $valDA) {
                                    if( $valDA != $oldBadge["name"] && !empty($valDA) )
                                        $aS[] = $valDA;
                                }
                                $aS[] = $params["elt"]["name"];
                                $c["actionSecondaire"] = $aS ;
                            }

                            if(!empty($c["cibleDDPrincipal"]) && $c["cibleDDPrincipal"] == $oldBadge["name"])
                                $c["cibleDDPrincipal"] = $params["elt"]["name"] ;

                            if(!empty($c["cibleDDSecondaire"]) && array_search($oldBadge["name"], $c["cibleDDSecondaire"]) !== false ) {
                                $aS = array();
                                foreach ($c["cibleDDSecondaire"] as $keyDA => $valDA) {
                                    if( $valDA != $oldBadge["name"] && !empty($valDA) )
                                        $aS[] = $valDA;
                                }
                                $aS[] = $params["elt"]["name"];
                                $c["cibleDDSecondaire"] = $aS ;
                            }
                            //Rest::json($c); exit;
                            $answer["answers"][$answer["formId"]]["answers"]["caracter"] = $c;
                            PHDB::update("answers",
                                array("_id"=>new MongoId($keyA)) , 
                                array('$set' => array("answers"=> $answer["answers"]))
                            );
                        }
                    }
                }
                $whereI =  array("source.key" => "ctenat",
                                '$or' => array(
                                    array( "domainAction" => array('$in' => array($oldBadge["name"]) ) ),
                                    array( "objectifDD" => array('$in' => array($oldBadge["name"]) ) )
                                ) ) ;
                
                $indicators = PHDB::find(Poi::COLLECTION, $whereI, array("name", "domainAction", "objectifDD"));
                
                if(!empty($indicators)){
                    foreach ($indicators as $key => $poi) {
                        if(isset($poi["domainAction"]) && !empty($poi["domainAction"]) && array_search($oldBadge["name"], $poi["domainAction"]) !== false ) {
                            //unset($poi["domainAction"][array_search($oldBadge["name"], $poi["domainAction"])]);

                            // $dA = array_splice($poi["domainAction"], array_search($oldBadge["name"], $poi["domainAction"])+1, -1);
                            $dA = array();
                            foreach ($poi["domainAction"] as $keyDA => $valDA) {
                                if( $valDA != $oldBadge["name"] && !empty($valDA) )
                                    $dA[] = $valDA;
                            }
                            $dA[] = $params["elt"]["name"];

                            // $dA = array_splice($poi["domainAction"], array_search("Connaissance et protection de la biodiversité et milieux naturels", $poi["domainAction"]), -1);
                            
                            //array_push($poi["domainAction"], $params["elt"]["name"]);
                            //$poi["domainAction"][] = $params["elt"]["name"];
                            
                            PHDB::update(Poi::COLLECTION,
                                array("_id"=>new MongoId($key)) , 
                                array('$set' => array("domainAction" => $dA))
                            );
                        } else if(isset($poi["objectifDD"]) && !empty($poi["objectifDD"]) && array_search($oldBadge["name"], $poi["objectifDD"]) !== false ){
                            // unset($poi["objectifDD"][array_search($oldBadge["name"], $poi["objectifDD"])]);
                            // $poi["objectifDD"][] = $params["elt"]["name"];
                            $oD = array();
                            foreach ($poi["objectifDD"] as $keyOd => $valOd) {
                                if( $valOd != $oldBadge["name"] && !empty($valOd) )
                                    $oD[] = $valOd;
                            }
                            $oD[] = $params["elt"]["name"];
                            PHDB::update(Poi::COLLECTION,
                                array("_id"=>new MongoId($key)) , 
                                array('$set' => array("objectifDD" => $oD))
                            );
                        }
                    }

                }
			}
		}
        //exit;
		return $params;
    }

    public static function init($params){
        $params["rolesCTE"]=array(
            "adminCTENat"=>false, 
            "partnerCTENat" => false, 
            "referentCTENat"=>array(), 
            "adminCTER"=>array(),
            "partnerCTER"=>array(),
            "referentCTER"=>array(),
            "adminAction"=>array(),
            "partnerAction"=>array(),
            "referentAction"=>array()
        );

        $params["listStatutAction"]=array(
            Ctenat::STATUT_ACTION_COMPLETED => Ctenat::STATUT_ACTION_COMPLETED,
            Ctenat::STATUT_ACTION_VALID => Ctenat::STATUT_ACTION_VALID,
            Ctenat::STATUT_ACTION_CONTRACT => Ctenat::STATUT_ACTION_CONTRACT,
            //Ctenat::STATUT_ACTION_MATURATION,
            Ctenat::STATUT_ACTION_MATURATION => Ctenat::STATUT_ACTION_MATURATION,
            Ctenat::STATUT_ACTION_REFUSE => Ctenat::STATUT_ACTION_REFUSE,
            Ctenat::STATUT_ACTION_CANDIDAT => Ctenat::STATUT_ACTION_CANDIDAT,
            Ctenat::STATUT_ACTION_CONTRACT => Ctenat::STATUT_ACTION_CONTRACT
        );

        if(isset(Yii::app()->session["userId"]) && !empty(Yii::app()->session["userId"])){
            $links=Element::getElementById(Yii::app()->session["userId"], Person::COLLECTION, null, array("links"));
            $memberOf=(isset($links["links"]["memberOf"])) ? $links["links"]["memberOf"] : array();
            $infos=array("name", "profilThumbImageUrl", "profilMediumImageUrl", "slug", "why", "description", "links");
            if(isset($params["isMember"]) && $params["isMember"]){
                if(isset($params["isCostumAdmin"]) && $params["isCostumAdmin"]){
                    $params["rolesCTE"]["adminCTENat"]=true;
                  //  return $params;
                }else if(!empty($params["communityLinks"]) && isset($params["communityLinks"]["members"])){
                    foreach($params["communityLinks"]["members"] as $e => $v){
                        if(isset($v["roles"]) && in_array("Partenaire", $v["roles"])){
                            if($e==Yii::app()->session["userId"] && !isset($v["toBeValidated"])){
                                $params["rolesCTE"]["partnerCTENat"]=true; break;          
                            }
                            if($v["type"]==Organization::COLLECTION 
                                && isset($memberOf[$e]) && !isset($memberOf[$e]["toBeValidated"])
                                    && isset($memberOf[$e]["roles"]) && in_array("Référent CTE", $memberOf[$e]["roles"]))
                                    $params["rolesCTE"]["referentCTENat"][$e]=Element::getElementById($e, $v["type"], null, $infos);
                        }
                    }
                }
            }
            $whereAdminCTER=array(
               "category"=>"cteR",
                "links.contributors.".Yii::app()->session["userId"]=>array('$exists'=>true),
                "links.contributors.".Yii::app()->session["userId"].".toBeValidated"=>array('$exists'=>false),
                "links.contributors.".Yii::app()->session["userId"].".isAdminPending"=>array('$exists'=>false),
                "links.contributors.".Yii::app()->session["userId"].".isAdmin"=>array('$exists'=>true)
            );
            $params["rolesCTE"]["adminCTER"]=PHDB::find(Project::COLLECTION, $whereAdminCTER, $infos);
            
            $wherePartnerCTER=array(
                "category"=>"cteR",
                "links.contributors.".Yii::app()->session["userId"]=>array('$exists'=>true),
                "links.contributors.".Yii::app()->session["userId"].".toBeValidated"=>array('$exists'=>false),
                "links.contributors.".Yii::app()->session["userId"].".roles"=>array('$in'=>["Partenaire"])
            );
            $params["rolesCTE"]["partnerCTER"]=PHDB::find(Project::COLLECTION, $wherePartnerCTER, $infos);
            $whereAdminAction=array(
                "category"=>"ficheAction",
                "links.contributors.".Yii::app()->session["userId"]=>array('$exists'=>true),
                "links.contributors.".Yii::app()->session["userId"].".toBeValidated"=>array('$exists'=>false),
                "links.contributors.".Yii::app()->session["userId"].".isAdminPending"=>array('$exists'=>false),
                "links.contributors.".Yii::app()->session["userId"].".isAdmin"=>array('$exists'=>true)
            );
            $params["rolesCTE"]["adminAction"]=PHDB::find(Project::COLLECTION, $whereAdminAction, $infos);
            $wherePartnerAction=array(
                "category"=>"ficheAction",
                "links.contributors.".Yii::app()->session["userId"]=>array('$exists'=>true),
                "links.contributors.".Yii::app()->session["userId"].".toBeValidated"=>array('$exists'=>false),
                "links.contributors.".Yii::app()->session["userId"].".roles"=>array('$in'=>["Partenaire"])
            );
            $params["rolesCTE"]["partnerAction"]=PHDB::find(Project::COLLECTION, $wherePartnerAction, $infos);
            // CHECK si l'uilisateur est membre d'une action ou d'un cteR celui qui l'ui ouvre les vue sur la grande communauté du CTE
            $where=array(
                '$and'=>array(
                    array('$or'=>array(
                        array("category"=>"cteR"), 
                        array("category"=>"ficheAction")
                        )
                    ),
                    array("links.contributors.".Yii::app()->session["userId"]=>array('$exists'=>true)),
                    array("links.contributors.".Yii::app()->session["userId"].".toBeValidated"=>array('$exists'=>false))
            ));
    		$project=PHDB::findOne(Project::COLLECTION, $where);
    		if(!empty($project))
    			$params["isMember"]=true;

    		if(!empty($memberOf)){
	    		$or=array();
                foreach($memberOf as $k => $v){
    				if(!isset($v["toBeValidated"])){
    					array_merge($or,  array("links.contributors.".$k => array('$exists'=>1)) );
    				    if(isset($v["roles"]) && in_array("Référent CTE", $v["roles"])){
                            $whereReferentPartnerAction=array(
                                "category"=>"ficheAction",
                                "links.contributors.".$k=>array('$exists'=>true),
                                "links.contributors.".$k.".toBeValidated"=>array('$exists'=>false),
                                "links.contributors.".$k.".roles"=>array('$in'=>["Partenaire"])
                            );
                            $params["rolesCTE"]["referentAction"]=PHDB::find(Project::COLLECTION, $whereReferentPartnerAction, $infos);
                            $wherePorteurAction=array(
                                "category"=>"ficheAction",
                                "links.contributors.".$k=>array('$exists'=>true),
                                "links.contributors.".$k.".toBeValidated"=>array('$exists'=>false),
                                "links.contributors.".$k.".roles"=>array('$in'=>["Porteur d'action"])
                            );
                            $porterAction=PHDB::find(Project::COLLECTION, $wherePorteurAction, $infos);
                            $whereReferentPartnerCTER=array(
                                "category"=>"cteR",
                                "links.contributors.".$k=>array('$exists'=>true),
                                "links.contributors.".$k.".toBeValidated"=>array('$exists'=>false),
                                "links.contributors.".$k.".roles"=>array('$in'=>["Partenaire"])
                            );
                            $params["rolesCTE"]["referentCTER"]=PHDB::find(Project::COLLECTION, $whereReferentPartnerCTER, $infos);
                            $wherePorteurCtE=array(
                                "category"=>"cteR",
                                "links.contributors.".$k=>array('$exists'=>true),
                                "links.contributors.".$k.".toBeValidated"=>array('$exists'=>false),
                                "links.contributors.".$k.".roles"=>array('$in'=>["Porteur ddu CTE"])
                            );
                            $porterCTE=PHDB::find(Project::COLLECTION, $wherePorteurCtE, $infos);
                            if(!empty($params["rolesCTE"]["referentCTER"]) 
                                || !empty($params["rolesCTE"]["referentAction"])
                                || !empty($porterCTE)
                                || !empty($porterAction)){
                                $partner=Element::getElementById($k, $v["type"], null, $infos);
                                $params["isMember"]=true;
                                if(!empty($params["rolesCTE"]["referentCTER"])){
                                    foreach($params["rolesCTE"]["referentCTER"] as $e => $v){
                                        $params["rolesCTE"]["referentCTER"][$e]["orgaPartner"]=array_merge($partner, array("id"=>$k));
                                    }
                                }
                                if(!empty($params["rolesCTE"]["referentAction"])){
                                    foreach($params["rolesCTE"]["referentAction"] as $e => $v){
                                        $params["rolesCTE"]["referentAction"][$e]["orgaPartner"]=array_merge($partner, array("id"=>$k));
                                    }
                                }
                                if(!empty($porterCTE)){
                                    foreach($porterCTE as $e => $v){
                                        $porterCTE[$e]["orgaPorter"]=array_merge($partner, array("id"=>$k));
                                    }
                                    $params["rolesCTE"]["adminCTER"]= array_merge($params["rolesCTE"]["adminCTER"], $porterCTE);
                                }
                                if(!empty($porterAction)){
                                    foreach($porterAction as $e => $v){
                                        $porterAction[$e]["orgaPorter"]=array_merge($partner, array("id"=>$k));
                                    }
                                    $params["rolesCTE"]["adminAction"]= array_merge($params["rolesCTE"]["adminAction"], $porterAction);
                                }
                            }
                        }
                    }
    			}
    			if($or && $params["isMember"]===false){
    				$where = array('$and'=>array(
    						'$or'=>array(array("category"=>"cteR"), array("category"=>"ficheAction")),
    						'$or'=>$or
    					)
    				);
    				$project=PHDB::findOne(Project::COLLECTION, $where);
    				if(!empty($project))
						$params["isMember"]=true;
	
    			}
	    	}
        }
    	return $params;
    }
    public static function canSeeAnswer($answer){
        $res = false;
        $answer=$answer["answer"];
        if($answer["answers"][$answer["formId"]]["answers"]["project"]){
                    $res = Authorisation::isElementMember( $answer["answers"][$answer["formId"]]["answers"]["project"]["id"]  , Project::COLLECTION, Yii::app()->session["userId"] );
       // if($res == false && $answer["answers"][$answer["formId"]]["answers"]["organization"]){
         //           $res = Authorisation::isElementMember( $answer["answers"][$answer["formId"]]["answers"]["organization"], Organization::COLLECTION, Yii::app()->session["userId"] );
        }
        return $res; 
    }
    public static function canEditAnswer($answer){
        $res = false;
        $answer=$answer["answer"];
        if($answer["answers"][$answer["formId"]]["answers"]["project"])
            $res = Authorisation::canEditItem(Yii::app()->session["userId"], Project::COLLECTION, $answer["answers"][$answer["formId"]]["answers"]["project"]["id"]  );
       
        return $res; 
    }
    public static function authorizedPanelAdmin(){
        return self::isElementMember(array("elementType"=>Yii::app()->session["costum"]["contextType"], "elementId"=>Yii::app()->session["costum"]["contextId"], "userId"=>Yii::app()->session["userId"]));
    }

    public static function isElementAdmin($params){
    	//echo "//".$params['elementId']."//";echo $params['elementType']; echo $params['userId'];exit;
    //	print_r($params); exit;
    	$elt=Element::getElementById( $params['elementId'], $params['elementType'], null, array("links", "parent", "category"));
    	if($params["elementType"]==Project::COLLECTION && isset($elt["category"]) && $elt["category"]=="ficheAction"){
    		if(isset($elt["links"]) && !empty($elt["links"]) && isset($elt["links"]["projects"])){
    			foreach($elt["links"]["projects"] as $k => $v){
    				$res=Authorisation::isElementAdmin($k, $v["type"], Yii::app()->session["userId"], true);
    				if(!empty($res))
                        return $res;

    			}
    		}
    	}
        if($params["elementType"]==Project::COLLECTION && isset($elt["links"]) && !empty($elt["links"]) && isset($elt["links"]["contributors"])){
            foreach($elt["links"]["contributors"] as $k => $v){
                if(isset($v["roles"]) && 
                    (($elt["category"]=="ficheAction" && in_array("Porteur d'action", $v["roles"])) 
                        || ($elt["category"]=="cteR" && in_array("Porteur du CTE", $v["roles"])))){
                    if($k==$params["userId"])
                        return true;
                    if($v["type"]==Organization::COLLECTION){
                        $orga=Element::getElementById( $k, $v["type"], null, array("links"));
                        if(isset($orga["links"]["members"]) 
                            && isset($orga["links"]["members"][$params["userId"]]) 
                                && ((isset($orga["links"]["members"][$params["userId"]]["isAdmin"]) && !isset($orga["links"]["members"][$params["userId"]]["isAdminPending"])) || (!isset($orga["links"]["members"][$params["userId"]]["toBeValidated"]) && isset($orga["links"]["members"][$params["userId"]]["roles"]) 
                                        && in_array("Référent CTE",$orga["links"]["members"][$params["userId"]]["roles"]))))
                            return true; 
                    }
                    if(!empty($res))
                        return $res;
                }
            }
        }
    	return false;
    }
    public static function isElementMember($params){
    	//echo "//".$params['elementId']."//";echo $params['elementType']; echo $params['userId'];exit;
    //	print_r($params); exit;

    	$elt=Element::getElementById( $params['elementId'], $params['elementType'], null, array("links", "parent", "category", "slug"));
    	// CHECK IF PARTENAIRE CTENAT
    	if($elt["slug"] != "ctenat"){
    		$cteNat=Element::getElementById( Yii::app()->session["costum"]["contextId"], Yii::app()->session["costum"]["contextType"], null, array("links", "parent", "category", "slug"));
    	}else{
    		$cteNat=$elt;
    	}
    	if(!empty($cteNat["links"]) && isset($cteNat["links"]["members"]) && !empty($cteNat["links"]["members"]) ){
    		foreach($cteNat["links"]["members"] as $k => $v){
    			if(isset($v["roles"]) && in_array("Partenaire",$v["roles"])){
	    			if($k==$params["userId"] && !isset($v["toBeValidated"]))
	    				return true;
	    			if($v["type"]==Organization::COLLECTION){
	    				$orga=Element::getElementById( $k, $v["type"], null, array("links"));
    						if(isset($orga["links"]["members"]) 
    							&& isset($orga["links"]["members"][$params["userId"]]) 
    								&& ((isset($orga["links"]["members"][$params["userId"]]["isAdmin"]) && !isset($orga["links"]["members"][$params["userId"]]["isAdminPending"])) || (!isset($orga["links"]["members"][$params["userId"]]["toBeValidated"]) && isset($orga["links"]["members"][$params["userId"]]["roles"]) 
    										&& in_array("Référent CTE",$orga["links"]["members"][$params["userId"]]["roles"]))))
    							return true; 
	    			}
    			}
    		}
    	}
        // CHECK IF PARTENAIRE CTER
    	if($params["elementType"]==Project::COLLECTION){
    		if(isset($elt["links"]) && !empty($elt["links"]) && isset($elt["links"]["contributors"])){
    			foreach($elt["links"]["contributors"] as $k => $v){
    				if(isset($v["roles"]) && in_array("Partenaire", $v["roles"])){
    					if($k==$params["userId"])
    						return true;
    					if($v["type"]==Organization::COLLECTION){
    						$orga=Element::getElementById( $k, $v["type"], null, array("links"));
                            //var_dump("iciiiii");var_dump($orga["links"]);var_dump($params["userId"]);exit;
    						if(isset($orga["links"]["members"]) 
    							&& isset($orga["links"]["members"][$params["userId"]]) 
    								&& ((isset($orga["links"]["members"][$params["userId"]]["isAdmin"]) && !isset($orga["links"]["members"][$params["userId"]]["isAdminPending"])) || (!isset($orga["links"]["members"][$params["userId"]]["toBeValidated"]) && isset($orga["links"]["members"][$params["userId"]]["roles"]) 
    										&& in_array("Référent CTE",$orga["links"]["members"][$params["userId"]]["roles"]))))
    							return true; 
    					}
    					if(!empty($res))
                        	return $res;
	    			}
	    		}
	    	}
    		if($params["elementType"]==Project::COLLECTION && isset($elt["category"]) && $elt["category"]=="ficheAction"){
    			if(!isset($params["checkParent"])){
		    		if(isset($elt["links"]) && !empty($elt["links"]) && isset($elt["links"]["projects"])){
		    			foreach($elt["links"]["projects"] as $k => $v){
		    				$res=self::isElementMember(array("elementId"=>$k, "elementType"=>$v["type"], "userId"=>Yii::app()->session["userId"], "checkParent" => true));
		    				if(!empty($res))
		                        return $res;

		    			}
		    		}
		    	}
	    	}
    	}
    	return false;
    }
    public static function elementAfterUpdate($data){
        $elt=Element::getElementById($data["id"], $data["collection"], null, array("name", "links", "category"));
        if($data["collection"]==Organization::COLLECTION){
            if( !empty($elt['links']['answers']) ){
                foreach ($elt['links']['answers'] as $keyA => $valA) {
                    $answersID = $keyA;
                    $answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answersID);
                    $cter=$answer["formId"];
                    if($data["id"]==$answer["answers"][$cter]["answers"]["organization"]["id"]){
                        $set = array("answers.".$cter.".answers.organization.name" => $elt["name"]);
                         //var_dump($set);
                        PHDB::update(Form::ANSWER_COLLECTION,
                                    array("_id" => new MongoId($answersID) ) , 
                                    array('$set' => $set)
                                    );
                    }
                }
            }
        }

        
    	self::elementAfterSave($data);
    }

    public static function elementAfterSave($data){
        //var_dump($data); exit;
    	$elt=Element::getElementById($data["id"], $data["collection"], null, array("links", "category", "type", 'parent'));
        // var_dump($data);
        if($data["collection"]==Poi::COLLECTION && $data["params"]["type"] = "indicator"){
            // var_dump("here 1");
            if( !empty($data["params"]['parent']) ){
                // var_dump("here 2");
                foreach ($data["params"]['parent'] as $keyP => $valP) {
                    // var_dump("here 3");
                    $parent=Element::getElementById($keyP, $valP["type"], null, array("links"));
                    
                    if(!empty($parent["links"]) && !empty($parent["links"]["answers"])){
                        // var_dump("here 4");
                        foreach ($parent["links"]["answers"] as $keyA => $valA) {
                            $answer=PHDB::findOneById("answers", $keyA,array("answers", "formId"));
                            // var_dump("here 5");
                            if(!empty($answer["answers"]) && 
                                !empty($answer["formId"])&& 
                                !empty($answer["answers"][$answer["formId"]]) && 
                                !empty($answer["answers"][$answer["formId"]]["answers"]) && 
                                !empty($answer["answers"][$answer["formId"]]["answers"]["murir"])) {
                                // var_dump("here 6");
                                    $r = array();
                                    if(!empty($answer["answers"][$answer["formId"]]["answers"]["murir"]["results"]))
                                        $r = $answer["answers"][$answer["formId"]]["answers"]["murir"]["results"];

                                    $r[] = array("indicateur" => $data["id"]);

                                    $answer["answers"][$answer["formId"]]["answers"]["murir"]["results"] = $r;
                                    PHDB::update(Form::ANSWER_COLLECTION,
                                        array("_id" => new MongoId($keyA)) , 
                                        array('$set' => array("answers" => $answer["answers"]))
                                    );
                            }
                        }
                    }
                }
            }
        }

        // Save orga in CTER and Link it
        if($data["collection"]==Organization::COLLECTION){
            if(!empty($data['postParams']) && !empty($data['postParams']['links']) && !empty($data['postParams']['links']['projects']) && !empty($data['postParams']["roles"])){
                $roles=(!empty($data['postParams']["roles"])) ?  explode(",", $data['postParams']["roles"]) : null;
                Link::connect($data['postParams']['links']['projects'], Project::COLLECTION, $data["id"], Organization::COLLECTION, Yii::app()->session["userId"], "contributors",false,false,false,false, $roles);
                            //links.projects rattaché le projet à l'orga porteuse 
                Link::connect($data["id"], Organization::COLLECTION, $data['postParams']['links']['projects'], Project::COLLECTION, Yii::app()->session["userId"], "projects",false,false,false, false, $roles);
            }
        }
    	if($data["collection"]==Project::COLLECTION && isset($elt["category"]) && $elt["category"]=="cteR"){
    		/*if(isset($elt["links"]["contributors"])){
    			foreach($elt["links"]["contributors"] as $k => $v){
    				if(isset($v["roles"]) && in_array("Porteur du CTE", $v["roles"]) && isset($elt["links"])){
    					Link::disconnect($k, $v["type"],$data["id"], $data["collection"], Yii::app()->session["userId"], "projects");
						Link::disconnect($data["id"], $data["collection"], $k, $v["type"], Yii::app()->session["userId"], "contributors");
    				}
    			}
    		}*/
    		if(isset($data["params"]["parent"]) && !empty($data["params"]["parent"])){
    			foreach($data["params"]["parent"] as $k => $v){
	    			Link::connect($data["id"], $data["collection"], $k, $v["type"], Yii::app()->session["userId"], "contributors",false,false,false,false, ["Porteur du CTE"]);
	    			Link::connect($k, $v["type"], $data["id"], $data["collection"], Yii::app()->session["userId"], "projects",false,false,false, false, ["Porteur du CTE"]);
    			}
    		}
    	}

        if($data["collection"]==Project::COLLECTION && isset($elt["category"]) && $elt["category"]=="ficheAction"){
            
            if( !empty($data['postParams']) && 
                !empty($data['postParams']['links']) && 
                !empty($data['postParams']['links']['answers']) ) {
                foreach ($data['postParams']['links']['answers'] as $keyA => $valA) {
                    //links.answers rattaché l'orga à la réponse
                    Link::connect($data["id"], Project::COLLECTION, $keyA, Form::ANSWER_COLLECTION, Yii::app()->session["userId"], "answers",false,false,false,false);
                }
                
            }

            if( !empty($data['postParams']) && 
                !empty($data['postParams']['links']) ){
                $answersID = null;
                $cter = null;
                $answer = null ;
                //links.contributors rattaché le projet action à l'auteur
                Link::connect($data["id"], Project::COLLECTION, Yii::app()->session["userId"], Person::COLLECTION, Yii::app()->session["userId"], "contributors",false,false,false,false, ["Créateur d'action"]);

                if( !empty($data['postParams']['links']['answers']) ){
                    foreach ($data['postParams']['links']['answers'] as $keyA => $valA) {
                        $answersID = $keyA;
                        //links.answers rattaché l'orga à la réponse
                        Link::connect($data["id"], Project::COLLECTION, $keyA, Form::ANSWER_COLLECTION, Yii::app()->session["userId"], "answers",false,false,false,false);
                    }
                }


                if( !empty($data['postParams']['links']['projects']) ){
                    foreach ($data['postParams']['links']['projects'] as $keyP => $valP) {
                        
                        $project =  PHDB::findOneById(Project::COLLECTION, $keyP, array("category", "name", "slug"));

                        if(!empty($project["category"]) && $project["category"] == "cteR"){
                            $cter = $project["slug"];
                            //links.projects rattaché le projet action au projet cter associé
                            Link::connect($data["id"], Project::COLLECTION, $keyP, Project::COLLECTION, Yii::app()->session["userId"], "projects",false,false,false,false, ["CTE Porteur"]);

                            //links.projects rattaché le projet action au projet cter associé
                            Link::connect($keyP, Project::COLLECTION, $data["id"], Project::COLLECTION,  Yii::app()->session["userId"], "projects",false,false,false,false, ["Action CTE"]);
                        }
                    }
                }


                if(!empty($data["id"]) && !empty($cter) && !empty($answersID)){
                    $set = array("answers.".$cter.".answers.project" => 
                                    array( "type" => $data["collection"], 
                                            "id" => $data["id"],
                                            "name" => $data["params"]["name"],
                                            "slug" => $data["params"]["slug"])) ;
                    //var_dump($set);
                    PHDB::update(Form::ANSWER_COLLECTION,
                                array("_id" => new MongoId($answersID) ) , 
                                array('$set' => $set)
                                );

                    $answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answersID, array("answers"));

                    if(!empty($answer["answers"]) &&
                        !empty($answer["answers"][$cter]) &&
                         !empty($answer["answers"][$cter]["answers"]) &&
                         !empty($answer["answers"][$cter]["answers"]["organization"]) &&
                         !empty($answer["answers"][$cter]["answers"]["organization"]["id"])  ){
                        
                        $keyO = $answer["answers"][$cter]["answers"]["organization"]["id"]; 
                        //links.contributors rattaché le projet action à l'organization
                        Link::connect($data["id"], Project::COLLECTION, $keyO, Organization::COLLECTION, Yii::app()->session["userId"], "contributors",false,false,false,false, ["Porteur d'action"]);
                        //links.projects rattaché le projet à l'orga porteuse 
                        Link::connect($keyO, Organization::COLLECTION, $data["id"], Project::COLLECTION, Yii::app()->session["userId"], "projects",false,false,false, false, ["Porteur d'action"]);
                    }
                }
                
            }
        }
        if($data["collection"]==Badge::COLLECTION && !empty($data['postParams'])
            && !empty($data['postParams']['links']) && !empty($data['postParams']['links']["projects"]) ){
            PHDB::update($data["collection"],
                            array("_id" => new MongoId($data["id"]) ) , 
                            array('$set' => array("links.projects"=>$data['postParams']['links']["projects"]))
                            );
        }

        if($data["collection"]==Organization::COLLECTION && !empty($data['postParams'])
            && !empty($data['postParams']['links']) ){

            $answersID = null;
            $cter = null;
            if( !empty($data['postParams']['links']['answers']) ){
                if(is_array($data['postParams']['links']['answers'])){
                    foreach ($data['postParams']['links']['answers'] as $keyA => $valA) {
                        //links.answers rattaché l'orga à la réponse
                        Link::connect($data["id"], Organization::COLLECTION, $keyA, Form::ANSWER_COLLECTION, Yii::app()->session["userId"], "answers",false,false,false,false);
                        $answersID = $keyA ;
                    }
                }else if(is_string($data['postParams']['links']['answers'])){
                    Link::connect($data["id"], Organization::COLLECTION, $data['postParams']['links']['answers'], Form::ANSWER_COLLECTION, Yii::app()->session["userId"], "answers",false,false,false,false);
                        $answersID = $data['postParams']['links']['answers'];
                }
            }


            if( !empty($data['postParams']['links']['projects']) ){
                if(is_array($data['postParams']['links']['projects'])){
                    foreach ($data['postParams']['links']['projects'] as $keyP => $valP) {
                        
                        $project =  PHDB::findOneById(Project::COLLECTION, $keyP, array("category", "name", "slug"));
                        if(!empty($project["category"]) && $project["category"] == "cteR")
                            $cter = $project["slug"];
                        
                    }
                }else if(is_string($data['postParams']['links']['projects'])){
                    $project =  PHDB::findOneById(Project::COLLECTION, $data['postParams']['links']['projects'], array("category", "name", "slug"));
                        if(!empty($project["category"]) && $project["category"] == "cteR")
                            $cter = $project["slug"];
                }
            }
            
            if(!empty($data["id"]) && !empty($cter) && !empty($answersID)){
                $set = array("answers.".$cter.".answers.organization" => 
                                array( "type" => $data["collection"], 
                                        "id" => $data["id"],
                                        "name" => $data["params"]["name"],
                                        "slug" => $data["params"]["slug"])) ;
                //var_dump($set);
                PHDB::update(Form::ANSWER_COLLECTION,
                            array("_id" => new MongoId($answersID) ) , 
                            array('$set' => $set)
                            );
            }
        }
    }

    public static function surveyAfterSave($data){
    	//var_dump("surveyAfterSave");
    	if($data["collection"]==Form::ANSWER_COLLECTION && 
            !empty($data["id"])
    		/* !empty($data["params"]) && 
    		!empty($data["params"]["answers"]) && 
    		!empty($data["params"]["answers"]["project"]) && 
    		!empty($data["params"]["answers"]["project"]["id"])&& 
    		!empty($data["params"]["answers"]["organization"]) && 
    		!empty($data["params"]["answers"]["organization"]["id"]) */ ){

            PHDB::update(   $data["collection"], 
                                        array("_id" => new MongoId($data["id"])), 
                                        array('$set' => array("priorisation" => "Action Candidate") ) );
            //$cterPorteur = Slug::getElementBySlug($data["params"]["formId"]);

    		//$pElt = $data["params"]["answers"]["project"];
    		//$elt=Element::getElementById($pElt["id"], Project::COLLECTION, null, array("links", "category"));
    		//$org = $data["params"]["answers"]["organization"];

            //links.contributors rattaché le projet action à l'organization
    		//Link::connect($pElt["id"], Project::COLLECTION, $org["id"], Organization::COLLECTION, Yii::app()->session["userId"], "contributors",false,false,false,false, ["Porteur d'action"]);

            // //links.projects rattaché le projet action au projet cter associé
            // Link::connect($pElt["id"], Project::COLLECTION, $cterPorteur["id"], $cterPorteur["type"], Yii::app()->session["userId"], "projects",false,false,false,false, ["CTE Porteur"]);

            // //links.projects rattaché le projet action au projet cter associé
            // Link::connect($cterPorteur["id"], $cterPorteur["type"],$pElt["id"], Project::COLLECTION,  Yii::app()->session["userId"], "projects",false,false,false,false, ["Action CTE"]);

            //links.contributors rattaché le projet action à l'auteur
            //Link::connect($pElt["id"], Project::COLLECTION, Yii::app()->session["userId"], Person::COLLECTION, Yii::app()->session["userId"], "contributors",false,false,false,false, ["Créateur d'action"]);

            //links.answers rattaché le projet à la réponse
            //Link::connect($pElt["id"], Project::COLLECTION, $data['id'], Form::ANSWER_COLLECTION, Yii::app()->session["userId"], "answers",false,false,false,false);
	    	
            //links.projects rattaché le projet à l'orga porteuse 
            //Link::connect($org["id"], Organization::COLLECTION, $pElt["id"], Project::COLLECTION, Yii::app()->session["userId"], "projects",false,false,false, false, ["Porteur d'action"]);
            
            //links.answers rattaché l'orga à la réponse
            //Link::connect($org["id"], Organization::COLLECTION, $data["id"], Form::ANSWER_COLLECTION, Yii::app()->session["userId"], "answers",false,false,false,false);
    		
    	}
    }

    public static  function importPreviewData($post, $notCheck=false){

        $params = array("result"=>false);
        $elements = array();
        $saveCities = array();
        $headFile = array();
        $nb = 0 ;
        $elementsWarnings = array();
        //Rest::json($post['file']); exit;
        
        if( !empty($post['file']) && !empty($post['nameFile']) && !empty($post['typeFile']) ){
            // $mapping = json_decode(file_get_contents("../../modules/co2/data/import/".Element::getControlerByCollection($post["typeElement"]).".json", FILE_USE_INCLUDE_PATH), true);
            if($post['typeFile'] == "csv"){
                $file = $post['file'];

                $headFile = $file[0];
                unset($file[0]);
            }elseif ((!isset($post['pathObject'])) || ($post['pathObject'] == "")) {
                $file = json_decode($post['file'][0], true);
            }else {
                $file = json_decode($post['file'][0], true);
                $file = @$file[$post["pathObject"]];
            }

            //Rest::json($headFile); exit;
            //Rest::json($file); exit;
            $source =  array("source.key" => "ctenat");
            $cteterList = array();
            $cteterListName = array();
            $cteterListNameErreur = array();

            $porteurTerList = array();
            $porteurTerName = array();
            $projectList = array();
            $addressList = array();
            
            $sourceU = array("insertOrign" => "import",
								"key" => "ctenat",
								"keys" => array("ctenat") );
            $bigboss = PHDB::findOne(Person::COLLECTION, array("email" => "antoine.daval@gmail.com"));
            $listType = array(
				"entreprise" =>	Organization::TYPE_BUSINESS,
				"agriculteur / groupements" =>	Organization::TYPE_BUSINESS,
				"Enseignement" =>	Organization::TYPE_GOV,
				"enseignement" =>	Organization::TYPE_GOV,
				"EPCI" =>	Organization::TYPE_GOV,
				"Préfecture" =>	Organization::TYPE_GOV,
				"préfecture" =>	Organization::TYPE_GOV,
				"DR" =>	Organization::TYPE_GOV,
				"ADEME" =>	Organization::TYPE_GOV,
				"bailleur" =>	Organization::TYPE_GOV,
				"Chambre consulaire / fédérations profesionnelles" =>	Organization::TYPE_GOV,
				"syndicat" =>	Organization::TYPE_NGO,
				"Syndicat" =>	Organization::TYPE_NGO,
				"VNF" =>	Organization::TYPE_GOV,
				"Commune"  =>	Organization::TYPE_GOV,
				"commune" =>	Organization::TYPE_GOV,
				"Groupement EPCI" =>	Organization::TYPE_GOV,
				"groupement EPCI" =>	Organization::TYPE_GOV,
				"association" =>	Organization::TYPE_NGO,
				"Association" =>	Organization::TYPE_NGO,
				"région" =>	Organization::TYPE_GOV,
				"Région" =>	Organization::TYPE_GOV,
				"établissement public" =>	Organization::TYPE_GOV,
				"Transport" =>	Organization::TYPE_GOV,
				"SCI" =>	Organization::TYPE_GOV,
				"SEM" =>	Organization::TYPE_BUSINESS,
				"département" =>	Organization::TYPE_GOV,
				"mixte" =>	Organization::TYPE_GOV,
				"Mixte" =>	Organization::TYPE_GOV,
				"DDT" =>	Organization::TYPE_GOV,
				"Pays" =>	Organization::TYPE_GOV,
				"groupement communes" =>	Organization::TYPE_GOV,
				"Département" =>	Organization::TYPE_GOV,
			);

            if(!empty($file)){
            	//Rest::json($file); exit;
            	foreach ($file as $keyFile => $vFile){
					$nb++;

					if(!empty($vFile)){
						//************* Gestion CTETER
						if( !isset($cteterListName[$vFile[0]]) ){

							$where = $source;
							$where["name"] = $vFile[0];
							$cteter = PHDB::findOne(Project::COLLECTION, $where);
							if(!empty($cteter)){
								$cteterListName[$vFile[0]] = (String)$cteter["_id"];
								$cteterList[(String)$cteter["_id"]] = $cteter;

								//************* Gestion address CTETER
								if(!empty($vFile[1])){
									$strA =$vFile[1].$vFile[2].$vFile[3].$vFile[4];
									if(empty($cteter["address"]) && !empty($strA)){
										if(!isset($addressList[$vFile[1].$vFile[2].$vFile[3].$vFile[4]])){
											$address = array(
												"addressCountry" => $vFile[1],
												"postalCode" => $vFile[2],
												"addressLocality" => $vFile[3],
												"streetAddress" => $vFile[4]
											);

											$addressList[$strA] = array();
											$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
								            if($detailsLocality["result"] == true){
								                $cteter["address"] = $detailsLocality["address"] ;
								                $cteter["geo"] = $detailsLocality["geo"] ;
								                $cteter["geoPosition"] = $detailsLocality["geoPosition"] ;

								                $addressList[$strA] = array(
								                	"address" => $detailsLocality["address"],
									                "geo" => $detailsLocality["geo"],
									                "geoPosition" => $detailsLocality["geoPosition"]
								                );

								                PHDB::update( Project::COLLECTION,
												    array("_id"=>new MongoId( (String)$cteter["_id"]) ), 
													array('$set' => array( "address" => $cteter["address"] ,
																			"geo" => $cteter["geo"] ,
																			"geoPosition" => $cteter["geoPosition"]) ) );
								            }
										}else if(!empty($addressList[$strA]["adresse"])){
											PHDB::update( Project::COLLECTION,
												    array("_id"=>new MongoId( (String)$cteter["_id"]) ), 
													array('$set' => array( "address" => $addressList[$strA]["address"] ,
																			"geo" => $addressList[$strA]["geo"] ,
																			"geoPosition" => $addressList[$strA]["geoPosition"]) ) );
										}
									}
								}
								
								// //************* Gestion porteur CTETER
								// if(!isset($porteurTerName[$vFile[5]]) && !empty($vFile[5]) ){
								// 	$whereP = $source;
								// 	$whereP["name"] = $vFile[5];
								// 	$porteurTer1 = PHDB::findOne(Organization::COLLECTION, $whereP);
								// 	if(!empty($porteurTer1)){
								// 		$porteurTerName[$vFile[5]] = (String)$porteurTer1["_id"];
								// 		$porteurTerList[(String)$porteurTer1["_id"]] = $porteurTer1;
								// 	}else{
								// 		$porteurTerName[$vFile[5]] = array();
								// 		$eltPorteurTer = array(	"name" => $vFile[5],
								// 								"type" => Organization::TYPE_GOV,
								// 								"collection" => Organization::COLLECTION,
								// 								"source" => $sourceU );
								// 		if(!empty($vFile[6])){
								// 			$strA =$vFile[6].$vFile[7].$vFile[8].$vFile[9];
								// 			if(!empty($strA)){
								// 				if(!isset($addressList[$strA])){
								// 					$address = array(
								// 						"addressCountry" => $vFile[6],
								// 						"postalCode" => $vFile[7],
								// 						"addressLocality" => $vFile[8],
								// 						"streetAddress" => $vFile[9]
								// 					);

								// 					$addressList[$strA] = array();
								// 					$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
								// 		            if($detailsLocality["result"] == true){
								// 						$eltPorteurTer["address"] = $detailsLocality["address"] ;
								// 						$eltPorteurTer["geo"] = $detailsLocality["geo"] ;
								// 						$eltPorteurTer["geoPosition"] = $detailsLocality["geoPosition"] ;

								// 						$addressList[$strA] = array(
								// 							"address" => $detailsLocality["address"],
								// 							"geo" => $detailsLocality["geo"],
								// 							"geoPosition" => $detailsLocality["geoPosition"]
								// 						);
								// 		            }
								// 				}else if(!empty($addressList[$strA]["adresse"])){
								// 					$eltPorteurTer["address"] = $addressList[$strA]["address"] ;
								// 	                $eltPorteurTer["geo"] = $addressList[$strA]["geo"] ;
								// 	                $eltPorteurTer["geoPosition"] = $addressList[$strA]["geoPosition"] ;
								// 				}
												
								// 			}
								// 		}
								// 		//Rest::json($eltPorteurTer); exit;
								// 		if(!empty($eltPorteurTer) && !empty($eltPorteurTer["name"]) )
								// 			$res = Element::save($eltPorteurTer);
								// 		else{
								// 			var_dump($vFile[5]);
								// 			var_dump($eltPorteurTer);
								// 		}
								// 		//
								// 		if(!empty($res) && !empty($res["result"]) && $res["result"] == true ){
								// 			$porteurTerName[$vFile[5]] = $res["id"];
								// 			$porteurTer1 = $res["map"];
								// 			$porteurTerList[$res["id"]] = $res["map"];
								// 		}
								// 	}
								// } else {
								// 	if(!empty($vFile[5]))
								// 		$porteurTerName[$vFile[5]] = array();
								// }

								// //************* Gestion porteur CTETER 2
								// if(!isset($porteurTerName[$vFile[12]]) ){
								// 	$whereP = $source;
								// 	$whereP["name"] = $vFile[12];
								// 	$porteurTer2 = PHDB::findOne(Organization::COLLECTION, $whereP);
								// 	if(!empty($porteurTer2)){
								// 		$porteurTerName[$vFile[12]] = (String)$porteurTer2["_id"];
								// 		$porteurTerList[(String)$porteurTer2["_id"]] = $porteurTer2;
								// 	}else if(!empty($vFile[12])){
								// 		$porteurTerName[$vFile[12]] = array();
								// 		$eltPorteurTer = array(	"name" => $vFile[12],
								// 								"type" => Organization::TYPE_GOV,
								// 								"collection" => Organization::COLLECTION,
								// 								"source" => $sourceU );
								// 		if(!empty($vFile[13])){
								// 			$strA =$vFile[13].$vFile[14].$vFile[15].$vFile[16];
								// 			if(!empty($strA)){
								// 				if(!isset($addressList[$strA])){
								// 					$address = array(
								// 						"addressCountry" => $vFile[13],
								// 						"postalCode" => $vFile[14],
								// 						"addressLocality" => $vFile[15],
								// 						"streetAddress" => $vFile[16]
								// 					);

								// 					$addressList[$strA] = array();
								// 					$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
								// 		            if($detailsLocality["result"] == true){
								// 						$eltPorteurTer["address"] = $detailsLocality["address"] ;
								// 						$eltPorteurTer["geo"] = $detailsLocality["geo"] ;
								// 						$eltPorteurTer["geoPosition"] = $detailsLocality["geoPosition"] ;

								// 						$addressList[$strA] = array(
								// 							"address" => $detailsLocality["address"],
								// 							"geo" => $detailsLocality["geo"],
								// 							"geoPosition" => $detailsLocality["geoPosition"]
								// 						);
								// 		            }
								// 				}else if(!empty($addressList[$strA]["adresse"])){
								// 					$eltPorteurTer["address"] = $addressList[$strA]["address"] ;
								// 	                $eltPorteurTer["geo"] = $addressList[$strA]["geo"] ;
								// 	                $eltPorteurTer["geoPosition"] = $addressList[$strA]["geoPosition"] ;
								// 				}
												
								// 			}
								// 		}
								// 		//Rest::json($eltPorteurTer); exit;

								// 		$res = Element::save($eltPorteurTer);
								// 		//
								// 		if(!empty($res) && !empty($res["result"]) && $res["result"] == true ){
								// 			$porteurTerName[$vFile[12]] = $res["id"];
								// 			$porteurTer2 = $res["map"];
								// 			$porteurTerList[$res["id"]] = $res["map"];
								// 		}
								// 	}
								// } else {
								// 	$porteurTerName[$vFile[12]] = array();
								// }

								// //************* Gestion porteur CTETER 3
								// if(!isset($porteurTerName[$vFile[19]]) ){
								// 	$whereP = $source;
								// 	$whereP["name"] = $vFile[19];
								// 	$porteurTer3 = PHDB::findOne(Organization::COLLECTION, $whereP);
								// 	if(!empty($porteurTer3)){
								// 		$porteurTerName[$vFile[19]] = (String)$porteurTer3["_id"];
								// 		$porteurTerList[(String)$porteurTer3["_id"]] = $porteurTer3;
								// 	}else if(!empty($vFile[19])){
								// 		$porteurTerName[$vFile[19]] = array();
								// 		$eltPorteurTer = array(	"name" => $vFile[19],
								// 								"type" => Organization::TYPE_GOV,
								// 								"collection" => Organization::COLLECTION,
								// 								"source" => $sourceU );
								// 		if(!empty($vFile[20])){
								// 			$strA =$vFile[20].$vFile[21].$vFile[22].$vFile[23];
								// 			if(!empty($strA)){
								// 				if(!isset($addressList[$strA])){
								// 					$address = array(
								// 						"addressCountry" => $vFile[20],
								// 						"postalCode" => $vFile[21],
								// 						"addressLocality" => $vFile[22],
								// 						"streetAddress" => $vFile[23]
								// 					);

								// 					$addressList[$strA] = array();
								// 					$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
								// 		            if($detailsLocality["result"] == true){
								// 						$eltPorteurTer["address"] = $detailsLocality["address"] ;
								// 						$eltPorteurTer["geo"] = $detailsLocality["geo"] ;
								// 						$eltPorteurTer["geoPosition"] = $detailsLocality["geoPosition"] ;

								// 						$addressList[$strA] = array(
								// 							"address" => $detailsLocality["address"],
								// 							"geo" => $detailsLocality["geo"],
								// 							"geoPosition" => $detailsLocality["geoPosition"]
								// 						);
								// 		            }
								// 				}else if(!empty($addressList[$strA]["adresse"])){
								// 					$eltPorteurTer["address"] = $addressList[$strA]["address"] ;
								// 	                $eltPorteurTer["geo"] = $addressList[$strA]["geo"] ;
								// 	                $eltPorteurTer["geoPosition"] = $addressList[$strA]["geoPosition"] ;
								// 				}
												
								// 			}
								// 		}
								// 		//Rest::json($eltPorteurTer); exit;

								// 		$res = Element::save($eltPorteurTer);
								// 		//
								// 		if(!empty($res) && !empty($res["result"]) && $res["result"] == true ){
								// 			$porteurTerName[$vFile[19]] = $res["id"];
								// 			$porteurTer3 = $res["map"];
								// 			$porteurTerList[$res["id"]] = $res["map"];
								// 		}
								// 	}
								// } else {
								// 	$porteurTerName[$vFile[19]] = array();
								// }

								

								// //************* LINK porteur CTETER AND CTETER
								// if(!empty($porteurTer1) && !empty($porteurTer1["_id"]) ){
								// 	if(empty($porteurTer1["links"]))
								// 		$porteurTer1["links"] = array();

								// 	if(empty($porteurTer1["links"]["projects"]))
								// 		$porteurTer1["links"]["projects"] = array();

								// 	if(empty($porteurTer1["links"]["projects"][(String)$cteter["_id"]]) ) {
								// 		$porteurTer1["links"]["projects"][(String)$cteter["_id"]] = array( "type" => Project::COLLECTION);

								// 		PHDB::update( Organization::COLLECTION,
								// 		    array("_id"=>new MongoId( (String)$porteurTer1["_id"]) ), 
								// 			array('$set' => array( "links" => $porteurTer1["links"] ) ) );

								// 		if(!empty($porteurTer1) && !empty($vFile[5]) ){
								// 			if(empty($porteurTerName[$vFile[5]]))
								// 				$porteurTerName[$vFile[5]] = (String)$porteurTer1["_id"] ;
								// 			$porteurTerList[$porteurTerName[$vFile[5]]] = $porteurTer1;
								// 		}
										
								// 	}

								// 	$linksUser = (!empty($vFile[11]) ? PHDB::findOne(Person::COLLECTION, array("email" => $vFile[11])): null );
								// 	if(empty($linksUser))
								// 		$linksUser = $bigboss;
								// 	//$linksUser = (!empty($perscteter) ? $perscteter : $bigboss );
								// 	if(!empty($linksUser)){
								// 		$child = array("childId" => (String)$linksUser["_id"], "childType" => Person::COLLECTION);
								// 		Link::connectParentToChild( (String)$porteurTer1["_id"], Organization::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;
								// 	}
								// }


								// if(!empty($porteurTer2) && !empty($porteurTer2["_id"])){

								// 	// Rest::json($porteurTer2);
								// 	if(empty($porteurTer2["links"]))
								// 		$porteurTer2["links"] = array();

								// 	if(empty($porteurTer2["links"]["projects"]))
								// 		$porteurTer2["links"]["projects"] = array();

								// 	if(empty($porteurTer2["links"]["projects"][(String)$cteter["_id"]]) ) {
								// 		$porteurTer2["links"]["projects"][(String)$cteter["_id"]] = array( "type" => Project::COLLECTION);

								// 		PHDB::update( Organization::COLLECTION,
								// 		    array("_id"=>new MongoId( (String)$porteurTer2["_id"]) ), 
								// 			array('$set' => array( "links" => $porteurTer2["links"] ) ) );

								// 		if(!empty($porteurTer2) && !empty($vFile[12]) ){
								// 			if(empty($porteurTerName[$vFile[12]]))
								// 				$porteurTerName[$vFile[12]] = (String)$porteurTer2["_id"] ;
								// 			$porteurTerList[$porteurTerName[$vFile[12]]] = $porteurTer2;
								// 		}
								// 		else{
								// 			var_dump($vFile[12]);
								// 			var_dump($porteurTerName[$vFile[12]]);
								// 			Rest::json($porteurTer2);
								// 		}
								// 	}

								// 	$linksUser = (!empty($vFile[18]) ? PHDB::findOne(Person::COLLECTION, array("email" => $vFile[18])) : null );
								// 	if(empty($linksUser))
								// 		$linksUser = $bigboss;
								// 	//$linksUser = (!empty($perscteter) ? $perscteter : $bigboss );
								// 	if(!empty($linksUser)){
								// 		$child = array("childId" => (String)$linksUser["_id"], "childType" => Person::COLLECTION);
								// 		//Rest::json($child); exit;
								// 		Link::connectParentToChild( (String)$porteurTer2["_id"], Organization::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;
								// 	}
								// }

								// if(!empty($porteurTer3) && !empty($porteurTer3["_id"])){
								// 	if(empty($porteurTer3["links"]))
								// 		$porteurTer3["links"] = array();

								// 	if(empty($porteurTer3["links"]["projects"]))
								// 		$porteurTer3["links"]["projects"] = array();

								// 	if(empty($porteurTer3["links"]["projects"][(String)$cteter["_id"]]) ) {
								// 		$porteurTer3["links"]["projects"][(String)$cteter["_id"]] = array( "type" => Project::COLLECTION);

								// 		PHDB::update( Organization::COLLECTION,
								// 		    array("_id"=>new MongoId( (String)$porteurTer3["_id"]) ), 
								// 			array('$set' => array( "links" => $porteurTer3["links"] ) ) );

								// 		$porteurTerList[$porteurTerName[$vFile[19]]] = $porteurTer3;
								// 	}

								// 	$linksUser = (!empty($vFile[25]) ?  PHDB::findOne(Person::COLLECTION, array("email" => $vFile[25])): null );
								// 	if(empty($linksUser))
								// 		$linksUser = $bigboss;
								// 	//$linksUser = (!empty($perscteter) ? $perscteter : $bigboss );
								// 	if(!empty($linksUser)){
								// 		$child = array("childId" => (String)$linksUser["_id"], "childType" => Person::COLLECTION);
								// 		Link::connectParentToChild( (String)$porteurTer3["_id"], Organization::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;
								// 	}
								// }


								// if(empty($cteter["parent"]))
								// 	$cteter["parent"] = array();

								// if(!empty($porteurTer1) && empty($cteter["parent"][(String)$porteurTer1["_id"]]) ) {
								// 	$cteter["parent"][(String)$porteurTer1["_id"]] = array( "type" => Organization::COLLECTION);
								// }

								// if(!empty($porteurTer2) && empty($cteter["parent"][(String)$porteurTer2["_id"]]) ) {
								// 	$cteter["parent"][(String)$porteurTer2["_id"]] = array( "type" => Organization::COLLECTION);
								// }

								// if(!empty($porteurTer3) && empty($cteter["parent"][(String)$porteurTer3["_id"]]) ) {
								// 	$cteter["parent"][(String)$porteurTer3["_id"]] = array( "type" => Organization::COLLECTION);
								// }

								// if(count($cteter["parent"]) > 0){
								// 	PHDB::update( Project::COLLECTION,
								// 	    array("_id"=>new MongoId( (String)$cteter["_id"]) ), 
								// 		array('$set' => array( "parent" => $cteter["parent"] ) ) );
								// }
								

							} else {
								if(!in_array($vFile[0], $cteterListNameErreur))
									$cteterListNameErreur[]= $vFile[0];
							}
						} else {
							$cteter = $cteterList[$cteterListName[$vFile[0]]] ;
						}
						//************* ANSWER
						$aPrincipal = "";
						$aSecondaire = array();
						$cibleDD = array();
						if(!empty( $vFile[78]))
							$aPrincipal = $vFile[78];
						if(!empty( $vFile[79]))
							$aSecondaire[] = $vFile[79];
						if(!empty( $vFile[80]))
							$aSecondaire[] = $vFile[80];
						if(!empty( $vFile[81]))
							$cibleDD[] = $vFile[81];
						if(!empty( $vFile[82]))
							$cibleDD[] = $vFile[82];
						if(!empty( $vFile[83]))
							$cibleDD[] = $vFile[83];

						if(!empty($cteter["slug"])){
							$answers = array(
								"formId" => $cteter["slug"],
								"session" => "1",
								"user" => (String)$bigboss["_id"],
								"name" => $bigboss["name"],
								"email" => $bigboss["email"],
								"answers" => array($cteter["slug"] => array("answers" => array(
									"caracter" => array(
										"actionPrincipal" => $aPrincipal,
										"actionSecondaire" => $aSecondaire,
										"cibleDDPrincipal" => $cibleDD ),
									"financement"  => array(
										"depensesfonctionnement" => array(
											"label" => "Dépenses de fonctionnement",
											"total" => ( !empty($vFile[58]) ? floatval(str_replace(" ", "", $vFile[58])) : "" )
										),
										"depensesinvestissement" => array(
											"label" => "Dépenses d'investissement",
											"total" => ( !empty($vFile[59]) ? floatval(str_replace(" ", "", $vFile[59])) : "" )
										),"totaldepenses" => array(
											"label" => "Total des dépenses",
											"total" => ( !empty($vFile[60]) ? floatval(str_replace(" ", "", $vFile[60])) : "" )
										),"totalfinancements" => array(
											"label" => "Total des financements",
											"total" => ( !empty($vFile[61]) ? floatval(str_replace(" ", "", $vFile[61])) : "" )
										),"financeursnonprecises" => array(
											"label" => "Financeurs non précisés",
											"total" => ( !empty($vFile[63]) ? floatval(str_replace(" ", "", $vFile[63])) : "" )
										),"autofinancement" => array(
											"label" => "Acteurs économiques (auto-financement)",
											"total" => ( !empty($vFile[64]) ? floatval(str_replace(" ", "", $vFile[64])) : "" )
										),"financeur" => array(
											"label" => "Acteurs économiques (financeur)",
											"total" => ( !empty($vFile[65]) ? floatval(str_replace(" ", "", $vFile[65])) : "" )
										),"colautofinancement)" => array(
											"label" => "Collectivité (auto-financement)",
											"total" => ( !empty($vFile[66]) ? floatval(str_replace(" ", "", $vFile[66])) : "" )
										),"colfinanceur" => array(
											"label" => "collectivité (financeur)",
											"total" => ( !empty($vFile[67]) ? floatval(str_replace(" ", "", $vFile[67])) : "" )
										),"dep" => array(
											"label" => "Département",
											"total" => ( !empty($vFile[68]) ? floatval(str_replace(" ", "", $vFile[68])) : "" )
										),"region" => array(
											"label" => "Région",
											"total" => ( !empty($vFile[69]) ? floatval(str_replace(" ", "", $vFile[69])) : "" )
										),"etat" => array(
											"label" => "Etat (DSIL, villes respirables, CPER,AFD,FNADT)",
											"total" => ( !empty($vFile[70]) ? floatval(str_replace(" ", "", $vFile[70])) : "" )
										),"ademe" => array(
											"label" => "ADEME",
											"total" => ( !empty($vFile[71]) ? floatval(str_replace(" ", "", $vFile[71])) : "" )
										),"caisseconsignations" => array(
											"label" => "Caisse des Consignations",
											"total" => ( !empty($vFile[72]) ? floatval(str_replace(" ", "", $vFile[72])) : "" )
										),"banque" => array(
											"label" => "Banque",
											"total" => ( !empty($vFile[73]) ? floatval(str_replace(" ", "", $vFile[73])) : "" )
										),"europe" => array(
											"label" => "Europe (FEDER, FEAMP)",
											"total" => ( !empty($vFile[74]) ? floatval(str_replace(" ", "", $vFile[74])) : "" )
										),"chambresconsulaires" => array(
											"label" => "Chambres consulaires",
											"total" => ( !empty($vFile[75]) ? floatval(str_replace(" ", "", $vFile[75])) : "" )
										),"agencedeleau" => array(
											"label" => "Agence de l'eau",
											"total" => ( !empty($vFile[76]) ? floatval(str_replace(" ", "", $vFile[76])) : "" )
										),"fondations" => array(
											"label" => "Fondations, crownfounding",
											"total" => ( !empty($vFile[77]) ? floatval(str_replace(" ", "", $vFile[77])) : "" )
										) ) ) ) ),
								"source" => $sourceU 
							);
							//Create Project ANSWERS
							
							if(!empty($vFile[26])){

								$where = $source;
								$where["name"] = $vFile[26];
								$projectA = PHDB::findOne(Project::COLLECTION, $where);
								if(empty($projectA)){


									$projectA = array(	"name" => $vFile[26],
														"collection" => Project::COLLECTION,
														"category" => "ficheAction",
														"preferences" => array(
															"private" => true
														),
														"source" => $sourceU );

									if(!empty($vFile[27])){
										$strA =$vFile[27].$vFile[28].$vFile[29].$vFile[30];
										if(!empty($strA)){
											if(!isset($addressList[$strA])){
												$address = array(
													"addressCountry" => $vFile[27],
													"postalCode" => $vFile[28],
													"addressLocality" => $vFile[29],
													"streetAddress" => $vFile[30]
												);

												$addressList[$strA] = array();
												$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
									            if($detailsLocality["result"] == true){
													$projectA["address"] = $detailsLocality["address"] ;
													$projectA["geo"] = $detailsLocality["geo"] ;
													$projectA["geoPosition"] = $detailsLocality["geoPosition"] ;

													$addressList[$strA] = array(
														"address" => $detailsLocality["address"],
														"geo" => $detailsLocality["geo"],
														"geoPosition" => $detailsLocality["geoPosition"]
													);
									            }
											}else if(!empty($addressList[$strA]["adresse"])){
												$projectA["address"] = $addressList[$strA]["address"] ;
								                $projectA["geo"] = $addressList[$strA]["geo"] ;
								                $projectA["geoPosition"] = $addressList[$strA]["geoPosition"] ;
											}
											
										}
									}
									
									
									$res = Element::save($projectA);
									

									if(!empty($res) && !empty($res["result"]) && $res["result"] == true ){
										//$projectList[$vFile[5]] = $res["id"];
										$projectA = $res["map"];
										$projectList[$res["id"]] = $res["map"];
									}
								} else {
									$projectList[(String)$projectA["_id"]] = $projectA;
								}



								if(!empty($projectA)){
									$child = array("childId" => (String)$projectA["_id"], "childType" => Project::COLLECTION);
									Link::connectParentToChild( (String)$cteter["_id"], Project::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;

									$answers["answers"][$cteter["slug"]]["answers"]["project"] = array(
										"type" => Project::COLLECTION,
										"id" => (String) $projectA["_id"],
										"name" => $projectA["name"],
										"email" => ( ( !empty($projectA["email"]) ) ? $projectA["email"] : "" )
									);
								}



								//Create Porteur ANSWERS
								$eltPorteurA1 = array();
								if(!empty($vFile[31]) && !empty($vFile[33])){
									if(!empty($porteurTerName) && !empty($porteurTerName[$vFile[31]])){
										$eltPorteurA1 = $porteurTerList[$porteurTerName[$vFile[31]]];
									} else {

										$where = $source;
										$where["name"] = $vFile[31];
										$eltPorteurA1 = PHDB::findOne(Organization::COLLECTION, $where);
										if(empty($eltPorteurA1)){
											$porteurTerName[$vFile[31]] = array();
											$eltPorteurA1 = array(	"name" => $vFile[31],
																	"type" => $listType[$vFile[33]],
																	"collection" => Organization::COLLECTION,
																	"source" => $sourceU,
																	"tags" => array($vFile[32], $vFile[33]) );
											if(!empty($vFile[34])){
												$strA =$vFile[34].$vFile[35].$vFile[36].$vFile[37];
												if(!empty($strA)){
													if(!isset($addressList[$strA])){
														$address = array(
															"addressCountry" => $vFile[34],
															"postalCode" => $vFile[35],
															"addressLocality" => $vFile[36],
															"streetAddress" => $vFile[37]
														);

														$addressList[$strA] = array();
														$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
											            if($detailsLocality["result"] == true){
															$eltPorteurA1["address"] = $detailsLocality["address"] ;
															$eltPorteurA1["geo"] = $detailsLocality["geo"] ;
															$eltPorteurA1["geoPosition"] = $detailsLocality["geoPosition"] ;

															$addressList[$strA] = array(
																"address" => $detailsLocality["address"],
																"geo" => $detailsLocality["geo"],
																"geoPosition" => $detailsLocality["geoPosition"]
															);
											            }
													}else if(!empty($addressList[$strA]["adresse"])){
														$eltPorteurA1["address"] = $addressList[$strA]["address"] ;
										                $eltPorteurA1["geo"] = $addressList[$strA]["geo"] ;
										                $eltPorteurA1["geoPosition"] = $addressList[$strA]["geoPosition"] ;
													}
													
												}
											}

											$res = Element::save($eltPorteurA1);
												//
											if(!empty($res) && !empty($res["result"]) && $res["result"] == true ){
												$porteurTerName[$vFile[31]] = $res["id"];
												$eltPorteurA1 = $res["map"];
												$porteurTerList[$res["id"]] = $res["map"];
											}
										} else {
											$projectList[(String)$eltPorteurA1["_id"]] = $eltPorteurA1;
											
										}
									}
								}
								

								//Create Porteur ANSWERS 2
								$eltPorteurA2 = array();
								if(!empty($vFile[40]) && !empty($vFile[42])){
									if(!empty($porteurTerName) && !empty($porteurTerName[$vFile[40]])){
										$eltPorteurA2 = $porteurTerList[$porteurTerName[$vFile[40]]];
									} else {
										$where = $source;
										$where["name"] = $vFile[40];
										$eltPorteurA2 = PHDB::findOne(Organization::COLLECTION, $where);
										if(empty($eltPorteurA2)){
											$porteurTerName[$vFile[40]] = array();
											$eltPorteurA2 = array(	"name" => $vFile[40],
																	"type" => $listType[$vFile[42]],
																	"collection" => Organization::COLLECTION,
																	"source" => $sourceU,
																	"tags" => array($vFile[41], $vFile[42]) );
											if(!empty($vFile[43])){
												$strA =$vFile[43].$vFile[44].$vFile[45].$vFile[46];
												if(!empty($strA)){
													if(!isset($addressList[$strA])){
														$address = array(
															"addressCountry" => $vFile[43],
															"postalCode" => $vFile[44],
															"addressLocality" => $vFile[45],
															"streetAddress" => $vFile[46]
														);

														$addressList[$strA] = array();
														$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
											            if($detailsLocality["result"] == true){
															$eltPorteurA2["address"] = $detailsLocality["address"] ;
															$eltPorteurA2["geo"] = $detailsLocality["geo"] ;
															$eltPorteurA2["geoPosition"] = $detailsLocality["geoPosition"] ;

															$addressList[$strA] = array(
																"address" => $detailsLocality["address"],
																"geo" => $detailsLocality["geo"],
																"geoPosition" => $detailsLocality["geoPosition"]
															);
											            }
													}else if(!empty($addressList[$strA]["adresse"])){
														$eltPorteurA2["address"] = $addressList[$strA]["address"] ;
										                $eltPorteurA2["geo"] = $addressList[$strA]["geo"] ;
										                $eltPorteurA2["geoPosition"] = $addressList[$strA]["geoPosition"] ;
													}
													
												}
											}

											$res = Element::save($eltPorteurA2);
												//
											if(!empty($res) && !empty($res["result"]) && $res["result"] == true ){
												$porteurTerName[$vFile[40]] = $res["id"];
												$eltPorteurA2 = $res["map"];
												$porteurTerList[$res["id"]] = $res["map"];
											}
										} else {
											$projectList[(String)$eltPorteurA2["_id"]] = $eltPorteurA2;
											
										}
									}
								}
								

								//Create Porteur ANSWERS 3
								$eltPorteurA3 = array();
								if(!empty($vFile[49]) && !empty($vFile[51])){
									if(!empty($porteurTerName) && !empty($porteurTerName[$vFile[49]])){
										$eltPorteurA3 = $porteurTerList[$porteurTerName[$vFile[49]]];
									} else {

										$where = $source;
										$where["name"] = $vFile[49];
										$eltPorteurA3 = PHDB::findOne(Organization::COLLECTION, $where);
										if(empty($eltPorteurA3)){
											$porteurTerName[$vFile[49]] = array();
											$eltPorteurA3 = array(	"name" => $vFile[49],
																	"type" => $listType[$vFile[51]],
																	"collection" => Organization::COLLECTION,
																	"source" => $sourceU,
																	"tags" => array($vFile[50], $vFile[51]) );
											if(!empty($vFile[52])){
												$strA =$vFile[52].$vFile[53].$vFile[54].$vFile[55];
												if(!empty($strA)){
													if(!isset($addressList[$strA])){
														$address = array(
															"addressCountry" => $vFile[52],
															"postalCode" => $vFile[53],
															"addressLocality" => $vFile[54],
															"streetAddress" => $vFile[55]
														);

														$addressList[$strA] = array();
														$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
											            if($detailsLocality["result"] == true){
															$eltPorteurA3["address"] = $detailsLocality["address"] ;
															$eltPorteurA3["geo"] = $detailsLocality["geo"] ;
															$eltPorteurA3["geoPosition"] = $detailsLocality["geoPosition"] ;

															$addressList[$strA] = array(
																"address" => $detailsLocality["address"],
																"geo" => $detailsLocality["geo"],
																"geoPosition" => $detailsLocality["geoPosition"]
															);
											            }
													}else if(!empty($addressList[$strA]["adresse"])){
														$eltPorteurA3["address"] = $addressList[$strA]["address"] ;
										                $eltPorteurA3["geo"] = $addressList[$strA]["geo"] ;
										                $eltPorteurA3["geoPosition"] = $addressList[$strA]["geoPosition"] ;
													}
													
												}
											}

											$res = Element::save($eltPorteurA3);
												//
											if(!empty($res) && !empty($res["result"]) && $res["result"] == true ){
												$porteurTerName[$vFile[49]] = $res["id"];
												$eltPorteurA3 = $res["map"];
												$porteurTerList[$res["id"]] = $res["map"];
											}
										} else {
											$projectList[(String)$eltPorteurA3["_id"]] = $eltPorteurA3;
										}
									}
								}

								if(!empty($eltPorteurA1)){
									$answers["answers"][$cteter["slug"]]["answers"]["organization"] = array(
										"type" => Organization::COLLECTION,
										"id" => (String) $eltPorteurA1["_id"],
										"name" => $eltPorteurA1["name"],
										"email" => ( ( !empty($eltPorteurA1["email"]) ) ? $eltPorteurA1["email"] : "" )
									);

									if(empty($eltPorteurA1["links"]))
										$eltPorteurA1["links"] = array();

									if(empty($eltPorteurA1["links"]["projects"]))
										$eltPorteurA1["links"]["projects"] = array();

									if(empty($eltPorteurA1["links"]["projects"][(String)$projectA["_id"]]) ) {
										$eltPorteurA1["links"]["projects"][(String)$projectA["_id"]] = array( "type" => Project::COLLECTION);

										PHDB::update( Organization::COLLECTION,
										    array("_id"=>new MongoId( (String)$eltPorteurA1["_id"]) ), 
											array('$set' => array( "links" => $eltPorteurA1["links"] ) ) );

										//$porteurTerList[$porteurTerName[$vFile[31]]] = $eltPorteurA1;

										if(!empty($eltPorteurA1) && !empty($vFile[31]) ){
								 			if(empty($porteurTerName[$vFile[31]]))
								 				$porteurTerName[$vFile[31]] = (String)$eltPorteurA1["_id"] ;
								 			$porteurTerList[$porteurTerName[$vFile[31]]] = $eltPorteurA1;
								 		}
								 		else{
								 			var_dump($vFile[31]);
								 			var_dump($porteurTerName[$vFile[31]]);
								 			Rest::json($eltPorteurA1);
								 		}
									}

									$linksUser = (!empty($vFile[39]) ? PHDB::findOne(Person::COLLECTION, array("email" => $vFile[39])): null );
									if(empty($linksUser))
										$linksUser = $bigboss;
									//$linksUser = (!empty($perscteter) ? $perscteter : $bigboss );
									if(!empty($linksUser)){
										$child = array("childId" => (String)$linksUser["_id"], "childType" => Person::COLLECTION);
										Link::connectParentToChild( (String)$eltPorteurA1["_id"], Organization::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;
									}
								}

								if(!empty($eltPorteurA2)){
									if(empty($answers["answers"][$cteter["slug"]]["answers"]["organizations"]))
										$answers["answers"][$cteter["slug"]]["answers"]["organizations"] = array();

									$answers["answers"][$cteter["slug"]]["answers"]["organizations"][(String) $eltPorteurA2["_id"]] = array(
										"type" => Organization::COLLECTION,
										"id" => (String) $eltPorteurA2["_id"],
										"name" => $eltPorteurA2["name"],
										"email" => ( ( !empty($eltPorteurA2["email"]) ) ? $eltPorteurA2["email"] : "" )
									);

									if(empty($eltPorteurA2["links"]))
										$eltPorteurA2["links"] = array();

									if(empty($eltPorteurA2["links"]["projects"]))
										$eltPorteurA2["links"]["projects"] = array();

									if(empty($eltPorteurA2["links"]["projects"][(String)$projectA["_id"]]) ) {
										$eltPorteurA2["links"]["projects"][(String)$projectA["_id"]] = array( "type" => Project::COLLECTION);

										PHDB::update( Organization::COLLECTION,
										    array("_id"=>new MongoId( (String)$eltPorteurA2["_id"]) ), 
											array('$set' => array( "links" => $eltPorteurA2["links"] ) ) );

										//$porteurTerList[$porteurTerName[$vFile[40]]] = $eltPorteurA2;


										if(!empty($eltPorteurA2) && !empty($vFile[40]) ){
								 			if(empty($porteurTerName[$vFile[40]]))
								 				$porteurTerName[$vFile[40]] = (String)$eltPorteurA2["_id"] ;
								 			$porteurTerList[$porteurTerName[$vFile[40]]] = $eltPorteurA2;
								 		}
								 		else{
								 			var_dump($vFile[40]);
								 			var_dump($porteurTerName[$vFile[40]]);
								 			Rest::json($eltPorteurA2);
								 		}
									}

									$linksUser = (!empty($vFile[48]) ? PHDB::findOne(Person::COLLECTION, array("email" => $vFile[48])): null );
									if(empty($linksUser))
										$linksUser = $bigboss;
									//$linksUser = (!empty($perscteter) ? $perscteter : $bigboss );
									if(!empty($linksUser)){
										$child = array("childId" => (String)$linksUser["_id"], "childType" => Person::COLLECTION);
										Link::connectParentToChild( (String)$eltPorteurA2["_id"], Organization::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;
									}
								}


								if(!empty($eltPorteurA3)){
									if(empty($answers["answers"][$cteter["slug"]]["answers"]["organizations"]))
										$answers["answers"][$cteter["slug"]]["answers"]["organizations"] = array();

									$answers["answers"][$cteter["slug"]]["answers"]["organizations"][(String) $eltPorteurA3["_id"]] = array(
										"type" => Organization::COLLECTION,
										"id" => (String) $eltPorteurA3["_id"],
										"name" => $eltPorteurA3["name"],
										"email" => ( ( !empty($eltPorteurA3["email"]) ) ? $eltPorteurA3["email"] : "" )
									);

									if(empty($eltPorteurA3["links"]))
										$eltPorteurA3["links"] = array();

									if(empty($eltPorteurA3["links"]["projects"]))
										$eltPorteurA3["links"]["projects"] = array();

									if(empty($eltPorteurA3["links"]["projects"][(String)$projectA["_id"]]) ) {
										$eltPorteurA3["links"]["projects"][(String)$projectA["_id"]] = array( "type" => Project::COLLECTION);

										PHDB::update( Organization::COLLECTION,
										    array("_id"=>new MongoId( (String)$eltPorteurA3["_id"]) ), 
											array('$set' => array( "links" => $eltPorteurA3["links"] ) ) );

										$porteurTerList[$porteurTerName[$vFile[49]]] = $eltPorteurA3;

										if(!empty($eltPorteurA3) && !empty($vFile[49]) ){
								 			if(empty($porteurTerName[$vFile[49]]))
								 				$porteurTerName[$vFile[49]] = (String)$eltPorteurA3["_id"] ;
								 			$porteurTerList[$porteurTerName[$vFile[49]]] = $eltPorteurA3;
								 		}
								 		else{
								 			var_dump($vFile[49]);
								 			var_dump($porteurTerName[$vFile[49]]);
								 			Rest::json($eltPorteurA3);
								 		}
									}

									$linksUser = (!empty($vFile[57]) ? PHDB::findOne(Person::COLLECTION, array("email" => $vFile[57])): null );
									if(empty($linksUser))
										$linksUser = $bigboss;
									//$linksUser = (!empty($perscteter) ? $perscteter : $bigboss );
									if(!empty($linksUser)){
										$child = array("childId" => (String)$linksUser["_id"], "childType" => Person::COLLECTION);
										Link::connectParentToChild( (String)$eltPorteurA3["_id"], Organization::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;
									}
								}

								if(empty($projectA["parent"]))
									$projectA["parent"] = array();

								if(!empty($eltPorteurA1) && empty($projectA["parent"][(String)$eltPorteurA1["_id"]]) ) {
									$projectA["parent"][(String)$eltPorteurA1["_id"]] = array( "type" => Organization::COLLECTION);
								}

								if(!empty($eltPorteurA2) && empty($projectA["parent"][(String)$eltPorteurA2["_id"]]) ) {
									$projectA["parent"][(String)$eltPorteurA2["_id"]] = array( "type" => Organization::COLLECTION);
								}

								if(!empty($eltPorteurA3) && empty($projectA["parent"][(String)$eltPorteurA3["_id"]]) ) {
									$projectA["parent"][(String)$eltPorteurA3["_id"]] = array( "type" => Organization::COLLECTION);
								}

								if(!empty($projectA["parent"])){
									PHDB::update( Project::COLLECTION,
									    array("_id"=>new MongoId( (String)$projectA["_id"]) ), 
										array('$set' => array( "parent" => $projectA["parent"] ) ) );
								}
								

								if(!empty($answers)){
									$res = PHDB::insert("answers", $answers);
									//Rest::json($answers); exit;


									$p = PHDB::findOneById(Project::COLLECTION, (String)$projectA["_id"]);
									if(empty($p["links"]))
										$p["links"] = array();
									if(empty($p["links"]["answers"]))
										$p["links"]["answers"] = array();
									if(empty($p["links"]["answers"][(String)$answers["_id"]])) {
										$p["links"]["answers"][(String)$answers["_id"]] = array( "type" => "answers");
									}

									PHDB::update( Project::COLLECTION,
									    array("_id"=>new MongoId( (String)$p["_id"]) ), 
										array('$set' => array( "links" => $p["links"] ) ) );
								}
							}
							
						}
							

					}
	                
	            }
            }

            //Rest::json($cteterListName); exit;

            $params = array( 
            				"nb" => $nb,
            				"headFile" => $headFile,
            				"cteterList" => $cteterList,
		        			"cteterListName" => $cteterListName,
		        			"cteterListNameErreur" => $cteterListNameErreur,
		        			"porteurTerList" => $porteurTerList,
		        			"porteurTerName" => $porteurTerName,
		        			"projectList" => $projectList,
		        			"addressList" => $addressList);
            
            // $params = array("result"=>true,
            //                 "elements"=>json_encode(json_decode(json_encode($elements),true)),
            //                 "elementsWarnings"=>json_encode(json_decode(json_encode($elementsWarnings),true)),
            //                 "saveCities"=>json_encode(json_decode(json_encode($saveCities),true)),
            //                 "listEntite"=>self::createArrayList(array_merge($elements, $elementsWarnings)));
        }
        return $params;
    }

    public static function getIndicator($actions = null, $cible = null, $fields = array("name","unity1"), $idElt=null){

        $where = array( "type" => "indicator", 
                        "source.key" => "ctenat" );

        if(!empty($actions))
            $whereA["domainAction"] = array('$in' => $actions);

        if(!empty($cible))
            $whereC["objectifDD"] = array('$in' => $cible);

        if(!empty($actions) && !empty($cible)){
            $where['$or'] = array($whereA, $whereC);
        } else {
            if(!empty($actions))
               $where["domainAction"] = array('$in' => $actions); 
            if(!empty($cible))
               $where["objectifDD"] = array('$in' => $cible);
        }

        if(!empty($idElt)){
            $wherep['$or'] = array($where,  array(
                        "type" => "indicator", 
                        "source.key" => "ctenat",
                        "parent.".$idElt => array('$exists' => 1) ) );

            $where = $wherep;
        }


        //Rest::json($where); 
        $pois = PHDB::find(Poi::COLLECTION, $where, $fields );
        $res = array();
        foreach ($pois as $key => $value) {
            if(!empty($value["name"])){
                $res[$key] = $value["name"];
            
                if(!empty($value["unity1"]))
                    $res[$key] = $res[$key]."<br/>( ".$value["unity1"]." )";
            }
        }

        return $res;
    }

    public static function indicatorCount($results, $indicId)
    {
        $res = 0;
        foreach ( $results as $ix => $ind) 
        {
            if( $ind["indicateur"] == $indicId && isset($ind["objectif"]) )
            {
                if(isset($ind["objectif"]["res2019"]))
                    $res += $ind["objectif"]["res2019"];
                if(isset($ind["objectif"]["res2020"]))
                    $res += $ind["objectif"]["res2020"];
                if(isset($ind["objectif"]["res2021"]))
                    $res += $ind["objectif"]["res2021"];
                if(isset($ind["objectif"]["res2022"]))
                    $res += $ind["objectif"]["res2022"];
            }
        }
        return $res;
    }

    public static function saveOrga($params){
        //var_dump("HelloThere"); exit;
        $params = Import::newStart($params);
        $res = Import::previewData($params);
        //Rest::json($res); exit;
        $resultImport = array();
        if(!empty($res) && !empty($res["elementsObj"])){

            foreach ($res["elementsObj"] as $key => $value) {

                if(!empty($value["name"])){
                    $listParent = null ;
                    if(!empty($value["parent"])){
                        $listParent = $value["parent"];
                        unset($value["parent"]);
                    }

                    $admins = null ;
                    if(!empty($value["admins"])){
                        $admins = explode(";", $value["admins"]);;
                        unset($value["admins"]);
                    }


                    if(!empty($value["address"]) && !empty($value["address"]["osmID"])){
                        unset($value["address"]);
                        unset($value["geo"]);
                        unset($value["geoPosition"]);
                    }

                    $update = false;
                    $id = false;
                    
                    $resElt = array(
                        "update" => false,
                        "id" => false,
                        "name" => @$value["name"]
                    );

                    if(!empty($value["id"])){
                        $p = PHDB::findOneById(Organization::COLLECTION, $value["id"], array("name", "_id") );
                        if(!empty($p)){
                            $resElt["id"] = $value["id"];
                            $resElt["update"] = true;
                        }
                        unset($value["id"]);
                    }else{
                        $value["creator"] = Yii::app()->session["userId"];
                        $value["created"] = time();
                    }

                    if($resElt["update"] == true){
                        PHDB::update(Organization::COLLECTION,
                            array("_id" => new MongoId($resElt["id"]) ) , 
                            array('$set' => $value)
                        );
                    } else {
                        //Element::save($value);
                        PHDB::insert(Organization::COLLECTION, $value );
                        $slug = Slug::checkAndCreateSlug( $value["name"], Organization::COLLECTION, (String) $value["_id"] );
                        Slug::save(Organization::COLLECTION, (String) $value["_id"], $slug);
                        $value["slug"]=$slug;
                        Element::updateField( Organization::COLLECTION, (String) $value["_id"], "slug", $slug);
                    }

                    
                    

                    if( !empty($listParent) ){
                        $where = array( "source.key" => "ctenat",
                                        "name" => $listParent);
                       
                        $parent = PHDB::findOne( Organization::COLLECTION, $where, array("name", "_id") );
                        
                        //Rest::json(array("parent" => $parent, "value" => $value)); exit;
                        if(!empty($parent) && !empty($parent["_id"]) && !empty($value["_id"])){
                            // Link::connect((String) $value["_id"], Organization::COLLECTION, (String)$parent["_id"], Organization::COLLECTION, Yii::app()->session["userId"], "memberOf",false,false,false,false, null);

                            PHDB::update(Organization::COLLECTION,
                                array("_id" => new MongoId((String) $value["_id"]) ) , 
                                array('$set' => array("parent" => array(
                                            (String)$parent["_id"] => array("type" => Organization::COLLECTION))))
                            );


                            Link::connect((String)$parent["_id"], Organization::COLLECTION, (String) $value["_id"], Organization::COLLECTION, Yii::app()->session["userId"], "members",false,false,false, false, null);
                        }
                    }

                    if( !empty($admins) ){

                        foreach ($admins as $keyA => $valA) {
                            $where = array("email" => trim($valA) );
                            $adminP = PHDB::findOne( Person::COLLECTION, $where, array("name", "_id", "links") );
                            
                            if(!empty($adminP) && !empty($adminP["_id"]) ){
                                Link::connect( (String)$value["_id"], Organization::COLLECTION, (String)$adminP["_id"], Person::COLLECTION, Yii::app()->session["userId"], "members",true,false,false,false, null);

                                Link::connect((String)$adminP["_id"], Person::COLLECTION, (String)$value["_id"], Organization::COLLECTION, Yii::app()->session["userId"], "memberOf",true,false,false, false, null);
                            }
                        }
                        
                    }
                    $resultImport[] = $resElt;
                }
            } 
        }

        $res["resultImport"] = $resultImport;
        
        Rest::json($res); exit;
    }

    public static function saveIndicateur($params){
        //var_dump("HelloThere"); exit;
        $params = Import::newStart($params);
        $res = Import::previewData($params, true, true);
        $resultImport = array();
        if(!empty($res) && !empty($res["elementsObj"])){
            $where = array( "source.key" => "ctenat",
                            "type" => "indicator" );

            // if(!empty($res["indexStart"]) && ( $res["indexStart"] == 1 || $res["indexStart"] == "1") )
            //     PHDB::remove( Poi::COLLECTION, $where );

            //Rest::json($res["elementsObj"]);
            foreach ($res["elementsObj"] as $key => $value) {

                if(!empty($value["name"])){
                    $value["type"] = "indicator";
                    $value["creator"] = Yii::app()->session["userId"];
                    $value["created"] = time();

                    if(!empty($value["periode"])){
                        $value["periode"] = explode(";", $value["periode"]);
                    }
                    if(!empty($value["echelleApplication"])){
                        $value["echelleApplication"] = explode(";", $value["echelleApplication"]);
                    }

                    if(!empty($value["perimetreGeo"])){
                        $value["perimetreGeo"] = explode(";", $value["perimetreGeo"]);
                    }
                    if(!empty($value["domainAction"])){
                        $value["domainAction"] = explode(";", $value["domainAction"]);
                    }
                    if(!empty($value["objectifDD"])){
                        $value["objectifDD"] = explode(";", $value["objectifDD"]);
                    }

                    $update = false;
                    $id = false;

                    
                    $resElt = array(
                        "update" => false,
                        "id" => false,
                        "name" => @$value["name"]
                    );
                    if(!empty($value["id"])){
                        $p = PHDB::findOneById(Poi::COLLECTION, $value["id"], array("name") );
                        if(!empty($p)){
                            $resElt["id"] = $value["id"];
                            $resElt["update"] = true;
                        }

                        unset($value["id"]);
                    }
                    if($resElt["update"] == true){
                        PHDB::update(Poi::COLLECTION,
                            array("_id" => new MongoId($resElt["id"]) ) , 
                            array('$set' => $value)
                        );
                    } else {
                        //Element::save($value);
                        PHDB::insert(Poi::COLLECTION, $value );
                    }
                    $resultImport[] = $resElt;
                }
                
            } 
        }

        $res["resultImport"] = $resultImport;
        
        Rest::json($res); exit;
    }


    public static function pdfElement($init) {
        // $controller=$this->getController();
        // ini_set('max_execution_time',1000);

        $controller = $init["controller"];
        $slug =  $init["slug"]; 
        $admin= $init["admin"]; 
        $id= $init["id"]; 
        $idElt= $init["idElt"];
        $answerList = array();
        //Rest::json($answerList); exit;
        if(empty($idElt)){

            if( !empty($admin) ){
                $cter = PHDB::find(Project::COLLECTION, array("source.key" => $slug, "category" => "cteR"), array("slug"));
            } else  {
                $cter[$id] = array("slug" => $slug);
            }
            $search="";

            $searchRegExp = Search::accentToRegex($search);
            if( !empty($admin) ){
                $query = array("source.key" => $slug ) ;
                PHDB::remove( Form::ANSWER_COLLECTION , 
                                        [ "formId"=>$slug, 
                                          "answers"=> ['$exists'=>0] ] );
            } else {
                $query = array("formId" => $slug ) ;
                PHDB::remove( Form::ANSWER_COLLECTION , 
                                        [ "formId"=>$slug, 
                                          "answers.".$slug.".answers.project.id"=> ['$exists'=>0] ] );
            }
            $querySearch =  array();
            if(!empty($_POST["name"])){
                foreach ($cter as $keyC => $valC) {
                    array_push($querySearch,array( '$or' => array(
                                    array( "name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
                                    array( "email" => new MongoRegex("/.*{$searchRegExp}.*/i")),
                                    array( "answers.".$valC["slug"].".answers.organization.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
                                    array( "answers.".$valC["slug"].".answers.project.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
                                )));
                }
                $querySearch = array('$or' => $querySearch);

            }else if(count($cter) > 1){
                $querySearch = array( '$or' => array(
                                    array( "name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
                                    array( "email" => new MongoRegex("/.*{$searchRegExp}.*/i")),
                                    array( "answers.".$slug.".answers.organization.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
                                    array( "answers.".$slug.".answers.project.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
                                ));
                
            }
            

            if(!empty($querySearch))
                $query = array('$and' => array( $query , $querySearch ) ) ;

            $answers = PHDB::find( Form::ANSWER_COLLECTION , $query);
            $answerList = Form::listForAdmin($answers) ;
        } else {
            $answer = PHDB::findOneById( Form::ANSWER_COLLECTION, $idElt);
            //Rest::json($answer); exit;
            $answers[$idElt] = $answer ;
            $answerList = Form::listForAdmin($answers) ;
        }
         
        //Rest::json($answerList); exit;
        foreach ($answerList as $key => $value) {
            if( !empty($idElt) ||
                (empty($idElt) &&
                !empty($answerList[$key]["priorisation"]) && 
                $answerList[$key]["priorisation"] == Ctenat::STATUT_ACTION_VALID) ){

                if( !empty($answerList[$key]) &&
                    !empty($answerList[$key]["project"]) && 
                    !empty($answerList[$key]["project"]["id"]) ){
                    $p = PHDB::findOneById( Project::COLLECTION, $answerList[$key]["project"]["id"], array("description", "shortDescription", "tags", "expected") );
                    if(!empty($p)){
                        if(!empty($p["description"])){
                            $answerList[$key]["project"]["description"] = $p["description"];
                        }

                        if(!empty($p["shortDescription"])){
                            $answerList[$key]["project"]["shortDescription"] = $p["shortDescription"];
                        }

                        if(!empty($p["tags"])){
                            $answerList[$key]["project"]["tags"] = $p["tags"];
                        }

                        if(!empty($p["expected"])){
                            $answerList[$key]["project"]["expected"] = $p["expected"];
                        }
                    } else
                        $answerList[$key]["project"] = null ;
                    
                }

                if( !empty($answerList[$key]["answers"]) &&
                    !empty($answerList[$key]["formId"]) &&
                    !empty($answerList[$key]["answers"][$answerList[$key]["formId"]]) &&
                    !empty($answerList[$key]["answers"][$answerList[$key]["formId"]]["answers"]) ) {
                    $answerList[$key]["answers"] = $answerList[$key]["answers"][$answerList[$key]["formId"]]["answers"];
                    $orgsListName = "";
                    if(!empty($answerList[$key]["answers"]["organization"])  && !empty($answerList[$key]["answers"]["organization"]["id"])){
                        $org = PHDB::findOneById(Organization::COLLECTION,$answerList[$key]["answers"]["organization"]["id"], array("name", "type") );
                        $orgsListName = $org["name"] ;
                    }

                    if(!empty($answerList[$key]["answers"]["organizations"])){
                        foreach ($answerList[$key]["answers"]["organizations"] as $keyORG => $valueORG) {
                            if($keyORG !== "orgasLinked"){
                                $org = PHDB::findOneById(Organization::COLLECTION,$valueORG["id"], array("name", "type") );

                                if( !empty($org) && !empty($org["name"]) )
                                    $orgsListName .= "<br>".$org["name"]." " ;
                            }else{
                                foreach ($valueORG as $keyORG2 => $valueORG2) {
                                    if($keyORG2 !== "orgasLinked"){
                                        $org = PHDB::findOneById(Organization::COLLECTION,$valueORG2["id"], array("name", "type") );
                                        if(!empty($org) && !empty($org["name"])){

                                            $orgsListName .= "<br>".$org["name"]." " ;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    $answerList[$key]["answers"]["organizations"] = $orgsListName;


                    if(!empty($answerList[$key]["answers"]["organization"]))
                        unset($answerList[$key]["answers"]["organization"]);

                    if(!empty($answerList[$key]["answers"]["project"]))
                        unset($answerList[$key]["answers"]["project"]);
                } else {
                    $answerList[$key]["answers"] = array();
                }

                if(!empty($answerList[$key]["comment"]) )
                    unset($answerList[$key]["comment"]);

                if(!empty($answerList[$key]["organization"]) && !empty($answerList[$key]["organization"]["_id"]) )
                    unset($answerList[$key]["organization"]["_id"]);

                if(!empty($answerList[$key]["project"]) && !empty($answerList[$key]["project"]["_id"] ) )
                    unset($answerList[$key]["project"]["_id"]);

                if($answerList[$key]["project"] === null )
                    unset($answerList[$key]);
            } else
                unset($answerList[$key]);
        }
        //Rest::json($answerList); exit;
        $params = array("answers" => $answerList);
        if(isset($init["saveOption"])){
            $params["comment"]=$init["comment"];
            $params["saveOption"]=$init["saveOption"];
            $params["urlPath"]=$init["urlPath"];
            $params["docName"]=$init["docName"];
        }

        foreach ($answerList as $key => $elt) {
            $form = PHDB::findOne( Form::COLLECTION , array("id"=>$elt["formId"]));
            $forms = PHDB::find( Form::COLLECTION , array("parentSurvey"=>$elt["formId"]));
            foreach ($forms as $key => $value) {
                $forms[$value["id"]] = $value;
            }
            $formSrc = null;
            if( is_string($form["scenario"]) && stripos( $form["scenario"] , "db.") !== false){
                $pathT = explode(".",$form["scenario"]); 
                $formSrc = PHDB::findOne( $pathT[1] , array( $pathT[2] => $pathT[3] ) );
                $form["scenario"] = $formSrc[$pathT[4]];
            }
            $params["htmls"][] = $controller->renderPartial('costum.views.custom.ctenat.pdf.answers', array("id"=> $key, "answer"=>$elt, "form" => $form, "forms" => $forms), true);
        }
        //Rest::json($params); exit;

        Pdf::createPdf($params);
    }


    public static function updatePrioAnswers($answerId, $formId, $status, $section) {
        $answer = PHDB::findOne( Form::ANSWER_COLLECTION , array("_id"=>new MongoId( $answerId ) ));

        //$form = PHDB::findOne( Form::COLLECTION , array("id"=> $formId ) );

        //$formEl = PHDB::findOne( $form["parentType"] , array("_id" =>new MongoId($form["parentId"] ) ) );
        $eltSlug = Slug::getElementBySlug($formId );
        $formEl = $eltSlug;
        //Rest::json($formEl); exit;
        $addTags = array( "actionPrincipal", "actionSecondaire", "cibleDDPrincipal", "cibleDDSecondaire");

        $res = array("result"=> false, "answer"=> $answer , "formEl"=> $formEl);
        if(!empty($answer) /* && !empty($form) */ && !empty($formEl)){
            $key = $section;
            $value = (!empty($status) ? $status : null);
            $verb = '$set';

            PHDB::update( Form::ANSWER_COLLECTION,
                array("_id"=>new MongoId($answerId)), 
                array($verb => array($key => $value)));

            $setP =  array();
            $setF =  array();
            if( !empty($answer["answers"]) && 
                !empty($answer["answers"][$formId]) &&
                !empty($answer["answers"][$formId]["answers"]) &&
                !empty($answer["answers"][$formId]["answers"]["project"]) ){
                $idP = $answer["answers"][$formId]["answers"]["project"]["id"] ;
                
                $project = PHDB::findOne( Project::COLLECTION , array("_id"=>new MongoId( $idP ) ));
                
                if( $status == Ctenat::STATUT_ACTION_MATURATION ||
                    $status == Ctenat::STATUT_ACTION_VALID ){
                    if(!empty($answer["answers"][$formId]["answers"]["caracter"])){
                        $caracter = $answer["answers"][$formId]["answers"]["caracter"];
                        foreach ($addTags as $kLT => $valLT) {
                            //Rest::json(Yii::app()->session["costum"]); exit;
                            $listC = ( in_array($valLT, array("actionPrincipal", "actionSecondaire")) ? Yii::app()->session["costum"]["lists"]["domainAction"] : Yii::app()->session["costum"]["lists"]["cibleDD"] );

                            if( !empty($caracter[$valLT]) ){
                                if( empty($project["tags"]) )
                                    $project["tags"] = array();

                                if( empty($formEl["tags"]) )
                                    $formEl["tags"] = array();
                                if(is_array($caracter[$valLT])){
                                    foreach ($caracter[$valLT] as $kT => $valT) {
                                        if(!in_array($valT, $project["tags"]))
                                            $project["tags"][] = $valT ;

                                        if(!in_array($valT, $formEl["tags"]))
                                            $formEl["tags"][] = $valT ;
                                        if(!empty($listC)){
                                            foreach ($listC as $key => $childBadges) {
                                                if(!empty($childBadges)){
                                                    foreach ($childBadges as $ic => $cb) {
                                                        if( !in_array($key, $project["tags"]) && $valT == $cb )
                                                            $project["tags"][] = $key;

                                                        if( !in_array($key, $formEl["tags"]) && $valT == $cb )
                                                            $formEl["tags"][] = $key;
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }else if(is_string($caracter[$valLT])){
                                    if(!in_array($caracter[$valLT], $project["tags"]))
                                        $project["tags"][] = $caracter[$valLT] ;

                                    if(!in_array($caracter[$valLT], $formEl["tags"]))
                                        $formEl["tags"][] = $caracter[$valLT] ;
                                    if(!empty($listC)){
                                        foreach ($listC as $key => $childBadges) {
                                            if(!empty($childBadges)){
                                                foreach ($childBadges as $ic => $cb) {
                                                    if( !in_array($key, $project["tags"]) && $caracter[$valLT] == $cb )
                                                        $project["tags"][] = $key;

                                                    if( !in_array($key, $formEl["tags"]) && $caracter[$valLT] == $cb )
                                                        $formEl["tags"][] = $key;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        unset($project["preferences"]["private"]);
                        if(!empty($project["tags"]) )
                            $setP["tags"] = $project["tags"];
                        if(!empty($project["preferences"]))
                            $setP["preferences"] = $project["preferences"];
                        if(!empty($formEl["tags"]))
                            $setF["tags"] = $formEl["tags"];
                    }
                } else if($status == "Action refusée" || $status == "Action Candidate"){
                    $project["preferences"]["private"] = true ;
                    $setP["preferences"] = $project["preferences"];
                }

                if(!empty($setP)){
                    PHDB::update( Project::COLLECTION,
                    array("_id"=>new MongoId($idP)), 
                    array($verb => $setP) ) ;
                }
                
                if(!empty($setF)){
                    PHDB::update( $eltSlug["type"],
                        array("_id"=>new MongoId($eltSlug["id"])), 
                        array($verb => $setF ) );
                }

            }

            $res = [ "result"=> true, "msg"=> "Contact admin", "answer"=> $answer, "setF"=> $setF, "setP"=> $setP];
        }
        
        return $res ;
    }


    public static function urlAfterSave($data){
        //var_dump($data); 
        if($data["collection"]==Poi::COLLECTION && $data["elt"]["type"] = "indicator"
            && !empty($data["elt"]["parent"]) ){
           //var_dump("here 1");
            foreach ($data["elt"]['parent'] as $keyP => $valP) {
                $parent=Element::getElementById($keyP, $valP["type"], null, array("category", "slug"));
                //var_dump("here 2");
                if(!empty($parent["category"]) && $parent["category"] == "ficheAction"){
                    //var_dump("here 3");
                    return "#@".$parent["slug"].".view.answers.subview.murir";
                }
            }

        }
        return null ;

    }

    public static function chiffreFinancement($answers,$slug){
        $res = ["total"=>0,"public"=>0,"private"=>0,"lbl"=>"Millions"];
        foreach ($answers as $i => $a) {
            if(isset($a["answers"][$slug]["answers"]["murir"]["planFinancement"]))
            {
                $fin = $a["answers"][$slug]["answers"]["murir"]["planFinancement"];
                
                foreach ($fin as $ix => $f) {
                    $cumul = 0;
                    if(!empty($f["amount2019"]))
                        $cumul += intval( $f["amount2019"] );
                    if(!empty($f["amount2020"]))
                        $cumul += intval( $f["amount2020"] );
                    if(!empty($f["amount2021"]))
                        $cumul += intval( $f["amount2021"] );
                    if(!empty($f["amount2022"]))
                        $cumul += intval( $f["amount2022"] );
                    
                    $res["total"] += $cumul;
                    if( in_array($f["financerType"], Ctenat::$financeurTypePublic) )
                        $res["public"] += $cumul;
                    else 
                        $res["private"] += $cumul;
                }
            }
        }
        $res["total"] = round($res["total"]/1000000,1);
        $res["public"] = round($res["public"]/1000000,1);
        $res["private"] = round($res["private"]/1000000,1);
        return $res;
    }

    public static function chiffreFinancementByType($answers,$slug=null,$returnRes = true){
        $finance = [];
        $financeLbl = [];
        $res = ["data"=>[],"lbls"=>[],"total"=>0];
        foreach ($answers as $i => $a) {
            if(empty($slug))
                $slug = $a["formId"];
            if(isset($a["answers"][$slug]["answers"]["murir"]["planFinancement"]))
            {
                $fin = $a["answers"][$slug]["answers"]["murir"]["planFinancement"];
                
                foreach ($fin as $ix => $f) {

                    $cumul = 0;
                    if(!empty($f["amount2019"]))
                        $cumul += intval( $f["amount2019"] );
                    if(!empty($f["amount2020"]))
                        $cumul += intval( $f["amount2020"] );
                    if(!empty($f["amount2021"]))
                        $cumul += intval( $f["amount2021"] );
                    if(!empty($f["amount2022"]))
                        $cumul += intval( $f["amount2022"] );
                    
                    if(!isset($finance[$f["financerType"]])){
                        $finance[ $f["financerType"] ] = $cumul;
                        $financeLbl[ $f["financerType"] ] = (isset(Ctenat::$financerTypeList[$f["financerType"]])) ? Ctenat::$financerTypeList[$f["financerType"]] : $f["financerType"] ;
                    }
                    else 
                        $finance[$f["financerType"]] += $cumul;
                }
            }
        }
        if($returnRes){
            foreach (array_keys($finance) as $k => $v) {
                $res["data"][] = $finance[$v];
                $res["total"] += intval($finance[$v]);
                $res["lbls"][]   = $financeLbl[$v];
            }
            $res["total"] = round($res["total"]/1000000,1);
            return $res;
        } else {
            return $finance;
        }
    }

}
?>