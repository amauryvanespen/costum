<?php

class Costum {
	const COLLECTION = "costum";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	
	//used in initJs.php for the modules definition
	public static function getConfig($context=null){
		return array(
			"collection"    => self::COLLECTION,
            "controller"   => self::CONTROLLER,
            "module"   => self::MODULE,
            "categories" => CO2::getModuleContextList(self::MODULE, "categories", $context),
       		"lbhp" => true
		);
	}

    public static $paramsCo= array(
            "label",
            "subdomainName",
            "placeholderMainSearch",
            "icon",
            "useFilter",
            "slug",
            "formCreate",
            "initType", 
            "inMenu",
            "types",
            "actions", 
            //ELEMENT CONFIG
            "labelClass",
            "view",
            "action",
            "dir",
            "edit",
            "dataTarget", 
            "id",  
            "class",
            "dataAttr",
            "target",
            "toggle",
            "typeAllow",
            "userConnected"
    );
        
    //$ctrl if == null we are jsut initing the session data, no rendering 
    //$id 
    // if given with a type it concerns an element
    // otherwise it's just costum can be the slugname or the digital id 
    //$type : 
        // if given with an id it concerns an element
    //$slug 
        // it's the slug of an element
    //$init this action can render a result for a page request
    //$jsonOrDB defines if working with the data/xxxx.json file
    public static function init($ctrl, $id=null,$type=null,$slug=null,$source="db",$test=null,$where=null){
        //$params = CO2::getThemeParams();
        //Yii::app()->session['paramsConfig']=$params;
        $elParams = ["shortDescription", "profilImageUrl", "profilThumbImageUrl", "profilMediumImageUrl","profilBannerUrl", "name", "tags", "description","costum", "links", "profilRealBannerUrl","slug"];
        $tmp = array();
        //ex : http://127.0.0.1/ph/costum/co/index/slug/cocampagne
        //le slug appartient à un élément
$debug = false;
if($debug){
var_dump("where ".$where);
var_dump("id ".$id);
var_dump("type ".$type);
var_dump("slug ".$slug);
 //exit;
}
        if ( isset($test) ) { 
if($debug)var_dump('by test '.$test);
            $id = $test; 
            $el = Slug::getElementBySlug($slug, $elParams );
            $tmp['contextType'] = $el["type"];
            $tmp['contextId'] = $el["id"];
            
        }
        else if ( isset($slug) ) { 
if($debug)var_dump('by element slug '.$slug);
            $el = Slug::getElementBySlug($slug, $elParams );
            // var_dump($el);exit;
            $tmp['contextType'] = $el["type"];
            $tmp['contextId'] = $el["id"];
            if(isset($el["el"]["costum"]['slug'])){
                $id = (isset($el["el"]["costum"]['id'])) ? $el["el"]["costum"]['id'] : $el["el"]["costum"]['slug'];
            }
            else {
if($debug)var_dump('by default costum slug costumBuilder');
                //generate the costum tag on the element with a default COstum template 
                //can be changed in the costum admin 
                if(isset(Yii::app()->session["costum"]["slug"]))
                    $id = Yii::app()->session["costum"]["slug"]; 
                else 
                    $id = "costumBuilder"; 
            }

        } 

        //ex : http://127.0.0.1/ph/costum/co/index/id/xxxx/type/organizations
        //l'id et le type permettent de retrouver un élément
        else if(isset($type) && isset($id)){
//<<<<<<< HEAD
if($debug)var_dump('by type and ID '.$id." : ".$type);
// =======
//             // Rest::json($type); Rest::json($id);  exit;
// >>>>>>> qa
            $el = array("el"=>Element::getByTypeAndId($type, $id, $elParams ));
            $tmp['contextType'] = $type;
            $tmp['contextId'] = $id;
            if(@$el["el"]["costum"]){
                $id = (@$el["el"]["costum"]['id']) ? $el["el"]["costum"]['id'] : $el["el"]["costum"]['slug'];
            }
            
        }
        //ex : http://127.0.0.1/ph/costum/co/index/id/cocampagne
        //l'id est celui du costum
        else if(isset($_GET["id"])){
            $id=$_GET["id"];
            // var_dump("else"); Rest::json($id);  exit;
        } 
        // else if(!isset($id) && Yii::app()->session["costum"]["slug"] ) {
        //     self::init ($ctrl,$id,$type,Yii::app()->session["costum"]["slug"],$source,$test,"Costum::init l.123");
        //     return true;
        // } 
            

if($debug){
 var_dump('contextId : '.@$tmp['contextId']);
 var_dump('contextType : '.@$tmp['contextType']);
  var_dump('session contextId : '.@Yii::app()->session['costum']['contextId']);
 var_dump('session contextType : '.@Yii::app()->session['costum']['contextType']);
var_dump("id ".$id);
}
//  exit;
        if(!empty($id)){
                //Rest::json($id); exit ;
                if(strlen($id) == 24 && ctype_xdigit($id)  ){
                    $c = PHDB::findOne( Costum::COLLECTION , array("_id"=> new MongoId($id)));
                    
                }
                else {
                    $c = PHDB::findOne( Costum::COLLECTION , array("slug"=> $id));
                    //Rest::json($id); exit ;
                }
//var_dump($id);exit;
// var_dump($c["slug"]);exit;    
                //ATTENTION : only for dev and debug purposes
                //to be sure this never appears on prod
                if(isset($c["slug"]) && YII_DEBUG) 
                {
                    $docsJSON = file_get_contents("../../modules/costum/data/".$c["slug"].".json", FILE_USE_INCLUDE_PATH);
                    $c = json_decode($docsJSON,true);
                    
                    if($source == "json"){
                        echo "********** FROM costum/data/".$c["slug"].".json **********<br/>";
                        if($c == null){
                            echo "********************<br/>";
                            echo "Check you json file : BAD STRUCTURE. <br/>";
                            echo "Le data ".$c["slug"]." json ne charge<br/>";
                            echo "********************<br/>";
                            echo @$docsJSON;
                            exit;
                        }
                        else {
                            //could be cool to show in json render 
                            var_dump($c);
                            exit;
                        }
                        echo "********** FROM db custom/slug : ".$c["slug"]." **********<br/>";
                    }
                    
                } else {
                    $docsJSON = file_get_contents("../../modules/costum/data/".$id.".json", FILE_USE_INCLUDE_PATH);
                    $c = json_decode($docsJSON,true);
                }
            
        }
        else if(@$_GET["host"]){
            $c = PHDB::findOne( Costum::COLLECTION , array("host"=> $_GET["host"]));
        }

// var_dump($c);
//  exit;

        if(isset($c) && !empty($c)){ 

            if(isset($c["redirect"])){
               // $slug=explode(".", $_GET["el"])[1];
                $redirect = PHDB::findOne( Costum::COLLECTION , array("slug"=> $c["redirect"]));
                $url = (isset($redirect["host"])) ? "https://".$redirect["host"] : Yii::app()->createUrl("/costum/co/index/id/".$redirect["slug"]);
                header('Location: '.$url);//Yii::app()->createUrl("/costum/co/index/id/".$redirect["slug"]));
                exit;
            }

            if(!isset($slug)){
                $el = Slug::getElementBySlug($c["slug"], $elParams );
                $slug = $c["slug"];
                $tmp['contextType'] = $el["type"];
                $tmp['contextId'] = $el["id"];
            }
            
            if(isset($tmp['contextType'])){
                $c['contextType'] = $tmp['contextType'];
                $c['contextId'] = $tmp['contextId'];
            }

// var_dump($c);
// exit;

            if(!empty($el["el"]))
            {
                $element=array(
                    "contextType" => $el["type"],
                    "contextId" => $el["id"],
                    "contextSlug"=> $el["el"]["slug"],
                    "title"=> $el["el"]["name"],
                    "description"=> @$el["el"]["shortDescription"],
                    "descriptions" => @$el["el"]["description"],
                    "tags"=> @$el["el"]["tags"],
                    "communityLinks"=> @$el["el"]["links"],
                    "isCostumAdmin"=>false,
                    "assetsUrl"=> Yii::app()->getModule("costum")->getAssetsUrl(true),
                    "url"=> "/costum/co/index/id/".$c["slug"],
                    "banner" => @$el["el"]["profilRealBannerUrl"] );
//  var_dump($element);
// exit;

                if( @$el["el"]["profilImageUrl"] ) 
                    $c["logo"] = Yii::app()->createUrl($el["el"]["profilImageUrl"]);
                
                if(@$c["logoMin"])
                    $c["logoMin"] = Yii::app()->getModule( "costum" )->getAssetsUrl().$c["logoMin"];
                
                $c = array_merge( $c , $element );
                $c["admins"]= Element::getCommunityByTypeAndId($el["type"], $el["id"], Person::COLLECTION,"isAdmin");
if($debug)var_dump("array_merge( c , element ) : ".$c['contextId']);
//  var_dump($el["links"]); exit;
                //$links = (!empty($el["links"])) ? $el["links"] : null ;
                $c["isMember"]= Link::isLinked($el["id"], $el["type"], Yii::app()->session["userId"],@$c["communityLinks"]);
// var_dump(Authorisation::isInterfaceAdmin()); var_dump($c["admins"]); 
// var_dump(Yii::app()->session["userId"]);
                if(Authorisation::isInterfaceAdmin() && !isset($c["admins"][Yii::app()->session["userId"]])){
                    $c["admins"][Yii::app()->session["userId"]]=array("type"=>Person::COLLECTION, "isAdmin"=>true);
                }
                if(isset($c["admins"][Yii::app()->session["userId"]])){
 //   var_dump("iciiii"); exit;
                    Yii::app()->session['isCostumAdmin']=true;
                    $c["isCostumAdmin"]=true;
                }else{
                    Yii::app()->session['isCostumAdmin']=null;
                    $c["isCostumAdmin"]=false;
                }
                
                //possibly overload the costum template 
                if( isset( $el["el"]["costum"] ) && $c["slug"] == @$el["el"]["costum"]["slug"]  ){
                    unset($el["el"]["costum"]['id']);
                    unset($el["el"]["costum"]['slug']);
                    $c = self::combine($c,$el["el"]["costum"] ); 
                }
                //Check if we are in prod and on the host costum
//<<<<<<< HEAD
                if(isset($c["host"]) && strpos(Yii::app()->getRequest()->getBaseUrl(true), str_replace("wwww.", "", $c["host"]))===false){
                    if (isset($ctrl->module->relCanonical)) 
                        $ctrl->module->relCanonical = "https://".$c["host"];
// =======
//                 //if(isset($c["host"]) && strpos(Yii::app()->getRequest()->getBaseUrl(true), str_replace("wwww.", "", $c["host"]))===false)
//                 if(isset($c["host"]) && YII_DEBUG) 
// >>>>>>> qa
                    unset($c["host"]);
                } else if (isset($ctrl->module->relCanonical)) 
                    $ctrl->module->relCanonical = Yii::app()->createUrl($c["url"]);
                /* metadata */
                //Costum::initMetaData($c, $this->getController()->module);
                if(isset($c["metaDesc"]))
                    $shortDesc=$c["metaDesc"];
                else{
                    $shortDesc =  @$c["shortDescription"] ? $c["shortDescription"] : "";
                        if($shortDesc=="")
                            $shortDesc = @$c["description"] ? $c["description"] : "";
                }
                if(isset($c["language"])){
                    Yii::app()->language=$c["language"];
                }
                if(isset($ctrl)){
                    $ctrl->module->description = $shortDesc;
                    $ctrl->module->pageTitle = (isset($c["metaTitle"])) ? $c["metaTitle"] : @$c["title"];
                    $ctrl->module->author = (isset($c["metaTitle"])) ? $c["metaTitle"] : @$c["title"];
                    if(isset($c["metaKeywords"]))
                        $ctrl->module->keywords = $c["metaKeywords"];
                    else
                       $ctrl->module->keywords = (@$c["tags"]) ? implode(",", @$c["tags"]) : "";
                    if (isset($c["favicon"])) {
                        $mod = $ctrl->module->id;
                        //ex images can be given 
                        if( substr_count($c["favicon"], '#') ){
                            $pieces = explode("#", $c["favicon"]);
                            $mod = $pieces[0];
                            $c["favicon"] = $pieces[1];
                        }
                        $ctrl->module->favicon = Yii::app()->getModule( $mod )->getAssetsUrl().$c["favicon"];
                    }
                    if (@$c["metaImg"]) {
                        $mod = $ctrl->module->id;
                        //ex images can be given 
                        if( substr_count($c["metaImg"], '#') ){
                            $pieces = explode("#", $c["metaImg"]);
                            $mod = $pieces[0];
                            $c["metaImg"] = $pieces[1];
                        }
                        $ctrl->module->image = Yii::app()->getModule( $mod )->getAssetsUrl().$c["metaImg"];
                    }
                    else if(@$c["logo"]){
                        $mod = $ctrl->module->id;
                        //ex images can be given 
                        if( substr_count($c["logo"], '#') ){
                            $pieces = explode("#", $c["logo"]);
                            $mod = $pieces[0];
                            $c["logo"] = $pieces[1];
                        }
                        $ctrl->module->image = $c["logo"];
                    }
                    if(@$c["logo"]){
                        $ctrl->module->image = $c["logo"];
                    }
                    //$ctrl->module->relCanonical = (@$c["host"]) ? $c["host"] : Yii::app()->createUrl($c["url"]);   
                }
                // Besoin d'approfindir ce sujet des jsons et de leurs utilisations
                // Ici je l'ai utilisé pour injecter des tags spécifiques au pacte / on pourrait aussi penser au catégories d'événement
//var_dump($c["tags"]); exit;
                if(isset($c["lists"]))
                    $c=Costum::getAndConvertLists($c);
                if(isset($c["json"])){
                    foreach($c["json"] as $k => $v){
                        
                        $c[$k]=Costum::getContextList($c["slug"],$v);
                        if($k=="tags")
                            $c["request"]["searchTag"]=$c["tags"];
                            //$c["request"]["searchTag"]=[$c["tags"]];          
                    }
                }
                
                if( isset($c["searchTag"]) ) 
                    $c["request"]["searchTag"]=[$c["searchTag"]];
                if(isset(Yii::app()->session["userId"])){
                    $c=Costum::checkUserPreferences($c, Yii::app()->session["userId"]);
                }
                if(isset($c["searchTag"])) $c["request"]["searchTag"]=[$c["searchTag"]];
                if(isset($c["scopeSelector"]))
                    $c=Costum::initScopeSelector($c);
                /* metadata */
            }

            //Vérifie si le slug du costum est générique ou non
            if ([$c["slug"]] != [$el["el"]["slug"]]) {
                $c["request"]["sourceKey"] = [$el["el"]["slug"]];
            }else{
                $c["request"]["sourceKey"] = [$c["slug"]];
            }
            // var_dump($c["request"]["sourceKey"]);exit;

            // $c["contextId"] = $ctxId;
            // $c["contextType"] = $ctxType;

            Yii::app()->session['costum'] = Costum::sameFunction("init",$c);
            //if(!@Yii::app()->session['paramsConfig'] || empty(Yii::app()->session['paramsConfig'])) 
            Yii::app()->session['paramsData'] = []; 
            Yii::app()->session['paramsConfig'] = CO2::getThemeParams(); 

            Yii::app()->session["paramsConfig"]=Costum::filterThemeInCustom(Yii::app()->session["paramsConfig"]);
        // var_dump($c);exit;
        }    
 if($debug)var_dump('session contextId : '.@Yii::app()->session['costum']['contextId']);
 if($debug)var_dump('session contextType : '.@Yii::app()->session['costum']['contextType']);

    }

    //merge and update existing fields 
    // to be used when applying specific changes defined on an element into a template costum object
    public static function combine($a1, $a2) {
        foreach($a2 as $k => $v) {
            if(is_array($v)) {
                if(!isset($a1[$k]))
                    $a1[$k] = null;

                $a1[$k] = self::combine($a1[$k], $v);
            } else {
                $a1[$k] = $v;
            }
        }
        return $a1;
    }

	/**
	 * get a Poi By Id
	 * @param String $id : is the mongoId of the poi
	 * @return poi
	 */
	public static function getById($id) { 
	  	$classified = PHDB::findOneById( self::COLLECTION ,$id );
	  	//$classified["parent"] = Element::getElementByTypeAndId()
	  	$classified["typeClassified"]=@$classified["type"];
	  	$classified["type"]=self::COLLECTION;
	  	$where=array("id"=>@$id, "type"=>self::COLLECTION, "doctype"=>"image");
	  	$classified["images"] = Document::getListDocumentsWhere($where, "image");//(@$id, self::COLLECTION);
	  	return $classified;
	}

    public static function getBySlug($id) { 
        return PHDB::findOne( self::COLLECTION ,array("slug"=>$id) );
    }
    public static function getAndConvertLists($costum){
        foreach($costum["lists"] as $key => $v){
            if(isset($v["type"]) && $v["type"]==Badge::COLLECTION){
                $costum=self::getListByBadges($key, $v, $costum);
            }
        }
        return $costum;
    }
    public static function getListByBadges($keyList, $search, $costum, $parentId=null){
        if(empty($parentId))
            $where=array("parent"=>array('$exists'=>false));
        else
            $where=array("parent.".$parentId=>array('$exists'=>true));
        if(isset($search["sourceKey"]) && !empty($search["sourceKey"]))
            $where["source.key"]=$costum["slug"];
        if(isset($search["category"]))
            $where["category"]=$search["category"];
        $badges=Badge::getByWhere($where, true);
        if(empty($parentId))
            $costum["lists"][$keyList]=[];
        if(!empty($badges)){
            if(!isset($costum["badges"])) $costum["badges"]=array();
            if(!isset($costum["badges"][$keyList])) $costum["badges"][$keyList]=array();
            foreach($badges as $k => $v){
                $costum["badges"][$keyList][$k]=$v;
                if(!empty($parentId))
                    array_push($costum["lists"][$keyList][$costum["badges"][$keyList][$parentId]["name"]], $v["name"]);
                else
                    $costum["lists"][$keyList][$v["name"]]=array();
                $costum=self::getListByBadges($keyList, $search, $costum, $k);
            }
        }else if(empty($badges) && empty($parentId)){
            if(!isset($costum["badges"])) $costum["badges"]=array();
            if(!isset($costum["badges"][$keyList])) $costum["badges"][$keyList]=array();
        }
        // var_dump($costum);
        return $costum;
    }
	/**
	* Function called by @self::filterThemeInCustom in order to treat params.json of communecter app considering like the config
	* This loop permit to genericly update value, add values or delete value in the tree
	* return the communecter entry filtered by costum expectations
	**/
	public static function checkCOstumList($check, $filter,$k=null){
        $newObj=array();
        $arrayInCustom=["label","subdomainName","placeholderMainSearch","icon","height","imgPath","useFilter", "showMap", "useMapBtn","slug","formCreate", "setParams", "show", "initType", "inMenu","types","actions", "dataTarget", "dynform", "id", "filters", "tagsList", "calendar", "class", "onlyAdmin", "onlyMember", "img", "nameMenuTop", "header", "name", "inMenuTop"];
        
        foreach($check as $key => $v){
            
            if(!empty($v)){
                $newObj[$key]=(isset($filter[$key])) ? $filter[$key] : $v;
                $checkArray=true;
                foreach($arrayInCustom as $entry){
                    if(isset($v[$entry])){
                        $newObj[$key][$entry]=$v[$entry];
                        $checkArray=false;
                    }
                }
            }
            if(is_array($v) && $checkArray)
                $newObj[$key]=self::checkCOstumList($v, $newObj[$key], $k);
        }
        // var_dump($newObj);
        return $newObj;
    }
    public static function checkAndReplace($costum, $coParams){
        $filteringObj=array();
        if(empty($costum)) return false;
        foreach($costum as $key => $v){
            if(!empty($v)){
                $filteringObj[$key]=(isset($coParams[$key])) ? $coParams[$key] : $v;
                //Si simplement à true on récupére la valeur des paramètres de communecter
                if(is_bool($v) && isset($coParams[$key]) && is_bool($coParams[$key]) )
                    $filteringObj[$key]=$v;
                else if($v!==true && is_string($v) && isset($filteringObj[$key]))
                    $filteringObj[$key]=$v;
                else if(is_array($v) && isset($filteringObj[$key])){
                    foreach($filteringObj[$key] as $i => $p){
                        if(!isset($v[$i]) && in_array($i, self::$paramsCo) ) $v[$i]=$p;
                    }
                    $filteringObj[$key]=self::checkAndReplace($v, $filteringObj[$key]);
                }
            }//else{
                //$filteringObj[$key]=false;
            //}
        }
        return (!empty($filteringObj)) ? $filteringObj : false;
        
    }
    /*
    * @filterThemeInCustom permits to clean, update or add entry in paramsConfig of communecter thanks to costumParams 
    *	Receive Yii::app()->session["paramsConfig"] in order to modify it if Yii::app()->session["costum"] is existed
    *	
    */
    public static function filterThemeInCustom($params){
        //filter menu app custom 
        // Html construction of communecter
        if(isset(Yii::app()->session["costum"]["htmlConstruct"])){
            $constructParams=Yii::app()->session["costum"]["htmlConstruct"];
            //var_dump($params);exit;
            
            // Check about header construction
            // - Entry banner to adda tpl path to a custom banner, by default not isset
            // - Entry menuTop will config btn present in menu top (navLeft & navRight)
            if(isset($constructParams["header"])){
                if(isset($constructParams["header"]["banner"])) //URL
                    $params["header"]["banner"]=$constructParams["header"]["banner"];
                if(isset($constructParams["header"]["css"]))
                    $params["header"]["css"]=$constructParams["header"]["css"];
                if(isset($constructParams["header"]["menuTop"])){
                    if(isset($constructParams["header"]["menuTop"]["navLeft"]))
                        $params["header"]["menuTop"]["navLeft"]=self::checkCOstumList($constructParams["header"]["menuTop"]["navLeft"],$params["header"]["menuTop"]["navLeft"]);
                    if(isset($constructParams["header"]["menuTop"]["navRight"])){
                        $params["header"]["menuTop"]["navRight"]=self::checkCOstumList($constructParams["header"]["menuTop"]["navRight"],$params["header"]["menuTop"]["navRight"]);
                    }
                }
            }
            if(isset($constructParams["subMenu"])){
                if(empty($constructParams["subMenu"]))
                    $params["subMenu"]=false;
                else
                    $params["subMenu"]=self::checkCOstumList($constructParams["subMenu"],$params["subMenu"]);
            }
            if(isset($constructParams["footer"])){
                if(!empty($constructParams["footer"]))
                    $params["footer"]=self::checkCOstumList($constructParams["footer"],$params["footer"]);
                else
                    $params["footer"]=$constructParams["footer"];
            }
            
            // Check about admin Panel navigation (cosstumized space for costum administration expectations)
            if(isset($constructParams["adminPanel"])){
                 if(isset($constructParams["adminPanel"]["js"]))
                    $params["adminPanel"]["js"]=$constructParams["adminPanel"]["js"];
               
                if(isset($constructParams["adminPanel"]["add"]))
                    $params["adminPanel"]["add"]=$constructParams["adminPanel"]["add"];
                if(isset($constructParams["adminPanel"]["menu"]))
                    $params["adminPanel"]["menu"]=self::checkCOstumList($constructParams["adminPanel"]["menu"],$params["adminPanel"]["menu"]);

                
            }
            // if(isset($constructParams["directory"]))
              //  $params["directory"]=self::checkCOstumList($constructParams["directory"],$params["directory"]);
                
            // Check about element page organization and content
            // - #initView is a param to give first view loaded when arrived in a page (ex : newstream, detail, agenda or another)
            // - #menuLeft of element => TODO PLUG ON checkCOstumList
            // - #menuTop of element => TODO finish customization & add it in params.json
            if(isset($constructParams["element"])){
                if(isset($constructParams["element"]["initView"]))
                    $params["element"]["initView"]=$constructParams["element"]["initView"];
                if(isset($constructParams["element"]["banner"])){
                    $params["element"]["banner"]=self::checkAndReplace($constructParams["element"]["banner"],$params["element"]["banner"]);
                }
                if(isset($constructParams["element"]["containerClass"])){
                  //  var_dump($constructParams["element"]["containerClass"]);exit;
                    $params["element"]["containerClass"]=self::checkAndReplace($constructParams["element"]["containerClass"],$params["element"]["containerClass"]);
                }
                if(isset($constructParams["element"]["js"]))
                    $params["element"]["js"]=$constructParams["element"]["js"];
                
                if(isset($constructParams["element"]["css"]))
                    $params["element"]["css"]=$constructParams["element"]["css"];
                
                if(isset($constructParams["element"]["menuTop"])){
                    $params["element"]["menuTop"]=self::checkAndReplace($constructParams["element"]["menuTop"],$params["element"]["menuTop"]);
                }
                if(isset($constructParams["element"]["menuLeft"])){
                    $params["element"]["menuLeft"]=self::checkAndReplace($constructParams["element"]["menuLeft"],$params["element"]["menuLeft"]);
                }
               
            }
             if(isset($constructParams["preview"])){
                if(isset($constructParams["preview"]["toolBar"]))
                    $params["preview"]["toolBar"]=self::checkAndReplace($constructParams["preview"]["toolBar"],$params["preview"]["toolBar"]);
                if(isset($constructParams["preview"]["banner"])){
                    $params["preview"]["banner"]=self::checkAndReplace($constructParams["preview"]["banner"],$params["preview"]["banner"]);
                }
                if(isset($constructParams["preview"]["body"])){
                    $params["preview"]["body"]=self::checkAndReplace($constructParams["preview"]["body"],$params["preview"]["body"]);
                }
            }
            // #appRendering permit to get horizontal and vertical organization of menu 
            //	 - horizontal = first version of co2 with menu of apps in horizontal 
            //	 - vertical = current version of co2 with menu of apps on the left 
            if(isset($constructParams["appRendering"]))
                $params["appRendering"]=$constructParams["appRendering"];
            if(isset($constructParams["loadingModal"]))
                $params["loadingModal"]=$constructParams["loadingModal"];
            
            if(isset($constructParams["directory"])){
                //var_dump($params["directory"]);
                $directoryP=$params["directory"];
                //var_dump($directoryP);
                $params["directory"]=$constructParams["directory"];
                //var_dump($params["directory"]);
                foreach($directoryP as $k => $v){
                    if(!isset($params["directory"][$k]))
                        $params["directory"][$k]=$v;
                }
            }

            // # redirect permits to application in case of undefined view or error or automatic redirection to fall on costum redirect
            // To be setting for logged user && unlogged user
            if(isset($constructParams["redirect"]))
                $params["pages"]["#app.index"]["redirect"]=$constructParams["redirect"];
            if(isset($constructParams["menuBottom"]))
                $params["menuBottom"]=self::checkCOstumList($constructParams["menuBottom"],$params["menuBottom"]);
        }
        // Check about app (#search, #live, #etc) if should costumized
        $menuApp = array("#annonces", "#search", "#agenda", "#live", "#dda", "#map");

        if(isset(Yii::app()->session["costum"]["app"])){
            $params["numberOfApp"]=count(Yii::app()->session["costum"]["app"]);
            $menuPages=self::checkCOstumList(Yii::app()->session["costum"]["app"], $params["pages"]);
           // Rest::json($menuPages); exit ;
          //var_dump($menuPages); exit ;
            foreach($params["pages"] as $hash => $v){
                if(!in_array($hash,$menuApp))
                    $menuPages[$hash]=$v;
            }
            $params["pages"]=$menuPages;
        }
        if(isset(Yii::app()->session["costum"]["typeObj"]))
            $params["typeObj"]=Yii::app()->session["costum"]["typeObj"];
        return $params;
    }
    public static function getContextList($slug, $contextName){

        $layoutPath ="../../modules/costum/json/".$slug."/".$contextName.".json";

        $str = file_get_contents($layoutPath);
        $list = json_decode($str, true);
        return $list;
    }

     /*
    * @checkUserPreferences will check for a costum presence of user settings
    * Example : Geographical selction to oriented a user experience (cf : @meuseCampagnes)
    * String $userId to get preferences 
    * Array $costum to find specific user preferences
    * Return costum array with or without user settings about this costum 
    */
    public static function checkUserPreferences($costum, $userId){
        $userPref=Preference::getPreferencesByTypeId($userId, Person::COLLECTION);
        if(!empty($userPref) && isset($userPref["costum"]) && isset($userPref["costum"][$costum["slug"]])){
            $costum["userPreferences"]=$userPref["costum"][$costum["slug"]];
        }
        return $costum;
    }
    public static function checkUserAndReference(){
        if(isset(Yii::app()->session["userId"])){
            $getPerson=Element::getElementById(Yii::app()->session["userId"], Person::COLLECTION, null , array("source", "reference"));
            $hasCostumK=false;
            if(!empty($getPerson["source"])){
                if(isset($getPerson["source.key"]) && $getPerson["source.key"]==Yii::app()->session["costum"]["slug"])
                    $hasCostumK=true;
                if(isset($getPerson["source.keys"]) && in_array(Yii::app()->session["costum"]["slug"], $getPerson["source.keys"]))
                    $hasCostumK=true; 
            }
            if(!empty($getPerson["reference"]) && !empty($getPerson["reference"]["costum"]) && in_array(Yii::app()->session["costum"]["slug"], $getPerson["reference"]["costum"]))
                $hasCostumK=true;
            if(!$hasCostumK){
                if(empty($getPerson["reference"]))
                    $set=array("costum"=>[Yii::app()->session["costum"]["slug"]]);
                else if(!isset($getPerson["reference"]["costum"]))
                    $set=$getPerson["reference"]["costum"]=[Yii::app()->session["costum"]["slug"]];
                else if(!empty($getPerson["reference"]["costum"]))
                    $set=array_push($getPerson["reference"]["costum"], Yii::app()->session["costum"]["slug"]);
                PHDB::update(Person::COLLECTION, 
                    array("_id"=> new MongoId(Yii::app()->session["userId"])), 
                    array('$set'=>array("reference"=>$set))
                );
            }
        }
    }
    /*
    *   @initMetaData will initialize variable to get set metaDatas in mainSearch.php
    *   Meta is composed of title, description, keywords, image and author
    */
    public static function initMetaData($c){

        if(isset($c["metaDesc"]))
            $shortDesc=$c["metaDesc"];
        else{
            $shortDesc =  @$c["shortDescription"] ? $c["shortDescription"] : "";
                if($shortDesc=="")
                    $shortDesc = @$c["description"] ? $c["description"] : "";
        }
        $this->getController()->module->description = $shortDesc;
        $this->getController()->module->pageTitle = (isset($c["metaTitle"])) ? $c["metaTitle"] : @$c["title"];
        if(isset($c["metaKeywords"]))
            $this->getController()->module->keywords = $c["metaKeywords"];
        else
           $this->getController()->module->keywords = (@$c["tags"]) ? implode(",", @$c["tags"]) : "";
        if (isset($c["favicon"])) {
            $mod = $this->getController()->module->id;
            //ex images can be given 
            if( substr_count($c["favicon"], '#') ){
                $pieces = explode("#", $c["favicon"]);
                $mod = $pieces[0];
                $c["favicon"] = $pieces[1];
            }
            $this->getController()->module->favicon = Yii::app()->getModule( $mod )->getAssetsUrl().$c["favicon"];
        }
        if (@$c["metaImg"]) {
            $mod = $this->getController()->module->id;
            //ex images can be given 
            if( substr_count($c["metaImg"], '#') ){
                $pieces = explode("#", $c["metaImg"]);
                $mod = $pieces[0];
                $c["metaImg"] = $pieces[1];
            }
            $this->getController()->module->image = Yii::app()->getModule( $mod )->getAssetsUrl().$c["metaImg"];
        }
        else if(@$c["logo"]){
            $this->getController()->module->image = $c["logo"];
        }
        //if(@)

    }
    /*
    *   @initScopeSelector will init geographical object of selection and user choice in costum
    *   return costum with entry necessary :
    *   Scopeseclector with entiere specifity of scope
    *   UserPreferences refering to its costum geographical experience
    */
    public static function initScopeSelector($params){
        $params["scopeSelector"]=Zone::getScopeByIds($params["scopeSelector"])["scopes"];
        /*foreach(["cities", "cp", "zones"] as $impact){
            if(isset($params["userPreferences"]) && isset($params["userPreferences"][$impact])){
                foreach($params["userPreferences"][$impact] as $key){
                    $valueScope=$params["scopeSelector"][$key];
                    $idScope=$params["scopeSelector"][$key]["id"];
                }
                //$params["filters"]=(!isset($params["filters"])) ? [] : $params["filters"];
                //$params["filters"]["scopes"][$impact]=$idScope;         
            }
        }*/
        return $params;
    }

    public static function sameFunction($function, $params=null){
        if(self::isSameFunction($function)) {
            $slug = ucfirst(Yii::app()->session["costum"]["slug"]) ;
            $params = $slug::$function($params);
        }
        return $params;
    }

    public static function isSameFunction($function){
        $res = false ;
        if(!empty(Yii::app()->session["costum"]["slug"]) && 
            !empty(Yii::app()->session["costum"]["class"]) && 
            !empty(Yii::app()->session["costum"]["class"]["function"]) && 
            in_array($function, Yii::app()->session["costum"]["class"]["function"])) {
            $res = true;
        }
        return $res;
    }

    public static function prepData($params){
        if(!empty(Yii::app()->session["costum"] ) ) {

            if( !empty($params["source"]) && !empty($params["source"]["toBeValidated"]) ) {
                $params["source"]["toBeValidated"] = array(Yii::app()->session["costum"]["slug"] => true);
            }


        }

        // if(!empty(Yii::app()->session["costum"]["slug"]) && 
        //     !empty(Yii::app()->session["costum"]["class"]) && 
        //     !empty(Yii::app()->session["costum"]["class"]["function"]) && 
        //     in_array("prepData", Yii::app()->session["costum"]["class"]["function"])) {
        //     $slug = ucfirst(Yii::app()->session["costum"]["slug"]) ;
        //     $params = $slug::prepData($params);
        // }

        $params = self::sameFunction("prepData", $params);


        if(!empty(Yii::app()->session["costum"]) && @Yii::app()->session["costum"]["slug"] == "siteDuPactePourLaTransition"){


            if($params["collection"] == Organization::COLLECTION && $params["type"] == Organization::TYPE_GROUP){
               /* if(!empty($params["scope"])){

                    $listScope =  array();
                    foreach ($params["scope"] as $key => $value) {
                        $listScope[] = array("scope.".$key => array('$exists' => 1));
                        $params["name"] = (!empty($value["cityName"]) ? $value["cityName"] : $value["name"]);
                    }
                    $where = array('$and' => 
                                array(  array("type" => Organization::TYPE_GROUP),
                                        array("source.keys" => array('$in' => array(Yii::app()->session["costum"]["slug"]))),
                                        array('$or' => $listScope)
                                    ) );

                    $orga = PHDB::find(Organization::COLLECTION, $where, array("name", "scope"));
                    if(!empty($orga)){
                        throw new CTKException("Ce scope est déjà pris par un autre groupe");
                    }
                    
                }*/
            } else if($params["collection"] == Action::COLLECTION){
            	if(!empty($params["measures"] )){
            		foreach ($params["measures"] as $key => $value) {
	                	$params["name"] = $value["name"];
	                }
                	//throw new CTKException("HERE "+$params["name"]);
            	}
                
            } else {
                //throw new CTKException("it's good ");
            }
            
        }
        return $params;
    }

    public static function value($value){
        if( substr_count($value , 'this.') > 0 ){
            $field = explode( ".", $value );
            if( isset( Yii::app()->session["costum"][ $field[1] ] ) )
                return Yii::app()->session["costum"][ $field[1] ];
        }
        else 
            return Yii::app()->session["costum"][ $value ];
    }


    public static function getRedirect(){
        $url = null ;
        if(isset( Yii::app()->session["costum"]) && isset( Yii::app()->session["costum"]["slug"]) ){
           // $slug=explode(".", $_GET["el"])[1];
            $redirect = PHDB::findOne( Costum::COLLECTION , array("slug"=> Yii::app()->session["costum"]["slug"]));
            $url = (isset($redirect["host"])) ? "https://".$redirect["host"] : Yii::app()->createUrl("/costum/co/index/id/".$redirect["slug"]);
            // var_dump($url);//Yii::app()->createUrl("/costum/co/index/id/".$redirect["slug"]));
            // exit;
        }
        return $url;
    }
}
?>